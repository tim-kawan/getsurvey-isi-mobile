import 'package:flutter_test/flutter_test.dart';
import 'package:get_survey_app/features/payment/data/data_sources/remote/dto/virtual_account_dto.dart';

void main() {
  test('cek split account number', () {
    var virtualAccountDto = VirtualAccountDto(accountNumber: '889089999671147');
    expect("8890-8999-9671-147", virtualAccountDto.splitAccountNumber());

    virtualAccountDto = VirtualAccountDto(accountNumber: '8890899996711471');
    expect("8890-8999-9671-1471", virtualAccountDto.splitAccountNumber());

    virtualAccountDto = VirtualAccountDto(accountNumber: '88908999967');
    expect("8890-8999-967", virtualAccountDto.splitAccountNumber());
  });
}
