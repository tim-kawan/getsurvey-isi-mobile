import 'package:flutter_test/flutter_test.dart';
import 'package:get_survey_app/core/network/network_info.dart';
import 'package:get_survey_app/features/voting/data/data_sources/remotes/entities/kandidat_dto.dart';
import 'package:get_survey_app/features/voting/data/repositories/data_sources/remotes/voting_remote.dart';
import 'package:get_survey_app/features/voting/data/repositories/voting_repository.dart';
import 'package:get_survey_app/features/voting/domain/entities/kandidat_entity.dart';
import 'package:mockito/mockito.dart';

class MockNetworkInfo extends Mock implements NetworkInfo {}

class MockVotingRemote extends Mock implements IVotingRemote {}

void main() {
  MockNetworkInfo mockNetworkInfo = MockNetworkInfo();
  MockVotingRemote mockVotingRemote = MockVotingRemote();
  VotingRepository votingRepository = VotingRepository(
      networkInfo: mockNetworkInfo, votingRemote: mockVotingRemote);

  void runTestsOnline(Function body) {
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });
      body();
    });
  }

  final listWakilKandidatEntity = [
    KandidatEntity(
      location: "",
      id: 1,
      name: "Aenal",
      photo: "",
      videoUrl: "",
      isSelected: null,
      isLast: false,
    )
  ];

  runTestsOnline(() {
    test('fetchKandidat', () async {
      when(mockVotingRemote.fetchKandidat(any)).thenAnswer((_) async => [
            KandidatDto(
                location: "", id: 1, name: "Aenal", photo: "", videoUrl: "")
          ]);

      final result = await votingRepository.fetchKandidat("wakil");
      result.fold((l) => null, (r) {
        expect(listWakilKandidatEntity[0].name, equals(r[0].name));
        expect(listWakilKandidatEntity[0].id, equals(r[0].id));
      });
    });
  });
}
