import 'package:flutter_test/flutter_test.dart';
import 'package:get_survey_app/features/voting/data/data_sources/remotes/voting_remote.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';

import '../../../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  final mockHttpClient = MockHttpClient();
  VotingRemote votingRemote;

  setUp(() {
    votingRemote = VotingRemote(mockHttpClient);
  });

  void setUpPostMockHttpClientSuccess200() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
        (_) async => http.Response(fixture_voting('kandidat.json'), 200));
  }

  void getResult200() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
        (_) async => http.Response(fixture_voting('vote_result.json'), 200));
  }

  test('fetchKandidat ketua', () async {
    setUpPostMockHttpClientSuccess200();
    final result = await votingRemote.fetchKandidat("ketua");
    expect(1, result.length);
    expect("Septian Nur Setiadi Aditama", result[0].name);
  });

  test('getResultVote ketua', () async {
    getResult200();
    final result = await votingRemote.getResultVote(1);
    expect("Reza Abdullah", result.ketua.name);
  });
}
