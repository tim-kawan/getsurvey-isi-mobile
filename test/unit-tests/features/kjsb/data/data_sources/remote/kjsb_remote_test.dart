import 'package:flutter_test/flutter_test.dart';
import 'package:get_survey_app/core/network/env.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/kjsb_remote.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';

import '../../../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  final mockHttpClient = MockHttpClient();
  KjsbRemote kjsbRemote;

  setUp(() {
    Constants.setEnvironment(Environment.DEV);
    kjsbRemote = KjsbRemote(mockHttpClient);
  });

  void setUpGetSurveyorSuccess() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
        (_) async => http.Response(fixture_kjsb('find_surveyor.json'), 200));
  }

  void setUpGetDetailKjsbSuccess() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
            (_) async => http.Response(fixture_kjsb('detail_kjsb.json'), 200));
  }

  test('get all surveyor', () async {
    setUpGetSurveyorSuccess();
    final result = await kjsbRemote.getSurveyorKjsb("11");
    expect(25, result.length);
  });

  test('get detail kjsb success', () async {
    setUpGetDetailKjsbSuccess();
    final result = await kjsbRemote.getKjsbById("11");
    expect("Q3-20211203-012", result.kjsbDto.code);
    expect("Wedo Merianto Budiono", result.surveyor.nama);
  });
}
