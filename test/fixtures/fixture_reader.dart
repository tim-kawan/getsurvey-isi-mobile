import 'dart:io';

String fixture_voting(String name) =>
    File('test/fixtures/voting/$name').readAsStringSync();


String fixture_kjsb(String name) =>
    File('test/fixtures/kjsb/$name').readAsStringSync();