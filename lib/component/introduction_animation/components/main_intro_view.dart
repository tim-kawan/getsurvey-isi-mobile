import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_survey_app/component/ImageUtils.dart';

class MainSplashView extends StatefulWidget {
  final AnimationController animationController;

  const MainSplashView({Key key, this.animationController}) : super(key: key);

  @override
  _MainSplashViewState createState() => _MainSplashViewState();
}

class _MainSplashViewState extends State<MainSplashView> {
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    final _introductionanimation =
        Tween<Offset>(begin: Offset(0, 0), end: Offset(0.0, -1.0))
            .animate(CurvedAnimation(
      parent: widget.animationController,
      curve: Interval(
        0.0,
        0.2,
        curve: Curves.fastOutSlowIn,
      ),
    ));
    return SlideTransition(
      position: _introductionanimation,
      child: Scaffold(
        body: Stack(
          children: [
            Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 0.0),
                      child: Container(
                          width: screenSize.width * 0.8,
                          height: 250,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                            image: ImageUtils.APP_LOGO_H,
                          ))),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 0.0, bottom: 8.0),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Image(image:
                            AssetImage('assets/GetSurvey.png'),
                              width: screenSize.width * 0.7,
                            ),
                          ),
                          Text(
                            "Menghubungkan Masyarakat\n dengan Surveyor",
                            style: TextStyle(
                                fontSize: 25.0, fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                    // Padding(
                    //   padding: EdgeInsets.only(left: 64, right: 64),
                    //   child: Text(
                    //     'Ada beragam banyak pilihan Surveyor, Sewa/Beli Alat Ukur  dan Jasa Surveyor dengan penawaran menarik',
                    //     textAlign: TextAlign.center,
                    //   ),
                    // ),
                    SizedBox(
                      height: 48,
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).padding.bottom + 16),
                      child: InkWell(
                        onTap: () {
                          widget.animationController.animateTo(0.2);
                        },
                        child: Container(
                            height: 58,
                            padding: EdgeInsets.only(
                              left: 56.0,
                              right: 56.0,
                              top: 16,
                              bottom: 16,
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(38.0),
                              color: Color(0xff132137),
                            ),
                            child: Icon(
                              CupertinoIcons.arrow_down,
                              color: Colors.white,
                            )),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
