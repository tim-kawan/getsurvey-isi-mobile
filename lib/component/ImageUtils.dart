import 'package:flutter/material.dart';

class ImageUtils {
  static const APP_LOGO = AssetImage("assets/logo.png");
  static const APP_LOGO_H = AssetImage("assets/getsurvey.png");

  static const BANNER_1 = AssetImage("assets/banner/banner1.jpg");
  static const BANNER_2 = AssetImage("assets/banner/banner2.jpg");
}
