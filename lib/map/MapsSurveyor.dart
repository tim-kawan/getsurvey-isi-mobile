import 'dart:async';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:get_survey_app/account/DetailSurveyor.dart';
import 'package:get_survey_app/component/ImageUtils.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/model/ProductService.dart';
import 'package:get_survey_app/model/Surveyor.dart';
import 'package:get_survey_app/service/MapsAPI.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class MapsSurveyorScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.grey[200],
      systemNavigationBarIconBrightness: Brightness.dark,
      systemNavigationBarDividerColor: Colors.black,
    ));

    return MaterialApp(
      home: HomePage(),
    );
  }
}

class _HomePageState extends State<HomePage> {
  final Completer<GoogleMapController> _controller = Completer();
  final TextEditingController _searchController = TextEditingController();
  final ScrollController listScrollController = new ScrollController();
  final DefaultCacheManager _cacheManager = new DefaultCacheManager();
  PanelController _pc = new PanelController();
  double _panelHeightOpen;
  double _panelHeightClosed = 95.0;
  var listSurveyor = <Surveyor>[];
  BitmapDescriptor pinLocationIcon;
  Set<Marker> _markers = Set();
  StreamSubscription<LocationData> _streamSubscription;
  LatLng latLng = LatLng(-6.2685497, 107.0496954);
  bool _isloading = false;

  @override
  Widget build(BuildContext context) {
    _panelHeightOpen = MediaQuery.of(context).size.height * .80;

    return Material(
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          SlidingUpPanel(
            controller: _pc,
            maxHeight: _panelHeightOpen,
            minHeight: _panelHeightClosed,
            parallaxEnabled: true,
            parallaxOffset: .5,
            body: _body(),
            panelBuilder: (sc) => _panel(sc),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(18.0),
              topRight: Radius.circular(18.0),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    if (_streamSubscription != null) {
      _streamSubscription.cancel();
    }

    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _doHere();
  }

  Widget _body() {
    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(devicePixelRatio: 2.5), 'assets/img/pin.png')
        .then((onValue) {
      pinLocationIcon = onValue;
    });

    return Stack(
      children: <Widget>[
        GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: CameraPosition(
            target: latLng,
            zoom: 14.4746,
          ),
          myLocationButtonEnabled: true,
          myLocationEnabled: true,
          markers: _markers,
          padding: EdgeInsets.only(top: 110.0),
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
        ),
        Padding(
          padding: const EdgeInsets.only(
            left: 10.0,
            right: 10.0,
            top: 50.0,
          ),
          child: Container(
            height: 60.0,
            alignment: AlignmentDirectional.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14.0),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(blurRadius: 10.0, color: Colors.black12)
                ]),
            padding:
                EdgeInsets.only(left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
            child: Theme(
              data: ThemeData(
                hintColor: Colors.transparent,
              ),
              child: TextFormField(
                textInputAction: TextInputAction.done,
                controller: _searchController,
                onFieldSubmitted: (term) => _doFind(term),
                decoration: InputDecoration(
                    suffixIcon: GestureDetector(
                      onTap: () => _doFind(_searchController.text),
                      child: Container(
                        height: MediaQuery.of(context).size.height,
                        width: 60.0,
                        child: Icon(
                          Icons.search,
                          color: primaryColor,
                        ),
                      ),
                    ),
                    border: InputBorder.none,
                    labelText: "Pencarian",
                    labelStyle: TextStyle(
                        fontSize: 15.0,
                        fontFamily: 'Sans',
                        letterSpacing: 0.3,
                        color: primaryColor,
                        fontWeight: FontWeight.w600)),
                keyboardType: TextInputType.text,
              ),
            ),
          ),
        ),
      ],
    );
  }

  _doFind(String query) async {
    _pc.open();
    if (query.length >= 3) {
      setState(() {
        _isloading = true;
      });
      var _picked = <int, Surveyor>{};
      var _tempSurveyor = await RestAPI.seeSurveyors();
      var _tempJasa = await RestAPI.seeServices();
      var _tempProduk = await RestAPI.seeProducts();
      int idUser = await Session.getValue(Session.USER_ID);

      RestAPI.doLogHit(
          idUser.toString(),
          latLng.longitude.toString(),
          latLng.latitude.toString(),
          DateFormat("dd MMM yyyy HH:mm:ss").format(DateTime.now()),
          query);

      _tempProduk.forEach((json) {
        ProductService produk = ProductService.fromJson(json);

        if (produk.nama.toLowerCase().contains(query.toLowerCase()) ||
            produk.deskripsi.toLowerCase().contains(query.toLowerCase())) {
          Surveyor surveyor = Surveyor.fromJson(json["user_surveyor"]);
          _picked.putIfAbsent(surveyor.id, () => surveyor);
        }
      });

      _tempJasa.forEach((json) {
        ProductService produk = ProductService.fromJson(json);

        if (produk.nama.toLowerCase().contains(query.toLowerCase()) ||
            produk.deskripsi.toLowerCase().contains(query.toLowerCase())) {
          Surveyor surveyor = Surveyor.fromJson(json["user_surveyor"]);
          _picked.putIfAbsent(surveyor.id, () => surveyor);
        }
      });

      _tempSurveyor.forEach((json) {
        Surveyor surveyor = Surveyor.fromJson(json);

        if (surveyor.name.toLowerCase().contains(query.toLowerCase()) ||
            surveyor.email.toLowerCase().contains(query.toLowerCase()) ||
            surveyor.phoneNum.toLowerCase().contains(query.toLowerCase()) ||
            surveyor.alamat.toLowerCase().contains(query.toLowerCase())) {
          _picked.putIfAbsent(surveyor.id, () => surveyor);
        }
      });

      if (_picked.isNotEmpty) {
        setState(() {
          _markers.clear();
          listSurveyor.clear();
        });

        _picked.forEach((key, agent) {
          listSurveyor.add(agent);
        });

        listSurveyor.forEach((surveyor) {
          setState(() {
            _markers.add(Marker(
              markerId: MarkerId(surveyor.id.toString()),
              position: LatLng(surveyor.lat, surveyor.long),
              icon: pinLocationIcon,
              infoWindow: InfoWindow(
                title: surveyor.name,
                snippet: surveyor.noAnggota,
              ),
            ));
          });
        });
      } else {
        setState(() {
          _markers.clear();
          listSurveyor.clear();
        });
      }

      setState(() {
        _isloading = false;
      });
    }
  }

  _doHere() async {
    await _cacheManager.emptyCache();
    Location _location = new Location();
    bool _isOn = await _location.requestService();
    PermissionStatus _isAllow = await _location.requestPermission();

    if (_isOn && _isAllow == PermissionStatus.GRANTED) {
      _location.changeSettings(interval: 900000); // reload after 15 minutes
      _streamSubscription =
          _location.onLocationChanged().listen((currentLocation) async {
        GoogleMapController mapController = await _controller.future;
        mapController
            .animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: LatLng(currentLocation.latitude, currentLocation.longitude),
          zoom: 14.4746,
        )));
        setState(() {
          latLng = LatLng(currentLocation.latitude, currentLocation.longitude);
          _markers.clear();
          listSurveyor.clear();
        });
        _getListSurveyor();
      });
    }
  }

  _getListSurveyor() async {
    setState(() {
      listSurveyor.clear();
      _isloading = true;
    });
    var _surveyors = await RestAPI.seeSurveyors();

    _surveyors.forEach((surveyor) async {
      double lat = surveyor["user"]["user_profile"]["lat"];
      double lon = surveyor["user"]["user_profile"]["long"];
      Surveyor agent = Surveyor.fromJson(surveyor);

      if (agent.isVisible) {
        if (lat != null && lon != null) {
          var _nears = await MapsAPI.distanceMatrix(latLng, LatLng(lat, lon));

          _nears.forEach((jarak) {
            if (jarak.distance.value <= 10000) {
              setState(() {
                _markers.add(Marker(
                  markerId: MarkerId(surveyor["id"].toString()),
                  position: LatLng(lat, lon),
                  icon: pinLocationIcon,
                  infoWindow: InfoWindow(
                    title: surveyor["user"]["nama"],
                    snippet: surveyor["no_anggota"],
                  ),
                ));
              });

              if (!listSurveyor.contains(agent)) {
                listSurveyor.add(agent);
                setState(() {});
              }
            }
          });
        }
      }
    });

    setState(() {
      _isloading = false;
    });
  }

  Widget _panel(ScrollController sc) {
    return MediaQuery.removePadding(
      context: context,
      removeTop: true,
      child: ListView(
        controller: sc,
        children: <Widget>[
          SizedBox(
            height: 12.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 30,
                height: 5,
                decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.all(Radius.circular(12.0))),
              ),
            ],
          ),
          SizedBox(
            height: 18.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Surveyor",
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 24.0,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 36.0,
          ),
          Container(
            height: MediaQuery.of(context).size.width,
            width: MediaQuery.of(context).size.width,
            child: (_isloading)
                ? Shimmer.fromColors(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        "Sedang melakukan pencarian data...",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.bold,
                          fontSize: 22.5,
                        ),
                      ),
                    ),
                    baseColor: Colors.grey[700],
                    highlightColor: Colors.grey[200],
                  )
                : listSurveyor.length > 0
                    ? ListView.builder(
                        itemCount: listSurveyor.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              height: 80.0,
                              width: MediaQuery.of(context).size.width / 1.2,
                              decoration: BoxDecoration(
                                  color: primaryColor,
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: InkWell(
                                onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => DetailSurveyor(
                                      img: listSurveyor[index].img,
                                      name: listSurveyor[index].name,
                                      id: listSurveyor[index].id,
                                      noAnggota: listSurveyor[index].noAnggota,
                                      alamat: listSurveyor[index].alamat,
                                      long: listSurveyor[index].long,
                                      lat: listSurveyor[index].lat,
                                      email: listSurveyor[index].email,
                                      dateJoin: listSurveyor[index].joinDate,
                                      rate: listSurveyor[index].rate,
                                      medal: listSurveyor[index].medali,
                                    ),
                                  ),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      CachedNetworkImage(
                                        imageUrl: listSurveyor[index].img,
                                        placeholder: (context, url) =>
                                            Container(
                                          width: 70.0,
                                          height: 70.0,
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: ImageUtils.APP_LOGO,
                                                fit: BoxFit.cover),
                                            borderRadius:
                                                BorderRadius.circular(80.0),
                                            border: Border.all(
                                              color: Colors.white,
                                              width: 2.0,
                                            ),
                                          ),
                                        ),
                                        cacheManager: _cacheManager,
                                        imageBuilder: (context, provider) =>
                                            Container(
                                          width: 70.0,
                                          height: 70.0,
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: provider,
                                              fit: BoxFit.cover,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(80.0),
                                            border: Border.all(
                                              color: Colors.white,
                                              width: 2.0,
                                            ),
                                          ),
                                        ),
                                        errorWidget: (context, url, error) =>
                                            Container(
                                          width: 70.0,
                                          height: 70.0,
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: NetworkImage(
                                                  "${RestAPI.FOTO_URL}/pp_0.jpg"),
                                              fit: BoxFit.cover,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(80.0),
                                            border: Border.all(
                                              color: Colors.white,
                                              width: 2.0,
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            listSurveyor[index]
                                                .name
                                                .toUpperCase(),
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                          Text(
                                            "#${listSurveyor[index].noAnggota}",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontStyle: FontStyle.italic),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                      )
                    : Center(
                        child: Text(
                          "Tidak Ada Data Surveyor Di dekat Anda",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey[700]),
                        ),
                      ),
          ),
          SizedBox(
            height: 24,
          ),
        ],
      ),
    );
  }
}
