import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/map/MapsScreen.dart';
import 'package:get_survey_app/service/MapsAPI.dart';
import 'package:location/location.dart';
import 'package:shimmer/shimmer.dart';

class PickLocation extends StatefulWidget {
  final int navigate;
  final String fullName;
  final String alamat;

  PickLocation({this.fullName, this.alamat, this.navigate, Key key})
      : super(key: key);

  @override
  _PickLocationState createState() => _PickLocationState();
}

class _PickLocationState extends State<PickLocation> {
  TextEditingController _searchController = TextEditingController();

  StreamSubscription<LocationData> _streamSubscription;
  ScrollController controller = new ScrollController();
  var listLocation = [];
  bool isLoad = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: primaryColor,
        title: Text(
          "Pilih Lokasi Anda",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 5.0, horizontal: 30.0),
                  child: Container(
                    height: 60.0,
                    alignment: AlignmentDirectional.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(14.0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(blurRadius: 10.0, color: Colors.black12)
                        ]),
                    padding: EdgeInsets.only(
                        left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
                    child: Theme(
                      data: ThemeData(
                        hintColor: Colors.transparent,
                      ),
                      child: TextFormField(
                        textInputAction: TextInputAction.done,
                        controller: _searchController,
                        onFieldSubmitted: (term) {
                          setState(() {
                            isLoad = true;
                          });
                          _doFind(_searchController.text, context);
                        },
                        decoration: InputDecoration(
                            suffixIcon: GestureDetector(
                              onTap: () {
                                setState(() {
                                  isLoad = true;
                                });
                                _doFind(_searchController.text, context);
                              },
                              child: Container(
                                height: MediaQuery.of(context).size.height,
                                width: 60.0,
                                child: Icon(
                                  Icons.search,
                                  color: primaryColor,
                                ),
                              ),
                            ),
                            border: InputBorder.none,
                            labelText: "Pilih Lokasi",
                            labelStyle: TextStyle(
                                fontSize: 15.0,
                                fontFamily: 'Sans',
                                letterSpacing: 0.3,
                                color: primaryColor,
                                fontWeight: FontWeight.w600)),
                        keyboardType: TextInputType.text,
                      ),
                    ),
                  ),
                ),
                Flexible(
                  child: (isLoad)
                      ? Shimmer.fromColors(
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              "Mencari Lokasi...",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 22.5,
                              ),
                            ),
                          ),
                          baseColor: Colors.grey[700],
                          highlightColor: Colors.grey[200],
                        )
                      : Padding(
                          padding: EdgeInsets.fromLTRB(8, 0, 8, 8),
                          child: ListView.builder(
                              itemCount: listLocation.length,
                              controller: controller,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: InkWell(
                                    onTap: () async {
                                      Location _location = new Location();
                                      bool _isOn =
                                          await _location.requestService();
                                      PermissionStatus _isAllow =
                                          await _location.requestPermission();

                                      if (_isOn &&
                                          _isAllow ==
                                              PermissionStatus.GRANTED) {
                                        Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => MapsScreen(
                                              navigate: widget.navigate,
                                              lat: listLocation[index]['lat'],
                                              lon: listLocation[index]['lon'],
                                              alamat: listLocation[index]
                                                  ['address'],
                                            ),
                                          ),
                                        );
                                      } else if (_isOn &&
                                          _isAllow ==
                                              PermissionStatus.GRANTED &&
                                          widget.navigate == 1) {
                                        Navigator.pushReplacement(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    MapsScreen(
                                                      navigate: widget.navigate,
                                                      lat: listLocation[index]
                                                          ['lat'],
                                                      lon: listLocation[index]
                                                          ['lon'],
                                                      alamat:
                                                          listLocation[index]
                                                              ['address'],
                                                    )));
                                      }
                                    },
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              width: 2, color: Colors.grey),
                                          borderRadius:
                                              BorderRadius.circular(5.0)),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text(
                                              listLocation[index]['name'],
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: primaryColor,
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              listLocation[index]["address"],
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: primaryColor,
                                                  fontSize: 12.0,
                                                  fontWeight: FontWeight.w600),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              }),
                        ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    if (_streamSubscription != null) {
      _streamSubscription.cancel();
    }

    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  _doFind(String searchLocation, BuildContext context) async {
    setState(() {
      listLocation.clear();
      isLoad = false;
    });
    var _results = await MapsAPI.searchPlaces(searchLocation);

    _results.forEach((element) {
      listLocation.add({
        'name': element["name"],
        'address': element["address"],
        "lat": element["lat"],
        "lon": element["lng"],
      });

      setState(() {});
    });
  }
}
