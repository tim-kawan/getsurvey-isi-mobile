import 'dart:async';
import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/service/MapsAPI.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class TrackingLocation extends StatefulWidget {
  final double lng;
  final double lat;
  final String namaPemilik;
  final String alamatTanah;
  final int id;

  const TrackingLocation(
      {Key key,
      this.lng,
      this.lat,
      this.id,
      this.namaPemilik,
      this.alamatTanah})
      : super(key: key);

  @override
  _TrackingLocationState createState() => _TrackingLocationState();
}

class _TrackingLocationState extends State<TrackingLocation> {
  final Completer<GoogleMapController> _controller = Completer();
  final DefaultCacheManager _cacheManager = new DefaultCacheManager();
  BitmapDescriptor sourceIcon, destinationIcon;
  Set<Marker> _markers = Set();
  Set<Polyline> _polylines = Set();
  StreamSubscription<LocationData> _streamSubscription;
  LatLng latLng = LatLng(-6.2685497, 107.0496954);
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  bool statusMulai = false;
  String jarak = "0 km", waktu = "0 detik";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tracking Lokasi"),
        centerTitle: true,
        backgroundColor: primaryColor,
      ),
      body: Stack(
        children: <Widget>[
          _body(),
          statusMulai
              ? Positioned(
                  top: 20,
                  left: 5,
                  child: InkWell(
                    onTap: () {
                      MapsAPI.openUrlMaps(
                          latLng, LatLng(widget.lat, widget.lng));
                    },
                    child: CircleAvatar(
                      backgroundColor: Colors.white,
                      child: Icon(
                        Icons.assistant_navigation,
                        size: 40,
                        color: Colors.red,
                      ),
                    ),
                  ),
                )
              : Container(),
          Positioned(
            bottom: 0,
            left: 5,
            right: 5,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                padding: EdgeInsets.all(10),
                height: 200,
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RichText(
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      text: TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                              text: "#IRK ${widget.id}",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14,
                                  color: Colors.black)),
                          TextSpan(
                              text: "- ${widget.namaPemilik}",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blueAccent,
                                  fontSize: 14)),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      widget.alamatTanah,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          child: Row(
                            children: [
                              Icon(
                                Icons.location_on,
                                color: Colors.red,
                                size: 20,
                              ),
                              Text(jarak,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                      color: Colors.grey)),
                            ],
                          ),
                        ),
                        Container(
                          child: Row(
                            children: [
                              Icon(
                                Icons.timer,
                                color: Colors.blue,
                                size: 20,
                              ),
                              Text(waktu,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                      color: Colors.grey)),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    !statusMulai
                        ? InkWell(
                            onTap: () async {
                              statusMulai = await RestAPI.trackStart(
                                  widget.id,
                                  latLng,
                                  LatLng(widget.lat, widget.lng),
                                  latLng);
                              setState(() {});
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              width: MediaQuery.of(context).size.width,
                              child: Center(
                                child: Text(
                                  "Mulai",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.circular(20)),
                            ),
                          )
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              InkWell(
                                onTap: () async {
                                  bool isUpdated = await RestAPI.trackGoing(
                                      widget.id, latLng, true);

                                  if (isUpdated) {
                                    Navigator.pop(context);
                                  }
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(vertical: 15),
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Center(
                                    child: Text(
                                      "Selesai",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.blue,
                                      borderRadius: BorderRadius.circular(20)),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  Component.showAppNotification(
                                      context, "Chat", "");
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(vertical: 15),
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Center(
                                    child: Text(
                                      "Chat",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.blue,
                                      borderRadius: BorderRadius.circular(20)),
                                ),
                              ),
                            ],
                          )
                  ],
                ),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    if (_streamSubscription != null) {
      _streamSubscription.cancel();
    }

    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _doHere();
  }

  Widget _body() {
    return GoogleMap(
      mapType: MapType.normal,
      initialCameraPosition: CameraPosition(
        target: latLng,
        zoom: 14.4746,
      ),
      myLocationButtonEnabled: true,
      myLocationEnabled: true,
      markers: _markers,
      polylines: _polylines,
      onMapCreated: (GoogleMapController controller) {
        _controller.complete(controller);
      },
    );
  }

  _doHere() async {

    await _cacheManager.emptyCache();
    Location _location = new Location();
    bool _isOn = await _location.requestService();
    PermissionStatus _isAllow = await _location.requestPermission();
    sourceIcon = await _bitmapDescriptorFromSvgAsset(
        context, 'assets/icons/ic_surveyor_map.svg');
    destinationIcon = await _bitmapDescriptorFromSvgAsset(
        context, 'assets/icons/ic_location_me.svg');
    var tracked = await RestAPI.trackGet(widget.id);
    statusMulai = tracked != null;
    setState(() {});

    if (_isOn && _isAllow == PermissionStatus.GRANTED) {
      _location.changeSettings(interval: 15000); // reload after 15 seconds
      _streamSubscription =
          _location.onLocationChanged().listen((currentLocation) async {
        latLng = LatLng(currentLocation.latitude, currentLocation.longitude);
        GoogleMapController mapController = await _controller.future;
        mapController
            .animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: latLng,
          zoom: 14.4746,
        )));
        await RestAPI.trackGoing(widget.id, latLng, false);
        setState(() {});
        _setMapPins();
        _setPolylines();
      });
    }
  }

  void _setMapPins() {
    _markers.clear();
    // source pin
    _markers.add(Marker(
      markerId: MarkerId("sourcePin"),
      position: latLng,
      icon: sourceIcon,
    ));
    // destination pin
    _markers.add(Marker(
      markerId: MarkerId("destPin"),
      position: LatLng(widget.lat, widget.lng),
      icon: destinationIcon,
    ));
    setState(() {});
  }

  _setPolylines() async {
    PolylineResult geometry = await polylinePoints?.getRouteBetweenCoordinates(
      RestAPI.GOOGLE_API_KEY,
      PointLatLng(latLng.latitude, latLng.longitude),
      PointLatLng(widget.lat, widget.lng),
      travelMode: TravelMode.driving,
    );
    var disDur =
        await MapsAPI.distanceMatrix(latLng, LatLng(widget.lat, widget.lng));
    _polylines.clear();
    polylineCoordinates.clear();

    disDur.forEach((element) {
      jarak = element.distance.text;
      waktu = element.duration.text;
    });

    geometry.points.forEach((element) {
      polylineCoordinates.add(LatLng(element.latitude, element.longitude));
    });

    _polylines.add(Polyline(
        polylineId: PolylineId("${widget.id}"),
        width: 3,
        color: Color.fromARGB(255, 40, 122, 198),
        points: polylineCoordinates));
    setState(() {});
  }

  Future<BitmapDescriptor> _bitmapDescriptorFromSvgAsset(
      BuildContext context, String assetName) async {
    var svgString = await DefaultAssetBundle.of(context).loadString(assetName);

    var svgDrawableRoot = await svg.fromSvgString(svgString, null);

    var queryData = MediaQuery.of(context);
    var devicePixelRatio = queryData.devicePixelRatio;
    var width = 48 * devicePixelRatio; // SVG's original width
    var height = 48 * devicePixelRatio; // same thing

    var picture = svgDrawableRoot.toPicture(size: Size(width, height));

    var image = await picture.toImage(width.toInt(), height.toInt());
    var bytes = await image.toByteData(format: ui.ImageByteFormat.png);
    return BitmapDescriptor.fromBytes(bytes.buffer.asUint8List());
  }

  void showAlertDialog(BuildContext context) {}
}
