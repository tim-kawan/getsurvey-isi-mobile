import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_survey_app/account/UpgradeToSurveyors.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/presentation/pages/UpgradeToSurveyorNew.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/presentation/provider/upgrade_surveyor_cubit.dart';
import 'package:get_survey_app/job/ListIrkUser/IzinRencanaKotaScreen.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapsScreen extends StatefulWidget {
  final int navigate;
  final String alamat;
  final double lat;
  final double lon;
  final String noKtp;
  final String fullName;
  final String email;
  final String noTelp;
  final String tipe;
  final int kelamin;

  MapsScreen(
      {this.lat,
      this.alamat,
      this.lon,
      this.noKtp,
      this.noTelp,
      this.fullName,
      this.email,
      this.tipe,
      this.kelamin,
      this.navigate,
      Key key})
      : super(key: key);

  @override
  State<MapsScreen> createState() => MapsScreenState();
}

class MapsScreenState extends State<MapsScreen> {
  Completer<GoogleMapController> _controller = Completer();
  Geolocator _geolocator = Geolocator()..forceAndroidLocationManager = true;
  LatLng _latLng = LatLng(-6.2721396, 106.8482382);
  List<Placemark> _geocoder = [];
  final _markers = <Marker>[];
  String _alamat;

  @override
  Widget build(BuildContext context) {
    UpgradeSurveyorCubit upgradeSurveyorCubit = di<UpgradeSurveyorCubit>();
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: primaryColor,
          title: Text("Lokasi"),
        ),
        body: GoogleMap(
          mapType: MapType.normal,
          markers: _markers.toSet(),
          myLocationEnabled: true,
          initialCameraPosition: CameraPosition(
            target: _latLng,
            zoom: 14,
          ),
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
          onTap: _doTap,
        ),
        bottomNavigationBar: widget.tipe == "1"
            ? null
            : BottomAppBar(
                child: Builder(
                  builder: (context) => StreamBuilder<Object>(
                    stream: null,
                    builder: (context, snapshot) {
                      return InkWell(
                        onTap: () async {
                          await Session.doSave("alamats", _alamat);
                          await Session.doSave("latitude", widget.lat);
                          await Session.doSave("longitude", widget.lon);
                          
                          print(_alamat);
                          widget.navigate == 0
                              ? Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => UpgradeToSurveyorNew()))
                              : Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => IzinRencanaKotaScreen(
                                            lokasiTanah: _alamat,
                                          )));
                        },
                        child: Container(
                          height: 50.0,
                          decoration: BoxDecoration(
                            color: primaryColor,
                          ),
                          child: Center(
                            child: Text(
                              "Pilih Lokasi",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: "Sans",
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      );
                    }
                  ),
                ),
              ));
  }

  @override
  void initState() {
    super.initState();
    _alamat = widget.alamat;
    _latLng = new LatLng(widget.lat, widget.lon);
    _markers.add(Marker(
      markerId: MarkerId(_latLng.toString()),
      position: _latLng,
    ));
  }

  _doTap(LatLng point) async {
    if (widget.tipe != "1") {
      _geocoder = await _geolocator.placemarkFromCoordinates(
        point.latitude,
        point.longitude,
      );
      print(_geocoder[0].thoroughfare);

      if (this.mounted) {
        setState(() {
          _markers.clear();
          _markers.add(Marker(
            markerId: MarkerId(point.toString()),
            position: point,
          ));

          if (_geocoder.isNotEmpty) {
            _alamat = "";

            if (_geocoder[0].thoroughfare.isNotEmpty) {
              _alamat += _geocoder[0].thoroughfare;
            }

            if (_geocoder[0].subThoroughfare.isNotEmpty) {
              _alamat += " No. ${_geocoder[0].subThoroughfare}";
            }

            if (_geocoder[0].subLocality.isNotEmpty) {
              _alamat += ", ${_geocoder[0].subLocality}";
            }

            if (_geocoder[0].locality.isNotEmpty) {
              _alamat += ", ${_geocoder[0].locality}";
            }

            if (_geocoder[0].subAdministrativeArea.isNotEmpty) {
              _alamat += ", ${_geocoder[0].subAdministrativeArea}";
            }

            if (_geocoder[0].administrativeArea.isNotEmpty) {
              _alamat += ", ${_geocoder[0].administrativeArea}";
            }

            if (_geocoder[0].postalCode.isNotEmpty) {
              _alamat += ", ${_geocoder[0].postalCode}";
            }

            if (_geocoder[0].country.isNotEmpty) {
              _alamat += ", ${_geocoder[0].country}";
            }
          }
        });
      }
    }
  }
}
