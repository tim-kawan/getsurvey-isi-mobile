import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/home/Rating.dart';
import 'package:get_survey_app/job/ListIrkSurveyor/TabbarlistIrk.dart';
import 'package:get_survey_app/job/ListIrkUser/ListInformasiRencanaKota.dart';
import 'package:get_survey_app/job/ListIrkUser/TabbarListIrkUser.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class Bubble extends StatelessWidget {
  final bool isMe;
  final String name;
  final String message;
  final String date;
  final bool flagJasa;
  final bool chatSistem;

  Bubble(
      {this.message,
      this.isMe,
      this.date,
      this.name,
      this.flagJasa,
      this.chatSistem});

  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      padding: isMe ? EdgeInsets.only(left: 40) : EdgeInsets.only(right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Column(
            mainAxisAlignment:
                isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
            crossAxisAlignment:
                isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 4.0,
              ),
              Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      stops: [
                        0.1,
                        1
                      ],
                      colors: [
                        Color(0xFFEBF5FC),
                        Color(0xFFEBF5FC),
                      ]),
                  borderRadius: isMe
                      ? BorderRadius.only(
                          topRight: Radius.circular(15),
                          topLeft: Radius.circular(15),
                          bottomRight: Radius.circular(0),
                          bottomLeft: Radius.circular(15),
                        )
                      : BorderRadius.only(
                          topRight: Radius.circular(15),
                          topLeft: Radius.circular(15),
                          bottomRight: Radius.circular(15),
                          bottomLeft: Radius.circular(0),
                        ),
                ),
                child: Column(
                  crossAxisAlignment:
                      isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      message.toString(),
                      textAlign: TextAlign.start,
                      style: TextStyle(color: Colors.grey[700], fontSize: 15),
                    ),
                    flagJasa
                        ? Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: InkWell(
                              onTap: () {
                                Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ListIzinRencanaKOta()),
                                    (Route<dynamic> route) => false);
                              },
                              child: Container(
                                padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
                                height: 50,
                                child: Row(
                                  children: [
                                    Image.asset(
                                      "assets/img/building.png",
                                      height: 30,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      "Get-Informasi Rencana Kota",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 13,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                                decoration: BoxDecoration(
                                    color: Colors.blue[900],
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                          blurRadius: 10.0,
                                          color: Colors.black12)
                                    ]),
                              ),
                            ),
                          )
                        : SizedBox(),
                  ],
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                date,
                textAlign: isMe ? TextAlign.end : TextAlign.start,
                style: TextStyle(
                  fontSize: 10,
                  color: Colors.grey[700],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class ChatScreen extends StatefulWidget {
  final Map room;
  final bool chatSistem;
  final String status;

  ChatScreen({
    this.room,
    this.chatSistem,
    this.status,
    Key key,
  }) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final TextEditingController chatController = new TextEditingController();
  final ScrollController listScrollController = new ScrollController();
  Timer _timer;
  List messages = [];
  bool _isLoad = true;
  int _rating = 0;
  bool _showJempol = false;
  bool _isAgreed = false;
  bool upDirection = false;
  bool flag = true;
  bool chatSistem = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.4,
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: primaryColor,
          title: Text(
            widget.room['nama'],
            style: TextStyle(color: Colors.white),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: _willPopCallback,
            color: Colors.blue,
          ),
          actions: <Widget>[
            (_showJempol)
                ? IconButton(
                    icon: Icon(
                      Icons.thumb_up,
                      color: Colors.white,
                    ),
                    onPressed: () async {
                      int idUSer = await Session.getValue(Session.USER_ID);
                      await RestAPI.doDeal(idUSer, widget.room['kode']);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RatingScreen(
                                    room: widget.room,
                                  )));
                    })
                : Container()
          ],
        ),
        body: ModalProgressHUD(
          inAsyncCall: _isLoad,
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[_buildChat(), _buildInput()],
              )
            ],
          ),
        ),
        floatingActionButton: upDirection == true
            ? Padding(
                padding: const EdgeInsets.only(bottom: 80.0),
                child: FloatingActionButton(
                  onPressed: () {
                    listScrollController
                        .jumpTo(listScrollController.position.maxScrollExtent);
                  },
                  backgroundColor: Colors.blueAccent[900],
                  child: Icon(
                    Icons.arrow_downward,
                    size: 40,
                  ),
                ),
              )
            : Container(),
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  Future<bool> _willPopCallback() async {
    if (widget.status == "Surveyor") {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => TabbarIRKScreen()),
          (Route<dynamic> route) => false);
    } else if (widget.status == "User") {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => TabbarIRKUserScreen()),
          (Route<dynamic> route) => false);
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => BtnNavigation(
                    atIndex: 1,
                  )),
          (Route<dynamic> route) => false);
    }
  }

  @override
  void initState() {
    super.initState();
    listScrollController.addListener(() {
      upDirection = listScrollController.position.userScrollDirection ==
          ScrollDirection.forward;

      // makes sure we don't call setState too much, but only when it is needed
      if (upDirection != flag) setState(() {});

      flag = upDirection;
    });
    _doTimingChat();
  }

  Widget _buildChat() {
    return Flexible(
      child: ListView.builder(
        itemCount: messages.length,
        controller: listScrollController,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: messages[index]["isSender"]
                  ? CrossAxisAlignment.end
                  : CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  messages[index]['userData']["nama"],
                  textAlign: messages[index]["isSender"]
                      ? TextAlign.end
                      : TextAlign.start,
                  style: TextStyle(
                    color: Colors.grey[700],
                  ),
                ),
                Bubble(
                  message: messages[index]["text"],
                  isMe: messages[index]["isSender"],
                  date: messages[index]['waktuFormat'].toString().substring(
                      0, messages[index]['waktuFormat'].toString().length - 3),
                  name: messages[index]['userData']["nama"],
                  flagJasa: widget.room["jasa"] > 0 && index == 0,
                  chatSistem: chatSistem,
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _buildInput() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
          color: Colors.grey[300],
          offset: Offset(-2, 0),
          blurRadius: 5,
        ),
      ]),
      child: _isAgreed
          ? Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  RaisedButton(
                    color: primaryColor,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RatingScreen(
                                    room: widget.room,
                                  )));
                    },
                    child: Text(
                      "Ubah penilaian rating ",
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                ],
              ),
            )
          : _isLoad
              ? Row()
              : Row(
                  children: <Widget>[
                    Expanded(
                      child: TextFormField(
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(255),
                        ],
                        controller: chatController,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(left: 15.0),
                          hintText: _isAgreed
                              ? widget.room["status"]
                              : "Pesan Anda...",
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                    _isAgreed
                        ? Container()
                        : IconButton(
                            onPressed: () async {
                              int idUser =
                                  await Session.getValue(Session.USER_ID);
                              await RestAPI.doChat(
                                widget.room["kode"],
                                idUser,
                                chatController.text,
                              );
                              chatController.clear();
                              _syncChat();
                              chatSistem = false;
                            },
                            icon: Icon(
                              Icons.send,
                              color: Color(0xff3E8DF3),
                            ),
                          ),
                  ],
                ),
    );
  }

  _doTimingChat() async {
    await _syncChat();
    int idUser = await Session.getValue(Session.USER_ID);
    print(widget.room);
    _isAgreed = widget.room["status"] == "Agreed";

    if (widget.room["produk"] > 0) {
      _rating =
          await RestAPI.seeMyRate("produk", idUser, widget.room["produk"]);
    } else if (widget.room["jasa"] > 0) {
      _rating = await RestAPI.seeMyRate("jasa", idUser, widget.room["jasa"]);
    } else if (widget.room["irk"] > 0) {
      _rating = await RestAPI.seeMyRate("irk", idUser, widget.room["irk"]);
    }

    if (widget.room['irkData'] != null) {
      if (widget.room["transactionData"]["entitas"] < 0 &&
          widget.room['irkData']['status'] == 5) {
        int statusSurveyor = await Session.getValue(Session.USER_LANJUT);

        _showJempol = statusSurveyor <= 0;
      }
    } else {
      if (widget.room["transactionData"]["entitas"] < 0) {
        _showJempol = true;
      }
    }

    if (widget.chatSistem != null) {
      chatSistem = true;
    }

    setState(() {});

    Future.delayed(Duration(seconds: 1), () {
      _timer = new Timer.periodic(Duration(seconds: 5), (_) => _syncChat());
    });
  }

  _syncChat() async {
    int idUser = await Session.getValue(Session.USER_ID);

    if (idUser == null) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => BtnNavigation(
            atIndex: 2,
          ),
        ),
      );
    } else {
      List temp = await RestAPI.seeChats(widget.room["kode"], idUser);
      temp.sort(
          (a, b) => a["waktu"].toString().compareTo(b["waktu"].toString()));

      if (temp.isNotEmpty) {
        messages.clear();
      }

      temp.forEach((element) {
        messages.add(element);
      });

      _isLoad = false;
      if (!upDirection) {
        Timer(
            Duration(milliseconds: 300),
            () => listScrollController
                .jumpTo(listScrollController.position.maxScrollExtent));
      }

      setState(() {});
    }
  }
}
