import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:get_survey_app/chat/Chat.dart';
import 'package:get_survey_app/component/ImageUtils.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/model/ChatRoom.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shimmer/shimmer.dart';

class ListChat extends StatefulWidget {
  @override
  _ListChatState createState() => _ListChatState();
}

class _ListChatState extends State<ListChat> {
  var listChat = [];
  List listOfRoom = [];
  final DateFormat dateFormat = DateFormat("dd MMM yyyy HH:mm:ss");
  bool _isLoading = true;
  final DefaultCacheManager _cacheManager = new DefaultCacheManager();
  final RefreshController _refreshController = RefreshController();
  Timer _timer;

  @override
  Widget build(BuildContext context) {
    Text statusUser(BuildContext context, RoomChat rooms) {
      Text text = Text("...");

      if (rooms.status == 1) {
        text = Text(
          'ACTIVE',
          style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
        );
      } else if (rooms.status == 2) {
        text = Text(
          'DEAL',
          style: TextStyle(
              color: (rooms.userDeal && rooms.ownerDeal)
                  ? Colors.green
                  : Colors.orange,
              fontWeight: FontWeight.bold),
        );
      } else if (rooms.status == 3) {
        text = Text(
          'CLOSE',
          style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
        );
      }

      return text;
    }

    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        centerTitle: true,
        leading: Container(),
        title: Text(
          "Daftar Chat",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: _isLoading
          ? Center(
              child: Shimmer.fromColors(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    "Tunggu Sebentar...",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 22.5,
                    ),
                  ),
                ),
                baseColor: Colors.grey[700],
                highlightColor: Colors.grey[200],
              ),
            )
          : listOfRoom.length > 0
              ? ListView.builder(
                  itemCount: listOfRoom.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: InkWell(
                        onTap: () async {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ChatScreen(
                                  room: listOfRoom[index],
                                ),
                              ));
                        },
                        child: Container(
                          height: 120.0,
                          width: screenSize.width / 1.5,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10.0)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: 70.0,
                                  height: 70.0,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: ImageUtils.APP_LOGO,
                                      fit: BoxFit.cover,
                                    ),
                                    borderRadius: BorderRadius.circular(80.0),
                                  ),
                                ),
                                SizedBox(
                                  width: 8.0,
                                ),
                                Flexible(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          listOfRoom[index]['jasaData'] != null
                                              ? Container(
                                                  width: screenSize.width / 1.7,
                                                  child: Text(
                                                    listOfRoom[index]["nama"] +
                                                        " (${listOfRoom[index]['jasaData']['nama']})",
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    maxLines: 2,
                                                    style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                )
                                              : listOfRoom[index]
                                                          ['produkData'] !=
                                                      null
                                                  ? Container(
                                                      width: screenSize.width /
                                                          1.7,
                                                      child: Text(
                                                        listOfRoom[index]
                                                                ["nama"] +
                                                            " (${listOfRoom[index]['produkData']['nama']})",
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    )
                                                  : Container(
                                                      width: screenSize.width /
                                                          1.7,
                                                      child: Text(
                                                        listOfRoom[index]
                                                                ["nama"] +
                                                            " (${listOfRoom[index]['irkData']['namaPemilik']})",
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    ),
                                          SizedBox(
                                            height: 3,
                                          ),
                                          Container(
                                            width: screenSize.width / 1.7,
                                            child: Text(
                                              listOfRoom[index]["with"]
                                                          ['nama'] ==
                                                      null
                                                  ? ""
                                                  : listOfRoom[index]["with"]
                                                      ['nama'],
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: Colors.black),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 3,
                                          ),
                                          Text(
                                            listOfRoom[index]["waktuFormat"],
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                          SizedBox(
                                            height: 3,
                                          ),
                                          Text(
                                            "Status : ${listOfRoom[index]["status"]}",
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                )
              : Center(
                  child: Text(
                    "Tidak Ada Daftar Chat",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.w600,
                        color: Colors.grey[700]),
                  ),
                ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _initData();
  }

  _getListRoomChat(int id, String privilege) async {
    var idUser = await Session.getValue(Session.USER_ID);
    List temp = await RestAPI.seeChatRoom(idUser);

    temp.forEach((roomChat) async {
      print(roomChat['with']['nama']);
      listOfRoom.add(roomChat);
    });
    setState(() {});
  }

  void _initData() async {
    await _cacheManager.emptyCache();
    String status;
    int type;
    type = await Session.getValue(Session.USER_STATUS);
    var idUser = await Session.getValue(Session.USER_ID);
    int statusSurveyor = await Session.getValue(Session.USER_LANJUT);

    if (type > 0) {
      if (statusSurveyor == 1 || statusSurveyor == 2) {
        status = "roomsOwn";
      } else {
        status = "roomsPick";
      }
    } else {
      status = "roomsPick";
    }

    await _getListRoomChat(idUser, status);
    _isLoading = false;
    setState(() {});
  }
}
