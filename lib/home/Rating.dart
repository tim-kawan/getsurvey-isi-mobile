import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/model/Surveyor.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class RatingScreen extends StatefulWidget {
  final Map room;

  RatingScreen({
    this.room,
    Key key,
  }) : super(key: key);

  @override
  _RatingScreenState createState() => _RatingScreenState();
}

class _RatingScreenState extends State<RatingScreen> {
  var rating = 0.0;
  final DefaultCacheManager _cacheManager = new DefaultCacheManager();
  TextEditingController _ulasanCrtl = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  List<Surveyor> listSurveyor = [];
  bool _isLoad = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        centerTitle: true,
        title: Text(
          "Beri Penilaian",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: ModalProgressHUD(
        inAsyncCall: _isLoad,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  "Silahkan Anda Berikan Penilaian dan ulasan Untuk Barang/jasa ini, Penilaian Sangat Diperlukan Untuk Bahan Evaluasi Surveyor dan Barang Terkait",
                  textAlign: TextAlign.justify,
                  style: TextStyle(color: primaryColor, fontSize: 18.0),
                ),
                SizedBox(
                  height: 10.0,
                ),
                SmoothStarRating(
                  rating: rating,
                  size: 50,
                  filledIconData: Icons.star,
                  defaultIconData: Icons.star_border,
                  starCount: 5,
                  allowHalfRating: false,
                  spacing: 2.0,
                  onRated: (value) {
                    setState(() {
                      rating = value;
                    });
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                    maxLines: 5,
                    controller: _ulasanCrtl,
                    decoration: InputDecoration(
                      labelText: "Berikan Ulasan",
                      fillColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25.0),
                        borderSide: BorderSide(
                          color: primaryColor,
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25.0),
                        borderSide: BorderSide(
                          color: primaryColor,
                          width: 2.0,
                        ),
                      ),
                    )),
                SizedBox(
                  height: 20.0,
                ),
                InkWell(
                  onTap: () async {
                    setState(() => _isLoad = true);
                    int parseRate = rating.toInt();
                    int idUser = await Session.getValue(Session.USER_ID);
                    if (parseRate != 0) {
                      bool rated = await RestAPI.rateProdServIRK(
                          idUser, parseRate,
                          product: widget.room["produk"],
                          service: widget.room["jasa"],
                          irk: widget.room["irk"],
                          ulasan: _ulasanCrtl.text.isEmpty
                              ? " "
                              : _ulasanCrtl.text);

                      if (rated) {
                        Alert(
                          context: context,
                          image: Image.asset(
                            "assets/alert/check.png",
                            color: Colors.green,
                          ),
                          type: AlertType.success,
                          title: "Berhasil",
                          desc:
                              "Anda telah memberikan Ulasan kepada surveyor. Terimakasih",
                          closeIcon: Container(),
                          onWillPopActive: true,
                          buttons: [
                            DialogButton(
                              child: Text(
                                "Kembali",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                              onPressed: () =>
                                  Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (context) => BtnNavigation(
                                                atIndex: 1,
                                              )),
                                      (Route<dynamic> route) => false),
                              width: 120,
                            )
                          ],
                        ).show();
                      } else {
                        Alert(
                          context: context,
                          image: Image.asset(
                            "assets/alert/eror.png",
                            color: Colors.green,
                          ),
                          type: AlertType.error,
                          title: "Gagal",
                          desc: "Terjadi kesalahan cobalah beberapa saat",
                          closeIcon: Container(),
                          onWillPopActive: true,
                          buttons: [
                            DialogButton(
                              child: Text(
                                "Kembali",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                              onPressed: () =>
                                  Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (context) => BtnNavigation(
                                                atIndex: 1,
                                              )),
                                      (Route<dynamic> route) => false),
                              width: 120,
                            )
                          ],
                        ).show();
                      }
                    } else {
                      Alert(
                        context: context,
                        image: Image.asset(
                          "assets/alert/eror.png",
                          color: Colors.green,
                        ),
                        type: AlertType.error,
                        title: "Gagal",
                        desc: "Ratig tidak boleh kosong/ bernial 0",
                        closeIcon: Container(),
                        onWillPopActive: true,
                        buttons: [
                          DialogButton(
                            child: Text(
                              "Kembali",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                            onPressed: () => Navigator.of(context),
                            width: 120,
                          )
                        ],
                      ).show();
                    }
                  },
                  child: Container(
                    height: 40.0,
                    decoration: BoxDecoration(
                        border: Border.all(), color: primaryColor),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          "Beri Penilaian",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    print(widget.room);
    _cacheManager.emptyCache();
    // _getListSurveyorId();
  }
}
