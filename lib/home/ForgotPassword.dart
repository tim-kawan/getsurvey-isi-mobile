import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  TextEditingController _emailController = TextEditingController();

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: primaryColor,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              }),
          title: Text(
            "Lupa kata Sandi",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        body: ModalProgressHUD(inAsyncCall: _isLoading, child: _body(context)));
  }

  Widget _body(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
          child: Text(
            'Masukkan Email Anda yang Terkait',
            style: TextStyle(color: primaryColor, fontSize: 18.0),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
          child: Container(
            height: 60.0,
            alignment: AlignmentDirectional.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14.0),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(blurRadius: 10.0, color: Colors.black12)
                ]),
            padding:
                EdgeInsets.only(left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
            child: Theme(
              data: ThemeData(
                hintColor: Colors.transparent,
              ),
              child: TextFormField(
                controller: _emailController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    labelText: "Email",
                    icon: Icon(
                      Icons.email,
                      color: primaryColor,
                    ),
                    labelStyle: TextStyle(
                        fontSize: 15.0,
                        fontFamily: 'Sans',
                        letterSpacing: 0.3,
                        color: primaryColor,
                        fontWeight: FontWeight.w600)),
                keyboardType: TextInputType.emailAddress,
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
              left: 30.0, right: 30.0, top: 20.0, bottom: 20.0),
          child: Builder(
            builder: (context) => InkWell(
              onTap: () async {
                await _onSubmit(
                  _emailController.text,
                  context,
                );
              },
              child: Container(
                height: 60.0,
                width: MediaQuery.of(context).size.width / 3,
                child: Center(
                  child: Text(
                    "Kirim",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                decoration: BoxDecoration(
                    color: primaryColor,
                    borderRadius: BorderRadius.circular(10.0)),
              ),
            ),
          ),
        ),
      ],
    );
  }

  _onSubmit(String email, BuildContext context) async {
    if (_emailController.text.isNotEmpty) {
      setState(() {
        _isLoading = true;
      });
      var signedInUser = await RestAPI.doLupaSandi(email);

      if (signedInUser) {
        Alert(
          context: context,
          image: Image.asset(
            "assets/alert/check.png",
            color: Colors.red,
          ),
          type: AlertType.success,
          title: "Berhasil",
          desc:
              "Silakan Menunggu Pesan yang akan dikirim Melalui Email Terkait. Silahkan kembali menuju halaman utama",
          closeIcon: Container(),
          onWillPopActive: true,
          buttons: [
            DialogButton(
              child: Text(
                "Kembali",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => BtnNavigation())),
              width: 120,
            )
          ],
        ).show();
      } else {
        setState(() {
          _isLoading = false;
        });
        Component.showAppNotification(
          context,
          "Pemberitahuan",
          "Email Yang Anda Masukkan Tidak Benar",
        );
      }
    } else {
      Component.showAppNotification(
        context,
        "Tidak Sesuai",
        "Masukkan Email",
      );
    }
  }
}
