import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_survey_app/account/DetailSurveyor.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/model/ProductService.dart';
import 'package:get_survey_app/model/Surveyor.dart';
import 'package:get_survey_app/product/DetailProductScreen.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  TextEditingController _searchController = TextEditingController();
  ScrollController controller = new ScrollController();
  final DefaultCacheManager _cacheManager = new DefaultCacheManager();
  List<ProductService> listProduct = [];
  List<ProductService> listService = [];
  List<Surveyor> listSurveyor = [];
  var rateJasa = Map<int, int>();
  List searchItemProduct = [];
  bool isLoad = false;
  bool isSearch = false;

  Position userLocation;
  String locationText = "Selamat datang...";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            color: Colors.blue,
            width: MediaQuery.of(context).size.width,
            child: Image.asset(
              "assets/img/building.jpeg",
              fit: BoxFit.cover,
              color: Color.fromRGBO(255, 255, 255, 10),
              colorBlendMode: BlendMode.modulate,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 160.0),
            child: Column(
              children: <Widget>[
                Container(
                  color: Colors.black12,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        "Temukan Surveyor Anda\nBerdasarkan Lokasi",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 25.0),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 30.0),
                  child: Container(
                    height: 60.0,
                    alignment: AlignmentDirectional.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(14.0),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(blurRadius: 10.0, color: Colors.black12)
                        ]),
                    padding: EdgeInsets.only(
                        left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
                    child: Theme(
                      data: ThemeData(
                        hintColor: Colors.transparent,
                      ),
                      child: TextFormField(
                        textInputAction: TextInputAction.done,
                        controller: _searchController,
                        onFieldSubmitted: (term) async {
                          setState(() {
                            isLoad = true;
                            isSearch = true;
                            searchItemProduct.clear();
                          });
                          int idUser = await Session.getValue(Session.USER_ID);
                          double lon = (userLocation == null)
                              ? 0
                              : userLocation.longitude;
                          double lat = (userLocation == null)
                              ? 0
                              : userLocation.latitude;
                          if (_searchController.text.isNotEmpty) {
                            _getListProductSearch(
                                _searchController.text, context);
                            RestAPI.doLogHit(
                                idUser.toString(),
                                lon.toString(),
                                lat.toString(),
                                DateFormat("dd MMM yyyy HH:mm:ss")
                                    .format(DateTime.now()),
                                _searchController.text);
                          }
                        },
                        decoration: InputDecoration(
                            suffixIcon: GestureDetector(
                              onTap: () async {
                                setState(() {
                                  isLoad = true;
                                  isSearch = true;
                                  searchItemProduct.clear();
                                });
                                int idUser =
                                    await Session.getValue(Session.USER_ID);
                                double lon = (userLocation == null)
                                    ? 0
                                    : userLocation.longitude;
                                double lat = (userLocation == null)
                                    ? 0
                                    : userLocation.latitude;
                                if (_searchController.text.isNotEmpty) {
                                  _getListProductSearch(
                                      _searchController.text, context);
                                  RestAPI.doLogHit(
                                      idUser.toString(),
                                      lon.toString(),
                                      lat.toString(),
                                      DateFormat("dd MMM yyyy HH:mm:ss")
                                          .format(DateTime.now()),
                                      _searchController.text);
                                }
                              },
                              child: Container(
                                height: MediaQuery.of(context).size.height,
                                width: 60.0,
                                child: Icon(
                                  Icons.search,
                                  color: primaryColor,
                                ),
                              ),
                            ),
                            border: InputBorder.none,
                            labelText: "Input Lokasi",
                            labelStyle: TextStyle(
                                fontSize: 15.0,
                                fontFamily: 'Sans',
                                letterSpacing: 0.3,
                                color: primaryColor,
                                fontWeight: FontWeight.w600)),
                        keyboardType: TextInputType.text,
                      ),
                    ),
                  ),
                ),
                Flexible(
                  child: (isLoad)
                      ? Shimmer.fromColors(
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              "Sedang melakukan pencarian data...",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 22.5,
                              ),
                            ),
                          ),
                          baseColor: Colors.grey[700],
                          highlightColor: Colors.grey[200],
                        )
                      : (searchItemProduct.length > 0)
                          ? ListView.builder(
                              itemCount: searchItemProduct.length,
                              controller: controller,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: InkWell(
                                    onTap: () {
                                      if (searchItemProduct[index]['type'] ==
                                          "produk") {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetailProductScreen(
                                                      id: searchItemProduct[
                                                              index]['data']
                                                          .id,
                                                      tipe: "item",
                                                      // rate: rateJasa[
                                                      //     searchItemProduct[
                                                      //             index]['data']
                                                      //         .id],
                                                    )));
                                      } else if (searchItemProduct[index]
                                              ['type'] ==
                                          "jasa") {
                                        print(searchItemProduct[index]['data']
                                            .name);
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetailProductScreen(
                                                      id: searchItemProduct[
                                                              index]['data']
                                                          .id,
                                                      tipe: "work",
                                                      rate: rateJasa[
                                                          searchItemProduct[
                                                                  index]['data']
                                                              .id],
                                                    )));
                                      } else {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DetailSurveyor(
                                                      img: searchItemProduct[
                                                              index]['data']
                                                          .img,
                                                      name: searchItemProduct[
                                                              index]['data']
                                                          .name,
                                                      id: searchItemProduct[
                                                              index]['data']
                                                          .id,
                                                      noAnggota:
                                                          searchItemProduct[
                                                                  index]['data']
                                                              .noAnggota,
                                                      alamat: searchItemProduct[
                                                              index]['data']
                                                          .alamat,
                                                      long: searchItemProduct[
                                                              index]['data']
                                                          .long,
                                                      lat: searchItemProduct[
                                                              index]['data']
                                                          .lat,
                                                      email: searchItemProduct[
                                                              index]['data']
                                                          .email,
                                                    )));
                                      }
                                    },
                                    child: Container(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              6,
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              width: 2, color: Colors.grey),
                                          borderRadius:
                                              BorderRadius.circular(5.0)),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              height: 150.0,
                                              width: 100,
                                              color: primaryColor,
                                              child: FadeInImage.assetNetwork(
                                                image: searchItemProduct[index]
                                                    ["img"],
                                                placeholder: "assets/logo.png",
                                              ),
                                            ),
                                            // CachedNetworkImage(
                                            //   imageUrl: searchItemProduct[index]
                                            //       ["img"],
                                            //   cacheManager: _cacheManager,
                                            //   imageBuilder:
                                            //       (context, provider) =>
                                            //
                                            //   placeholder: (context, url) =>
                                            //       Container(
                                            //           height: 150.0,
                                            //           width: 100,
                                            //           color: primaryColor,
                                            //           decoration: BoxDecoration(
                                            //               image: DecorationImage(
                                            //                   image: ImageUtils
                                            //                       .APP_LOGO,
                                            //                   fit: BoxFit
                                            //                       .cover))),
                                            //   errorWidget: (context, url,
                                            //           error) =>
                                            //       Container(
                                            //           height: 150.0,
                                            //           width: 100,
                                            //           color: primaryColor,
                                            //           decoration: BoxDecoration(
                                            //               image: DecorationImage(
                                            //                   image: ImageUtils
                                            //                       .APP_LOGO,
                                            //                   fit: BoxFit
                                            //                       .cover))),
                                            // ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 5.0),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Flexible(
                                                    child: Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width /
                                                              2,
                                                      child: Text(
                                                        searchItemProduct[index]
                                                            ['nama'],
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                            color: primaryColor,
                                                            fontSize: 18.0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    ),
                                                  ),
                                                  Flexible(
                                                    child: Container(
                                                      height:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .height /
                                                              2,
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width /
                                                              2,
                                                      child: Text(
                                                        searchItemProduct[index]
                                                            ['alamat'],
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        maxLines: 2,
                                                        style: TextStyle(
                                                            fontSize: 16.0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              })
                          : Center(
                              child: Text(
                                  (isSearch)
                                      ? "Data yang Anda Cari Tidak Ditemukan"
                                      : "",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.grey))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _cacheManager.emptyCache();
    Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .asStream()
        .listen((data) {
      _loadPlace(data);
      setState(() {
        userLocation = data;
      });
    });
  }

  _getListProductSearch(String text, BuildContext context) async {
    if ((text.isNotEmpty)) {
      var filterItem = {};
      var allListItem = [];

      await RestAPI.seeProducts().then((results) async {
        results.forEach((product) {
          listProduct.add(ProductService.fromJson(product));
        });
      });

      await RestAPI.seeServices().then((results) async {
        results.forEach((product) {
          listService.add(ProductService.fromJson(product));
        });
      });

      await RestAPI.seeSurveyors().then((_surveyors) async {
        _surveyors.forEach((surveyor) {
          setState(() {
            listSurveyor.add(Surveyor.fromJson(surveyor));
          });
        });
      });

      for (ProductService produk in listProduct) {
        allListItem.add({
          "img": "${RestAPI.FOTO_URL}/gambar1_p${produk.id}.jpg",
          "nama": produk.nama,
          "alamat": produk.deskripsi,
          "noTelp": "",
          "type": "produk",
          "data": produk
        });
      }

      for (ProductService jasa in listService) {
        allListItem.add({
          "img": "${RestAPI.FOTO_URL}/gambar1_j${jasa.id}.jpg",
          "nama": jasa.nama,
          "alamat": jasa.deskripsi,
          "noTelp": "",
          "type": "jasa",
          "data": jasa
        });
      }

      for (Surveyor agen in listSurveyor) {
        if (agen.isVisible) {
          allListItem.add({
            "img": "${RestAPI.FOTO_URL}/pp_${agen.id}.jpg",
            "nama": agen.name,
            "alamat": agen.alamat,
            "noTelp": agen.phoneNum,
            "type": "surveyor",
            "data": agen
          });
        }
      }

      for (int i = 0; i < allListItem.length; i++) {
        if (allListItem[i]['nama'].toLowerCase().contains(text.toLowerCase())) {
          filterItem.putIfAbsent(allListItem[i]['nama'], () => allListItem[i]);
        }

        if (allListItem[i]['alamat']
            .toLowerCase()
            .contains(text.toLowerCase())) {
          filterItem.putIfAbsent(allListItem[i]['nama'], () => allListItem[i]);
        }

        if (allListItem[i]['noTelp']
            .toLowerCase()
            .contains(text.toLowerCase())) {
          filterItem.putIfAbsent(allListItem[i]['nama'], () => allListItem[i]);
        }
      }

      print(filterItem);

      filterItem.forEach((key, filter) {
        searchItemProduct.add(filter);
      });

      setState(() {
        isLoad = false;
      });
    }
  }

  _loadPlace(Position loc) async {
    // print("search nearby locations ${loc}");
    Geolocator()
        .placemarkFromCoordinates(loc.latitude, loc.longitude)
        .asStream()
        .listen((result) {
      setState(() {
        if (result.isNotEmpty) {
          var lc = result.first;
          print(lc.locality);
          print(lc.toString());
          locationText =
              "${lc.subLocality}, ${lc.locality.isEmpty ? lc.administrativeArea : lc.locality}";
        } else {
          locationText = "Lokasi tidak ditemukan...";
        }
      });
    }).onError((error) {
      locationText = "Gagal mendapatkan lokasi....";
    });
  }
}
