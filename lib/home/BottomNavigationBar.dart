import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_survey_app/account/Profile.dart';
import 'package:get_survey_app/chat/ListChat.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/features/voting/presentation/pages/main_voting_screen.dart';
import 'package:get_survey_app/home/BannerPopUp.dart';
import 'package:get_survey_app/home/HomeScreen.dart';
import 'package:get_survey_app/home/LoginScreen.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';

class BtnNavigation extends StatefulWidget {
  final int atIndex;
  final String pesan;
  final int baner;

  const BtnNavigation({Key key, this.atIndex, this.pesan, this.baner})
      : super(key: key);

  @override
  _BtnNavigationState createState() => _BtnNavigationState();
}

class _BtnNavigationState extends State<BtnNavigation> {
  int _currentIndex = 0;
  bool isValid = false;
  var banner;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: callPage(_currentIndex),
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          currentIndex: _currentIndex,
          selectedItemColor: primaryColor,
          unselectedItemColor: Colors.grey,
          unselectedLabelStyle: TextStyle(color: Colors.grey),
          onTap: (index) {
            setState(() => _currentIndex = index);
          },
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
              ),
              label: 'Beranda',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.chat_bubble,
              ),
              label: 'Pesan',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.person,
              ),
              label: 'Profile',
            ),
          ]),
    );
  }

  Widget callPage(int current) {
    switch (current) {
      case 0:
        return Dashbord();

        break;
      case 1:
        return (!isValid) ? LoginScren() : ListChat();

        break;
      case 2:
        return (!isValid) ? LoginScren() : UserProfileScreen();

      default:
        return Dashbord();
    }
  }

  @override
  void initState() {
    super.initState();
    _doCek();
  }

  _doCek() async {
    bool valid = await Session.isExist(Session.USER_EMAIL);
    banner = await RestAPI.getBannerPopUp();
    // print(banner);

    setState(() {
      isValid = valid;
      _currentIndex = ((widget.atIndex != null) ? widget.atIndex : 0);
    });
    widget.baner == 1 && banner['display'] == true
        ? Future.delayed(Duration.zero, () {
            showDialog(
                context: context,
                builder: (context) => AlertDialog(
                    backgroundColor: Colors.transparent,
                    content: InkWell(
                      onTap: banner['link'] != null && banner['link'] != ""
                          ? () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => WebViewPopUp(
                                            url: banner['link'],
                                            title: 1,
                                          )));
                            }
                          : () {
                              Navigator.pop(context);
                            },
                      child: Container(
                        height: 400,
                        color: Colors.transparent,
                        child: Center(
                          child: Column(
                            children: [
                              Align(
                                  alignment: Alignment.topRight,
                                  child: IconButton(
                                      icon: Icon(
                                        Icons.close,
                                        color: Colors.white,
                                      ),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      })),
                              Container(
                                  height: 350,
                                  child: Image.network(
                                    banner['url'],
                                    fit: BoxFit.fill,
                                  )),
                            ],
                          ),
                        ),
                      ),
                    )));
          })
        : Future.delayed(Duration.zero, () {});
  }
}
