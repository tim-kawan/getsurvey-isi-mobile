import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/service/Helper.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:path_provider/path_provider.dart';

class ClearChacheScreen extends StatefulWidget {
  @override
  _ClearChacheScreenState createState() => _ClearChacheScreenState();
}

class _ClearChacheScreenState extends State<ClearChacheScreen> {
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: primaryColor,
          title: Text(
            "Notifikasi",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
        ),
        body: SafeArea(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Pengaturan Notifikasi',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
                  child: Text(
                    'Halaman ini digunakan untuk memastikan bahwa Anda mendapatkan notifikasi dari Aplikasi GetSurvey',
                    style: TextStyle(
                      fontSize: 14,
                    ),
                  ),
                ),
              ],
            ),
          ),
          _buildSeparator(screenSize),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              children: [
                Image(
                  image: AssetImage(
                    'assets/mascots/icon_gs.jpeg',
                  ),
                  width: 50,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Apakah Anda belum terima Notifikasi?',
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold),
                      ),
                      Container(
                        width: screenSize.width * 0.7,
                        child: Text(
                          'Hal ini bisa saja karena chace HP Anda penuh. Silakan bersihkan Chace di HP Anda dan Jika masih Belum muncul, silahkan bersihkan cache secara manual dengan masuk ke menu pengaturan',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 5,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          InkWell(
            borderRadius: BorderRadius.circular(16.0),
            onTap: () {
              // _deleteCacheDir();
              _deleteAppDir();
            },
            child: Container(
              width: MediaQuery.of(context).size.width / 1.5,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              decoration: BoxDecoration(
                  color: primaryColor, borderRadius: BorderRadius.circular(16)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Image(
                    image: AssetImage(
                      'assets/img/ic_brom.png',
                    ),
                    width: 30,
                    color: primaryWhite,
                  ),
                  Text(
                    "Bersihkan cache sekarang",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top:16.0,bottom: 16),
            child: _buildSeparator(screenSize),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              children: [
                Image(
                  image: AssetImage(
                    'assets/img/ic_alert_triangle.png',
                  ),
                  width: 50,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Logout/Login ulang ke Aplikasi Get-Survey',
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold),
                      ),
                      Container(
                        width: screenSize.width * 0.7,
                        child: Text(
                          'Jika Anda masih belum mendapatkan Notifikasi dari Aplikasi.',
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.justify,
                          maxLines: 5,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ));
  }

  Future<void> _deleteCacheDir() async {
    final cacheDir = await getTemporaryDirectory();

    if (cacheDir.existsSync()) {
      cacheDir.deleteSync(recursive: true);
    }
  }

  Future<void> _deleteAppDir() async {
    // final appDir = await getApplicationSupportDirectory();
    //
    // if (appDir.existsSync()) {
    //   appDir.deleteSync(recursive: true);
    // }
    String token = await Helper.fcmToken(context);
    var id = await Session.getValue(Session.USER_ID);
    await RestAPI.saveToken(id, token);
    await RestAPI.doCekNotif(id);
  }

  Widget _buildSeparator(Size screenSize) {
    return Container(
      width: screenSize.width,
      height: 8.0,
      color: Colors.grey[200],
      margin: EdgeInsets.only(top: 4.0),
    );
  }
}
