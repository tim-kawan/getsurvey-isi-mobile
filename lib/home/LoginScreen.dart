import 'package:flutter/material.dart';
import 'package:get_survey_app/account/RegisterScreen.dart';
import 'package:get_survey_app/component/ImageUtils.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/home/ForgotPassword.dart';
import 'package:get_survey_app/service/Helper.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class LoginScren extends StatefulWidget {
  @override
  _LoginScrenState createState() => _LoginScrenState();
}

class _LoginScrenState extends State<LoginScren> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool _obscureText = true;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: ModalProgressHUD(inAsyncCall: _isLoading, child: _body(context)));
  }

  @override
  void initState() {
    super.initState();
  }

  Widget radioButton(bool isSelected) => Container(
        width: 25.0,
        height: 25.0,
        padding: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(width: 2.0, color: primaryColor)),
        child: isSelected
            ? Container(
                width: double.infinity,
                height: double.infinity,
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.blue),
              )
            : Container(),
      );

  Widget _body(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 8.0, top: 40.0, right: 8.0),
      child: ListView(
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.only(bottom: 0.0),
              child: Container(
                height: 100.0,
                width: 200.0,
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: ImageUtils.APP_LOGO,
                )),
              )),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Center(
              child: RichText(
                text: TextSpan(
                  text: 'Get',
                  style: TextStyle(
                      fontFamily: "Gotik",
                      fontStyle: FontStyle.italic,
                      fontSize: 40.0,
                      color: primaryColor,
                      fontWeight: FontWeight.bold),
                  children: <TextSpan>[
                    TextSpan(
                        text: 'Survey',
                        style: TextStyle(
                            fontFamily: "Gotik",
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 40.0)),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
            child: Container(
              height: 60.0,
              alignment: AlignmentDirectional.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(14.0),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(blurRadius: 10.0, color: Colors.black12)
                  ]),
              padding: EdgeInsets.only(
                  left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
              child: Theme(
                data: ThemeData(
                  hintColor: Colors.transparent,
                ),
                child: TextFormField(
                  controller: _emailController,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      labelText: "Email",
                      icon: Icon(
                        Icons.email,
                        color: primaryColor,
                      ),
                      labelStyle: TextStyle(
                          fontSize: 15.0,
                          fontFamily: 'Sans',
                          letterSpacing: 0.3,
                          color: primaryColor,
                          fontWeight: FontWeight.w600)),
                  keyboardType: TextInputType.emailAddress,
                ),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 30.0),
            child: Container(
              height: 60.0,
              alignment: AlignmentDirectional.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(14.0),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(blurRadius: 10.0, color: Colors.black12)
                  ]),
              padding: EdgeInsets.only(
                  left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
              child: Theme(
                data: ThemeData(
                  hintColor: Colors.transparent,
                ),
                child: TextFormField(
                  textInputAction: TextInputAction.done,
                  controller: _passwordController,
                  obscureText: _obscureText,
                  decoration: InputDecoration(
                      suffixIcon: GestureDetector(
                        onTap: () => setState(() {
                          _obscureText = !_obscureText;
                        }),
                        child: Icon(
                          _obscureText
                              ? Icons.visibility_off
                              : Icons.visibility,
                          color: primaryColor,
                          semanticLabel:
                              _obscureText ? 'Lihat Sandi' : 'Tutup Sandi',
                        ),
                      ),
                      border: InputBorder.none,
                      labelText: "Kata Sandi",
                      icon: Icon(
                        Icons.lock,
                        color: primaryColor,
                      ),
                      labelStyle: TextStyle(
                          fontSize: 15.0,
                          fontFamily: 'Sans',
                          letterSpacing: 0.3,
                          color: primaryColor,
                          fontWeight: FontWeight.w600)),
                  keyboardType: TextInputType.text,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30.0, top: 8.0, right: 30.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ForgotPasswordScreen()));
                  },
                  child: Container(
                    height: 30.0,
                    width: MediaQuery.of(context).size.width / 2.4,
                    child: Center(
                      child: Text("Lupa Kata Sandi?",
                          style: TextStyle(
                              fontSize: 15,
                              color: primaryColor,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 30.0, right: 30.0, top: 20.0, bottom: 20.0),
            child: InkWell(
              onTap: () {
                _onSubmit(context);
              },
              child: Container(
                height: 60.0,
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Text(
                    "Masuk",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                decoration: BoxDecoration(
                    color: primaryColor,
                    borderRadius: BorderRadius.circular(10.0)),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "Belum Punya Akun? ",
                style: TextStyle(color: primaryGrey, fontSize: 15.0),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => RegisterScreen()));
                },
                child: Container(
                  height: 30.0,
                  child: Center(
                    child: Text(
                      "Daftar Sekarang",
                      style: TextStyle(color: primaryColor, fontSize: 15.0),
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  void _onSubmit(BuildContext context) async {
    setState(() {
      _isLoading = true;
    });
    if (_emailController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty) {
      var signedInUser = await RestAPI.doLogin(
          _emailController.text, _passwordController.text);

      if (signedInUser["status"]) {
        String token = await Helper.fcmToken(context);

        if (signedInUser["user"]["status_user"] == 1) {
          await Session.doSetActiveUser(signedInUser);
          print("token yang mau di save");
          print(token);
          await RestAPI.saveToken(signedInUser["user"]["id"], token);

          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => BtnNavigation()),
              (Route<dynamic> route) => false);
        } else {
          Alert(
            context: context,
            image: Image.asset(
              "assets/alert/error.png",
              color: Colors.red,
            ),
            type: AlertType.error,
            title: "Terjadi Kesalahan",
            desc: "Cek kembali Username dan kata Sandi Anda!",
            closeIcon: Container(),
            onWillPopActive: true,
            buttons: [
              DialogButton(
                child: Text(
                  "Kembali",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () => Navigator.pop(context),
                width: 120,
              )
            ],
          ).show();

          setState(() {
            _isLoading = false;
          });
        }
      } else {
        Alert(
          context: context,
          image: Image.asset(
            "assets/alert/error.png",
            color: Colors.red,
          ),
          title: "Gagal Masuk",
          type: AlertType.error,
          desc:
              "Cek kembali Email dan Kata Sandi Anda atau Konfirmasi Akun Anda melalui Email Terkait. Jika Anda lupa, klik lupa Kata Sandi!",
          closeIcon: Container(),
          onWillPopActive: true,
          buttons: [
            DialogButton(
              child: Text(
                "Kembali",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () => Navigator.pop(context),
              width: 120,
            )
          ],
        ).show();
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      Alert(
        context: context,
        image: Image.asset(
          "assets/alert/error.png",
          color: Colors.red,
        ),
        type: AlertType.error,
        title: "Tidak Sesuai",
        desc: "Email dan Kata Sandi Tidak Boleh Kosong!",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Kembali",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () => Navigator.pop(context),
            width: 120,
          )
        ],
      ).show();
      setState(() {
        _isLoading = false;
      });
    }
  }
}
