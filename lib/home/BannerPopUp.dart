import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPopUp extends StatefulWidget {
  final String url;
  final int title;
  WebViewPopUp({this.url, this.title});
  @override
  WebViewPopUpState createState() => WebViewPopUpState();
}

class WebViewPopUpState extends State<WebViewPopUp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: primaryColor,
        title: Text(
          widget.title == 1 ? "Banner Detail" : "File Sertifikat",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: WebView(
          initialUrl: widget.url,
          javascriptMode: JavascriptMode.unrestricted,
        ),
      ),
    );
  }
}
