import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';

class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  final pages = [
    PageViewModel(
      pageColor: Colors.white,
      bubbleBackgroundColor: const Color(0xFF374888),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            'Segala Macam Pilihan',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700),
          ),
          SizedBox(height: 10,),
          const Text(
            'Ada beragam banyak pilihan surveyor, produk dan jasa dengan penawaran menarik',
            style: TextStyle(color: Colors.grey, fontSize: 16),
          ),
        ],
      ),
      bodyTextStyle: const TextStyle(fontFamily: 'MyFont', color: Colors.black),
      mainImage: SvgPicture.asset('assets/intro/ob1.svg'),
    ),
    PageViewModel(
      pageColor: const Color(0xFFFFFFFF),
      bubbleBackgroundColor: const Color(0xFF374888),
      mainImage: SvgPicture.asset('assets/intro/OB2.svg'),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            'Mudah dan Cepat',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700),
          ),
          SizedBox(height: 10,),
          const Text(
            'Bantu keseharianmu lebih mudah dan efisian hanya dengan satu sentuhan',
            style: TextStyle(color: Colors.grey, fontSize: 16),
          ),
        ],
      ),
      bodyTextStyle: const TextStyle(fontFamily: 'MyFont', color: Colors.black),
    ),
    PageViewModel(
      pageColor: const Color(0xFFFFFFFF),
      bubbleBackgroundColor: const Color(0xFF374888),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            'Tanpa Perantara',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700),
          ),
          SizedBox(height: 10,),
          const Text(
            'Rasakan proses komunikasi-sepakat tanpa perantara dengan tampilan aplikasi yang user-friendly',
            style: TextStyle(color: Colors.grey, fontSize: 16),
          ),
        ],
      ),
      bodyTextStyle: const TextStyle(fontFamily: 'MyFont', color: Colors.black),
      mainImage: SvgPicture.asset('assets/intro/OB3.svg'),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (context) => IntroViewsFlutter(
          pages,
          showNextButton: true,
          showBackButton: true,
          showSkipButton: true,
          pageButtonsColor: Colors.red,
          nextText: Container(
            height: 40,
            width: 40,
            child: Icon(
              Icons.arrow_forward,
              color: Colors.blueAccent[900],
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 1,
                  blurRadius: 1,
                  offset: Offset(-1, 1), // changes position of shadow
                ),
              ],
            ),
          ),
          backText: Container(
            height: 40,
            width: 40,
            child: Icon(
              Icons.arrow_back,
              color: Colors.blueAccent[900],
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 1,
                  blurRadius: 1,
                  offset: Offset(-1, 1), // changes position of shadow
                ),
              ],
            ),
          ),
          skipText: Container(
            child: Text(
              "Skip",
              style: TextStyle(color: Colors.black),
            ),
          ),
          doneText: Container(
            padding: EdgeInsets.all(7),
            height: 40,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Mulai',
                  style: TextStyle(color: Colors.blueAccent, fontSize: 16),
                ),
                Icon(
                  Icons.arrow_forward,
                  color: Colors.blueAccent,
                ),
              ],
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 1,
                  blurRadius: 1,
                  offset: Offset(-1, 1), // changes position of shadow
                ),
              ],
            ),
          ),
          onTapDoneButton: () {
            Session.doSave("open", '1');
            Navigator.push(
              context,
              MaterialPageRoute(builder: (_) => BtnNavigation()),
            );
          },
          pageButtonTextStyles: const TextStyle(
            color: Colors.white,
            fontSize: 18.0,
          ),
        ),
      ),
    );
  }
}
