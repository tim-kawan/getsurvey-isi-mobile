import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_survey_app/component/ImageUtils.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/component/introduction_animation/introduction_animation_screen.dart';
import 'package:get_survey_app/service/Session.dart';

import 'BottomNavigationBar.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    var smallestDimension = MediaQuery.of(context).size.shortestSide;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Container(
                height: 250,
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: ImageUtils.APP_LOGO,
                ))),
          ),
          RichText(
            text: TextSpan(
              text: 'Get',
              style: TextStyle(
                  fontFamily: "Gotik",
                  fontStyle: FontStyle.italic,
                  fontSize: 50.0,
                  color: primaryColorMain,
                  fontWeight: FontWeight.bold),
              children: <TextSpan>[
                TextSpan(
                    text: 'Survey',
                    style: TextStyle(
                        fontFamily: "Gotik",
                        color: primaryColorMain2,
                        fontWeight: FontWeight.bold,
                        fontSize: 50.0)),
              ],
            ),
          ),
          Padding(
            padding: smallestDimension > 600
                ? const EdgeInsets.only(bottom: 30.0, top: 400.0)
                : const EdgeInsets.only(bottom: 30.0, top: 150.0),
            child: smallestDimension > 600
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("Powered By ",
                          style: TextStyle(
                              color: Colors.grey[700], fontSize: 20.0)),
                      Text("Ikatan Surveyor Indonesia",
                          style: TextStyle(color: Colors.blue, fontSize: 20.0))
                    ],
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("Powered By ",
                          style: TextStyle(
                              color: Colors.grey[700], fontSize: 16.0)),
                      Text("Ikatan Surveyor Indonesia",
                          style: TextStyle(color: Colors.blue, fontSize: 16.0))
                    ],
                  ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 2), () async {
      bool isOpened = false;
      if (await Session.isExist('opened')) {
        isOpened = await Session.getValue('opened');
      }
      isOpened
          ? Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (BuildContext context) => BtnNavigation(
                  baner: 1,
                ),
              ),
            )
          : Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (BuildContext context) =>
                    IntroductionAnimationScreen(),
              ),
            );
    });
  }
}
