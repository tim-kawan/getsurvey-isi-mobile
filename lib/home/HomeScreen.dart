import 'dart:async';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '''
package:flutter_svg/flutter_svg.dart''';
import 'package:get_survey_app/account/DetailSurveyor.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/component/search_field.dart';
import 'package:get_survey_app/core/network/env.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/home/SettingScreen.dart';
import 'package:get_survey_app/job/ListIrkSurveyor/TabbarlistIrk.dart';
import 'package:get_survey_app/job/ListIrkUser/TabbarListIrkUser.dart';
import 'package:get_survey_app/job/ListSurveyorAll.dart';
import 'package:get_survey_app/map/MapsSurveyor.dart';
import 'package:get_survey_app/product/DetailProductScreen.dart';
import 'package:get_survey_app/product/ListAlatukur.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:location/location.dart';
import 'package:new_version/new_version.dart';
import 'package:open_appstore/open_appstore.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shimmer/shimmer.dart';
import 'package:timer_count_down/timer_count_down.dart';
import 'package:url_launcher/url_launcher.dart';

class Dashbord extends StatefulWidget {
  @override
  _DashbordState createState() => _DashbordState();
}

class _DashbordState extends State<Dashbord> {
  List products = [];
  List services = [];
  List surveyors = [];

  bool _isLoading = true;
  String name = "";
  int idUser = 0;
  final RefreshController _refreshController = RefreshController();
  int cekStatusKeanggotaan = -1;
  int statusSurveyor = -1;
  String status = "...";
  int _current = 0;
  bool opened = false;

  List<String> slideShowimage = [];
  dynamic imageItem;

  var urlwa =
      "https://wa.me/+628111071151?text=Hallo Ikatan Surveyor Indonesia. Mohon informasi mengenai GetSurvey";

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: primaryColor4.withOpacity(0.8),
      body: SafeArea(
        child: SmartRefresher(
          controller: _refreshController,
          enablePullDown: true,
          header: WaterDropHeader(
            completeDuration: Duration(milliseconds: 1000),
            waterDropColor: Colors.lightBlueAccent,
            failed: Text(
              "Gagal di Refresh",
              style: TextStyle(color: Colors.white),
            ),
            complete: Text(
              "Berhasil diperbarui",
              style: TextStyle(color: Colors.white),
            ),
          ),
          onRefresh: () {
            _checkVersion();
            _initData();
            _fillListSurveyor();
            _fillListService();
            _fillListProduct();
            _refreshController.refreshCompleted();
          },
          child: ListView(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                width: screenSize.width,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SearchField(),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            // padding: EdgeInsets.all(8),
                            // height: 30,
                            width: 40,
                            decoration: BoxDecoration(
                              color: primaryWhite.withOpacity(0.5),
                              shape: BoxShape.circle,
                            ),
                            child: IconButton(
                                icon: Icon(
                                  Icons.settings,
                                  size: 25,
                                ),
                                color: primaryColor4,
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              SettingScreen()));
                                }),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          CircleAvatar(backgroundImage: imageItem)
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: Container(
                  color: Colors.white,
                  child: Column(
                    children: [
                      (_isLoading)
                          ? Container(
                              height: 120,
                              width: screenSize.width,
                              child: ListView(
                                scrollDirection: Axis.horizontal,
                                children: [
                                  Shimmer.fromColors(
                                    child: Padding(
                                        padding: EdgeInsets.only(
                                            left: 8,
                                            top: 10,
                                            right: 15,
                                            bottom: 15),
                                        child: Container(
                                          height: 100,
                                          width: 200,
                                          decoration: BoxDecoration(
                                              color:
                                                  backGround.withOpacity(0.1),
                                              border: Border.all(
                                                  width: 2,
                                                  color: backGround
                                                      .withOpacity(0.7)),
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                        )),
                                    baseColor: Colors.grey[700],
                                    highlightColor: Colors.grey[200],
                                  ),
                                  Shimmer.fromColors(
                                    child: Padding(
                                        padding: EdgeInsets.only(
                                            left: 8,
                                            top: 10,
                                            right: 15,
                                            bottom: 15),
                                        child: Container(
                                          height: 100,
                                          width: 200,
                                          decoration: BoxDecoration(
                                              color:
                                                  backGround.withOpacity(0.1),
                                              border: Border.all(
                                                  width: 2,
                                                  color: backGround
                                                      .withOpacity(0.7)),
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                        )),
                                    baseColor: Colors.grey[700],
                                    highlightColor: Colors.grey[200],
                                  ),
                                ],
                              ),
                            )
                          : Container(
                              height: screenSize.height / 6,
                              width: screenSize.width,
                              color: Colors.white,
                              child: slideShowimage.isNotEmpty
                                  ? CarouselSlider(
                                      options: CarouselOptions(
                                        enlargeCenterPage: false,
                                        disableCenter: true,
                                        autoPlay: true,
                                        autoPlayInterval: Duration(seconds: 15),
                                        viewportFraction: 0.7,
                                        enableInfiniteScroll: true,
                                      ),
                                      items: slideShowimage
                                          .map(
                                            (e) => Padding(
                                                padding: EdgeInsets.only(
                                                    left: 0,
                                                    top: 10,
                                                    right: 15),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          width: 2,
                                                          color: backGround
                                                              .withOpacity(
                                                                  0.7)),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20)),
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15.0),
                                                    child: Image.network(
                                                      e,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                )),
                                          )
                                          .toList(),
                                    )
                                  : Center(
                                      child: Text(
                                        "Tidak ada Informasi untuk saat ini",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                            ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10, top: 2),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: slideShowimage.map(
                            (image) {
                              int index = slideShowimage.indexOf(image);
                              return Container(
                                width: 8.0,
                                height: 8.0,
                                margin: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 2.0),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: _current == index
                                        ? primaryColor4
                                        : Color.fromRGBO(0, 0, 0, 0.4)),
                              );
                            },
                          ).toList(),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            InkWell(
                              onTap: () async {
                                Location _location = new Location();
                                bool _isOn = await _location.requestService();
                                PermissionStatus _isAllow =
                                    await _location.requestPermission();

                                if (_isOn &&
                                    _isAllow == PermissionStatus.GRANTED) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          MapsSurveyorScreen(),
                                    ),
                                  );
                                }
                              },
                              child: category2("assets/icons/get_surveyor.svg",
                                  "Get-Surveyor", context),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ListAlatUkurScreen(
                                              choseList: 1,
                                            )));
                              },
                              child: category2("assets/icons/get_alatukur.svg",
                                  "Get-Alat ukur", context),
                            ),
                            InkWell(
                              onTap: name == ""
                                  ? null
                                  : () {
                                      _navigationIRk(context);
                                    },
                              child: category2("assets/icons/get-irk.svg",
                                  "Get-Informasi \nRencana Kota", context),
                            ),
                          ],
                        ),
                      ),
                      _buildlistSurveyorMain(context, screenSize),
                      Container(
                        padding: EdgeInsets.fromLTRB(12, 12, 12, 8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            GestureDetector(
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.cyan[50],
                                    // color:backGround.withOpacity(0.1),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  padding: EdgeInsets.all(0),
                                  child: Column(children: <Widget>[
                                    Row(children: <Widget>[
                                      Container(
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(
                                                  left: 5, right: 5),
                                              height: 80.0,
                                              child: SvgPicture.asset(
                                                'assets/icons/cs.svg',
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Container(
                                          color: Colors.transparent,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              ConstrainedBox(
                                                constraints: BoxConstraints(
                                                    maxWidth:
                                                        screenSize.width * 0.7,
                                                    maxHeight: 30),
                                                child: Text(
                                                  'Perlu Bantuan? \nCS Kami Siap Membantu',
                                                  style: TextStyle(
                                                    fontSize: 12,
                                                    fontFamily: "BS",
                                                    letterSpacing: 0.6,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Row(
                                                children: [
                                                  ConstrainedBox(
                                                    constraints: BoxConstraints(
                                                        maxWidth:
                                                            screenSize.width *
                                                                0.7,
                                                        maxHeight: 30),
                                                    child: Image(
                                                      width: 15,
                                                      image: AssetImage(
                                                        'assets/wa.png',
                                                      ),
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                  ConstrainedBox(
                                                    constraints: BoxConstraints(
                                                        maxWidth:
                                                            screenSize.width *
                                                                0.7,
                                                        maxHeight: 20),
                                                    child: Text(
                                                      ' Chat Kami Sekarang',
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          fontSize: 15,
                                                          color: Colors.black,
                                                          fontStyle:
                                                              FontStyle.italic,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ]),
                                  ]),
                                ),
                                onTap: () async {
                                  if (await canLaunch(urlwa)) {
                                    await launch(urlwa);
                                  } else {
                                    throw 'Tidak dapat membuka $urlwa \nSilahkan periksa jaringan';
                                  }
                                }),
                          ],
                        ),
                      ),
                      _buildListProduct(context, screenSize),
                      _buildListServices(context, screenSize),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget category(Image img, String title) {
    return Container(
      padding: EdgeInsets.fromLTRB(12, 8, 12, 8),
      height: 120,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 13,
                  fontWeight: FontWeight.w500),
            ),
          ),
          Align(alignment: Alignment.bottomCenter, child: img)
        ],
      ),
      decoration: BoxDecoration(
          color: Colors.blue[900],
          borderRadius: BorderRadius.circular(10),
          boxShadow: [BoxShadow(blurRadius: 10.0, color: Colors.black12)]),
    );
  }

  Widget category2(String svgIcon, String title, BuildContext context) {
    var smallestDimension = MediaQuery.of(context).size.shortestSide;
    return SizedBox(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.all(12),
            height: smallestDimension < 600 ? 70 : 150,
            width: smallestDimension < 600 ? 70 : 150,
            decoration: BoxDecoration(
              color: backGround.withOpacity(0.1),
              borderRadius: BorderRadius.circular(10),
            ),
            child: SvgPicture.asset(svgIcon),
          ),
          SizedBox(height: 5),
          Text(title, textAlign: TextAlign.center)
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _checkVersion();
    imageItem = AssetImage("assets/logo.png");
    getName();
    _initData();
    _fillListBanner();
    _fillListSurveyor();
    _fillListService();
    _fillListProduct();
  }

  Widget _buildListProduct(
    BuildContext context,
    Size screenSize,
  ) {
    return Padding(
        padding: const EdgeInsets.only(
          top: 15.0,
          left: 8.0,
        ),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Rekomendasi Produk',
                style: TextStyle(
                    color: primaryColor4,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0),
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context)
                      .push(PageRouteBuilder(
                          opaque: false,
                          pageBuilder: (BuildContext context, _, __) =>
                              ListAlatUkurScreen(
                                choseList: 1,
                              )))
                      .then((_) {
                    _initData();
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Text(
                    'Lihat Semua',
                    style: TextStyle(
                        color: primaryBlack,
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5.0,
          ),
          (_isLoading)
              ? Container(
                  child: Row(
                    children: [
                      Shimmer.fromColors(
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            height: screenSize.height / 3,
                            width: screenSize.width / 2.5,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: backGround.withOpacity(0.1),
                                      blurRadius: 0.5,
                                      offset: Offset(0, 1))
                                ]),
                            child: Stack(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      height: 140.0,
                                      width: 150,
                                      decoration: BoxDecoration(
                                        color: backGround.withOpacity(0.3),
                                        borderRadius: BorderRadius.only(
                                            topLeft:
                                                const Radius.circular(16.0),
                                            topRight:
                                                const Radius.circular(16.0)),
                                      ),
                                    ),
                                    Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: Container(
                                          height: 10,
                                          width: 50,
                                          color: backGround.withOpacity(0.3),
                                        )),
                                  ],
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    width: 60,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(16),
                                        bottomLeft: Radius.circular(16),
                                      ),
                                      color: backGround.withOpacity(0.2),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          Container(
                                            height: 8,
                                            width: 8,
                                          ),
                                          Icon(
                                            Icons.star,
                                            color: backGround.withOpacity(0.3),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        baseColor: Colors.grey[700],
                        highlightColor: Colors.grey[200],
                      ),
                      Shimmer.fromColors(
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            height: screenSize.height / 3,
                            width: screenSize.width / 2.3,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: backGround.withOpacity(0.1),
                                      blurRadius: 0.5,
                                      offset: Offset(0, 1))
                                ]),
                            child: Stack(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      height: 140.0,
                                      width: 160,
                                      decoration: BoxDecoration(
                                        color: backGround.withOpacity(0.3),
                                        borderRadius: BorderRadius.only(
                                            topLeft:
                                                const Radius.circular(16.0),
                                            topRight:
                                                const Radius.circular(16.0)),
                                      ),
                                    ),
                                    Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: Container(
                                          height: 10,
                                          width: 50,
                                          color: backGround.withOpacity(0.3),
                                        )),
                                  ],
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    width: 60,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(16),
                                        bottomLeft: Radius.circular(16),
                                      ),
                                      color: backGround.withOpacity(0.2),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          Container(
                                            height: 8,
                                            width: 8,
                                          ),
                                          Icon(
                                            Icons.star,
                                            color: backGround.withOpacity(0.3),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        baseColor: Colors.grey[700],
                        highlightColor: Colors.grey[200],
                      ),
                    ],
                  ),
                )
              : Container(
                  width: screenSize.width,
                  height: 200.0,
                  child: products.isNotEmpty
                      ? ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: products.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: InkWell(
                                onTap: () {
                                  Navigator.of(context)
                                      .push(PageRouteBuilder(
                                          opaque: false,
                                          pageBuilder: (BuildContext context, _,
                                                  __) =>
                                              DetailProductScreen(
                                                id: products[index]["record"]
                                                    ["produk"]["id"],
                                                rate: products[index]["rate"],
                                                tipe: "item",
                                              )))
                                      .then((_) {
                                    _initData();
                                  });
                                },
                                child: Container(
                                  height: screenSize.height / 2.5,
                                  width: screenSize.width / 2.5,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16.0),
                                      boxShadow: [
                                        BoxShadow(
                                            color: backGround.withOpacity(0.1),
                                            blurRadius: 0.5,
                                            offset: Offset(0, 1))
                                      ]),
                                  child: Stack(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            height: 140.0,
                                            width: screenSize.width,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.only(
                                                  topLeft:
                                                      const Radius.circular(
                                                          16.0),
                                                  topRight:
                                                      const Radius.circular(
                                                          16.0)),
                                              image: DecorationImage(
                                                image: products[index]["record"]
                                                            ["images"]
                                                        .isEmpty
                                                    ? AssetImage(
                                                        'assets/logo.png')
                                                    : NetworkImage(
                                                        products[index]
                                                                ["record"]
                                                            ["images"][0]),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(5.0),
                                            child: Text(
                                              products[index]["record"]
                                                  ["produk"]["nama"],
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Align(
                                        alignment: Alignment.topRight,
                                        child: Container(
                                          width: 60,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(16),
                                              bottomLeft: Radius.circular(16),
                                            ),
                                            color: backGround.withOpacity(0.2),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.all(5.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: <Widget>[
                                                Text(
                                                  products[index]["rate"],
                                                  style: TextStyle(
                                                      color: primaryWhite,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Icon(
                                                  Icons.star,
                                                  color: primaryColor4,
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          })
                      : Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            height: screenSize.height / 6,
                            width: screenSize.width / 1.5,
                            child: Center(
                              child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                  text:
                                      'Barang yang di tampilkan hanya yang memiliki rating, silahkan ',
                                  style: TextStyle(
                                      fontFamily: "Gotik",
                                      color: primaryColor,
                                      fontWeight: FontWeight.bold),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: ' Lihat Semua',
                                        style: TextStyle(
                                          fontFamily: "Gotik",
                                          fontWeight: FontWeight.bold,
                                          color: primaryColor4,
                                        )),
                                    TextSpan(
                                        text: ' barang untuk lihat list barang',
                                        style: TextStyle(
                                          fontFamily: "Gotik",
                                          fontWeight: FontWeight.bold,
                                          color: primaryColor4,
                                        ))
                                  ],
                                ),
                              ),
                            ),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5.0),
                                border:
                                    Border.all(width: 1.0, color: Colors.grey)),
                          ),
                        ),
                )
        ]));
  }

  Widget _buildListServices(
    BuildContext context,
    Size screenSize,
  ) {
    return Padding(
        padding: const EdgeInsets.only(
          top: 15.0,
          left: 8.0,
        ),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Rekomendasi Jasa',
                style: TextStyle(
                    color: primaryColor4,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0),
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context)
                      .push(PageRouteBuilder(
                          opaque: false,
                          pageBuilder: (BuildContext context, _, __) =>
                              ListAlatUkurScreen(
                                choseList: 2,
                              )))
                      .then((_) {
                    _initData();
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Text(
                    'Lihat Semua',
                    style: TextStyle(
                        color: primaryBlack,
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5.0,
          ),
          (_isLoading)
              ? Container(
                  child: Row(
                    children: [
                      Shimmer.fromColors(
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            height: screenSize.height / 3,
                            width: screenSize.width / 2.5,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: backGround.withOpacity(0.1),
                                      blurRadius: 0.5,
                                      offset: Offset(0, 1))
                                ]),
                            child: Stack(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      height: 140.0,
                                      width: 150,
                                      decoration: BoxDecoration(
                                        color: backGround.withOpacity(0.3),
                                        borderRadius: BorderRadius.only(
                                            topLeft:
                                                const Radius.circular(16.0),
                                            topRight:
                                                const Radius.circular(16.0)),
                                      ),
                                    ),
                                    Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: Container(
                                          height: 10,
                                          width: 50,
                                          color: backGround.withOpacity(0.3),
                                        )),
                                  ],
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    width: 60,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(16),
                                        bottomLeft: Radius.circular(16),
                                      ),
                                      color: backGround.withOpacity(0.2),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          Container(
                                            height: 8,
                                            width: 8,
                                          ),
                                          Icon(
                                            Icons.star,
                                            color: backGround.withOpacity(0.3),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        baseColor: Colors.grey[700],
                        highlightColor: Colors.grey[200],
                      ),
                      Shimmer.fromColors(
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            height: screenSize.height / 3,
                            width: screenSize.width / 2.3,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: backGround.withOpacity(0.1),
                                      blurRadius: 0.5,
                                      offset: Offset(0, 1))
                                ]),
                            child: Stack(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      height: 140.0,
                                      width: 160,
                                      decoration: BoxDecoration(
                                        color: backGround.withOpacity(0.3),
                                        borderRadius: BorderRadius.only(
                                            topLeft:
                                                const Radius.circular(16.0),
                                            topRight:
                                                const Radius.circular(16.0)),
                                      ),
                                    ),
                                    Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: Container(
                                          height: 10,
                                          width: 50,
                                          color: backGround.withOpacity(0.3),
                                        )),
                                  ],
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    width: 60,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(16),
                                        bottomLeft: Radius.circular(16),
                                      ),
                                      color: backGround.withOpacity(0.2),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: <Widget>[
                                          Container(
                                            height: 8,
                                            width: 8,
                                          ),
                                          Icon(
                                            Icons.star,
                                            color: backGround.withOpacity(0.3),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        baseColor: Colors.grey[700],
                        highlightColor: Colors.grey[200],
                      ),
                    ],
                  ),
                )
              : Container(
                  width: screenSize.width,
                  height: 200.0,
                  child: products.isNotEmpty
                      ? ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: products.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: InkWell(
                                onTap: () {
                                  Navigator.of(context)
                                      .push(PageRouteBuilder(
                                          opaque: false,
                                          pageBuilder: (BuildContext context, _,
                                                  __) =>
                                              DetailProductScreen(
                                                id: services[index]["record"]
                                                    ["jasa"]["id"],
                                                rate: services[index]["rate"],
                                                tipe: "work",
                                              )))
                                      .then((_) {
                                    _initData();
                                  });
                                },
                                child: Container(
                                  height: screenSize.height / 2.5,
                                  width: screenSize.width / 2.5,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16.0),
                                      boxShadow: [
                                        BoxShadow(
                                            color: backGround.withOpacity(0.1),
                                            blurRadius: 0.5,
                                            offset: Offset(0, 1))
                                      ]),
                                  child: Stack(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            height: 140.0,
                                            width: screenSize.width,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.only(
                                                  topLeft:
                                                      const Radius.circular(
                                                          16.0),
                                                  topRight:
                                                      const Radius.circular(
                                                          16.0)),
                                              image: DecorationImage(
                                                image: services[index]["record"]
                                                            ["images"]
                                                        .isEmpty
                                                    ? AssetImage(
                                                        'assets/logo.png')
                                                    : NetworkImage(
                                                        services[index]
                                                                ["record"]
                                                            ["images"][0]),
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(5.0),
                                            child: Text(
                                              services[index]["record"]["jasa"]
                                                  ["nama"],
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16.0,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Align(
                                        alignment: Alignment.topRight,
                                        child: Container(
                                          width: 60,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(16),
                                              bottomLeft: Radius.circular(16),
                                            ),
                                            color:
                                                primaryWhite.withOpacity(0.8),
                                            // color: backGround.withOpacity(0.2),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.all(5.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: <Widget>[
                                                Text(
                                                  services[index]["rate"],
                                                  style: TextStyle(
                                                      color: primaryBlack,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Icon(
                                                  Icons.star,
                                                  color: primaryColor4,
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          })
                      : Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Container(
                            height: screenSize.height / 6,
                            width: screenSize.width / 1.5,
                            child: Center(
                              child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                  text:
                                      'Barang yang di tampilkan hanya yang memiliki rating, silahkan ',
                                  style: TextStyle(
                                      fontFamily: "Gotik",
                                      color: primaryColor,
                                      fontWeight: FontWeight.bold),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: ' Lihat Semua',
                                        style: TextStyle(
                                          fontFamily: "Gotik",
                                          fontWeight: FontWeight.bold,
                                          color: primaryColor4,
                                        )),
                                    TextSpan(
                                        text: ' barang untuk lihat list barang',
                                        style: TextStyle(
                                          fontFamily: "Gotik",
                                          fontWeight: FontWeight.bold,
                                          color: primaryColor4,
                                        ))
                                  ],
                                ),
                              ),
                            ),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5.0),
                                border:
                                    Border.all(width: 1.0, color: Colors.grey)),
                          ),
                        ),
                )
        ]));
  }

  Widget _buildlistSurveyorMain(BuildContext context, Size screenSize) {
    var smallestDimension = MediaQuery.of(context).size.shortestSide;
    return Padding(
      padding: const EdgeInsets.only(top: 15, left: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Rekomendasi Surveyor',
                  style: TextStyle(
                      color: primaryColor4,
                      fontWeight: FontWeight.bold,
                      fontSize: 14.0),
                ),
                InkWell(
                  onTap: () {
                    Navigator.of(context)
                        .push(PageRouteBuilder(
                            opaque: false,
                            pageBuilder: (BuildContext context, _, __) =>
                                ListSurveyorAll()))
                        .then((_) {
                      _initData();
                    });
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text(
                      'Lihat Semua',
                      style: TextStyle(
                          color: primaryBlack,
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 190,
            width: screenSize.width,
            child: (_isLoading)
                ? Row(
                    children: [
                      Shimmer.fromColors(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 50,
                              ),
                              SizedBox(
                                // width: smallestDimension > 600? screenSize.width / 4.5 :screenSize.width / 2.5,
                                width:
                                    screenSize.width / screenSize.width * 150,
                                child: Stack(
                                  alignment: Alignment.center,
                                  overflow: Overflow.visible,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(16),
                                        color: backGround.withOpacity(0.1),
                                      ),
                                      alignment: Alignment.bottomCenter,
                                      width: 160,
                                      height: 120,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 8.0),
                                                  child: Container(
                                                    height: 10,
                                                    width: 70,
                                                    color: backGround
                                                        .withOpacity(0.3),
                                                  )),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 8.0,
                                                    right: 8,
                                                    bottom: 8),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Expanded(
                                                      flex: 4,
                                                      child: Row(
                                                        children: [
                                                          Icon(
                                                            Icons.location_on,
                                                            color: backGround
                                                                .withOpacity(
                                                                    0.3),
                                                            size: 12,
                                                          ),
                                                          Flexible(
                                                              child: Container(
                                                            height: 10,
                                                            width: 50,
                                                            color: backGround
                                                                .withOpacity(
                                                                    0.3),
                                                          )),
                                                        ],
                                                      ),
                                                    ),
                                                    Expanded(
                                                      flex: 2,
                                                      child: Row(
                                                        children: [
                                                          Container(
                                                            height: 8,
                                                            width: 8,
                                                            color: backGround
                                                                .withOpacity(
                                                                    0.3),
                                                          ),
                                                          Icon(
                                                            Icons.star,
                                                            color: backGround
                                                                .withOpacity(
                                                                    0.3),
                                                            size: 20,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ), //Row
                                    ),
                                    Positioned(
                                      // left: 10,
                                      // right: 10,
                                      top: -50,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: backGround.withOpacity(0.3),
                                          borderRadius:
                                              BorderRadius.circular(16),
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.black54,
                                              // blurRadius: 1.0,
                                              // spreadRadius: 1.0,
                                            ),
                                          ],
                                        ),
                                        alignment: Alignment.bottomCenter,
                                        width: 120,
                                        height: 120,
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color:
                                                  backGround.withOpacity(0.3),
                                              borderRadius: BorderRadius.only(
                                                bottomRight:
                                                    Radius.circular(16),
                                                bottomLeft: Radius.circular(16),
                                              )),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Image.asset(
                                                "assets/medali/kompetensi.png",
                                                color: Colors.white,
                                                height: 20,
                                              ),
                                              SizedBox(
                                                width: 4.0,
                                              ),
                                              Image.asset(
                                                "assets/medali/kadastral.png",
                                                color: Colors.white,
                                                height: 20,
                                              ),
                                              SizedBox(
                                                width: 4.0,
                                              ),
                                              Image.asset(
                                                "assets/medali/ska.png",
                                                color: Colors.white,
                                                height: 20,
                                              ),
                                              SizedBox(
                                                width: 4.0,
                                              ),
                                              Image.asset(
                                                "assets/medali/irk.png",
                                                color: Colors.white,
                                                height: 20,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        baseColor: Colors.grey[700],
                        highlightColor: Colors.grey[200],
                      ),
                      Shimmer.fromColors(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 50,
                              ),
                              SizedBox(
                                // width: smallestDimension > 600? screenSize.width / 4.5 :screenSize.width / 2.5,
                                width:
                                    screenSize.width / screenSize.width * 150,
                                child: Stack(
                                  alignment: Alignment.center,
                                  overflow: Overflow.visible,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(16),
                                        color: backGround.withOpacity(0.1),
                                      ),
                                      alignment: Alignment.bottomCenter,
                                      width: 160,
                                      height: 120,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 8.0),
                                                  child: Container(
                                                    height: 10,
                                                    width: 70,
                                                    color: backGround
                                                        .withOpacity(0.3),
                                                  )),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 8.0,
                                                    right: 8,
                                                    bottom: 8),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Expanded(
                                                      flex: 4,
                                                      child: Row(
                                                        children: [
                                                          Icon(
                                                            Icons.location_on,
                                                            color: backGround
                                                                .withOpacity(
                                                                    0.3),
                                                            size: 12,
                                                          ),
                                                          Flexible(
                                                              child: Container(
                                                            height: 10,
                                                            width: 50,
                                                            color: backGround
                                                                .withOpacity(
                                                                    0.3),
                                                          )),
                                                        ],
                                                      ),
                                                    ),
                                                    Expanded(
                                                      flex: 2,
                                                      child: Row(
                                                        children: [
                                                          Container(
                                                            height: 8,
                                                            width: 8,
                                                            color: backGround
                                                                .withOpacity(
                                                                    0.3),
                                                          ),
                                                          Icon(
                                                            Icons.star,
                                                            color: backGround
                                                                .withOpacity(
                                                                    0.3),
                                                            size: 20,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ), //Row
                                    ),
                                    Positioned(
                                      // left: 10,
                                      // right: 10,
                                      top: -50,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: backGround.withOpacity(0.3),
                                          borderRadius:
                                              BorderRadius.circular(16),
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.black54,
                                              // blurRadius: 1.0,
                                              // spreadRadius: 1.0,
                                            ),
                                          ],
                                        ),
                                        alignment: Alignment.bottomCenter,
                                        width: 120,
                                        height: 120,
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color:
                                                  backGround.withOpacity(0.3),
                                              borderRadius: BorderRadius.only(
                                                bottomRight:
                                                    Radius.circular(16),
                                                bottomLeft: Radius.circular(16),
                                              )),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Image.asset(
                                                "assets/medali/kompetensi.png",
                                                color: Colors.white,
                                                height: 20,
                                              ),
                                              SizedBox(
                                                width: 4.0,
                                              ),
                                              Image.asset(
                                                "assets/medali/kadastral.png",
                                                color: Colors.white,
                                                height: 20,
                                              ),
                                              SizedBox(
                                                width: 4.0,
                                              ),
                                              Image.asset(
                                                "assets/medali/ska.png",
                                                color: Colors.white,
                                                height: 20,
                                              ),
                                              SizedBox(
                                                width: 4.0,
                                              ),
                                              Image.asset(
                                                "assets/medali/irk.png",
                                                color: Colors.white,
                                                height: 20,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        baseColor: Colors.grey[700],
                        highlightColor: Colors.grey[200],
                      ),
                    ],
                  )
                : products.isNotEmpty
                    ? ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: surveyors.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 50,
                                ),
                                InkWell(
                                  child: SizedBox(
                                    // width: smallestDimension > 600? screenSize.width / 4.5 :screenSize.width / 2.5,
                                    width: screenSize.width /
                                        screenSize.width *
                                        150,
                                    child: Stack(
                                      alignment: Alignment.center,
                                      overflow: Overflow.visible,
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(16),
                                            color: backGround.withOpacity(0.1),
                                          ),
                                          alignment: Alignment.bottomCenter,
                                          width: 160,
                                          height: 120,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 8.0),
                                                    child: Text(
                                                      surveyors[index]["record"]
                                                          ["nama"],
                                                      maxLines: 1,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                          color: primaryBlack,
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.w500),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 8.0,
                                                            right: 8,
                                                            bottom: 8),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Expanded(
                                                          flex: 4,
                                                          child: Row(
                                                            children: [
                                                              Icon(
                                                                Icons
                                                                    .location_on,
                                                                color:
                                                                    primaryColor4,
                                                                size: 12,
                                                              ),
                                                              Flexible(
                                                                child: Text(
                                                                  surveyors[index]
                                                                          [
                                                                          "record"]
                                                                      [
                                                                      "kotaTempatTinggal"],
                                                                  style:
                                                                      TextStyle(
                                                                    color:
                                                                        primaryBlack,
                                                                    fontSize:
                                                                        12,
                                                                  ),
                                                                  maxLines: 1,
                                                                  overflow:
                                                                      TextOverflow
                                                                          .clip,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Expanded(
                                                          flex: 2,
                                                          child: Row(
                                                            children: [
                                                              Text(
                                                                surveyors[index]
                                                                    ["rate"],
                                                                style:
                                                                    TextStyle(
                                                                  color:
                                                                      primaryBlack,
                                                                  fontSize: 14,
                                                                ),
                                                              ),
                                                              Icon(
                                                                Icons.star,
                                                                color:
                                                                    primaryColor4,
                                                                size: 20,
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ), //Row
                                        ),
                                        Positioned(
                                          // left: 10,
                                          // right: 10,
                                          top: -50,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                image: surveyors[index]
                                                                ["record"]
                                                            ["fotoUser"] ==
                                                        ""
                                                    ? AssetImage(
                                                        'assets/GetSurvey.png')
                                                    : NetworkImage(
                                                        surveyors[index]
                                                                ["record"]
                                                            ["fotoUser"]),
                                                fit: BoxFit.cover,
                                              ),
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(16),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.black54,
                                                  // blurRadius: 1.0,
                                                  // spreadRadius: 1.0,
                                                ),
                                              ],
                                            ),
                                            alignment: Alignment.bottomCenter,
                                            width: 120,
                                            height: 120,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: primaryColor4
                                                      .withOpacity(0.6),
                                                  borderRadius:
                                                      BorderRadius.only(
                                                    bottomRight:
                                                        Radius.circular(16),
                                                    bottomLeft:
                                                        Radius.circular(16),
                                                  )),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  surveyors[index]["record"]
                                                              ["noLisensi"] ==
                                                          ""
                                                      ? SizedBox()
                                                      : Image.asset(
                                                          "assets/medali/kompetensi.png",
                                                          height: 20,
                                                        ),
                                                  SizedBox(
                                                    width: 4.0,
                                                  ),
                                                  surveyors[index]["record"][
                                                              "noLisensiKadastral"] ==
                                                          ""
                                                      ? SizedBox()
                                                      : Image.asset(
                                                          "assets/medali/kadastral.png",
                                                          height: 20,
                                                        ),
                                                  SizedBox(
                                                    width: 4.0,
                                                  ),
                                                  surveyors[index]["record"]
                                                              ["noSkaSkt"] ==
                                                          ""
                                                      ? SizedBox()
                                                      : Image.asset(
                                                          "assets/medali/ska.png",
                                                          height: 20,
                                                        ),
                                                  SizedBox(
                                                    width: 4.0,
                                                  ),
                                                  surveyors[index]["record"][
                                                              "noLisensiIrk"] ==
                                                          ""
                                                      ? SizedBox()
                                                      : Image.asset(
                                                          "assets/medali/irk.png",
                                                          height: 20,
                                                        ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: () {
                                    Navigator.of(context)
                                        .push(PageRouteBuilder(
                                            opaque: false,
                                            pageBuilder: (BuildContext context,
                                                    _, __) =>
                                                DetailSurveyor(
                                                  id: surveyors[index]["record"]
                                                      ["idSurveyor"],
                                                  name: surveyors[index]
                                                      ["record"]["nama"],
                                                  img: surveyors[index]
                                                      ["record"]["fotoUser"],
                                                  long: surveyors[index]
                                                      ["record"]["long"],
                                                  lat: surveyors[index]
                                                      ["record"]["lat"],
                                                  alamat: surveyors[index]
                                                          ["record"]
                                                      ["alamatTempatTinggal"],
                                                  email: surveyors[index]
                                                      ["record"]["email"],
                                                  noAnggota: surveyors[index]
                                                      ["record"]["noAnggota"],
                                                  rate: surveyors[index]
                                                      ["rate"],
                                                  medal: surveyors[index]
                                                      ['medal'],
                                                  dateJoin: surveyors[index]
                                                          ["record"]
                                                      ["tanggalAwalBerlaku"],
                                                )))
                                        .then((_) {
                                      _initData();
                                    });
                                  },
                                ),
                              ],
                            ),
                          );
                        },
                      )
                    : Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Container(
                          height: screenSize.height / 6,
                          width: screenSize.width / 1.5,
                          child: Center(
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                text:
                                    'Surveyor yang di tampilkan hanya yang memiliki rating, silahkan ',
                                style: TextStyle(
                                    fontFamily: "Gotik",
                                    color: primaryColor,
                                    fontWeight: FontWeight.bold),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: ' Lihat Semua',
                                      style: TextStyle(
                                        fontFamily: "Gotik",
                                        fontWeight: FontWeight.bold,
                                        color: primaryColor4,
                                      )),
                                  TextSpan(
                                      text:
                                          ' surveyor untuk lihat list surveyor',
                                      style: TextStyle(
                                        fontFamily: "Gotik",
                                        fontWeight: FontWeight.bold,
                                        color: primaryColor4,
                                      ))
                                ],
                              ),
                            ),
                          ),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5.0),
                              border:
                                  Border.all(width: 1.0, color: Colors.grey)),
                        ),
                      ),
          ),
        ],
      ),
    );
  }

  void _checkVersion() async {
    if (Constants.ENV == Environment.PROD) {
      var newVersion =
          NewVersion(androidId: "id.or.isi.getsurvey", context: context);
      final status = await newVersion.getVersionStatus();

      if (status.localVersion != status.storeVersion) {
        Alert(
            context: context,
            image: Image.asset(
              "assets/alert/warning.png",
              color: Colors.green,
            ),
            type: AlertType.info,
            title: "Update Version",
            desc:
                "Pembaruan GetSurvey dari versi ${status.localVersion} ke Versi ${status.storeVersion}, mohon menunggu selama 3 detik",
            content: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Row(
                  children: [
                    Text(
                      "Navigasi Otomatis : ",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Countdown(
                      seconds: 3,
                      build: (BuildContext context, double time) => Text(
                        time.toString(),
                        style: TextStyle(color: Colors.red),
                      ),
                      interval: Duration(milliseconds: 100),
                      onFinished: () {},
                    ),
                  ],
                ),
              ),
            ),
            closeIcon: Container(),
            onWillPopActive: true,
            buttons: []).show();
        Future.delayed(
          Duration(seconds: 5),
          () {
            SystemNavigator.pop();
            OpenAppstore.launch(androidAppId: "id.or.isi.getsurvey");
          },
        );
      }
    }
  }

  void getName() async {
    String names = await Session.getValue(Session.USER_NAME);
    int type = await Session.getValue(Session.USER_STATUS);

    if (names == null) {
      name = "Pengunjung";
    } else {
      name = names;
    }

    if (type == null) {
      status = "Pengunjung";
    } else if (type > 0) {
      if (statusSurveyor == 1 || statusSurveyor == 2 || statusSurveyor == 3) {
        status = "Surveyor";
      } else {
        status = "User";
      }
    } else {
      status = "User";
    }
    setState(() {});
  }

  void _fillListProduct() async {
    products.clear();
    List produk = await RestAPI.seeProdServRate(tipe: "produk");
    products.addAll(produk);
    setState(() {});
    setState(() {
      _isLoading = false;
    });
  }

  void _fillListBanner() async {
    List banner = await RestAPI.getBanner();
    slideShowimage.clear();
    banner.forEach((i) {
      if (i['exist']) {
        slideShowimage.add(i['url']);
      }
    });
    setState(() {});
  }

  void _fillListService() async {
    services.clear();
    List jasa = await RestAPI.seeProdServRate(tipe: "jasa");
    services.addAll(jasa);
    setState(() {});
  }

  void _fillListSurveyor() async {
    surveyors.clear();
    List temp = await RestAPI.fetchTopSurveyors(limit: 8);
    surveyors.addAll(temp);
    setState(() {});
  }

  void _initData() async {
    statusSurveyor = await Session.getValue(Session.USER_LANJUT);
    idUser = await Session.getValue(Session.USER_ID);

    String imgAva = await Session.getValue(Session.USER_PHOTO);

    imageItem = idUser == null ? imageItem : NetworkImage(imgAva);
    setState(() {});
  }

  _navigationIRk(BuildContext context) {
    if (status == "Pengunjung") {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => BtnNavigation(
                    atIndex: 2,
                  )));
    } else if (status == "Surveyor") {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => TabbarIRKScreen()));
    } else if (status == "User") {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => TabbarIRKUserScreen()));
    }
  }
}
