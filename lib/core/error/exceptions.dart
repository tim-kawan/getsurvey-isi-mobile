class ServerException implements Exception {
  final message;
  final errorCode;

  ServerException(this.message, this.errorCode);
}

class AuthorizationException extends ServerException {
  AuthorizationException(message, errorCode) : super(message, errorCode);
}

class CacheException implements Exception {}
