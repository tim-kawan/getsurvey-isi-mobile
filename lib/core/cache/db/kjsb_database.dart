import 'package:get_survey_app/features/kjsb/data/data_sources/cache/db/kjsb_provider.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider {
  DatabaseProvider._();

  static final DatabaseProvider db = DatabaseProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }

    _database = await createDatabase();

    return _database;
  }

  Future<Database> createDatabase() async {
    String dbPath = await getDatabasesPath();

    return await openDatabase(join(dbPath, 'getsurvey.db'), version: 1,
        onCreate: (Database database, int version) async {
      await KjsbProvider.createTableKJSB(database);
      await KjsbProvider.createTableTagging(database);
    }, onUpgrade: (Database database, oldVersion, newVersion) async {});
  }
}
