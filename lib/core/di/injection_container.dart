import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:get_survey_app/core/network/network_info.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/di/injection_container.dart';
import 'package:get_survey_app/features/kjsb/di/injection_container.dart';
import 'package:get_survey_app/features/payment/di/injection_container.dart';
import 'package:http/http.dart' as http;

final di = GetIt.instance;

Future<void> init() async {
  initKjsb(di);
  initUpgradeSurveyor(di);
  initKjsbPayment(di);
  //! Core
  di.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(di()));
  //! External
  di.registerLazySingleton(() => http.Client());
  di.registerLazySingleton(() => DataConnectionChecker());
}
