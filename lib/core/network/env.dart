enum Environment { DEV, STAGING, PROD }

class Constants {
  static Map<String, dynamic> _config;

  static void setEnvironment(Environment env) {
    switch (env) {
      case Environment.DEV:
        _config = _Config.debugConstants;
        break;
      case Environment.STAGING:
        _config = _Config.stagingConstants;
        break;
      case Environment.PROD:
        _config = _Config.prodConstants;
        break;
    }
  }

  static get SERVER {
    return _config[_Config.SERVER];
  }

  static get ENV {
    return _config[_Config.ENV];
  }
}

class _Config {
  static const SERVER = "SERVER";
  static const ENV = "ENV";

  static Map<String, dynamic> debugConstants = {
    SERVER: "https://dev.getsurvey.id/",
    ENV: Environment.DEV,
  };

  static Map<String, dynamic> stagingConstants = {
    SERVER: "https://staging.getsurvey.id/",
    ENV: Environment.STAGING,
  };

  static Map<String, dynamic> prodConstants = {
    SERVER: "https://getsurvey.id/",
    ENV: Environment.PROD,
  };
}
