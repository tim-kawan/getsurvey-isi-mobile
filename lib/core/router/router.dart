import 'package:flutter/material.dart';
import 'package:get_survey_app/core/router/routes_path.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/presentation/pages/SurveyorTool.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/presentation/pages/UpgradeToSurveyorNew.dart';
import 'package:get_survey_app/features/kjsb/presentation/routes/router.dart';
import 'package:get_survey_app/home/HomeScreen.dart';
import 'package:get_survey_app/home/SplashScreen.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case RoutesPath.RootRoute:
      return MaterialPageRoute(builder: (context) => Dashbord());
    case RoutesPath.SplashRoute:
      return MaterialPageRoute(builder: (context) => SplashScreen());
    case RoutesUpgradeSurveyor.UpgradeSurveyorRoute:
      return MaterialPageRoute(builder: (context) => UpgradeToSurveyorNew());
    case RoutesUpgradeSurveyor.UpgradeSurveyorToolRoute:
      return MaterialPageRoute(builder: (context)=>SurveyorToolScreen());
  }

  return generateKjsbRoute(settings);
}

