class RoutesPath {
  static const String RootRoute = '/';
  static const String SplashRoute = 'splash';
}

class RoutesUpgradeSurveyor{
  static const String UpgradeSurveyorRoute = 'upgrade_surveyor';
  static const String UpgradeSurveyorToolRoute = 'surveyor_tool';
}
