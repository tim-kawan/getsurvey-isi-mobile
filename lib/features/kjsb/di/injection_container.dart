import 'package:get_it/get_it.dart';
import 'package:get_survey_app/core/cache/db/kjsb_database.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/cache/base/kjsb_cache.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/cache/base/tagging_cache.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/cache/kjsb_cache.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/cache/tagging_cache.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/base/kjsb_remote.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/base/tagging_remote.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/kjsb_remote.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/tagging_remote.dart';
import 'package:get_survey_app/features/kjsb/data/repositories/kjsb_repostitory.dart';
import 'package:get_survey_app/features/kjsb/data/repositories/tagging_repostitory.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/tagging_repository.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/approve_kjsb_schedule.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/approve_kjsb_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_document.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_schedule_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_tagging.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_all_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_all_kjsb_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_documents.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_kjsb_by_id.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_kjsb_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_locations.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_services.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/load_tagging.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/select_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/update_status.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/update_tagging.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/upload_tagging.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/verivied_kjsb_surveyor.dart';
import 'package:get_survey_app/features/kjsb/presentation/provider/get_kjsb_cubit.dart';
import 'package:get_survey_app/features/kjsb/presentation/provider/kjsb_input_cubit.dart';
import 'package:get_survey_app/features/kjsb/presentation/provider/kjsb_surveyor_cubit.dart';
import 'package:get_survey_app/features/kjsb/presentation/provider/tagging_cubit.dart';
import 'package:sqflite/sqflite.dart';

Future<void> initKjsb(GetIt di) async {
  // Bloc
  di.registerFactory(() => KjsbInputCubit(
      createKjsb: di(),
      getLocations: di(),
      getKjsb: di(),
      getDocunments: di(),
      updateStatus: di(),
      createDocument: di(),
      getServices: di()));
  di.registerFactory(() => GetKjsbCubit(
      loadTagging: di(),
      getAllKjsb: di(),
      approveKjsbSchedule: di(),
      getKjsbId: di()));
  di.registerFactory(() => TaggingCubit(
      createTagging: di(),
      loadTagging: di(),
      updateStatus: di(),
      updateTagging: di(),
      uploadTagging: di()));
  di.registerFactory(() => KjsbSurveyorCubit(
      getAllKjsbSurveyor: di(),
      selectSurveyor: di(),
      createScheduleKJSB: di(),
      getKjsbSurveyor: di(),
      verifiedKJSBSurveyor: di(),
      approveKjsbSurveyor: di()));

  // Use cases
  di.registerLazySingleton(() => CreateKJSB(kjsbRepository: di()));
  di.registerLazySingleton(() => GetAllKJSB(kjsbRepository: di()));
  di.registerLazySingleton(() => GetKJSBById(kjsbRepository: di()));
  di.registerLazySingleton(() => CreateTagging(taggingRepository: di()));
  di.registerLazySingleton(() => LoadTagging(taggingRepository: di()));
  di.registerLazySingleton(() => UpdateTagging(taggingRepository: di()));
  di.registerLazySingleton(() => GetAllKJSBSurveyor(kjsbRepository: di()));
  di.registerLazySingleton(() => SelectSurveyor(kjsbRepository: di()));
  di.registerLazySingleton(() => UploadTagging(taggingRepository: di()));
  di.registerLazySingleton(() => GetKJSBSurveyor(kjsbRepository: di()));
  di.registerLazySingleton(() => ApproveKJSBSurveyor(kjsbRepository: di()));
  di.registerLazySingleton(() => VerifiedKJSBSurveyor(kjsbRepository: di()));
  di.registerLazySingleton(() => CreateScheduleKJSB(kjsbRepository: di()));
  di.registerLazySingleton(() => ApproveKJSBSchedule(kjsbRepository: di()));
  di.registerLazySingleton(() => GetServices(kjsbRepository: di()));
  di.registerLazySingleton(() => GetLocations(kjsbRepository: di()));
  di.registerLazySingleton(() => CreateDocument(kjsbRepository: di()));
  di.registerLazySingleton(() => UpdateStatus(kjsbRepository: di()));
  di.registerLazySingleton(() => GetDocuments(kjsbRepository: di()));

  di.registerLazySingleton<IKjsbRepository>(
    () => KjsbRepository(networkInfo: di(), kjsbCache: di(), kjsbRemote: di()),
  );

  di.registerLazySingleton<ITaggingRepository>(
    () => TaggingRepository(
        networkInfo: di(), taggingCache: di(), taggingRemote: di()),
  );

  // Data sources
  /// remote
  di.registerLazySingleton<IKjsbRemote>(() => KjsbRemote(di()));
  di.registerLazySingleton<ITaggingRemote>(() => TaggingRemote(di()));

  /// cache
  di.registerLazySingleton<IKjsbCache>(() => KjsbCache(database: di()));
  di.registerLazySingleton<ITaggingCache>(() => TaggingCache(database: di()));
  di.registerLazySingleton<Future<Database>>(
      () => DatabaseProvider.db.database);
}
