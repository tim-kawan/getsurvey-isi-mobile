import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/network/network_info.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/cache/base/kjsb_cache.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/base/kjsb_remote.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/detail_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_document_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_service_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/list_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/location_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/transaction_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_document.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_schedule_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/save_locations.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/select_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/update_status.dart';

class KjsbRepository implements IKjsbRepository {
  final NetworkInfo networkInfo;
  final IKjsbCache kjsbCache;
  final IKjsbRemote kjsbRemote;

  KjsbRepository({this.networkInfo, this.kjsbCache, this.kjsbRemote});

  @override
  Future<Either<Failure, List<KjsbServiceEntity>>> getServices() async {
    if (await networkInfo.isConnected) {
      try {
        var result = await kjsbRemote.getKjsbServices();
        return Right(result.map((dto) => dto.toEntity()).toList());
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, List<LocationEntity>>> getLocations(
      String locationName) async {
    if (await networkInfo.isConnected) {
      try {
        var result = await kjsbRemote.getLocations(locationName);
        return Right(result.map((dto) => dto.toEntity()).toList());
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, int>> createKjsbDraft(NewKjsbParams params) async {
    if (await networkInfo.isConnected) {
      try {
        var remoteResult = await kjsbRemote.createKjsbDraft(params);
        await kjsbCache.createKjsb(params.setFromEntity(
            remoteResult.id.toString(), remoteResult.code));
        return Right(remoteResult.id);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }

  @override
  Stream<Either<Failure, List<ListKjsbEntity>>> getAllKjsb(int type) async* {
    if (await networkInfo.isConnected) {
      try {
        var localResult = await kjsbCache.getAllKjsbByType(type);
        yield (Right(
            localResult.map((kjsbDto) => kjsbDto.toListEntity()).toList()));
        var remoteResult = await kjsbRemote.getAllKjsbByType(type);
        yield (Right(
            remoteResult.map((kjsbDto) => kjsbDto.toEntity()).toList()));
        kjsbCache.createKjsb(
            remoteResult.map((kjsbDto) => kjsbDto.toEntity()).toList());
      } on ServerException {
        yield (Left(ServerFailure()));
      } on CacheException {
        yield (Left(CacheFailure()));
      }
    } else {
      try {
        var localResult = await kjsbCache.getAllKjsbByType(type);
        yield (Right(
            localResult.map((kjsbDto) => kjsbDto.toListEntity()).toList()));
      } on CacheException {
        yield (Left(CacheFailure()));
      }
    }
  }

  @override
  Future<Either<Failure, DetailKjsbEntity>> getKjsbById(String id) async {
    if (await networkInfo.isConnected) {
      try {
        var remoteKjsbResult = await kjsbRemote.getKjsbById(id);
        return Right(remoteKjsbResult.toEntity());
      } on ServerException {
        return Left(ServerFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    } else {
      try {
        return Left(CacheFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, List<TransactionKjsbEntity>>> getAllKJSBSurveyor(
      String kjsbId) async {
    if (await networkInfo.isConnected) {
      final result = await kjsbRemote.getSurveyorKjsb(kjsbId);
      return Right(result
          .map((e) => TransactionKjsbEntity(
              id: e.transaction.id,
              surveyorEntity: e.surveyor.toEntity(),
              priceLand: e.transaction.landPrice,
              landPriceTotal: e.transaction.landPriceTotal,
              totalPrice: e.transaction.total,
              distance: e.transaction.distance,
              priceLandLabel: e.transaction.landPriceLabel,
              landPriceTotalLabel: e.transaction.landPriceTotalLabel,
              landSize: e.transaction.landSize,
              totalPriceLabel: e.transaction.totalLabel,
              transportationPriceLabel: e.transaction.transportationPriceLabel,
              transportationPrice: e.transaction.transportationPrice))
          .toList());
    } else {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> selectSurveyor(
      SelectSurveyorParams params) async {
    try {
      final selectSurveyorRemote = await kjsbRemote.selectSurveyor(params);
      if (selectSurveyorRemote)
        return Right(await kjsbCache.selectSurveyor(params));
      else
        return Right(false);
    } on CacheException {
      return Left(CacheFailure());
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, DetailKjsbEntity>> getKJSBSurveyor(
      String kjsbId) async {
    if (await networkInfo.isConnected) {
      var remoteResult = await kjsbRemote.getKjsbById(kjsbId);
      return Right(DetailKjsbEntity(
        kjsbService: remoteResult.kjsbServiceDto.toEntity(),
        kjsb: remoteResult.kjsbDto.toEntity(),
        transaction: remoteResult.transactionInfoDto.toEntity(),
      ));
    }
  }

  @override
  Future<Either<Failure, bool>> approveKJSBSurveyor(String id) async {
    if (await networkInfo.isConnected) {
      try {
        return Right(await kjsbRemote.approveKjsbSchedule(id));
      } on ServerException {
        return Left(ServerFailure());
      }
    } else
      return Left(ServerFailure());
  }

  @override
  Future<Either<Failure, bool>> verifyBySurveyor(String id) async {
    if (await networkInfo.isConnected) {
      try {
        return Right(await kjsbRemote.verifyBySurveyor(id));
      } on ServerException {
        return Left(ServerFailure());
      }
    } else
      return Left(ServerFailure());
  }

  @override
  Future<Either<Failure, bool>> createScheduleKJSB(
      CreateScheduleParams params) async {
    if (await networkInfo.isConnected) {
      try {
        return Right(await kjsbRemote.createKjsbSchedule(params));
      } on CacheException {
        return Left(CacheFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> approveKjsbSchedule(int scheduledId) async {
    if (await networkInfo.isConnected) {
      try {
        var remoteResult = await kjsbRemote.approveSchedule(scheduledId);
        return Right(remoteResult);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> saveLocation(LocationParams params) async {
    try {
      return Right(await kjsbCache.saveLocation(params));
    } on CacheException {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, List<KjsbDocumentEntity>>> createDocument(
      DocumentParams params) async {
    try {
      final documentRemote = await kjsbRemote.createDocument(params);
      return Right([documentRemote.toEntity()]);
    } on CacheException {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> updateStatus(StatusParams params) async {
    try {
      final documentRemote = await kjsbRemote.updateStatus(params);
      if (documentRemote) await kjsbCache.updateStatus(params);
      return Right(documentRemote);
    } on CacheException {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, List<KjsbDocumentEntity>>> getDocuments(
      String kjsbId) async {
    try {
      final documentRemote = await kjsbRemote.getDocuments(kjsbId);
      return Right(documentRemote.map((e) => e.toEntity()).toList());
    } on CacheException {
      return Left(CacheFailure());
    }
  }
}
