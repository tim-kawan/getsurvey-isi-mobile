import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/network/network_info.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/cache/base/tagging_cache.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/base/tagging_remote.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/tagging_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/tagging_repository.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_tagging.dart';

class TaggingRepository implements ITaggingRepository {
  final NetworkInfo networkInfo;
  final ITaggingCache taggingCache;
  final ITaggingRemote taggingRemote;

  TaggingRepository({this.networkInfo, this.taggingCache, this.taggingRemote});

  @override
  Future<Either<Failure, List<TaggingEntity>>> addTagging(
      TaggingParams params) async {
    if (await networkInfo.isConnected) {
      try {
        var localResult = await taggingCache.addTagging(params: params);
        final resultRemote = await taggingRemote.uploadTagging(
            localResult.map((value) => value.toEntity()).toList(),
            params.kjsbId);
        if (!resultRemote) {
          return Left(ServerFailure());
        } else {
          await taggingCache.setHasSync(params.kjsbId);
        }
        final localAllTagging =
            await taggingCache.loadTagging(params.kjsbId, false);
        return Right(localAllTagging
            .map((taggingDto) => taggingDto.toEntity())
            .toList());
      } on ServerException {
        return Left(ServerFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    } else {
      try {
        await taggingCache.addTagging(params: params);
        final tagging = await taggingCache.loadTagging(params.kjsbId, false);
        return Right(tagging.map((value) => value.toEntity()).toList());
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, List<TaggingEntity>>> loadTagging(
      String kjsbId) async {
    if (await networkInfo.isConnected) {
      try {
        var localResult = await taggingCache.loadTagging(kjsbId, false);
        return Right(
            localResult.map((taggingDto) => taggingDto.toEntity()).toList());
      } on ServerException {
        return Left(ServerFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    } else {
      try {
        var localResult = await taggingCache.loadTagging(kjsbId, false);
        return Right(localResult.map((e) => e.toEntity()).toList());
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, bool>> updateTagging(TaggingParams params) async {
    if (await networkInfo.isConnected) {
      try {
        await taggingCache.updateTagging(params);
        final result = await taggingCache.getTagging(params.id);
        taggingRemote.updateTagging(result.toEntity());
        return Right(true);
      } on ServerException {
        return Left(ServerFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    } else {
      try {
        var localResult = await taggingCache.updateTagging(params);
        return Right(true);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, bool>> uploadTagging(String kjsbId) async {
    try {
      final allTagging = await taggingCache.loadTagging(kjsbId, true);
      if (allTagging.length > 0) {
        final result = await taggingRemote.uploadTagging(
            allTagging.map((value) => value.toEntity()).toList(), kjsbId);
        if(result)
          return Right(result);
        else
          Left(ServerFailure());
      } else
        return Right(true);
    } on CacheException {
      Left(CacheFailure());
    } on ServerException {
      Left(ServerFailure());
    }
  }
}
