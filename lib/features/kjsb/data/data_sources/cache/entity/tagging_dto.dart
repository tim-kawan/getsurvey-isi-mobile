import 'package:get_survey_app/features/kjsb/data/data_sources/cache/db/kjsb_provider.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/tagging_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_tagging.dart';

class TaggingDto {
  final String id;
  final String filePath;
  final String fileUrl;
  final double lat;
  final double lng;
  final bool hasSend;
  final String kjsbId;

  TaggingDto(
      {this.kjsbId,
      this.id,
      this.filePath,
      this.fileUrl,
      this.lat,
      this.lng,
      this.hasSend});

  static Map<String, dynamic> toMap(TaggingParams params, String fileUrl) {
    var map = <String, dynamic>{
      KjsbProvider.COLUMN_ID: params.id,
      KjsbProvider.COLUMN_KSJB_ID: params.kjsbId,
      KjsbProvider.COLUMN_FILE_PATH: params.filePath,
      KjsbProvider.COLUMN_LAT: params.lat,
      KjsbProvider.COLUMN_LON: params.lng,
      KjsbProvider.COLUMN_STATUS: 0,
      KjsbProvider.COLUMN_FILE_URL: fileUrl,
    };
    return map;
  }

  factory TaggingDto.fromMap(Map<String, dynamic> map) => TaggingDto(
        id: map[KjsbProvider.COLUMN_ID],
        kjsbId: map[KjsbProvider.COLUMN_KSJB_ID],
        filePath: map[KjsbProvider.COLUMN_FILE_PATH],
        lng: double.parse(map[KjsbProvider.COLUMN_LON]),
        lat: double.parse(map[KjsbProvider.COLUMN_LAT]),
        hasSend: map[KjsbProvider.COLUMN_STATUS] == 1
            ? true
            : map[KjsbProvider.COLUMN_STATUS] == 0
                ? false
                : map[KjsbProvider.COLUMN_STATUS] ?? false,
        fileUrl: map[KjsbProvider.COLUMN_FILE_URL],
      );

  TaggingEntity toEntity() => TaggingEntity(
      kjsbId: kjsbId,
      filePath: filePath,
      lat: lat,
      id: id,
      fileUrl: fileUrl,
      lng: lng);
}
