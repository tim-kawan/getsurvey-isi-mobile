import 'package:get_survey_app/features/kjsb/data/data_sources/cache/db/kjsb_provider.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_service_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/list_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_kjsb.dart';
import 'package:uuid/uuid.dart';

class KjsbDto {
  final String id;
  final String code;
  final String nama;
  final String nik;
  final String phone;
  final String alamatPemohon;
  final String email;
  final String lokasiTanah;
  final double lat;
  final double lon;
  final int luas;
  final int status;
  final int userId;
  final int serviceId;
  final String serviceName;
  final String serviceIcon;

  KjsbDto({
    this.id,
    this.code,
    this.nama,
    this.nik,
    this.phone,
    this.alamatPemohon,
    this.email,
    this.lokasiTanah,
    this.lat,
    this.lon,
    this.luas,
    this.status,
    this.userId,
    this.serviceId,
    this.serviceName,
    this.serviceIcon,
  });

  ListKjsbEntity toListEntity() => ListKjsbEntity(
      kjsb: KjsbEntity(
          phone: phone,
          nik: nik,
          lon: lon,
          lat: lat,
          id: id,
          luasTanah: luas ?? 0,
          nama: nama,
          iconUrl: serviceIcon,
          code: code ?? "",
          alamatPemohon: alamatPemohon,
          serviceName: serviceName,
          email: email,
          status: status ?? KjsbEntity.unknown,
          lokasiTanah: lokasiTanah,
          userId: userId),
      service: KjsbServiceEntity(
        iconUrl: serviceIcon,
        name: serviceName,
      ));

  factory KjsbDto.fromParams(NewKjsbParams params) => KjsbDto(
      id: params.id,
      code: params.code,
      lokasiTanah: params.lokasiTanah,
      email: params.email,
      alamatPemohon: params.alamatPemohon,
      nama: params.nama,
      lat: params.lat,
      lon: params.lon,
      nik: params.nik,
      luas: params.luas,
      status: KjsbEntity.draft,
      userId: params.userId,
      phone: params.phone);

  Map<String, dynamic> toMap() {
    var uuid = Uuid();
    var map = <String, dynamic>{
      KjsbProvider.COLUMN_ID: uuid.v4(),
      KjsbProvider.COLUMN_CODE: code,
      KjsbProvider.COLUMN_NAMA: nama,
      KjsbProvider.COLUMN_NIK: nik,
      KjsbProvider.COLUMN_PHONE: phone,
      KjsbProvider.COLUMN_LOKASI_TANAH: lokasiTanah,
      KjsbProvider.COLUMN_ALAMAT_PEMOHON: alamatPemohon,
      KjsbProvider.COLUMN_LAT: lat ?? "",
      KjsbProvider.COLUMN_LON: lon ?? "",
      KjsbProvider.COLUMN_LUAS: luas,
      KjsbProvider.COLUMN_STATUS: status,
      KjsbProvider.COLUMN_EMAIL: email,
      KjsbProvider.COLUMN_USER_ID: userId,
      KjsbProvider.COLUMN_SERVICE_ID: serviceId,
      KjsbProvider.COLUMN_SERVICE_NAME: serviceName,
      KjsbProvider.COLUMN_SERVICE_ICON: serviceIcon,
    };

    if (id != null) {
      map[KjsbProvider.COLUMN_ID] = id;
    }
    return map;
  }

  factory KjsbDto.fromMap(Map<String, dynamic> map) => KjsbDto(
        email: map[KjsbProvider.COLUMN_EMAIL],
        code: map[KjsbProvider.COLUMN_CODE],
        lokasiTanah: map[KjsbProvider.COLUMN_LOKASI_TANAH],
        alamatPemohon: map[KjsbProvider.COLUMN_ALAMAT_PEMOHON],
        nama: map[KjsbProvider.COLUMN_NAMA],
        id: map[KjsbProvider.COLUMN_ID],
        lon: double.parse(map[KjsbProvider.COLUMN_LON]),
        lat: double.parse(map[KjsbProvider.COLUMN_LAT]),
        luas: map[KjsbProvider.COLUMN_LUAS],
        nik: map[KjsbProvider.COLUMN_NIK],
        status: map[KjsbProvider.COLUMN_STATUS],
        phone: map[KjsbProvider.COLUMN_PHONE],
        userId: map[KjsbProvider.COLUMN_USER_ID],
        serviceId: map[KjsbProvider.COLUMN_SERVICE_ID],
        serviceName: map[KjsbProvider.COLUMN_SERVICE_NAME],
        serviceIcon: map[KjsbProvider.COLUMN_SERVICE_ICON],
      );

  factory KjsbDto.fromEntity(KjsbEntity params) => KjsbDto(
      id: params.id,
      code: params.code,
      lokasiTanah: params.lokasiTanah,
      email: params.email,
      alamatPemohon: params.alamatPemohon,
      nama: params.nama,
      lat: params.lat,
      lon: params.lon,
      nik: params.nik,
      luas: params.luasTanah,
      status: params.status,
      userId: params.userId,
      phone: params.phone,
      serviceName: params.serviceName,
      serviceId: params.serviceId,
      serviceIcon: params.iconUrl);
}
