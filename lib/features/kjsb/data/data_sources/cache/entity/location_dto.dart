import 'package:get_survey_app/features/kjsb/data/data_sources/cache/db/kjsb_provider.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/save_locations.dart';

class LocationDto {
  final String name;
  final String address;
  final double lat;
  final double lng;

  LocationDto({this.name, this.address, this.lat, this.lng});

  static int applicationLocation = 1;
  static int landLocation = 2;

  static Map<String, dynamic> toMap(LocationParams params) {
    var map = <String, dynamic>{
      KjsbProvider.COLUMN_ADDRESS: params.locationType,
      KjsbProvider.COLUMN_ADDRESS: params.address,
      KjsbProvider.COLUMN_LAT: params.lat,
      KjsbProvider.COLUMN_LON: params.lng,
    };
    return map;
  }
}
