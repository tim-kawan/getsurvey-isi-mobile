import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_tagging.dart';
import 'package:sqflite/sqflite.dart';

import 'base/tagging_cache.dart';
import 'db/kjsb_provider.dart';
import 'entity/tagging_dto.dart';

class TaggingCache implements ITaggingCache {
  final Future<Database> database;

  TaggingCache({this.database});

  @override
  Future<List<TaggingDto>> addTagging(
      {TaggingParams params, String fileUrl}) async {
    try {
      final db = await database;
      await db.insert(KjsbProvider.TABLE_TAGGING, TaggingDto.toMap(params, ""));
      String whereString =
          '${KjsbProvider.COLUMN_KSJB_ID} = ? AND ${KjsbProvider.COLUMN_STATUS} = ?';
      List<dynamic> whereArguments = [params.kjsbId, 0];
      ;
      final result = await db.query(KjsbProvider.TABLE_TAGGING,
          where: whereString, whereArgs: whereArguments);
      return result.map((element) => TaggingDto.fromMap(element)).toList();
    } on Exception {
      CacheException();
    }
  }

  @override
  Future<List<TaggingDto>> loadTagging(String kjsbId, bool isNotSync) async {
    try {
      final db = await database;
      String whereString;
      List<dynamic> whereArguments;
      if (isNotSync) {
        whereString =
            '${KjsbProvider.COLUMN_KSJB_ID} = ? AND ${KjsbProvider.COLUMN_STATUS} = ?';
        whereArguments = [kjsbId, 0];
      } else {
        whereString = '${KjsbProvider.COLUMN_KSJB_ID} = ?';
        whereArguments = [kjsbId];
      }
      final result = await db.query(KjsbProvider.TABLE_TAGGING,
          where: whereString, whereArgs: whereArguments);
      return result.map((element) => TaggingDto.fromMap(element)).toList();
    } on Exception {
      CacheException();
    }
  }

  @override
  Future<bool> updateTagging(TaggingParams params) async {
    try {
      final db = await database;
      String whereString = '${KjsbProvider.COLUMN_ID} = ?';
      List<dynamic> whereArguments = [params.id];
      final result = await db.update(
          KjsbProvider.TABLE_TAGGING,
          {
            KjsbProvider.COLUMN_LAT: params.lat,
            KjsbProvider.COLUMN_LON: params.lng,
          },
          where: whereString,
          whereArgs: whereArguments);
      return true;
    } on Exception {
      CacheException();
    }
  }

  @override
  Future<bool> uploadTagging(String kjsbId) async {
    try {
      final db = await database;
      String whereString = '${KjsbProvider.COLUMN_ID} = ?';
      List<dynamic> whereArguments = [kjsbId];
      await db.update(
          KjsbProvider.TABLE_KJSB,
          {
            KjsbProvider.COLUMN_STATUS: KjsbEntity.findSurveyor,
          },
          where: whereString,
          whereArgs: whereArguments);
      return true;
    } on Exception {
      CacheException();
    }
  }

  @override
  Future<TaggingDto> getTagging(String id) async {
    try {
      final db = await database;
      String whereString = '${KjsbProvider.COLUMN_ID} = ?';
      List<dynamic> whereArguments = [id];
      final result = await db.query(KjsbProvider.TABLE_TAGGING,
          where: whereString, whereArgs: whereArguments);
      final value =
          result.map((element) => TaggingDto.fromMap(element)).toList();
      if (value.length > 0) return value[0];
    } on Exception {
      CacheException();
    }
  }

  @override
  Future<bool> setHasSync(String kjsbId) async {
    try {
      final db = await database;
      String whereString = '${KjsbProvider.COLUMN_ID} = ?';
      List<dynamic> whereArguments = [kjsbId];
      await db.update(
          KjsbProvider.TABLE_TAGGING,
          {
            KjsbProvider.COLUMN_STATUS: 1,
          },
          where: whereString,
          whereArgs: whereArguments);
      return true;
    } on Exception {
      CacheException();
    }
  }
}
