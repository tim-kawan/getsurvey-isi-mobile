import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_schedule_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/save_locations.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/select_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/update_status.dart';
import 'package:sqflite/sqflite.dart';

import 'base/kjsb_cache.dart';
import 'db/kjsb_provider.dart';
import 'entity/kjsb_dto.dart';
import 'entity/location_dto.dart';
import 'entity/tagging_dto.dart';

class KjsbCache implements IKjsbCache {
  final Future<Database> database;

  KjsbCache({this.database});

  @override
  Future<bool> createKjsb(dynamic params) async {
    try {
      final db = await database;
      if (params is NewKjsbParams) {
        final maps = KjsbDto.fromParams(params).toMap();
        await db.insert(KjsbProvider.TABLE_KJSB, maps);
      } else if (params is KjsbEntity) {
        final maps = KjsbDto.fromEntity(params).toMap();
        await db.insert(KjsbProvider.TABLE_KJSB, maps);
      } else if (params is List<KjsbEntity>) {
        await db.delete(KjsbProvider.TABLE_KJSB);
        params.forEach((element) async {
          final maps = KjsbDto.fromEntity(element).toMap();
          await db.insert(KjsbProvider.TABLE_KJSB, maps);
        });
      } else
        CacheException();
      return true;
    } catch (Exception) {
      CacheException();
    }
  }

  @override
  Future<List<KjsbDto>> getAllKjsbByType(int type) async {
    try {
      final db = await database;
      final result = await db.query(KjsbProvider.TABLE_KJSB);
      return result.map((item) => KjsbDto.fromMap(item)).toList();
    } on Exception {
      CacheException();
    }
  }

  @override
  Future<KjsbDto> getKjsbById(String id) async {
    try {
      final db = await database;
      String whereString = '${KjsbProvider.COLUMN_ID} = ?';
      List<dynamic> whereArguments = [id];
      final result = await db.query(KjsbProvider.TABLE_KJSB,
          where: whereString, whereArgs: whereArguments, limit: 1);
      KjsbDto kjsbDto;
      result.forEach((element) {
        kjsbDto = KjsbDto.fromMap(element);
      });
      return kjsbDto;
    } on Exception {
      CacheException();
    }
  }

  @override
  Future<bool> selectSurveyor(SelectSurveyorParams params) async {
    try {
      final db = await database;
      String whereString = '${KjsbProvider.COLUMN_ID} = ?';
      List<dynamic> whereArguments = [params.kjsbId];
      await db.update(
          KjsbProvider.TABLE_KJSB,
          {
            KjsbProvider.COLUMN_STATUS: KjsbEntity.waitApproveBySurveyor,
          },
          where: whereString,
          whereArgs: whereArguments);
      return true;
    } on Exception {
      CacheException();
    }
  }

  @override
  Future<bool> approveKJSBSurveyor(String id) async {
    try {
      final db = await database;
      String whereString = '${KjsbProvider.COLUMN_ID} = ?';
      List<dynamic> whereArguments = [id];
      await db.update(
          KjsbProvider.TABLE_KJSB,
          {
            KjsbProvider.COLUMN_STATUS: KjsbEntity.verifyBySurveyor,
          },
          where: whereString,
          whereArgs: whereArguments);
      return true;
    } on Exception {
      CacheException();
    }
  }

  @override
  Future<bool> verifyBySurveyor(String id) async {
    try {
      final db = await database;
      String whereString = '${KjsbProvider.COLUMN_ID} = ?';
      List<dynamic> whereArguments = [id];
      await db.update(
          KjsbProvider.TABLE_KJSB,
          {
            KjsbProvider.COLUMN_STATUS: KjsbEntity.waitingPayment,
          },
          where: whereString,
          whereArgs: whereArguments);
      return true;
    } on Exception {
      CacheException();
    }
  }

  @override
  Future<bool> createScheduleKJSB(CreateScheduleParams params) async {
    try {
      final db = await database;
      String whereString = '${KjsbProvider.COLUMN_ID} = ?';
      List<dynamic> whereArguments = [params.kjsbId];
      await db.update(
          KjsbProvider.TABLE_KJSB,
          {
            KjsbProvider.COLUMN_STATUS: KjsbEntity.waitApproveScheduled,
          },
          where: whereString,
          whereArgs: whereArguments);
      return true;
    } on Exception {
      CacheException();
    }
  }

  @override
  Future<bool> approveKJSBSchedule(String kjsbId) async {
    try {
      final db = await database;
      String whereString = '${KjsbProvider.COLUMN_ID} = ?';
      List<dynamic> whereArguments = [kjsbId];
      await db.update(
          KjsbProvider.TABLE_KJSB,
          {
            KjsbProvider.COLUMN_STATUS: KjsbEntity.approvedSchedule,
          },
          where: whereString,
          whereArgs: whereArguments);
      return true;
    } on Exception {
      CacheException();
    }
  }

  @override
  Future<bool> saveLocation(LocationParams params) async {
    try {
      final db = await database;
      String whereString = '${KjsbProvider.COLUMN_LOCATION_TYPE} = ?';
      List<dynamic> whereArguments = [params.locationType];
      await db.delete(KjsbProvider.TABLE_LOCATION,
          where: whereString, whereArgs: whereArguments);
      await db.insert(KjsbProvider.TABLE_LOCATION, LocationDto.toMap(params));
      return true;
    } catch (Exception) {
      CacheException();
    }
  }

  @override
  Future<bool> updateStatus(StatusParams params) async {
    try {
      final db = await database;
      String whereString = '${KjsbProvider.COLUMN_ID} = ?';
      List<dynamic> whereArguments = [params.kjsbId];
      await db.update(
          KjsbProvider.TABLE_KJSB,
          {
            KjsbProvider.COLUMN_STATUS: params.status,
          },
          where: whereString,
          whereArgs: whereArguments);
      return true;
    } on Exception {
      CacheException();
    }
  }


}
