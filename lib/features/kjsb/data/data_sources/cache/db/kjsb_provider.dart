import 'package:sqflite/sqflite.dart';

class KjsbProvider {
  static const String TABLE_KJSB = "kjsb";
  static const String TABLE_TAGGING = "tagging";
  static const String TABLE_LOCATION = "location";
  static const String COLUMN_ID = "id";
  static const String COLUMN_CODE = "code";
  static const String COLUMN_KSJB_ID = "kjsb_id";
  static const String COLUMN_NAMA = "nama";
  static const String COLUMN_NIK = "nik";
  static const String COLUMN_USER_ID = "user_id";
  static const String COLUMN_PHONE = "phone";
  static const String COLUMN_ALAMAT_PEMOHON = "alamat_pemohon";
  static const String COLUMN_EMAIL = "email";
  static const String COLUMN_LOKASI_TANAH = "lokasi_tanah";
  static const String COLUMN_LAT = "lat";
  static const String COLUMN_LON = "lon";
  static const String COLUMN_STATUS = "status";
  static const String COLUMN_LUAS = "luas";
  static const String COLUMN_FILE_PATH = "file_path";
  static const String COLUMN_FILE_URL = "file_url";
  static const String COLUMN_FILE_URL_THUMB = "file_url_thumb";
  static const String COLUMN_LOCATION_TYPE = "location_type";
  static const String COLUMN_ADDRESS = "location";
  static const String COLUMN_SERVICE_ID = "service_id";
  static const String COLUMN_SERVICE_NAME = "service_name";
  static const String COLUMN_SERVICE_ICON = "service_icon";

  static Future<void> createTableKJSB(Database database) async {
    await database.execute("CREATE TABLE $TABLE_KJSB ("
        "$COLUMN_ID TEXT PRIMARY KEY,"
        "$COLUMN_NAMA TEXT,"
        "$COLUMN_CODE TEXT,"
        "$COLUMN_NIK TEXT,"
        "$COLUMN_PHONE TEXT,"
        "$COLUMN_ALAMAT_PEMOHON TEXT,"
        "$COLUMN_EMAIL TEXT,"
        "$COLUMN_LOKASI_TANAH TEXT,"
        "$COLUMN_LAT TEXT,"
        "$COLUMN_LON TEXT,"
        "$COLUMN_STATUS INTEGER,"
        "$COLUMN_SERVICE_ID INTEGER,"
        "$COLUMN_SERVICE_NAME TEXT,"
        "$COLUMN_SERVICE_ICON TEXT,"
        "$COLUMN_LUAS INTEGER,"
        "$COLUMN_USER_ID INTEGER"
        ")");
  }

  static Future<void> createTableTagging(Database database) async {
    await database.execute("CREATE TABLE $TABLE_TAGGING ("
        "$COLUMN_ID TEXT PRIMARY KEY,"
        "$COLUMN_KSJB_ID TEXT,"
        "$COLUMN_FILE_PATH TEXT,"
        "$COLUMN_FILE_URL TEXT,"
        "$COLUMN_FILE_URL_THUMB TEXT,"
        "$COLUMN_LAT TEXT,"
        "$COLUMN_LON TEXT,"
        "$COLUMN_STATUS INTEGER"
        ")");
  }

  static Future<void> createTableLocation(Database database) async {
    await database.execute("CREATE TABLE $TABLE_LOCATION ("
        "$COLUMN_LOCATION_TYPE TEXT PRIMARY KEY,"
        "$COLUMN_ADDRESS TEXT,"
        "$COLUMN_LAT TEXT,"
        "$COLUMN_LON TEXT"
        ")");
  }
}
