import 'package:get_survey_app/features/kjsb/data/data_sources/cache/entity/kjsb_dto.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_schedule_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/save_locations.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/select_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/update_status.dart';

abstract class IKjsbCache {
  Future<bool> createKjsb(dynamic params);

  Future<List<KjsbDto>> getAllKjsbByType(int type);

  Future<KjsbDto> getKjsbById(String id);

  Future<bool> updateStatus(StatusParams params);

  Future<bool> selectSurveyor(SelectSurveyorParams params);

  Future<bool> approveKJSBSurveyor(String id);

  Future<bool> verifyBySurveyor(String id);

  Future<bool> createScheduleKJSB(CreateScheduleParams params);

  Future<bool> approveKJSBSchedule(String kjsbId);

  Future<bool> saveLocation(LocationParams params);
}
