import 'package:get_survey_app/features/kjsb/data/data_sources/cache/entity/tagging_dto.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_tagging.dart';

abstract class ITaggingCache {
  Future<List<TaggingDto>> addTagging({TaggingParams params, String fileUrl});

  Future<List<TaggingDto>> loadTagging(String kjsbId, bool isNotSuccess);

  Future<bool> updateTagging(TaggingParams params);

  Future<bool> uploadTagging(String kjsbId);

  Future<TaggingDto> getTagging(String id);

  Future<bool> setHasSync(String kjsbId);

}
