import 'dart:convert';
import 'dart:io';

import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/document_dto.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/find_surveyor_dto.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/kjsb_service_dto.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/location_dto.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/surveyor_dto.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/transaction_info_dto.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_document.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_schedule_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/select_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/update_status.dart';
import 'package:get_survey_app/features/payment/data/data_sources/remote/dto/virtual_account_dto.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'package:path/path.dart';

import 'base/kjsb_remote.dart';
import 'dto/detail_kjsb_dto.dart';
import 'dto/kjsb_dto.dart';
import 'dto/kjsb_result_dto.dart';
import 'dto/kjsb_schedule_dto.dart';
import 'dto/kjsb_schedule_log_dto.dart';

class KjsbRemote implements IKjsbRemote {
  final http.Client client;

  KjsbRemote(this.client);

  @override
  Future<KjsbDto> createKjsbDraft(NewKjsbParams params) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.post(
        Uri.parse("${RestAPI.API_URL}/skb/submission/draft"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authToken
        },
        body: json.encode({
          "skb_services_id": params.servicesId,
          "applicant": params.nama,
          "nik": params.nik,
          "phone": params.phone,
          "applicant_address": params.alamatPemohon,
          "email": params.email,
          "land_size": params.luas,
          "land_address": params.lokasiTanah,
          "lat": params.lat,
          "lon": params.lon,
        }));
    if (response.statusCode == 200) {
      return KjsbDto.fromJson(json.decode(response.body)['data']);
    } else {
      ServerException(response.body.toString(), response.statusCode);
    }
  }

  @override
  Future<List<KjsbServiceDto>> getKjsbServices() async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.get(
      Uri.parse("${RestAPI.API_URL}/skb/service/all"),
      headers: {'Content-Type': 'application/json', 'Authorization': authToken},
    );

    if (response.statusCode == 200) {
      return (json.decode(response.body)['results'] as List)
          .map((dto) => KjsbServiceDto.fromJson(dto))
          .toList();
    } else
      throw ServerException(response.body.toString(), response.statusCode);
  }

  @override
  Future<List<LocationDto>> getLocations(String locationName) async {
    final places = new GoogleMapsPlaces(apiKey: RestAPI.GOOGLE_API_KEY);
    PlacesSearchResponse placesResponse =
        await places.searchByText(locationName);

    if (placesResponse.isOkay) {
      return placesResponse.results
          .map((element) => LocationDto(
                name: element.name,
                address: element.formattedAddress,
                lat: element.geometry.location.lat,
                lng: element.geometry.location.lng,
              ))
          .toList();
    } else {
      ServerException(placesResponse.errorMessage, placesResponse.status);
    }
  }

  @override
  Future<DocumentDto> createDocument(DocumentParams params) async {
    var request = new http.MultipartRequest(
        "POST", Uri.parse("${RestAPI.API_URL}/skb/document/store"));
    final authToken = await Session.getValue(Session.AUTH_TOKEN);

    Map<String, String> headers = {
      "Authorization": authToken,
      "Content-type": "multipart/form-data"
    };
    final file = File(params.path);
    final mimeType = lookupMimeType(params.path).split("/");
    request.files.add(
      http.MultipartFile(
        'upload_file',
        file.readAsBytes().asStream(),
        file.lengthSync(),
        contentType: MediaType(mimeType[0], mimeType[1]),
        filename: basename(params.path),
      ),
    );
    request.headers.addAll(headers);
    request.fields.addAll(
        {"type": params.documentType, "skb_submissions_id": params.kjsbId});
    final response = await request.send();
    if (response.statusCode == 200) {
      final result = json.decode(await response.stream.bytesToString());
      return DocumentDto.fromJson((result['data']));
    } else
      throw ServerException(
          await response.stream.bytesToString(), response.statusCode);
  }

  @override
  Future<bool> updateStatus(StatusParams params) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.put(
        Uri.parse("${RestAPI.API_URL}/skb/submission/change-status"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authToken
        },
        body: json.encode({
          "skb_submissions_id": params.kjsbId,
          "status": params.status,
        }));
    if (response.statusCode == 200) {
      return true;
    } else {
      ServerException(response.body.toString(), response.statusCode);
    }
  }

  @override
  Future<List<KjsbResultDto>> getAllKjsbByType(int type) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client
        .get(Uri.parse("${RestAPI.API_URL}/skb/submission/lists"), headers: {
      'Content-Type': 'application/json',
      'Authorization': authToken
    });
    if (response.statusCode == 200) {
      return (json.decode(response.body)['data'] as List)
          .map((dto) => KjsbResultDto(
              kjsb: KjsbDto.fromJson(dto),
              service: KjsbServiceDto.fromJson(dto['service'])))
          .toList();
    } else {
      ServerException(response.body.toString(), response.statusCode);
    }
  }

  @override
  Future<List<DocumentDto>> getDocuments(String kjsbId) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.get(
        Uri.parse("${RestAPI.API_URL}/skb/document/lists/$kjsbId"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authToken
        });
    if (response.statusCode == 200) {
      return (json.decode(response.body)['data'] as List)
          .map((dto) => DocumentDto.fromJson(dto))
          .toList();
    } else {
      ServerException(response.body.toString(), response.statusCode);
    }
  }

  @override
  Future<List<FindSurveyorDto>> getSurveyorKjsb(String kjsbId) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.get(
        Uri.parse("${RestAPI.API_URL}/user-surveyor/surveyor/location/$kjsbId"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authToken
        });
    if (response.statusCode == 200) {
      return (json.decode(response.body)['data'] as List)
          .map((dto) => FindSurveyorDto(
              surveyor: SurveyorDto?.fromJson(dto['surveyor']),
              transaction: TransactionInfoDto.fromJson(dto)))
          .toList();
    } else {
      ServerException(response.body.toString(), response.statusCode);
    }
  }

  @override
  Future<bool> selectSurveyor(SelectSurveyorParams params) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.post(
        Uri.parse("${RestAPI.API_URL}/skb/submission/surveyor"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authToken
        },
        body: json.encode({
          "skb_submissions_id": params.kjsbId,
          "distance": params.distance,
          "transportation_price": params.transportationPrice,
          "total_price": params.totalPrice,
          "surveyor_id": params.surveyorId
        }));
    if (response.statusCode == 200) {
      return true;
    } else {
      ServerException(response.body.toString(), response.statusCode);
    }
  }

  @override
  Future<DetailKjsbDto> getKjsbById(String kjsbId) async {
    try {
      final authToken = await Session.getValue(Session.AUTH_TOKEN);
      var response = await client.get(
          Uri.parse("${RestAPI.API_URL}/skb/submission/detail/$kjsbId"),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': authToken
          });
      if (response.statusCode == 200) {
        final data = json.decode(response.body)['data'];
        final transaction = data['transactions'] as List;
        final schadules = data['schadules'] as List;
        final payments = transaction.length > 0
            ? (transaction[0]['payments'] as List)
            : null;
        final logs = data['logs'] as List;
        final surveyor = transaction.map((e) => e['user']).toList();
        final listTransaction =
            transaction.map((e) => TransactionInfoDto.fromJson(e)).toList();

        return (DetailKjsbDto(
            kjsbServiceDto: KjsbServiceDto.fromJson(data['service']),
            kjsbDto: KjsbDto.fromJson(data),
            schedule:
                schadules.map((e) => KjsbScheduleDto.fromJson(e)).toList(),
            payments:
                payments?.map((e) => VirtualAccountDto.fromJson(e))?.toList(),
            scheduledLogs:
                logs.map((e) => KjsbScheduleLogDto.fromJson(e)).toList(),
            transactionInfoDto:
                (listTransaction.length > 0) ? listTransaction[0] : null,
            surveyor: (surveyor.length > 0)
                ? SurveyorDto?.fromJson(surveyor[0])
                : null));
      } else {
        ServerException(response.body.toString(), response.statusCode);
      }
    } on Exception {
      ServerException("Unknown error", 100);
    }
  }

  @override
  Future<bool> approveSchedule(int scheduledId) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.put(
        Uri.parse("${RestAPI.API_URL}/skb/schedule/active/$scheduledId"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authToken
        });
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<bool> approveKjsbSchedule(String kjsbId) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.post(
        Uri.parse("${RestAPI.API_URL}/skb/submission/approve"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authToken
        },
        body: json.encode({"skb_submissions_id": kjsbId}));
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<bool> verifyBySurveyor(String id) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.post(
        Uri.parse("${RestAPI.API_URL}/skb/submission/document-verfied"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authToken
        },
        body: json.encode({"skb_submissions_id": id}));
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<bool> createKjsbSchedule(CreateScheduleParams param) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.post(
        Uri.parse("${RestAPI.API_URL}/skb/schedule/store"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authToken
        },
        body: json.encode({
          "skb_submissions_id": param.kjsbId,
          "type": "Create Schedule",
          "start_date": "${param.date} ${param.time}:00",
          "end_date": "${param.date} ${param.time}:00",
          "notes": "Create Schedule"
        }));
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
