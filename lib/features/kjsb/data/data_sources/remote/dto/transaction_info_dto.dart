import 'package:get_survey_app/features/kjsb/domain/entities/transaction_kjsb_entity.dart';
import 'package:intl/intl.dart';

class TransactionInfoDto {
  TransactionInfoDto({
    this.id,
    this.distance,
    this.transportationPrice,
    this.transportationPriceLabel,
    this.landSize,
    this.landPrice,
    this.landPriceLabel,
    this.landPriceTotal,
    this.landPriceTotalLabel,
    this.total,
    this.totalLabel,
  });

  TransactionInfoDto.fromJson(dynamic json) {
    id = json['id'];
    distance = json['value'];
    transportationPrice = (json['transportation_price'] is String)
        ? int.parse(json['transportation_price'])
        : json['transportation_price'];
    transportationPriceLabel = formatRp(transportationPrice);
    landSize = json['land_size'];
    landPrice = json['land_price'];
    landPriceLabel = formatRp(landPrice ?? 0);
    landPriceTotal = (landPrice ?? 0) * (landSize ?? 0);
    landPriceTotalLabel = formatRp(landPriceTotal ?? 0);
    total = (json['total_price'] != null)
        ? ((json['total_price'] is String)
            ? int.parse(json['total_price'])
            : json['total_price'])
        : landPriceTotal + transportationPrice;
    totalLabel = formatRp(total);
  }

  TransactionKjsbEntity toEntity() => TransactionKjsbEntity(
      id: id, totalPrice: total, totalPriceLabel: formatRp(total));

  String formatRp(int value) {
    final currency = new NumberFormat("#,##0", "id_ID");
    return currency.format(value);
  }

  int id;
  int distance;
  int transportationPrice;
  String transportationPriceLabel;
  int landSize;
  int landPrice;
  String landPriceLabel;
  int landPriceTotal;
  String landPriceTotalLabel;
  int total;
  String totalLabel;
}
