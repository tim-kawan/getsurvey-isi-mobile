import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_service_entity.dart';

class KjsbServiceDto {
  KjsbServiceDto({
    this.id,
    this.code,
    this.name,
    this.iconUrl,
    this.iconUrlThumb,
    this.createdAt,
    this.updatedAt,
  });

  KjsbServiceDto.fromJson(dynamic json) {
    id = json['id'];
    code = json['code'];
    name = json['name'];
    iconUrl = json['icon_url'];
    iconUrlThumb = json['icon_url_thumb'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  int id;
  String code;
  String name;
  String iconUrl;
  String iconUrlThumb;
  String createdAt;
  String updatedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['code'] = code;
    map['name'] = name;
    map['icon_url'] = iconUrl;
    map['icon_url_thumb'] = iconUrlThumb;
    map['created_at'] = createdAt;
    map['updated_at'] = updatedAt;
    return map;
  }

  KjsbServiceEntity toEntity() => KjsbServiceEntity(
        id: id,
        name: name,
        code: name,
        iconUrl: iconUrl,
        iconUrlThumb: iconUrlThumb,
      );
}
