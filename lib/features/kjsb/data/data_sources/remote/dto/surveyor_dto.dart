import 'package:get_survey_app/features/kjsb/domain/entities/surveyor_entity.dart';
import 'package:intl/intl.dart';

class SurveyorDto {
  SurveyorDto({
    this.nama,
    this.email,
    this.id,
    this.terdaftar,
    this.lat,
    this.long,
    this.fotoUser,
  });

  SurveyorDto.fromJson(dynamic json) {
    nama = json['nama'];
    email = json['email'];
    id = json["user_surveyor"]['id'];
    terdaftar = json["user_surveyor"]['terdaftar'];
    lat = json['user_profile']['lat'];
    long = json['user_profile']['long'];
    fotoUser = json['user_profile']['foto_user'];
  }

  String nama;
  String email;
  int id;
  String terdaftar;
  double lat;
  double long;
  String fotoUser;

  DateTime strToDate(String date) => DateTime.parse(date);

  String formatDate(DateTime date) {
    return DateFormat("dd MMM yyyy").format(date);
  }

  SurveyorEntity toEntity() => SurveyorEntity(
      name: nama,
      id: id.toString(),
      totalLicences: 0,
      registerAt: (terdaftar!=null ) ? formatDate(strToDate(terdaftar)) : "- ",
      rating: 4.5.toString(),
      photoPath: fotoUser);
}
