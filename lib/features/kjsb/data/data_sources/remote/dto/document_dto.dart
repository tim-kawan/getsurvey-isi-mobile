import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_document_entity.dart';

class DocumentDto {
  DocumentDto({
    this.id,
    this.skbSubmissionsId,
    this.type,
    this.mime,
    this.file,
    this.updatedAt,
    this.createdAt,
  });

  DocumentDto.fromJson(dynamic json) {
    id = json['id'];
    skbSubmissionsId = json['skb_submissions_id'];
    type = json['type'];
    mime = json['mime'];
    file = json['file'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
  }

  int id;
  int skbSubmissionsId;
  String type;
  String mime;
  String file;
  String updatedAt;
  String createdAt;

  KjsbDocumentEntity toEntity() => KjsbDocumentEntity(
      id: id,
      file: file,
      mime: mime,
      type: type,
      skbSubmissionsId: skbSubmissionsId);
}
