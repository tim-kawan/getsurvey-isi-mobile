import 'package:get_survey_app/features/kjsb/domain/entities/list_kjsb_entity.dart';

import 'kjsb_dto.dart';
import 'kjsb_service_dto.dart';

class KjsbResultDto {
  final KjsbDto kjsb;
  final KjsbServiceDto service;

  KjsbResultDto({this.kjsb, this.service});

  ListKjsbEntity toEntity() =>
      ListKjsbEntity(kjsb: kjsb.toEntity(), service: service.toEntity());
}
