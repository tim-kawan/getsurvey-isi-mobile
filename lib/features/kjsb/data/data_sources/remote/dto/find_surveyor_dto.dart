import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/surveyor_dto.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/transaction_info_dto.dart';

class FindSurveyorDto {
  final TransactionInfoDto transaction;
  final SurveyorDto surveyor;

  FindSurveyorDto({this.transaction, this.surveyor});
}
