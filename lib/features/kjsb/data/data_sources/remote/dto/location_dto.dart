import 'package:get_survey_app/features/kjsb/domain/entities/location_entity.dart';

class LocationDto {
  final String name;
  final String address;
  final double lat;
  final double lng;

  LocationDto({this.name, this.address, this.lat, this.lng});

  LocationEntity toEntity() =>
      LocationEntity(name: name, lat: lat, lng: lng, address: address);
}
