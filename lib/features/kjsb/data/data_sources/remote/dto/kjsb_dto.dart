import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';

class KjsbDto {
  KjsbDto({
    this.id,
    this.skbServicesId,
    this.code,
    this.applicant,
    this.nik,
    this.phone,
    this.applicantAddress,
    this.email,
    this.landSize,
    this.landAddress,
    this.lat,
    this.lon,
    this.status,
    this.applicantId,
    this.createdAt,
    this.updatedAt,
  });

  KjsbDto.fromJson(dynamic json) {
    id = json['id'];
    skbServicesId = json['skb_services_id'];
    code = json['code'];
    applicant = json['applicant'];
    nik = json['nik'];
    phone = json['phone'];
    applicantAddress = json['applicant_address'];
    email = json['email'];
    landSize = json['land_size'];
    landAddress = json['land_address'];
    lat = json['lat'];
    lon = json['lon'];
    status = json['status'];
    applicantId = json['applicant_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  int id;
  int skbServicesId;
  String code;
  String applicant;
  String nik;
  String phone;
  String applicantAddress;
  String email;
  int landSize;
  String landAddress;
  double lat;
  double lon;
  int status;
  int applicantId;
  String createdAt;
  String updatedAt;

  KjsbEntity toEntity() => KjsbEntity(
      id: id.toString(),
      code: code,
      email: email,
      lokasiTanah: landAddress,
      alamatPemohon: applicantAddress,
      lon: lon,
      nik: nik,
      phone: phone,
      userId: applicantId,
      lat: lat,
      nama: applicant,
      luasTanah: landSize,
      status: status);
}
