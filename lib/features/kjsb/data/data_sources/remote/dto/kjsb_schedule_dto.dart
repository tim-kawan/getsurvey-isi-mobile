import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_schedule_entity.dart';
import 'package:intl/intl.dart';

class KjsbScheduleDto {
  KjsbScheduleDto({
    this.id,
    this.skbSubmissionsId,
    this.type,
    this.notes,
    this.startDate,
    this.endDate,
    this.active,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  KjsbScheduleDto.fromJson(dynamic json) {
    id = json['id'];
    skbSubmissionsId = json['skb_submissions_id'];
    type = json['type'];
    notes = json['notes'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    active = json['active'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  DateTime strToDate(String date) => DateTime.parse(date);

  String formatDate(DateTime date) {
    return DateFormat("dd MMM yyyy").format(date);
  }

  String formatTime(DateTime date) {
    return DateFormat("HH:mm").format(date);
  }

  int id;
  int skbSubmissionsId;
  String type;
  String notes;
  String startDate;
  String endDate;
  bool active;
  int status;
  String createdAt;
  String updatedAt;

  KjsbScheduleEntity toEntity() => KjsbScheduleEntity(
        id: id,
        status: status,
        type: type,
        startDateLabel: formatDate(strToDate(startDate)),
        timeLabel: formatTime(strToDate(startDate)),
        skbSubmissionsId: skbSubmissionsId,
        active: active,
      );
}
