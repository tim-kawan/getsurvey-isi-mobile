import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/kjsb_dto.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/surveyor_dto.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/transaction_info_dto.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/detail_kjsb_entity.dart';
import 'package:get_survey_app/features/payment/data/data_sources/remote/dto/virtual_account_dto.dart';

import 'kjsb_schedule_dto.dart';
import 'kjsb_schedule_log_dto.dart';
import 'kjsb_service_dto.dart';

class DetailKjsbDto {
  final KjsbDto kjsbDto;
  final KjsbServiceDto kjsbServiceDto;
  final SurveyorDto surveyor;
  final TransactionInfoDto transactionInfoDto;
  final List<KjsbScheduleLogDto> scheduledLogs;
  final List<VirtualAccountDto> payments;
  final List<KjsbScheduleDto> schedule;

  DetailKjsbDto(
      {this.kjsbServiceDto,
      this.kjsbDto,
      this.surveyor,
      this.schedule,
      this.scheduledLogs,
      this.payments,
      this.transactionInfoDto});

  DetailKjsbEntity toEntity() => DetailKjsbEntity(
      schedules: (schedule.length > 0) ? schedule[0].toEntity() : null,
      scheduledLogs: scheduledLogs?.map((e) => e.toEntity())?.toList(),
      transaction: transactionInfoDto?.toEntity(),
      kjsbService: kjsbServiceDto.toEntity(),
      virtualAccount:
          ((payments?.length ?? 0) > 0) ? payments[0].toEntity() : null,
      kjsb: kjsbDto.toEntity().addService(kjsbServiceDto.toEntity()),
      surveyor: surveyor?.toEntity());
}
