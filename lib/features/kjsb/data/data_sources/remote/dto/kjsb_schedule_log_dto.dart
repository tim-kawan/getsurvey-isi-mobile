import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_schedule_log_dto.dart';

class KjsbScheduleLogDto {
  KjsbScheduleLogDto({
    this.id,
    this.skbSubmissionsId,
    this.type,
    this.date,
    this.milisecond,
    this.createdAt,
    this.updatedAt,
  });

  KjsbScheduleLogDto.fromJson(dynamic json) {
    id = json['id'];
    skbSubmissionsId = json['skb_submissions_id'];
    type = json['type'];
    date = json['date'];
    milisecond = json['milisecond'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  int id;
  int skbSubmissionsId;
  int type;
  String date;
  int milisecond;
  String createdAt;
  String updatedAt;

  KjsbScheduleLogEntity toEntity() => KjsbScheduleLogEntity(
        id: id,
        skbSubmissionsId: skbSubmissionsId,
        type: type,
        date: date,
        milisecond: milisecond,
      );
}
