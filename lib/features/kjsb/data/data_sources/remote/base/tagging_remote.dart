import 'package:get_survey_app/features/kjsb/domain/entities/tagging_entity.dart';

abstract class ITaggingRemote {
  Future<bool> uploadTagging(List<TaggingEntity> entity, String kjsbId);

  Future<bool> updateTagging(TaggingEntity entity);
}
