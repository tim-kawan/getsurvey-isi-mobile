import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/detail_kjsb_dto.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/document_dto.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/find_surveyor_dto.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/kjsb_dto.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/kjsb_result_dto.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/kjsb_service_dto.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/location_dto.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_document.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_schedule_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/select_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/update_status.dart';

abstract class IKjsbRemote {
  Future<KjsbDto> createKjsbDraft(NewKjsbParams params);

  Future<List<KjsbResultDto>> getAllKjsbByType(int type);

  Future<List<KjsbServiceDto>> getKjsbServices();

  Future<List<LocationDto>> getLocations(String locationName);

  Future<DocumentDto> createDocument(DocumentParams params);

  Future<bool> updateStatus(StatusParams params);

  Future<bool> createKjsbSchedule(CreateScheduleParams param);

  Future<bool> approveKjsbSchedule(String kjsbId);

  Future<List<DocumentDto>> getDocuments(String kjsbId);

  Future<List<FindSurveyorDto>> getSurveyorKjsb(String kjsbId);

  Future<DetailKjsbDto> getKjsbById(String kjsbId);

  Future<bool> selectSurveyor(SelectSurveyorParams params);

  Future<bool> approveSchedule(int scheduledId);

  Future<bool> verifyBySurveyor(String id);
}
