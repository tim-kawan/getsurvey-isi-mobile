import 'dart:convert';
import 'dart:io';

import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/tagging_entity.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'package:path/path.dart';

import 'base/tagging_remote.dart';

class TaggingRemote implements ITaggingRemote {
  final http.Client client;

  TaggingRemote(this.client);

  @override
  Future<bool> uploadTagging(List<TaggingEntity> entity, String kjsbId) async {
    try {
      var request = new http.MultipartRequest(
          "POST", Uri.parse("${RestAPI.API_URL}/skb/border/store"));
      final authToken = await Session.getValue(Session.AUTH_TOKEN);

      Map<String, String> headers = {
        "Authorization": authToken,
        "Content-type": "multipart/form-data"
      };

      request.headers.addAll(headers);
      entity.forEach((element) {
        final file = File(element.filePath);
        final mimeType = lookupMimeType(element.filePath).split("/");
        request.files.add(
          http.MultipartFile(
            'upload_file',
            file.readAsBytes().asStream(),
            file.lengthSync(),
            contentType: MediaType(mimeType[0], mimeType[1]),
            filename: basename(element.filePath),
          ),
        );
      });
      request.fields.addAll({
        "id": json.encode(entity.map((value) => value.id).toList()),
        "skb_submissions_id":
            json.encode(entity.map((value) => value.kjsbId).toList()),
        "lat": json.encode(entity.map((value) => value.lat).toList()),
        "lon": json.encode(entity.map((value) => value.lng).toList())
      });

      final response = await request.send();
      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } on Exception {
      ServerException("Kesalahan server", 500);
    }
  }

  @override
  Future<bool> updateTagging(TaggingEntity entity) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.post(
        Uri.parse("${RestAPI.API_URL}/skb/submission/draft"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authToken
        },
        body: json.encode({
          "id": entity.id,
          "skb_submissions_id": entity.kjsbId,
          "lat": entity.lat,
          "lon": entity.lng,
        }));
    if (response.statusCode == 200) {
      return true;
    } else {
      ServerException(response.body.toString(), response.statusCode);
    }
  }
}
