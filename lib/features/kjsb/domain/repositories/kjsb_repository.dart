import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/detail_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_document_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_service_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/list_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/location_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/transaction_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_document.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_schedule_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/save_locations.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/select_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/update_status.dart';

abstract class IKjsbRepository {
  Future<Either<Failure, List<KjsbServiceEntity>>> getServices();

  Future<Either<Failure, List<LocationEntity>>> getLocations(
      String locationName);

  Future<Either<Failure, int>> createKjsbDraft(NewKjsbParams params);

  Stream<Either<Failure, List<ListKjsbEntity>>> getAllKjsb(int type);

  Future<Either<Failure, DetailKjsbEntity>> getKjsbById(String id);

  Future<Either<Failure, List<TransactionKjsbEntity>>> getAllKJSBSurveyor(
      String kjsbId);

  Future<Either<Failure, DetailKjsbEntity>> getKJSBSurveyor(String kjsbId);

  Future<Either<Failure, bool>> selectSurveyor(SelectSurveyorParams params);

  Future<Either<Failure, bool>> approveKJSBSurveyor(String id);

  Future<Either<Failure, bool>> verifyBySurveyor(String kjsbId);

  Future<Either<Failure, bool>> createScheduleKJSB(CreateScheduleParams params);

  Future<Either<Failure, bool>> approveKjsbSchedule(int scheduledId);

  Future<Either<Failure, bool>> saveLocation(LocationParams params);

  Future<Either<Failure, List<KjsbDocumentEntity>>> createDocument(
      DocumentParams params);

  Future<Either<Failure, bool>> updateStatus(StatusParams params);

  Future<Either<Failure, List<KjsbDocumentEntity>>> getDocuments(String kjsbId);
}
