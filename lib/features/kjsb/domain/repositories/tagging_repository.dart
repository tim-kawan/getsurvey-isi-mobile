import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/tagging_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_tagging.dart';

abstract class ITaggingRepository {
  Future<Either<Failure, List<TaggingEntity>>> addTagging(TaggingParams params);

  Future<Either<Failure, List<TaggingEntity>>> loadTagging(String kjsbId);

  Future<Either<Failure, bool>> updateTagging(TaggingParams params);

  Future<Either<Failure, bool>> uploadTagging(String kjsbId);
}
