import 'kjsb_entity.dart';
import 'kjsb_service_entity.dart';

class ListKjsbEntity {
  final KjsbEntity kjsb;
  final KjsbServiceEntity service;

  ListKjsbEntity({this.kjsb, this.service});
}
