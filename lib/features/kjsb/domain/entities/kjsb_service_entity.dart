class KjsbServiceEntity {
  int id;
  String code;
  String name;
  String iconUrl;
  String iconUrlThumb;

  KjsbServiceEntity({
    this.id,
    this.code,
    this.name,
    this.iconUrl,
    this.iconUrlThumb,
  });
}
