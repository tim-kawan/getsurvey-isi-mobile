class TaggingEntity {
  final String id;
  final String kjsbId;
  final String filePath;
  final String fileUrl;
  final double lat;
  final double lng;

  TaggingEntity({this.kjsbId,this.id, this.filePath, this.fileUrl, this.lat, this.lng});
}
