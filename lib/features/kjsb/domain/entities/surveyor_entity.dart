class SurveyorEntity {
  final String id;
  final String name;
  final String photoPath;
  final String rating;
  final String registerAt;
  final int totalLicences;

  SurveyorEntity(
      {this.id,
      this.name,
      this.photoPath,
      this.rating,
      this.registerAt,
      this.totalLicences});
}
