import 'package:flutter/material.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/surveyor_entity.dart';

import 'kjsb_entity.dart';

class TransactionKjsbEntity {
  final int id;
  final SurveyorEntity surveyorEntity;
  final KjsbEntity kjsb;
  final int landSize;
  final int distance;
  final int priceLand;
  final String priceLandLabel;
  final int landPriceTotal;
  final String landPriceTotalLabel;
  final int transportationPrice;
  final String transportationPriceLabel;
  final int totalPrice;
  final String totalPriceLabel;

  TransactionKjsbEntity({
    @required this.id,
    this.surveyorEntity,
    this.priceLand,
    this.distance,
    this.landSize,
    this.priceLandLabel,
    this.landPriceTotal,
    this.landPriceTotalLabel,
    this.transportationPrice,
    this.transportationPriceLabel,
    this.kjsb,
    this.totalPrice,
    this.totalPriceLabel,
  });
}
