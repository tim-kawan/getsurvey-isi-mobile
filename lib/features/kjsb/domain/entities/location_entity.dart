class LocationEntity {
  final String name;
  final String address;
  final double lat;
  final double lng;

  LocationEntity({this.name, this.address, this.lat, this.lng});

  static int applicationLocation = 1;
  static int landLocation = 2;
}
