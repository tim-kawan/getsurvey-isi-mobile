class KjsbScheduleLogEntity {
  KjsbScheduleLogEntity({
    this.id,
    this.skbSubmissionsId,
    this.type,
    this.date,
    this.milisecond,
  });

  int id;
  int skbSubmissionsId;
  int type;
  String date;
  int milisecond;
}
