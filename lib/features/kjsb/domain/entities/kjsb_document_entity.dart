class KjsbDocumentEntity {
  KjsbDocumentEntity({
    this.id,
    this.skbSubmissionsId,
    this.type,
    this.mime,
    this.file,
    this.updatedAt,
    this.createdAt,
  });

  int id;
  int skbSubmissionsId;
  String type;
  String mime;
  String file;
  String updatedAt;
  String createdAt;

  static const String ALAS_HAK = "DOC-01";
  static const String PERMOHONAN_PENGUKURAN = "DOC-02";
  static const String SPT_PBB = "DOC-03";
  static const String AJB = "DOC-04";
  static const String KTP = "DOC-05";
  static const String PELIMPAHAN_PENGUKURAN = "DOC-06";
  static const String SURAT_PERMOHONAN = "DOC-07";
  static const String PENGUASAAN_FISIK = "DOC-08";
  static const String SURAT_KUASA = "DOC-09";
  static const String AKTA_PENDIRIAN = "DOC-10";
}
