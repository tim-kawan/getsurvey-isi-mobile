import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_schedule_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_service_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/surveyor_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/transaction_kjsb_entity.dart';
import 'package:get_survey_app/features/payment/domain/entities/virtual_account_entity.dart';

import 'kjsb_entity.dart';
import 'kjsb_schedule_log_dto.dart';

class DetailKjsbEntity {
  final KjsbEntity kjsb;
  final KjsbServiceEntity kjsbService;
  final SurveyorEntity surveyor;
  final TransactionKjsbEntity transaction;
  final VirtualAccountEntity virtualAccount;
  final List<KjsbScheduleLogEntity> scheduledLogs;
  final KjsbScheduleEntity schedules;

  DetailKjsbEntity({
    this.scheduledLogs,
    this.transaction,
    this.kjsb,
    this.kjsbService,
    this.schedules,
    this.surveyor,
    this.virtualAccount,
  });
}
