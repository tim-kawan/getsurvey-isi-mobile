class KjsbScheduleEntity {
  KjsbScheduleEntity({
    this.id,
    this.skbSubmissionsId,
    this.type,
    this.notes,
    this.startDate,
    this.startDateLabel,
    this.timeLabel,
    this.endDate,
    this.active,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int skbSubmissionsId;
  String type;
  String notes;
  String startDate;
  String startDateLabel;
  String timeLabel;
  String endDate;
  bool active;
  int status;
  String createdAt;
  String updatedAt;
}
