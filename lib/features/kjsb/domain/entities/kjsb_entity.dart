import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_service_entity.dart';

class KjsbEntity {
  final String id;
  final String code;
  final String nama;
  final String nik;
  final String phone;
  final String alamatPemohon;
  final String email;
  final String lokasiTanah;
  final double lat;
  final double lon;
  final int luasTanah;
  final int status;
  final int userId;
  final int serviceId;
  final String iconUrl;
  final String serviceName;

  static const unknown = -1;
  static const draft = 0;
  static const taggingLocation = 1;
  static const findSurveyor = 2;
  static const waitApproveBySurveyor = 3;
  static const verifyBySurveyor = 4;
  static const waitingPayment = 5;
  static const paymentGatewayCreated = 6;
  static const waitScheduled = 7;
  static const waitApproveScheduled = 8;
  static const shareLocation = 9;
  static const waitApproveLocation = 10;
  static const survey = 11;
  static const waitingResult = 12;
  static const finish = 13;
  static const rejected = 14;

  static int hasPaid = 106;
  static int approvedSchedule = 109;
  static int onLocation = 112;

  String labelStatus() {
    if (status == draft)
      return "DRAF";
    else if (status == taggingLocation) {
      return "PEMASANGAN PATOK";
    } else if (status == waitApproveBySurveyor) {
      return "MENUNGGU KONFIRMASI SURVEYOR";
    } else if (status == findSurveyor)
      return "CARI SURVEYOR";
    else if (status == verifyBySurveyor)
      return "VERIFIKASI SURVEYOR";
    else if (status == waitingPayment || status == paymentGatewayCreated)
      return "MENUNGGU PEMBAYARAN";
    else if (status == hasPaid)
      return "MENUNGGU VERIFIKASI PEMBAYARAN";
    else if (status == waitScheduled)
      return "MENUNGGU JADWAL PENGUKURAN";
    else if (status == waitApproveScheduled)
      return "MENUNGGU PERSETUJUAN JADWAL";
    else if (status == approvedSchedule)
      return "JADWAL PENGUKURAN  DISTUJUI";
    else if (status == shareLocation)
      return "JADWAL PENGUKURAN  DISTUJUI";
    else
      return "TIDAK DIKETAHUI";
  }

  KjsbEntity(
      {this.id,
      this.code,
      this.serviceId,
      this.serviceName,
      this.nama,
      this.nik,
      this.iconUrl,
      this.phone,
      this.alamatPemohon,
      this.email,
      this.lokasiTanah,
      this.lat,
      this.lon,
      this.luasTanah,
      this.status,
      this.userId});

  KjsbEntity addService(KjsbServiceEntity service) => KjsbEntity(
      id: id.toString(),
      code: code,
      email: email,
      lokasiTanah: lokasiTanah,
      alamatPemohon: alamatPemohon,
      lon: lon,
      nik: nik,
      phone: phone,
      userId: userId,
      lat: lat,
      nama: nama,
      luasTanah: luasTanah,
      status: status,
      serviceName: service.name,
      serviceId: service.id,
      iconUrl: service.iconUrlThumb);
}
