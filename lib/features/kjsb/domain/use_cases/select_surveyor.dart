import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class SelectSurveyor extends UseCase<bool, SelectSurveyorParams> {
  final IKjsbRepository kjsbRepository;

  SelectSurveyor({this.kjsbRepository});

  @override
  Future<Either<Failure, bool>> call(SelectSurveyorParams params) async {
    return kjsbRepository.selectSurveyor(params);
  }
}

class SelectSurveyorParams extends Equatable {
  final String kjsbId;
  final String surveyorId;
  final int distance;
  final int transportationPrice;
  final int totalPrice;

  SelectSurveyorParams({
    this.kjsbId,
    this.surveyorId,
    this.distance,
    this.transportationPrice,
    this.totalPrice,
  });

  @override
  List<Object> get props => [kjsbId, surveyorId];
}
