import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/location_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class GetLocations extends UseCase<List<LocationEntity>, String> {
  final IKjsbRepository kjsbRepository;

  GetLocations({this.kjsbRepository});

  @override
  Future<Either<Failure, List<LocationEntity>>> call(
      String locationName) async {
    return kjsbRepository.getLocations(locationName);
  }
}
