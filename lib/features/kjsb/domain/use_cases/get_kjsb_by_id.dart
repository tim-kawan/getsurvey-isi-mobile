import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/detail_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class GetKJSBById extends UseCase<DetailKjsbEntity, String> {
  final IKjsbRepository kjsbRepository;

  GetKJSBById({this.kjsbRepository});

  @override
  Future<Either<Failure, DetailKjsbEntity>> call(String id) async {
    return kjsbRepository.getKjsbById(id);
  }
}
