import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/tagging_repository.dart';

class UploadTagging extends UseCase<bool, String> {
  final ITaggingRepository taggingRepository;

  UploadTagging({this.taggingRepository});

  @override
  Future<Either<Failure, bool>> call(String kjsbId) {
    return taggingRepository.uploadTagging(kjsbId);
  }
}
