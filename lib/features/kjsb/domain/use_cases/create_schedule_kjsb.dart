import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class CreateScheduleKJSB extends UseCase<bool, CreateScheduleParams> {
  final IKjsbRepository kjsbRepository;

  CreateScheduleKJSB({this.kjsbRepository});

  @override
  Future<Either<Failure, bool>> call(CreateScheduleParams params) async {
    return await kjsbRepository.createScheduleKJSB(params);
  }
}

class CreateScheduleParams {
  final String kjsbId;
  final String date;
  final String time;

  CreateScheduleParams({this.kjsbId, this.date, this.time});
}
