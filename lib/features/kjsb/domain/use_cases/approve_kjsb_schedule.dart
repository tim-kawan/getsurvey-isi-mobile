import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class ApproveKJSBSchedule extends UseCase<bool, int> {
  final IKjsbRepository kjsbRepository;

  ApproveKJSBSchedule({this.kjsbRepository});

  @override
  Future<Either<Failure, bool>> call(int scheduledId) async {
    return await kjsbRepository.approveKjsbSchedule(scheduledId);
  }
}
