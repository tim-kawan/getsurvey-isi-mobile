import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/tagging_repository.dart';

import 'create_tagging.dart';

class UpdateTagging extends UseCase<bool, TaggingParams> {
  final ITaggingRepository taggingRepository;

  UpdateTagging({this.taggingRepository});

  @override
  Future<Either<Failure, bool>> call(TaggingParams params) async {
    return taggingRepository.updateTagging(params);
  }
}
