import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class UpdateStatus extends UseCase<bool, StatusParams> {
  final IKjsbRepository kjsbRepository;

  UpdateStatus({this.kjsbRepository});

  @override
  Future<Either<Failure, bool>> call(StatusParams params) async {
    return kjsbRepository.updateStatus(params);
  }
}

class StatusParams {
  final int kjsbId;
  final int status;

  StatusParams(this.kjsbId, this.status);
}
