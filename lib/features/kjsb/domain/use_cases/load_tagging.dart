import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/tagging_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/tagging_repository.dart';

class LoadTagging extends UseCase<List<TaggingEntity>, String> {
  final ITaggingRepository taggingRepository;

  LoadTagging({this.taggingRepository});

  @override
  Future<Either<Failure, List<TaggingEntity>>> call(String kjsbId) async {
    return taggingRepository.loadTagging(kjsbId);
  }
}
