import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_document_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class GetDocuments extends UseCase<List<KjsbDocumentEntity>, String> {
  final IKjsbRepository kjsbRepository;

  GetDocuments({this.kjsbRepository});

  @override
  Future<Either<Failure, List<KjsbDocumentEntity>>> call(String kjsbId) {
    return kjsbRepository.getDocuments(kjsbId);
  }
}
