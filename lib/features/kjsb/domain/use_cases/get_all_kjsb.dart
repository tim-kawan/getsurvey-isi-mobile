import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/stream_usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/list_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class GetAllKJSB extends StreamUseCase<List<ListKjsbEntity>, int> {
  final IKjsbRepository kjsbRepository;

  GetAllKJSB({this.kjsbRepository});

  @override
  Stream<Either<Failure, List<ListKjsbEntity>>> call(int params) async* {
    yield* kjsbRepository.getAllKjsb(params);
  }
}
