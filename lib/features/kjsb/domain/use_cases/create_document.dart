import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_document_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class CreateDocument extends UseCase<List<KjsbDocumentEntity>, DocumentParams> {
  final IKjsbRepository kjsbRepository;

  CreateDocument({this.kjsbRepository});

  @override
  Future<Either<Failure, List<KjsbDocumentEntity>>> call(
      DocumentParams params) async {
    return kjsbRepository.createDocument(params);
  }
}

class DocumentParams extends Equatable {
  final String kjsbId;
  final String path;
  final String documentType;

  DocumentParams({this.kjsbId, this.path, this.documentType});

  @override
  List<Object> get props => [kjsbId, path];
}
