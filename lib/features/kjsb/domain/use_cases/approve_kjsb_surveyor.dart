import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class ApproveKJSBSurveyor extends UseCase<bool, String> {
  final IKjsbRepository kjsbRepository;

  ApproveKJSBSurveyor({this.kjsbRepository});

  @override
  Future<Either<Failure, bool>> call(String kjsbId) async {
    return await kjsbRepository.approveKJSBSurveyor(kjsbId);
  }
}
