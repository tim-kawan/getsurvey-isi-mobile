import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class SaveLocations extends UseCase<bool, LocationParams> {
  final IKjsbRepository kjsbRepository;

  SaveLocations({this.kjsbRepository});

  @override
  Future<Either<Failure, bool>> call(LocationParams params) async {
    return kjsbRepository.saveLocation(params);
  }
}

class LocationParams {
  final String address;
  final int locationType;
  final double lat;
  final double lng;

  LocationParams(this.address, this.locationType, this.lat, this.lng);
}
