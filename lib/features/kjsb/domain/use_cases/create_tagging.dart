import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/tagging_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/tagging_repository.dart';

class CreateTagging extends UseCase<List<TaggingEntity>, TaggingParams> {
  final ITaggingRepository taggingRepository;

  CreateTagging({this.taggingRepository});

  @override
  Future<Either<Failure, List<TaggingEntity>>> call(
      TaggingParams params) async {
    return taggingRepository.addTagging(params);
  }
}

class TaggingParams extends Equatable {
  final String id;
  final String kjsbId;
  final String filePath;
  final double lat;
  final double lng;

  TaggingParams({this.filePath, this.lat, this.lng, this.id, this.kjsbId});

  @override
  List<Object> get props => [filePath, lat, lng];
}
