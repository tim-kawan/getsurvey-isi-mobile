import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_service_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class GetServices extends UseCase<List<KjsbServiceEntity>, NoParams> {
  final IKjsbRepository kjsbRepository;

  GetServices({this.kjsbRepository});

  @override
  Future<Either<Failure, List<KjsbServiceEntity>>> call(NoParams params) async {
    return kjsbRepository.getServices();
  }
}
