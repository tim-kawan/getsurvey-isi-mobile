import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/transaction_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class GetAllKJSBSurveyor extends UseCase<List<TransactionKjsbEntity>, String> {
  final IKjsbRepository kjsbRepository;

  GetAllKJSBSurveyor({this.kjsbRepository});

  @override
  Future<Either<Failure, List<TransactionKjsbEntity>>> call(String kjsbId) async {
    return await kjsbRepository.getAllKJSBSurveyor(kjsbId);
  }
}
