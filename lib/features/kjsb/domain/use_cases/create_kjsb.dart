import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class CreateKJSB extends UseCase<int, NewKjsbParams> {
  final IKjsbRepository kjsbRepository;

  CreateKJSB({this.kjsbRepository});

  @override
  Future<Either<Failure, int>> call(NewKjsbParams params) async {
    return kjsbRepository.createKjsbDraft(params);
  }
}

class NewKjsbParams extends Equatable {
  final String nama;
  final String nik;
  final String code;
  final String id;
  final int servicesId;
  final String phone;
  final String alamatPemohon;
  final String email;
  final String lokasiTanah;
  final double lat;
  final double lon;
  final int luas;
  final int userId;

  NewKjsbParams(
      {this.nama,
      this.code,
      this.id,
      this.nik,
      this.servicesId,
      this.phone,
      this.alamatPemohon,
      this.email,
      this.lokasiTanah,
      this.luas,
      this.lat,
      this.lon,
      this.userId});

  NewKjsbParams setFromEntity(String id, String code) => NewKjsbParams(
      nama: nama,
      lat: lat,
      userId: userId,
      id: id,
      phone: phone,
      nik: nik,
      lon: lon,
      alamatPemohon: alamatPemohon,
      lokasiTanah: lokasiTanah,
      email: email,
      code: code,
      luas: luas,
      servicesId: servicesId);

  @override
  List<Object> get props => [nama, nik, lokasiTanah];
}
