import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/detail_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/transaction_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/repositories/kjsb_repository.dart';

class GetKJSBSurveyor extends UseCase<DetailKjsbEntity, String> {
  final IKjsbRepository kjsbRepository;

  GetKJSBSurveyor({this.kjsbRepository});

  @override
  Future<Either<Failure, DetailKjsbEntity>> call(String kjsbId) async {
    return await kjsbRepository.getKJSBSurveyor(kjsbId);
  }
}
