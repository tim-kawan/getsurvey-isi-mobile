import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/presentation/provider/kjsb_surveyor_cubit.dart';
import 'package:get_survey_app/features/kjsb/presentation/widgets/shimmer_detail_kjsb_surveyor.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:slidable_button/slidable_button.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class DetailKJSBScreen extends StatelessWidget {
  final String kjsbId;
  int height = 200;

  DetailKJSBScreen({Key key, this.kjsbId}) : super(key: key);
  KjsbSurveyorCubit kjsbSurveyorCubit;

  @override
  Widget build(BuildContext context) {
    kjsbSurveyorCubit = di<KjsbSurveyorCubit>();
    return BlocProvider(
      create: (context) => kjsbSurveyorCubit,
      child: LoaderOverlay(
        useDefaultLoading: false,
        overlayWidget: Center(
          child: SpinKitCubeGrid(
            color: primaryColor,
            size: 50.0,
          ),
        ),
        child: Scaffold(
          appBar: AppBar(
              leading: IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.white),
                  onPressed: () {
                    Navigator.pop(context, true);
                  }),
              centerTitle: true,
              backgroundColor: Color(0xff2CBFCD),
              title: Text(
                "Detail Permohonan",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              )),
          body: BlocConsumer<KjsbSurveyorCubit, KjsbSurveyorState>(
              listener: (context, state) {},
              builder: (context, state) {
                if (state is GetKjsbSurveyorInitial || state is LoadingState) {
                  context.read<KjsbSurveyorCubit>().getKjsbById(kjsbId);
                  return ShimmerWidget();
                } else if (state is DetailKjsbState)
                  return SlidingUpPanel(
                      renderPanelSheet: false,
                      isDraggable: false,
                      minHeight: MediaQuery.of(context).size.width -
                          state.subtractionPanelHeight,
                      body: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          _header(state.detailEntity.kjsb),
                          _splitter(),
                          _detailKJSBDocument(context, state),
                        ],
                      ),
                      panel: _panel(context, state));
                return Container();
              }),
        ),
      ),
    );
  }

  Widget _header(KjsbEntity entity) {
    return Container(
      padding: EdgeInsets.only(
        left: 20,
        right: 25,
      ),
      child: Padding(
        padding: EdgeInsets.only(top: 7, bottom: 7),
        child: Column(
          children: [
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  entity.labelStatus(),
                  style: TextStyle(fontSize: 14, color: Color(0xff343434)),
                ),
                Container(
                  child: Row(
                    children: [
                      Text(
                        "Lihat",
                        style: TextStyle(
                            fontSize: 14,
                            color: Color(0xff349699),
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "INVOICE ${entity.code}",
                  style: TextStyle(fontSize: 14, color: Color(0xff343434)),
                ),
                Container(
                  child: Row(
                    children: [
                      Text(
                        "Lihat",
                        style: TextStyle(
                            fontSize: 14,
                            color: Color(0xff349699),
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }

  Widget _detailKJSB(BuildContext context, KjsbSurveyorState state) {
    if (state is DetailKjsbState)
      return Padding(
        padding: const EdgeInsets.only(left: 16, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Pilihan Layanan",
              style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 20),
            Container(
              padding: EdgeInsets.all(12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                  bottomLeft: Radius.circular(5),
                  bottomRight: Radius.circular(5),
                ),
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 0.3, blurRadius: 4, color: Colors.grey[400])
                ],
                color: Colors.white,
              ),
              child: Row(
                children: [
                  CachedNetworkImage(
                    width: 42,
                    imageUrl: state.detailEntity.kjsbService.iconUrlThumb ?? "",
                    placeholder: (context, url) => Container(
                      child: Center(
                        child: Container(
                            width: 42,
                            height: 42,
                            child: SpinKitCubeGrid(
                              color: primaryColor,
                              size: 30.0,
                            )),
                      ),
                      width: 42.0,
                      height: 42.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        border: Border.all(
                          color: Colors.white,
                          width: 2.0,
                        ),
                      ),
                    ),
                    // cacheManager: _cacheManager,
                  ),
                  SizedBox(width: 20),
                  Container(
                    color: Colors.brown,
                    height: 49,
                    width: 1,
                  ),
                  SizedBox(width: 20),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Get - Layanan Pertanahan",
                          style:
                              TextStyle(fontSize: 13, color: Color(0xff0C6164)),
                        ),
                        Text(
                          state.detailEntity.kjsbService.name ?? "",
                          style: TextStyle(fontSize: 13),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Rp. ${state.detailEntity?.transaction.totalPriceLabel ?? 0}",
                              style: TextStyle(
                                  fontSize: 9, color: Colors.grey[800]),
                            ),
                            Text(
                              "Unduh Berkas",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: Color(0xffFA5523),
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),
            SizedBox(height: 20),
            Text(
              "Rincian Pembayaran",
              style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Jasa Layanan (1 Bidang)",
                  style: TextStyle(fontSize: 12),
                ),
                Text(
                  "Rp. ${state.detailEntity?.transaction.totalPriceLabel ?? 0}",
                  style: TextStyle(fontSize: 12),
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Biaya lain-lain",
                  style: TextStyle(fontSize: 12),
                ),
                Text("-"),
              ],
            ),
            SizedBox(height: 10),
            Container(
              height: 1,
              color: Colors.black12,
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Total Bayar",
                    style:
                        TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
                Text(
                    "Rp. ${state.detailEntity?.transaction.totalPriceLabel ?? 0}",
                    style:
                        TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
              ],
            ),
            SizedBox(height: 20),
          ],
        ),
      );
    else
      Container();
  }

  Widget _splitter() {
    return Container(
      height: 15,
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: Color(0x1A707070),
            width: 1.0,
          ),
        ),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0x1A707070),
            Colors.white10,
          ],
        ),
      ),
    );
  }

  Widget _detailKJSBDocument(BuildContext context, KjsbSurveyorState state) {
    return Expanded(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _detailKJSB(context, state),
            _splitter(),
            _detailUser(context, state)
          ],
        ),
      ),
    );
  }

  Widget _panel(BuildContext context, KjsbSurveyorState state) {
    if (state is WaitApproveBySurveyorState)
      return _confirmNewRequest(context, state);
    else if (state is DocumentNotDownloadState)
      return _hasConfirm(context, state);
    else if (state is WaitPaymentState)
      return Container();
    else if (state is DownloadDocumentState)
      return _downloading(context);
    else if (state is VerifyBySurveyorState)
      return _hashVerify(context, state);
    else if (state is WaitScheduledState)
      return _dialogSchedule(context, state);
    else if (state is SetScheduledState)
      return _createSchedule(context, state);
    else
      return Container();
  }

  Widget _detailUser(BuildContext context, KjsbSurveyorState state) {
    if (state is DetailKjsbState)
      return Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Biodata Pemohon",
              style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Nama Pemohon", style: TextStyle(fontSize: 12)),
                Text(state.detailEntity?.kjsb?.nama ?? "",
                    style: TextStyle(fontSize: 12))
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Tanggal Permohonan", style: TextStyle(fontSize: 12)),
                Text("30 Sept 2021 | 14:41 WIB",
                    style: TextStyle(fontSize: 12)),
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("NIK", style: TextStyle(fontSize: 12)),
                Text(state.detailEntity?.kjsb?.nik ?? "",
                    style: TextStyle(fontSize: 12)),
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Nomor Handphone", style: TextStyle(fontSize: 12)),
                Text(state.detailEntity.kjsb.phone,
                    style: TextStyle(fontSize: 12)),
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Alamat Pemohon", style: TextStyle(fontSize: 12)),
                SizedBox(width: 30),
                Expanded(
                    child: Text(state.detailEntity.kjsb.alamatPemohon,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.end,
                        style: TextStyle(fontSize: 12))),
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Email", style: TextStyle(fontSize: 12)),
                Text(state.detailEntity.kjsb.email,
                    style: TextStyle(fontSize: 12)),
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Alamat Bidang Tanah", style: TextStyle(fontSize: 12)),
                Expanded(
                    child: Text(state.detailEntity.kjsb.lokasiTanah,
                        textAlign: TextAlign.end,
                        style: TextStyle(fontSize: 12))),
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Total Bayar",
                    style:
                        TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
                Text("Rp. ${state.detailEntity.transaction.totalPriceLabel}",
                    style:
                        TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
              ],
            ),
            Container(height: 400)
          ],
        ),
      );
  }

  Widget _confirmNewRequest(BuildContext context, KjsbSurveyorState state) {
    if (state is WaitApproveBySurveyorState)
      return Padding(
          padding: EdgeInsets.only(top: 20),
          child: Container(
            padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(24.0),
                    topLeft: Radius.circular(24.0)),
                boxShadow: [
                  BoxShadow(
                    blurRadius: 5.0,
                    color: Colors.grey,
                  ),
                ]),
            margin: const EdgeInsets.only(left: 10.0, right: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Container(
                      height: 4,
                      width: 60,
                      decoration: BoxDecoration(
                          color: Color(0xff77838F),
                          borderRadius: BorderRadius.circular(3))),
                ),
                SizedBox(height: 20),
                Text("Terima Permohonan Berkas?",
                    style:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                SizedBox(height: 10),
                Text(
                    "Anda memiliki waktu 30 menit untuk mengkonfirmasi permohonan berkas.",
                    style:
                        TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
                SizedBox(height: 20),
                SlidableButton(
                    color: Color(0xffFFA7B6),
                    width: MediaQuery.of(context).size.width / 1,
                    height: 49,
                    // color: Theme.of(context).accentColor.withOpacity(0.5),
                    buttonColor: Color(0xffF25F5F),
                    dismissible: false,
                    borderRadius: BorderRadius.circular(5),
                    label: Center(
                        child: Icon(
                      Icons.arrow_forward,
                      color: Colors.white,
                    )),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Tolak",
                              style: TextStyle(
                                  fontSize: 19,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white))
                        ],
                      ),
                    ),
                    onChanged: (position) {
                      print("A");
                    }),
                SizedBox(height: 10),
                SlidableButton(
                    color: Color(0xffAEE9EB),
                    width: MediaQuery.of(context).size.width / 1,
                    height: 49,
                    // color: Theme.of(context).accentColor.withOpacity(0.5),
                    buttonColor: Color(0xff00C045),
                    dismissible: false,
                    borderRadius: BorderRadius.circular(5),
                    label: Center(
                        child: Icon(
                      Icons.arrow_forward,
                      color: Colors.white,
                    )),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Terima",
                              style: TextStyle(
                                  fontSize: 19,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white))
                        ],
                      ),
                    ),
                    onChanged: (position) {
                      context
                          .read<KjsbSurveyorCubit>()
                          .approveBySurveyor(kjsbId);
                    }),
              ],
            ),
          ));
    else
      Container();
  }

  Widget _hasConfirm(BuildContext context, KjsbSurveyorState state) {
    return Padding(
        padding: EdgeInsets.only(top: 20),
        child: Container(
          padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(24.0),
                  topLeft: Radius.circular(24.0)),
              boxShadow: [
                BoxShadow(
                  blurRadius: 5.0,
                  color: Colors.grey,
                ),
              ]),
          margin: const EdgeInsets.only(left: 10.0, right: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Container(
                    height: 4,
                    width: 60,
                    decoration: BoxDecoration(
                        color: Color(0xff77838F),
                        borderRadius: BorderRadius.circular(3))),
              ),
              SizedBox(height: 20),
              Text("Berkas Pemohon diterima",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              SizedBox(height: 10),
              Text(
                  "Anda telah menerima berkas pemohon, silahkan menekan \"Unduh\" untuk mengunduh dokument permohonan",
                  style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
              SizedBox(height: 20),
              Container(
                height: 46,
                child: InkWell(
                  onTap: () {
                    if (state is DocumentNotDownloadState)
                      context
                          .read<KjsbSurveyorCubit>()
                          .downloadDocumentKjsb(state.detailEntity);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Color(0xff2CBFCD),
                    ),
                    height: 30,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Unduh",
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Widget _dialogSchedule(BuildContext context, KjsbSurveyorState state) {
    return Padding(
        padding: EdgeInsets.only(top: 20),
        child: Container(
          padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(24.0),
                  topLeft: Radius.circular(24.0)),
              boxShadow: [
                BoxShadow(
                  blurRadius: 5.0,
                  color: Colors.grey,
                ),
              ]),
          margin: const EdgeInsets.only(left: 10.0, right: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Container(
                    height: 4,
                    width: 60,
                    decoration: BoxDecoration(
                        color: Color(0xff77838F),
                        borderRadius: BorderRadius.circular(3))),
              ),
              SizedBox(height: 20),
              Text("Buat Jadwal Pengukuran",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              SizedBox(height: 10),
              Text(
                  "Pemohon telah melunasi pembayaran. Segera buat Jadwal Pengukuran",
                  style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
              SizedBox(height: 20),
              Container(
                height: 46,
                child: InkWell(
                  onTap: () {
                    if (state is WaitScheduledState)
                      context
                          .read<KjsbSurveyorCubit>()
                          .setScheduled(state.detailEntity);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Color(0xff2CBFCD),
                    ),
                    height: 30,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "OK",
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Widget _createSchedule(BuildContext context, SetScheduledState state) {
    return Padding(
        padding: EdgeInsets.only(top: 20),
        child: Container(
          padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(24.0),
                  topLeft: Radius.circular(24.0)),
              boxShadow: [
                BoxShadow(
                  blurRadius: 5.0,
                  color: Colors.grey,
                ),
              ]),
          margin: const EdgeInsets.only(left: 10.0, right: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Container(
                    height: 4,
                    width: 60,
                    decoration: BoxDecoration(
                        color: Color(0xff77838F),
                        borderRadius: BorderRadius.circular(3))),
              ),
              SizedBox(height: 20),
              Center(
                child: Text("Jadwal Pengukuran",
                    style:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              ),
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Tanggal Mulai".toUpperCase(),
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.bold)),
                          InkWell(
                            onTap: () {
                              DatePicker.showDatePicker(context,
                                  showTitleActions: true,
                                  minTime: DateTime(2021, 6, 5),
                                  maxTime: DateTime(2021, 6, 31),
                                  onChanged: (date) {
                                print('change $date');
                              }, onConfirm: (date) {
                                kjsbSurveyorCubit.setDate(date);
                              },
                                  currentTime: DateTime.now(),
                                  locale: LocaleType.id);
                            },
                            child: Container(
                              height: 31,
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  border:
                                      Border.all(width: 1, color: Colors.grey)),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Icon(
                                    Icons.calendar_today_rounded,
                                    size: 11,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Container(width: 1, color: Colors.grey),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Expanded(
                                      child: StreamBuilder(
                                          stream: kjsbSurveyorCubit.dateStream,
                                          builder: (context, snapshot) {
                                            return Text(
                                              snapshot.hasData
                                                  ? snapshot.data
                                                  : "Pilih Tangggal",
                                              style: TextStyle(fontSize: 10),
                                            );
                                          }))
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Pukul".toUpperCase(),
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold),
                          ),
                          InkWell(
                            onTap: () {
                              DatePicker.showTimePicker(context,
                                  showTitleActions: true, onChanged: (date) {
                                print('change $date');
                              }, onConfirm: (date) {
                                kjsbSurveyorCubit.setTime(date);
                              },
                                  currentTime: DateTime.now(),
                                  locale: LocaleType.id);
                            },
                            child: Container(
                              height: 31,
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  border:
                                      Border.all(width: 1, color: Colors.grey)),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Icon(
                                    Icons.timer,
                                    size: 13,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Container(width: 1, color: Colors.grey),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Expanded(
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          StreamBuilder(
                                              stream:
                                                  kjsbSurveyorCubit.timeStream,
                                              builder: (context, snapshot) {
                                                return Text(
                                                    snapshot.hasData
                                                        ? snapshot.data
                                                            .toString()
                                                        : "Pilih Waktu",
                                                    style: TextStyle(
                                                        fontSize: 10));
                                              }),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text("WIB",
                                                style: TextStyle(fontSize: 10)),
                                          ),
                                        ]),
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 15),
              _waringCreateScedjule(),
              SizedBox(height: 20),
              Text(
                  "Geser tombol berbentuk arah panah untuk mengirim Jadwal Pengukuran ke pemohon",
                  style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
              SizedBox(height: 20),
              SlidableButton(
                  color: Color(0xffAEE9EB),
                  width: MediaQuery.of(context).size.width / 1,
                  height: 49,
                  // color: Theme.of(context).accentColor.withOpacity(0.5),
                  buttonColor: Color(0xff2CBFCD),
                  dismissible: false,
                  borderRadius: BorderRadius.circular(5),
                  label: Center(
                      child: Icon(
                    Icons.arrow_forward,
                    color: Colors.white,
                  )),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Kirim",
                            style: TextStyle(
                                fontSize: 19,
                                fontWeight: FontWeight.bold,
                                color: Colors.white))
                      ],
                    ),
                  ),
                  onChanged: (position) {
                    if (state is SetScheduledState)
                      kjsbSurveyorCubit
                          .createScheduled(state.detailEntity.kjsb);
                  }),
            ],
          ),
        ));
  }

  Widget _downloading(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(top: 20),
        child: Container(
          padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(24.0),
                  topLeft: Radius.circular(24.0)),
              boxShadow: [
                BoxShadow(
                  blurRadius: 5.0,
                  color: Colors.grey,
                ),
              ]),
          margin: const EdgeInsets.only(left: 10.0, right: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Container(
                    height: 4,
                    width: 60,
                    decoration: BoxDecoration(
                        color: Color(0xff77838F),
                        borderRadius: BorderRadius.circular(3))),
              ),
              SizedBox(height: 20),
              Text("Mengunduh . . .",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              SizedBox(height: 10),
              Text(
                  "Proses pengunduhan berkas pemohon sedang berlangsung harap menunggu",
                  style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
              SizedBox(height: 20),
            ],
          ),
        ));
  }

  Widget _hashVerify(BuildContext context, KjsbSurveyorState state) {
    return Padding(
        padding: EdgeInsets.only(top: 20),
        child: Container(
          padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(24.0),
                  topLeft: Radius.circular(24.0)),
              boxShadow: [
                BoxShadow(
                  blurRadius: 5.0,
                  color: Colors.grey,
                ),
              ]),
          margin: const EdgeInsets.only(left: 10.0, right: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Container(
                    height: 4,
                    width: 60,
                    decoration: BoxDecoration(
                        color: Color(0xff77838F),
                        borderRadius: BorderRadius.circular(3))),
              ),
              SizedBox(height: 20),
              Text("Apakah Berkas Terverifikasi?",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              SizedBox(height: 10),
              Text("Anda diberikan waktu 1x24 jam untuk verifikasi berkas.",
                  style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
              SizedBox(height: 20),
              SlidableButton(
                  color: Color(0xffEFC081),
                  width: MediaQuery.of(context).size.width / 1,
                  height: 49,
                  // color: Theme.of(context).accentColor.withOpacity(0.5),
                  buttonColor: Color(0xffFA9928),
                  dismissible: false,
                  borderRadius: BorderRadius.circular(5),
                  label: Center(
                      child: Icon(
                    Icons.arrow_forward,
                    color: Colors.white,
                  )),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Revisi",
                            style: TextStyle(
                                fontSize: 19,
                                fontWeight: FontWeight.bold,
                                color: Colors.white))
                      ],
                    ),
                  ),
                  onChanged: (position) {
                    print("A");
                  }),
              SizedBox(height: 10),
              SlidableButton(
                  color: Color(0xffAEE9EB),
                  width: MediaQuery.of(context).size.width / 1,
                  height: 49,
                  // color: Theme.of(context).accentColor.withOpacity(0.5),
                  buttonColor: Color(0xff2CBFCD),
                  dismissible: false,
                  borderRadius: BorderRadius.circular(5),
                  label: Center(
                      child: Icon(
                    Icons.arrow_forward,
                    color: Colors.white,
                  )),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Terverifikasi",
                            style: TextStyle(
                                fontSize: 19,
                                fontWeight: FontWeight.bold,
                                color: Colors.white))
                      ],
                    ),
                  ),
                  onChanged: (position) {
                    if (state is VerifyBySurveyorState)
                      context
                          .read<KjsbSurveyorCubit>()
                          .verifyBySurveyor(state.detailEntity.kjsb.id);
                  }),
            ],
          ),
        ));
  }

  _waringCreateScedjule() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
        decoration: BoxDecoration(
            color: Color(0xb7fcbebe), borderRadius: BorderRadius.circular(10)),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Icon(
              Icons.warning_amber_outlined,
              color: Colors.white,
              size: 16,
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
                child: Text(
                    "Anda diwajibkan menyelesaikan pengukuran sebelum tanggal  13 Oktober 2021",
                    style: TextStyle(
                        fontSize: 10,
                        color: Colors.black,
                        fontWeight: FontWeight.w500)))
          ],
        ),
      ),
    );
  }
}
