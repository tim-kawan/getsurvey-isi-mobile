import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_service_entity.dart';
import 'package:get_survey_app/features/kjsb/presentation/provider/kjsb_input_cubit.dart';
import 'package:get_survey_app/features/kjsb/presentation/routes/kjsb_routes_path.dart';
import 'package:get_survey_app/features/kjsb/presentation/widgets/shimmer_get_services.dart';
import 'package:no_context_navigation/no_context_navigation.dart';

class CategoriesScreen extends StatelessWidget {
  const CategoriesScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => di<KjsbInputCubit>(),
      child: Scaffold(
        body: BlocBuilder<KjsbInputCubit, KjsbInputState>(
          builder: (context, state) => Padding(
            padding: const EdgeInsets.only(left: 24, right: 24, top: 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _header(context),
                SizedBox(height: 20),
                _warning(),
                SizedBox(height: 20),
                _labelJenisLayanan(),
                Expanded(child: _gridMenuServices(context, state)),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _header(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 42, left: 0, right: 0),
      child: Row(
        children: [
          InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(
              Icons.close,
              color: Color(0xff40A0A4),
              size: 24.0,
            ),
          ),
          SizedBox(width: 10),
          Text(
            "Get - Layanan Pertanahan",
            style: TextStyle(fontSize: 18, color: Color(0xff40A0A4)),
          ),
        ],
      ),
    );
  }

  Widget _warning() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Color(0xffE3F1F6),
      ),
      padding: EdgeInsets.only(top: 14, left: 9, bottom: 14, right: 11),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(
            Icons.warning_amber_rounded,
            color: Color(0xff002741),
            size: 24.0,
            semanticLabel: 'Text to announce in accessibility modes',
          ),
          SizedBox(width: 10),
          Expanded(
            child: Text(
              "Pilih salah satu kategori layanan di bawah sesuai dengan kebutuhan anda.",
              style: TextStyle(fontSize: 11, color: Color(0xff002741)),
            ),
          ),
        ],
      ),
    );
  }

  Widget _labelJenisLayanan() {
    return Text(
      "Jenis Layanan",
      style: TextStyle(fontSize: 18),
    );
  }

  Widget _gridMenuServices(BuildContext context, KjsbInputState state) {
    if (state is KjsbInputInitial) {
      context.read<KjsbInputCubit>().getAllServices();
      return ShimmerWidget();
    } else if (state is KjsbServicesState)
      return Container(
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 6,
            mainAxisSpacing: 14,
            childAspectRatio: MediaQuery.of(context).size.width /
                (MediaQuery.of(context).size.height / 4),
          ),
          itemCount: state.listService.length,
          itemBuilder: (context, int key) {
            return createItem(context, state.listService[key]);
          },
        ),
      );
    else
      return Container();
  }

  Widget createItem(BuildContext context, KjsbServiceEntity listService) {
    return InkWell(
      onTap: () {
        navService.pushNamed(KjsbRoutesPath.FormKjsbRoute,
            args: {'service': listService}).then((value) {
          if (value != null && value) Navigator.pop(context);
        });
      },
      child: Padding(
        padding: EdgeInsets.only(left: 2, right: 2),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5),
            ),
            boxShadow: [
              BoxShadow(
                  spreadRadius: 1.1, blurRadius: 3, color: Colors.grey[200])
            ],
            color: Colors.white,
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 13, right: 10),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: Text(
                  listService.name,
                  style: TextStyle(fontSize: 9, color: Color(0xff0C6164)),
                )),
                SizedBox(width: 5),
                CachedNetworkImage(
                  height: 42,
                  width: 42,
                  imageUrl: listService.iconUrl,
                  placeholder: (context, url) => SvgPicture.asset(
                    "assets/kjsb/item_service.svg",
                    height: 32,
                    width: 32,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
