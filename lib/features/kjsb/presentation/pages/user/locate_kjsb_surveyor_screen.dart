import 'dart:async';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/transaction_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/presentation/provider/kjsb_surveyor_cubit.dart';
import 'package:get_survey_app/features/kjsb/presentation/widgets/shimmer_find_surveyor.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class HomePage extends StatefulWidget {
  final String kjsbId;

  const HomePage({Key key, this.kjsbId}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState(kjsbId);
}

class LocateKJSBSurveyorScreen extends StatelessWidget {
  final String kjsbId;

  const LocateKJSBSurveyorScreen({Key key, this.kjsbId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: HomePage(
        kjsbId: kjsbId,
      ),
    );
  }
}

class _HomePageState extends State<HomePage> {
  final String kjsbId;
  List<TransactionKjsbEntity> listSurveyor = [];
  final Completer<GoogleMapController> _controller = Completer();
  final ScrollController listScrollController = new ScrollController();
  PanelController _pc = new PanelController();
  double _panelHeightOpen;
  double _panelHeightClosed = 300.0;
  BitmapDescriptor pinLocationIcon;

  LatLng latLng = LatLng(-6.2685497, 107.0496954);

  _HomePageState(this.kjsbId);

  @override
  Widget build(BuildContext context) {
    _panelHeightOpen = MediaQuery.of(context).size.height * .6;

    return LoaderOverlay(
      useDefaultLoading: false,
      overlayWidget: Center(
        child: SpinKitCubeGrid(
          color: primaryColor,
          size: 50.0,
        ),
      ),
      child: Scaffold(
        body: BlocProvider(
          create: (context) => di<KjsbSurveyorCubit>(),
          child: BlocConsumer<KjsbSurveyorCubit, KjsbSurveyorState>(
            listener: (context, state) {
              if (state is SelectSurveyorSuccessState) {
                Navigator.of(context).pop(true);
              } else if (state is LoadingState) {
                context.loaderOverlay.show();
                Navigator.pop(context);
              } else if (state is UnLoadingState) context.loaderOverlay.hide();
            },
            builder: (context, state) => Stack(
              alignment: Alignment.topCenter,
              children: <Widget>[
                SlidingUpPanel(
                  renderPanelSheet: false,
                  controller: _pc,
                  maxHeight: _panelHeightOpen,
                  minHeight: _panelHeightClosed,
                  parallaxEnabled: false,
                  body: _body(),
                  panelBuilder: (sc) => _panel(sc),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(18.0),
                    topRight: Radius.circular(18.0),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  Widget _body() {
    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(devicePixelRatio: 2.5), 'assets/img/pin.png')
        .then((onValue) {
      pinLocationIcon = onValue;
    });

    return Stack(
      children: <Widget>[
        GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: CameraPosition(
            target: latLng,
            zoom: 14.4746,
          ),
          myLocationButtonEnabled: true,
          myLocationEnabled: false,
          padding: EdgeInsets.only(top: 110.0),
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
        ),
        _header(context)
      ],
    );
  }

  Widget _panel(ScrollController sc) {
    return Padding(
      padding: EdgeInsets.only(top: 20),
      child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(24.0),
                  topLeft: Radius.circular(24.0)),
              boxShadow: [
                BoxShadow(
                  blurRadius: 20.0,
                  color: Colors.grey,
                ),
              ]),
          margin: const EdgeInsets.only(left: 10.0, right: 10),
          child: MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: Column(
              children: [
                SizedBox(
                  height: 5.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 80,
                      height: 5,
                      decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius:
                              BorderRadius.all(Radius.circular(12.0))),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Jumlah",
                          style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 13.0,
                            color: Colors.grey[500],
                          ),
                        ),
                        BlocBuilder<KjsbSurveyorCubit, KjsbSurveyorState>(
                            builder: (context, state) {
                          int total = 0;
                          if (state is ResultGetAllKjsbSurveyorState)
                            total = state.listSurveyor.length;
                          return Text(
                            total.toString(),
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: 13.0,
                              color: Colors.grey[500],
                            ),
                          );
                        })
                      ]),
                ),
                Expanded(
                  child: BlocBuilder<KjsbSurveyorCubit, KjsbSurveyorState>(
                    builder: (context, state) {
                      if (state is GetKjsbSurveyorInitial ||
                          state is LoadingState) {
                        context
                            .read<KjsbSurveyorCubit>()
                            .getSurveyorKjsb(kjsbId);
                        return Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: ShimmerWidget(),
                        );
                      } else if (state is ResultGetAllKjsbSurveyorState) {
                        return Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: GridView.builder(
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              crossAxisSpacing: 6,
                              mainAxisSpacing: 14,
                              childAspectRatio: MediaQuery.of(context)
                                      .size
                                      .width /
                                  (MediaQuery.of(context).size.height / 1.85),
                            ),
                            itemCount: state.listSurveyor.length,
                            itemBuilder: (context, int key) {
                              return _itemSurveyor(
                                  context, state.listSurveyor[key]);
                            },
                          ),
                        );
                      }
                      return Container();
                    },
                  ),
                ),
              ],
            ),
          )),
    );
  }

  Widget _header(BuildContext context) {
    return Positioned(
      left: 10,
      right: 10,
      top: 42,
      child: Container(
        height: 48,
        child: Row(
          children: [
            InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                height: 42,
                width: 42,
                decoration: BoxDecoration(
                    color: Color(0xff44A5A9),
                    borderRadius: BorderRadius.circular(30)),
                child: Icon(
                  Icons.arrow_back_rounded,
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  style: TextStyle(
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.normal,
                      color: Colors.black),
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    prefixIcon: Padding(
                      padding: const EdgeInsetsDirectional.only(end: 0),
                      child: Icon(Icons
                          .search_rounded), // myIcon is a 48px-wide widget.
                    ),
                    contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                    hintText: 'Cari Surveyor',
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            SvgPicture.asset("assets/kjsb/filter.svg")
          ],
        ),
      ),
    );
  }

  Widget _itemSurveyor(
      BuildContext context, TransactionKjsbEntity surveyorKjsbEntity) {
    return Padding(
      padding: EdgeInsets.only(left: 3, right: 3, top: 5),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10),
          ),
          boxShadow: [
            BoxShadow(spreadRadius: 1.1, blurRadius: 4, color: Colors.grey[200])
          ],
          color: Colors.white,
        ),
        child: Padding(
          padding: EdgeInsets.only(left: 0, right: 0, top: 10),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CachedNetworkImage(
                      height: 35,
                      width: 35,
                      imageUrl: surveyorKjsbEntity.surveyorEntity.photoPath,
                      placeholder: (context, url) => SpinKitCubeGrid(
                        color: primaryColor,
                        size: 30.0,
                      ),
                    ),
                    SizedBox(width: 5),
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          surveyorKjsbEntity.surveyorEntity.name,
                          style:
                              TextStyle(fontSize: 10, color: Color(0xff0C6164)),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.star,
                              color: Colors.amber,
                              size: 14,
                            ),
                            SizedBox(
                              width: 3,
                            ),
                            Text(
                              surveyorKjsbEntity.surveyorEntity.rating,
                              style: TextStyle(fontSize: 11),
                            )
                          ],
                        )
                      ],
                    )),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Row(children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Terdaftar",
                            style: TextStyle(fontSize: 8),
                          ),
                          Text(
                            surveyorKjsbEntity.surveyorEntity.registerAt,
                            style: TextStyle(fontSize: 9),
                          ),
                        ]),
                    Expanded(child: Container()),
                    Column(children: [
                      Text(
                        "Lisensi",
                        style: TextStyle(fontSize: 8),
                      ),
                      Text(
                        surveyorKjsbEntity.surveyorEntity.totalLicences
                            .toString(),
                        style: TextStyle(fontSize: 9),
                      ),
                    ])
                  ]),
                ),
              ),
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Row(children: [
                  Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SvgPicture.asset("assets/kjsb/are.svg",
                                    width: 13, height: 13),
                                Expanded(child: SizedBox(height: 0)),
                                Text(
                                  "Rp ${surveyorKjsbEntity.landPriceTotalLabel}",
                                  style: TextStyle(fontSize: 8),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            children: [
                              SvgPicture.asset("assets/kjsb/motocycle.svg"),
                              Expanded(
                                child: SizedBox(
                                  width: 2,
                                ),
                              ),
                              Text(
                                "Rp. ${surveyorKjsbEntity.transportationPriceLabel}",
                                style: TextStyle(fontSize: 8),
                              ),
                            ],
                          ),
                        ]),
                  ),
                  SizedBox(width: 2),
                  Container(
                    width: 1,
                    height: 25,
                    color: Colors.brown[100],
                  ),
                  SizedBox(width: 3),
                  Expanded(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Total Harga",
                                style: TextStyle(fontSize: 8),
                              ),
                              SizedBox(
                                width: 3,
                              ),
                              Container(
                                child: Icon(
                                  Icons.info_outlined,
                                  size: 10,
                                  color: Colors.black,
                                ),
                              )
                            ],
                          ),
                          Text(
                            "Rp ${surveyorKjsbEntity.totalPriceLabel}",
                            style: TextStyle(fontSize: 10),
                          ),
                        ]),
                  )
                ]),
              ),
              SizedBox(height: 10),
              Row(children: [
                Expanded(
                  child: Container(
                    height: 35,
                    decoration: BoxDecoration(
                        color: Color(0xff5789BA),
                        borderRadius:
                            BorderRadius.only(bottomLeft: Radius.circular(10))),
                    child: InkWell(
                      onTap: () {},
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(12),
                              bottomLeft: Radius.circular(12)),
                          color: Color(0xff5789BA),
                        ),
                        height: 30,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Lihat Profil",
                              style: TextStyle(
                                  fontSize: 10,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 35,
                    decoration: BoxDecoration(
                        color: Color(0xff44A5A9),
                        borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10))),
                    child: InkWell(
                      onTap: () {
                        _showBottomSheet(context, surveyorKjsbEntity);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(12),
                          ),
                          color: Color(0xff44A5A9),
                        ),
                        height: 30,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Pakai Jasa",
                              style: TextStyle(
                                  fontSize: 10,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ])
            ],
          ),
        ),
      ),
    );
  }

  void _showBottomSheet(BuildContext context, TransactionKjsbEntity entity) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10)),
        ),
        context: context,
        builder: (_) {
          return Padding(
            padding: const EdgeInsets.only(left: 24, right: 24),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 2),
                Container(
                    height: 4,
                    width: 100,
                    decoration: BoxDecoration(
                        color: Color(0xff77838F),
                        borderRadius: BorderRadius.circular(3))),
                SizedBox(height: 20),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "Detail Harga ",
                      style: TextStyle(
                          color: Color(0xff3E3D3D),
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 20),
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Luas lahan (are)",
                            style: TextStyle(
                              color: Color(0xff3E3D3D),
                            ),
                          ),
                          Text(
                            entity.landSize.toString(),
                            style: TextStyle(
                              color: Color(0xff3E3D3D),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 5),
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Biaya pengukuran",
                            style: TextStyle(
                              color: Color(0xff3E3D3D),
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Rp"),
                                Text(
                                  entity.priceLandLabel,
                                  style: TextStyle(
                                    color: Color(0xff3E3D3D),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 5),
                    Container(
                      height: 1,
                      color: Colors.grey[300],
                    ),
                    SizedBox(height: 10),
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Subtotal",
                            style: TextStyle(
                                color: Color(0xff3E3D3D),
                                fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 120,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Rp"),
                                Text(
                                  entity.landPriceTotalLabel,
                                  style: TextStyle(
                                    color: Color(0xff3E3D3D),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Tarif transport",
                            style: TextStyle(
                              color: Color(0xff3E3D3D),
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Rp"),
                                Text(
                                  entity.transportationPriceLabel,
                                  style: TextStyle(
                                    color: Color(0xff3E3D3D),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      decoration: BoxDecoration(
                          color: Color(0x6644A5A9),
                          borderRadius: BorderRadius.circular(3)),
                      padding: EdgeInsets.only(
                          top: 10, bottom: 10, left: 10, right: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Total Harga",
                            style: TextStyle(
                                color: Color(0xff3E3D3D),
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 120,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Rp",
                                  style: TextStyle(
                                      color: Color(0xff3E3D3D),
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  entity.totalPriceLabel,
                                  style: TextStyle(
                                      color: Color(0xff3E3D3D),
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      height: 46,
                      child: InkWell(
                        onTap: () {
                          context
                              .read<KjsbSurveyorCubit>()
                              .selectSurveyorKjsb(kjsbId, entity);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xff8E99AF),
                          ),
                          height: 30,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Lanjut",
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                  ],
                ),
              ],
            ),
          );
        });
  }
}
