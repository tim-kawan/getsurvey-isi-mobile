import 'package:cached_network_image/cached_network_image.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_schedule_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/surveyor_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/transaction_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/presentation/provider/get_kjsb_cubit.dart';
import 'package:get_survey_app/features/kjsb/presentation/widgets/shimmer_detail_kjsb.dart';
import 'package:get_survey_app/features/payment/domain/entities/virtual_account_entity.dart';
import 'package:get_survey_app/features/payment/presentation/pages/invoice_penawaran.dart';
import 'package:get_survey_app/features/payment/presentation/pages/payment_invoice_pdf.dart';
import 'package:get_survey_app/features/payment/presentation/pages/payment_screen_va.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:shimmer/shimmer.dart';
import 'package:slidable_button/slidable_button.dart';
import 'package:sprintf/sprintf.dart';

import 'boundary_kjsb_screen.dart';
import 'locate_kjsb_surveyor_screen.dart';

class DetailKJSBScreen extends StatefulWidget {
  final String kjsbId;

  const DetailKJSBScreen({Key key, this.kjsbId}) : super(key: key);

  @override
  _DetailKJSBScreenState createState() =>
      _DetailKJSBScreenState(kjsbId: kjsbId);
}

class _DetailKJSBScreenState extends State<DetailKJSBScreen> {
  final String kjsbId;
  bool navigateToRefresh = false;
  GetKjsbCubit getKjsbCubit = di<GetKjsbCubit>();

  _DetailKJSBScreenState({Key key, this.kjsbId});

  Future<bool> _onWillPop() async {
    Navigator.of(context).pop(navigateToRefresh);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: _onWillPop,
      child: LoaderOverlay(
        useDefaultLoading: false,
        overlayWidget: Center(
          child: SpinKitCubeGrid(
            color: primaryColor,
            size: 50.0,
          ),
        ),
        child: Scaffold(
          body: BlocProvider(
            create: (_) => getKjsbCubit,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                BlocConsumer<GetKjsbCubit, GetKjsbState>(
                    listener: (context, state) {
                  if (state is LoadingState)
                    context.loaderOverlay.show();
                  else if (state is UnLoadingState)
                    context.loaderOverlay.hide();
                  else if (state is ApproveScheduledSuccessState)
                    context.loaderOverlay.hide();
                }, builder: (context, state) {
                  if (state is GetKjsbInitial)
                    context.read<GetKjsbCubit>().getKjsbById(kjsbId);
                  return SizedBox();
                }),
                _header(context),
                SizedBox(height: 20),
                BlocBuilder<GetKjsbCubit, GetKjsbState>(
                    builder: (context, state) {
                  return _document(context, state);
                }),
                Expanded(
                  child: SingleChildScrollView(
                    child: BlocBuilder<GetKjsbCubit, GetKjsbState>(
                      builder: (context, state) => Column(
                        children: [
                          _iconProcess(state),
                          _statusKJSB(context, state),
                          _warningStatus(state),
                          SizedBox(height: 20)
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _header(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 42, left: 24, right: 24),
      child: Row(
        children: [
          InkWell(
            onTap: () {
              Navigator.of(context).pop(navigateToRefresh);
            },
            child: Icon(
              Icons.arrow_back,
              color: Color(0xff40A0A4),
              size: 24.0,
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Container(),
          ),
          Text(
            "Berkas Saya",
            style: TextStyle(fontSize: 18, color: Color(0xff40A0A4)),
          ),
          Expanded(
            child: Container(),
          ),
          SvgPicture.asset("assets/kjsb/history.svg"),
        ],
      ),
    );
  }

  Widget _document(BuildContext context, GetKjsbState state) {
    return SingleChildScrollView(
      child: ExpandablePanel(
        theme: const ExpandableThemeData(
            headerAlignment: ExpandablePanelHeaderAlignment.top,
            tapBodyToCollapse: true,
            hasIcon: false),
        header: Container(
          decoration: BoxDecoration(
            color: Color(0xffE3F1F6),
          ),
          padding: EdgeInsets.only(top: 5, left: 24, bottom: 5, right: 11),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Rincian",
                style: TextStyle(fontSize: 10, color: Color(0xff343434)),
              ),
              Container(
                child: Row(
                  children: [
                    Text(
                      "Buka",
                      style: TextStyle(fontSize: 10, color: Color(0xff343434)),
                    ),
                    Icon(Icons.arrow_drop_down)
                  ],
                ),
              ),
            ],
          ),
        ),
        collapsed: Container(),
        expanded: (state is DetailKjsbState)
            ? Container(
                padding:
                    EdgeInsets.only(top: 0, left: 24, bottom: 5, right: 20),
                decoration: BoxDecoration(
                  color: Color(0xffE3F1F6),
                ),
                child: Row(
                  children: [
                    Column(
                      children: [
                        CachedNetworkImage(
                          width: 50,
                          imageUrl:
                              state.detailKjsbEntity.kjsbService.iconUrl ?? "",
                          placeholder: (context, url) => Container(
                            child: Center(
                              child: Container(
                                  width: 50,
                                  height: 50,
                                  child: SpinKitCubeGrid(
                                    color: primaryColor,
                                    size: 40.0,
                                  )),
                            ),
                            width: 50.0,
                            height: 50.0,
                          ),
                          // cacheManager: _cacheManager,
                        ),
                      ],
                    ),
                    SizedBox(width: 10),
                    Container(
                      height: 48,
                      color: Color(0xffDFEFFF),
                      width: 1,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Jenis Layanan",
                                style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.blue,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    state.detailKjsbEntity.kjsbService.name ??
                                        "",
                                    style: TextStyle(
                                        fontSize: 10, color: Color(0xff343434)),
                                  ),
                                  Text(
                                    state.detailKjsbEntity.kjsb.code,
                                    style: TextStyle(
                                        fontSize: 10, color: Color(0xff939598)),
                                  ),
                                ],
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Icon(
                                          Icons.location_on,
                                          size: 14,
                                        ),
                                        SizedBox(width: 5),
                                        Expanded(
                                          child: Text(
                                            state.detailKjsbEntity.kjsb
                                                .lokasiTanah,
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Color(0xff343434)),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Icon(
                                          Icons.aspect_ratio,
                                          size: 14,
                                        ),
                                        SizedBox(width: 5),
                                        Text(
                                          state.detailKjsbEntity.kjsb.luasTanah
                                              .toString(),
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: Color(0xff939598)),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            : Container(),
        builder: (_, collapsed, expanded) {
          return Expandable(
            collapsed: collapsed,
            expanded: expanded,
            theme: const ExpandableThemeData(crossFadePoint: 0),
          );
        },
      ),
    );
  }

  Widget _takeBoundariesKJSB(BuildContext context, String id) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _showBottomSheetTakeBoundaries(context, id);
    });

    return InkWell(
      onTap: () {
        _showBottomSheetTakeBoundaries(context, id);
      },
      child: Padding(
        padding: EdgeInsets.only(left: 24, right: 24, top: 30),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5),
            ),
            boxShadow: [
              BoxShadow(
                  spreadRadius: 0.3, blurRadius: 4, color: Colors.grey[400])
            ],
            color: Colors.white,
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 13, right: 10, top: 10, bottom: 10),
            child: Column(
              children: [
                Row(
                  children: <Widget>[
                    Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                          color: Color(0xff4A566E),
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    SizedBox(width: 5),
                    Expanded(
                        child: Text(
                      "Upload foto tanda batas tanah anda",
                      style: TextStyle(fontSize: 11, color: Color(0xff717171)),
                    )),
                  ],
                ),
                SizedBox(height: 10),
                Text(
                  "Segera upload foto tanda batas tanah Anda sebelum tanggal 30 Sept 2021 (21:00)",
                  style: TextStyle(
                      color: Color(0xff717171),
                      fontSize: 12,
                      fontWeight: FontWeight.w500),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _findSurveyorKJSB(BuildContext context, FindSurveyorState state) {
    return InkWell(
      onTap: () {
        _showBottomSheetTakeFindSurveyor(context);
      },
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(left: 24, right: 24, top: 30),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                  bottomLeft: Radius.circular(5),
                  bottomRight: Radius.circular(5),
                ),
                boxShadow: [
                  BoxShadow(
                      spreadRadius: 0.3, blurRadius: 4, color: Colors.grey[400])
                ],
                color: Colors.white,
              ),
              child: Padding(
                padding:
                    EdgeInsets.only(left: 13, right: 10, top: 10, bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: <Widget>[
                        Container(
                          height: 10,
                          width: 10,
                          decoration: BoxDecoration(
                              color: Color(0xff4A566E),
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        SizedBox(width: 5),
                        Expanded(
                            child: Text(
                          "Tanda Batas Tersimpan",
                          style:
                              TextStyle(fontSize: 11, color: Color(0xff717171)),
                        )),
                      ],
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Foto tanda batas berhasil di unggah, silahkan lakukan pemilihan surveyor sekarang",
                      style: TextStyle(
                          color: Color(0xff717171),
                          fontSize: 12,
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Cari Surveyor",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff44A5A9)),
                    ),
                  ],
                ),
              ),
            ),
          ),
          _sendBoundariesFiles(state.numberBorder)
        ],
      ),
    );
  }

  Widget _iconProcess(GetKjsbState state) {
    if (state is WaitPaymentState) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 51, 0, 0),
        child: Container(
            height: 194,
            child:
                Center(child: SvgPicture.asset("assets/kjsb/waiting_pay.svg"))),
      );
    } else if (state is WaitApproveBySurveyorState) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 51, 0, 0),
        child: Container(
            height: 194,
            child: Center(
                child: SvgPicture.asset("assets/kjsb/wait_approve.svg"))),
      );
    } else if (state is VerifyBySurveyorState) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 51, 0, 0),
        child: Container(
            height: 194,
            child: Center(
                child: SvgPicture.asset("assets/kjsb/confirm_surveyor.svg"))),
      );
    } else if (state is WaitScheduledState) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 51, 0, 0),
        child: Container(
            height: 194,
            child: Center(
                child: SvgPicture.asset("assets/kjsb/wait_schedule.svg"))),
      );
    } else if (state is WaitApproveScheduledState) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 51, 0, 0),
        child: Container(
            height: 194,
            child: Center(
                child: SvgPicture.asset("assets/kjsb/confirm_schedule.svg"))),
      );
    } else if (state is ScheduledApprovedState) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 51, 0, 0),
        child: Container(
            height: 194,
            child: Center(
                child: SvgPicture.asset("assets/kjsb/schedule_success.svg"))),
      );
    } else if (state is TakeBoundariesState || state is FindSurveyorState) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 51, 0, 0),
        child: Container(
            height: 194,
            child:
                Center(child: SvgPicture.asset("assets/kjsb/upload_sign.svg"))),
      );
    } else if (state is WaitLiveLocationState) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 51, 0, 0),
        child: Container(
            height: 194,
            child: Center(
                child:
                    SvgPicture.asset("assets/kjsb/wait_share_location.svg"))),
      );
    } else {
      return Padding(
          padding: const EdgeInsets.fromLTRB(0, 51, 0, 0),
          child: Shimmer.fromColors(
            baseColor: Colors.grey[500],
            highlightColor: Colors.grey[100],
            child: Container(
              height: 194,
              width: 194,
              decoration: BoxDecoration(
                  color: backGround.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(16.0),
                  boxShadow: [
                    BoxShadow(
                        color: backGround.withOpacity(0.1),
                        blurRadius: 0.5,
                        offset: Offset(0, 1))
                  ]),
            ),
          ));
    }
  }

  _warning() {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24, top: 30),
      child: Container(
        padding: EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15),
        decoration: BoxDecoration(
            color: Color(0xffFCBEBE), borderRadius: BorderRadius.circular(10)),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Icon(
              Icons.warning_amber_outlined,
              color: Colors.white,
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
                child: Text(
                    "Saya setuju apabila di kemudian hari terdapat perbedaan luasan tanah, maka saya bersedia untuk membayar sisa luasan tanah tersebut",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w500)))
          ],
        ),
      ),
    );
  }

  _warningVerified() {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24, top: 30),
      child: Container(
        padding: EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15),
        decoration: BoxDecoration(
            color: Color(0xffFCBEBE), borderRadius: BorderRadius.circular(10)),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Icon(
              Icons.warning_amber_outlined,
              color: Colors.white,
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
                child: Text(
                    "PERHATIAN! Waspada terhadap pihak-pihak yang mengatasnamakan Get-Survey untuk bertransaksi di luar aplikasi Get-Survey. Pastikan Anda mentransfer dana hanya ke rekening resmi milik PT. Get-Survey",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w500)))
          ],
        ),
      ),
    );
  }

  void _showBottomSheetTakeBoundaries(BuildContext context, String id) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10)),
        ),
        context: context,
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.only(left: 24, right: 24),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 2),
                Container(
                    height: 4,
                    width: 100,
                    decoration: BoxDecoration(
                        color: Color(0xff77838F),
                        borderRadius: BorderRadius.circular(3))),
                SizedBox(height: 20),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "Foto tanda batas",
                      style: TextStyle(
                          color: Color(0xff3E3D3D),
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 20),
                    Text(
                      "Untuk melanjutkan, foto dulu tanda batas tanah Anda.",
                      style: TextStyle(
                        color: Color(0xff3E3D3D),
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      height: 46,
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BoundaryKJSBScreen(
                                        kjsbId: id,
                                      ))).then((value) {
                            if (value != null && value)
                              navigateToRefresh = true;
                            context
                                .read<GetKjsbCubit>()
                                .getKjsb(int.parse(kjsbId));
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xff8E99AF),
                          ),
                          height: 30,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Lanjut",
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                  ],
                ),
              ],
            ),
          );
        });
  }

  void _showBottomSheetTakeFindSurveyor(BuildContext context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10)),
        ),
        context: context,
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.only(left: 24, right: 24),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 2),
                Container(
                    height: 4,
                    width: 100,
                    decoration: BoxDecoration(
                        color: Color(0xff77838F),
                        borderRadius: BorderRadius.circular(3))),
                SizedBox(height: 20),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "Cari Surveyor",
                      style: TextStyle(
                          color: Color(0xff3E3D3D),
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 20),
                    Text(
                      "Untuk melanjutkan, silahkan cari dan pilih surveyor Anda.",
                      style: TextStyle(
                        color: Color(0xff3E3D3D),
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      height: 46,
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      LocateKJSBSurveyorScreen(
                                        kjsbId: kjsbId,
                                      ))).then((value) {
                            if (value != null && value) {
                              navigateToRefresh = value;
                              getKjsbCubit.getKjsbById(kjsbId);
                              setState(() {});
                            }
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xff8E99AF),
                          ),
                          height: 30,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Lanjut",
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                  ],
                ),
              ],
            ),
          );
        });
  }

  void _showBottomSheetApproveSchedule(
      BuildContext context, KjsbScheduleEntity schedule) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10)),
        ),
        context: context,
        builder: (_) {
          return SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(left: 24, right: 24),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: 2),
                  Container(
                      height: 4,
                      width: 100,
                      decoration: BoxDecoration(
                          color: Color(0xff77838F),
                          borderRadius: BorderRadius.circular(3))),
                  SizedBox(height: 10),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Center(
                        child: Text(
                          "Jadwal Pengukuran",
                          style: TextStyle(
                              color: Color(0xff3E3D3D),
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(height: 10),
                      Center(
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                schedule.startDateLabel,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff3E3D3D),
                                ),
                              ),
                              Text(
                                schedule.timeLabel,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff3E3D3D),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Container(
                        height: 1,
                        color: Color(0xff707070),
                      ),
                      SizedBox(height: 10),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Color(0xdff719c),
                        ),
                        child: Column(children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("1. "),
                              Expanded(
                                  child: Text(
                                      "Pemohon bersedia hadir sesuai jadwal diatas")),
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("2. "),
                              Expanded(
                                  child: Text(
                                      "Pemohon bersedia untuk menghadirkan tetangga dan saksi ukur (Kades dan 2 perangkat desa/kelurahan)")),
                            ],
                          ),
                        ]),
                      ),
                      SizedBox(height: 5),
                      StreamBuilder(
                          stream: getKjsbCubit.agreeKjsbScheduleStream,
                          builder: (context, snapshot) {
                            return InkWell(
                              onTap: () {
                                getKjsbCubit.agreeWithScheduled(
                                    (snapshot.hasData) ? !snapshot.data : true);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Checkbox(
                                      value: (snapshot.hasData)
                                          ? snapshot.data
                                          : false),
                                  Expanded(
                                      child: Text(
                                          "Saya telah membaca dan menyetujui persyaratan"))
                                ],
                              ),
                            );
                          }),
                      SizedBox(height: 10),
                      SlidableButton(
                          color: Color(0xffEFC081),
                          width: MediaQuery.of(context).size.width / 1,
                          height: 49,
                          // color: Theme.of(context).accentColor.withOpacity(0.5),
                          buttonColor: Color(0xffFA9928),
                          dismissible: false,
                          borderRadius: BorderRadius.circular(5),
                          label: Center(
                              child: Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          )),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Ganti Jadwal",
                                    style: TextStyle(
                                        fontSize: 19,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white))
                              ],
                            ),
                          ),
                          onChanged: (position) {}),
                      SizedBox(height: 10),
                      StreamBuilder(
                          stream: getKjsbCubit.agreeKjsbScheduleStream,
                          builder: (_, snapshot) {
                            return Container(
                              height: 46,
                              child: InkWell(
                                onTap: () {
                                  if (snapshot.hasData && snapshot.data) {
                                    context
                                        .read<GetKjsbCubit>()
                                        .approveSchedule(schedule.id);
                                    Navigator.of(context).pop();
                                  }
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: (snapshot.hasData && snapshot.data)
                                        ? Color(0xff2CBFCD)
                                        : Color(0x652cbfcd),
                                  ),
                                  height: 30,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Lanjut",
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }),
                      SizedBox(height: 20),
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget _statusKJSB(BuildContext context, GetKjsbState state) {
    if (state is TakeBoundariesState)
      return _takeBoundariesKJSB(context, state.detailKjsbEntity.kjsb.id);
    if (state is FindSurveyorState)
      return _findSurveyorKJSB(context, state);
    else if (state is WaitApproveBySurveyorState)
      return _waitApprove(
          state.detailKjsbEntity.kjsb, state.detailKjsbEntity.surveyor);
    else if (state is VerifyBySurveyorState)
      return _verifyBySurveyor(
          state.detailKjsbEntity.kjsb, state.detailKjsbEntity.surveyor);
    else if (state is WaitPaymentState)
      return _waitPayment(
          context,
          state.detailKjsbEntity.kjsb,
          state.detailKjsbEntity.surveyor,
          state.detailKjsbEntity.transaction,
          state.remainingTimeToPay,
          state.detailKjsbEntity.virtualAccount);
    else if (state is HasPaidState)
      return _hasPaid(context);
    else if (state is WaitScheduledState)
      return _waitSchedule(context, state);
    else if (state is WaitApproveScheduledState)
      return _confirmSchedule(context, state);
    else if (state is ScheduledApprovedState)
      return _approveSchedule(context, state);
    else if (state is WaitLiveLocationState)
      return _waitLiveLocation(context, state);
    else
      return ShimmerWidget();
  }

  Widget _waitApprove(KjsbEntity kjsb, SurveyorEntity surveyor) {
    return InkWell(
      onTap: () {},
      child: Padding(
        padding: EdgeInsets.only(left: 24, right: 24, top: 30),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5),
            ),
            boxShadow: [
              BoxShadow(
                  spreadRadius: 0.3, blurRadius: 4, color: Colors.grey[400])
            ],
            color: Colors.white,
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 13, right: 10, top: 10, bottom: 10),
            child: Column(
              children: [
                Row(
                  children: <Widget>[
                    Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                          color: Color(0xffF5C974),
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    SizedBox(width: 5),
                    Expanded(
                        child: Text(
                      "Menunggu Konfirmasi Surveyor",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff717171)),
                    )),
                  ],
                ),
                SizedBox(height: 10),
                Text(
                  "Harap menunggu surveyor mengkonfirmasi permohonan anda",
                  style: TextStyle(
                      color: Color(0xffC3BFBF),
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10),
                Container(
                  height: 1,
                  color: Colors.black12,
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    CachedNetworkImage(
                      width: 50,
                      imageUrl: surveyor.photoPath ?? "",
                      placeholder: (context, url) => Container(
                        child: Center(
                          child: Container(
                              width: 50,
                              height: 50,
                              child: SpinKitCubeGrid(
                                color: primaryColor,
                                size: 40.0,
                              )),
                        ),
                        width: 50.0,
                        height: 50.0,
                      ),
                      imageBuilder: (context, provider) => Container(
                        width: 50.0,
                        height: 50.0,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: provider,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                          border: Border.all(
                            color: Colors.white,
                            width: 2.0,
                          ),
                        ),
                      ),
                      // cacheManager: _cacheManager,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            surveyor.name ?? "",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 15),
                          ),
                          Row(
                            children: [
                              Icon(Icons.star_outlined,
                                  size: 18, color: Colors.amber),
                              Text("4.8")
                            ],
                          )
                        ],
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Terdaftar",
                          style:
                              TextStyle(fontSize: 12, color: Color(0xffACB1C0)),
                        ),
                        Text(surveyor.registerAt,
                            style: TextStyle(
                                fontSize: 13, color: Color(0xff222B45)))
                      ],
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Column(
                      children: [
                        Text(
                          "Lisensi",
                          style:
                              TextStyle(fontSize: 12, color: Color(0xffACB1C0)),
                        ),
                        Text(
                          "3",
                          style:
                              TextStyle(fontSize: 13, color: Color(0xff222B45)),
                        )
                      ],
                    ),
                    SizedBox(
                      width: 5,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _verifyBySurveyor(KjsbEntity entity, SurveyorEntity surveyor) {
    return InkWell(
      onTap: () {},
      child: Padding(
        padding: EdgeInsets.only(left: 24, right: 24, top: 30),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5),
            ),
            boxShadow: [
              BoxShadow(
                  spreadRadius: 0.3, blurRadius: 4, color: Colors.grey[400])
            ],
            color: Colors.white,
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 13, right: 10, top: 10, bottom: 10),
            child: Column(
              children: [
                Row(
                  children: <Widget>[
                    Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                          color: Color(0xffF5C974),
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    SizedBox(width: 5),
                    Expanded(
                        child: Text(
                      "Proses Verifikasi Berkas",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff717171)),
                    )),
                  ],
                ),
                SizedBox(height: 10),
                Text(
                  "Permohonan diterima, berkas Anda sedang dalam tahap verifikasi harap menunggu 1 x 24 Jam",
                  style: TextStyle(
                      color: Color(0xffC3BFBF),
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10),
                Container(
                  height: 1,
                  color: Colors.black12,
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    CachedNetworkImage(
                      width: 50,
                      imageUrl: surveyor.photoPath ?? "",
                      placeholder: (context, url) => Container(
                        child: Center(
                          child: Container(
                              width: 50,
                              height: 50,
                              child: SpinKitCubeGrid(
                                color: primaryColor,
                                size: 40.0,
                              )),
                        ),
                        width: 50.0,
                        height: 50.0,
                      ),
                      imageBuilder: (context, provider) => Container(
                        width: 50.0,
                        height: 50.0,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: provider,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                          border: Border.all(
                            color: Colors.white,
                            width: 2.0,
                          ),
                        ),
                      ),
                      // cacheManager: _cacheManager,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            surveyor.name ?? "",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 15),
                          ),
                          Row(
                            children: [
                              Icon(Icons.star_outlined,
                                  size: 18, color: Colors.amber),
                              Text("4.8")
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(child: SvgPicture.asset("assets/kjsb/phone.svg")),
                    SizedBox(
                      width: 20,
                    ),
                    Container(
                      height: 20,
                      width: 1,
                      color: Colors.black12,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Container(child: SvgPicture.asset("assets/kjsb/chat.svg")),
                    SizedBox(
                      width: 15,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _hasPaid(BuildContext context) {
    return Column(
      children: [_infoPayment(), _infoDetailPayment(context, null, null, null)],
    );
  }

  Widget _waitPayment(
      BuildContext context,
      KjsbEntity entity,
      SurveyorEntity surveyor,
      TransactionKjsbEntity transaction,
      int remainingTimeToPay,
      VirtualAccountEntity payment) {
    return Column(
      children: [
        _infoTimePayment(surveyor, remainingTimeToPay),
        _infoDetailPayment(context, entity, transaction, payment)
      ],
    );
  }

  Widget _waitSchedule(BuildContext context, ScheduledState state) {
    return Column(
      children: [
        _infoWaitSchedule(context, state),
        _invoiceInfo(context, state)
      ],
    );
  }

  Widget _confirmSchedule(BuildContext context, ScheduledState state) {
    return Column(
      children: [
        _infoConfirmSchedule(context, state),
        _invoiceInfo(context, state)
      ],
    );
  }

  Widget _approveSchedule(BuildContext context, ScheduledState state) {
    return Column(
      children: [
        _onConfirmSchedule(context, state),
        _scheduleInfo(),
        _invoiceInfo(context, state),
      ],
    );
  }

  Widget _waitLiveLocation(BuildContext context, ScheduledState state) {
    return Column(
      children: [
        _infoWaitLiveLocation(context, state),
        _sendBoundariesFiles(0),
        _scheduleInfo(),
        _invoiceInfo(context, state),
      ],
    );
  }

  Widget _warningStatus(GetKjsbState state) {
    if (state is VerifyBySurveyorState) {
      return _warningVerified();
    } else {
      return _warning();
    }
  }

  Widget _infoPayment() {
    return InkWell(
      onTap: () {},
      child: Padding(
        padding: EdgeInsets.only(left: 24, right: 24, top: 30),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5),
            ),
            boxShadow: [
              BoxShadow(
                  spreadRadius: 0.3, blurRadius: 4, color: Colors.grey[400])
            ],
            color: Colors.white,
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 13, right: 10, top: 10, bottom: 10),
            child: Column(
              children: [
                Row(
                  children: <Widget>[
                    Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                          color: Color(0xffFF719C),
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    SizedBox(width: 5),
                    Expanded(
                        child: Text(
                      "Menunggu Verifikasi Pembayaran",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff717171)),
                    )),
                  ],
                ),
                SizedBox(height: 10),
                Text(
                  "Pembayaran Anda sudah kami terima, kami sedang melakukan verifikasi pembayaran anda",
                  style: TextStyle(
                      color: Color(0xffC3BFBF),
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10),
                Container(
                  height: 1,
                  color: Colors.black12,
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Image.asset(
                      "assets/logo.png",
                      height: 40,
                      width: 40,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Zaenal Abidin",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 15),
                          ),
                          Row(
                            children: [
                              Icon(Icons.star_outlined,
                                  size: 18, color: Colors.amber),
                              Text("4.8")
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(child: SvgPicture.asset("assets/kjsb/phone.svg")),
                    SizedBox(
                      width: 20,
                    ),
                    Container(
                      height: 20,
                      width: 1,
                      color: Colors.black12,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Container(child: SvgPicture.asset("assets/kjsb/chat.svg")),
                    SizedBox(
                      width: 15,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _infoWaitSchedule(BuildContext context, WaitScheduledState state) {
    return InkWell(
      onTap: () {},
      child: Padding(
        padding: EdgeInsets.only(left: 24, right: 24, top: 30),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5),
            ),
            boxShadow: [
              BoxShadow(
                  spreadRadius: 0.3, blurRadius: 4, color: Colors.grey[400])
            ],
            color: Colors.white,
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 13, right: 10, top: 10, bottom: 10),
            child: Column(
              children: [
                Row(
                  children: <Widget>[
                    Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                          color: Color(0xffFFAA38),
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    SizedBox(width: 5),
                    Expanded(
                        child: Text(
                      "Menunggu Jadwal Pengukuran",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff717171)),
                    )),
                  ],
                ),
                SizedBox(height: 10),
                Text(
                  "Surveyor akan mengirim jadwal pengukuran paling lambat pada tanggal 10 Okt 2021 (21:00)",
                  style: TextStyle(
                      color: Color(0xffC3BFBF),
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10),
                Container(
                  height: 1,
                  color: Colors.black12,
                ),
                SizedBox(height: 10),
                _actionInfoSurveyor(state)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _infoConfirmSchedule(BuildContext context, ScheduledState state) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _showBottomSheetApproveSchedule(
          context, state.detailKjsbEntity.schedules);
    });
    return InkWell(
      onTap: () {
        _showBottomSheetApproveSchedule(
            context, state.detailKjsbEntity.schedules);
      },
      child: Padding(
        padding: EdgeInsets.only(left: 24, right: 24, top: 30),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5),
            ),
            boxShadow: [
              BoxShadow(
                  spreadRadius: 0.3, blurRadius: 4, color: Colors.grey[400])
            ],
            color: Colors.white,
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 13, right: 10, top: 10, bottom: 10),
            child: Column(
              children: [
                Row(
                  children: <Widget>[
                    Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                          color: Color(0xffCC7C11),
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    SizedBox(width: 5),
                    Expanded(
                        child: Text(
                      "Konfirmasi Jadwal Pengukuran",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff717171)),
                    )),
                  ],
                ),
                SizedBox(height: 10),
                Text(
                  "Harap segera mengkonfirmasi jadwal pengukuran yang telah di kirim oleh surveyor",
                  style: TextStyle(
                      color: Color(0xffC3BFBF),
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10),
                Container(
                  height: 1,
                  color: Colors.black12,
                ),
                SizedBox(height: 10),
                _actionInfoSurveyor(state)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _infoWaitLiveLocation(BuildContext context, ScheduledState state) {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24, top: 30),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(5),
            topRight: Radius.circular(5),
            bottomLeft: Radius.circular(5),
            bottomRight: Radius.circular(5),
          ),
          boxShadow: [
            BoxShadow(spreadRadius: 0.3, blurRadius: 4, color: Colors.grey[400])
          ],
          color: Colors.white,
        ),
        child: Padding(
          padding: EdgeInsets.only(left: 13, right: 10, top: 10, bottom: 10),
          child: Column(
            children: [
              Row(
                children: <Widget>[
                  Container(
                    height: 10,
                    width: 10,
                    decoration: BoxDecoration(
                        color: Color(0xffe38731),
                        borderRadius: BorderRadius.circular(10)),
                  ),
                  SizedBox(width: 5),
                  Expanded(
                      child: Text(
                    "Menunggu Live Location",
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff717171)),
                  )),
                ],
              ),
              SizedBox(height: 10),
              Text(
                "Surveyor akan mengirim live location paling lambat jam 10:00",
                style: TextStyle(
                    color: Color(0xffC3BFBF),
                    fontSize: 12,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              Container(
                height: 1,
                color: Colors.black12,
              ),
              SizedBox(height: 10),
              _actionInfoSurveyor(state)
            ],
          ),
        ),
      ),
    );
  }

  Widget _onConfirmSchedule(BuildContext context, ScheduledState state) {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24, top: 30),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(5),
            topRight: Radius.circular(5),
            bottomLeft: Radius.circular(5),
            bottomRight: Radius.circular(5),
          ),
          boxShadow: [
            BoxShadow(spreadRadius: 0.3, blurRadius: 4, color: Colors.grey[400])
          ],
          color: Colors.white,
        ),
        child: Padding(
          padding: EdgeInsets.only(left: 13, right: 10, top: 10, bottom: 10),
          child: Column(
            children: [
              Row(
                children: <Widget>[
                  Container(
                    height: 10,
                    width: 10,
                    decoration: BoxDecoration(
                        color: Color(0xff2CBFCD),
                        borderRadius: BorderRadius.circular(10)),
                  ),
                  SizedBox(width: 5),
                  Expanded(
                      child: Text(
                    "Jadwal Pengukuran Disetujui",
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff717171)),
                  )),
                ],
              ),
              SizedBox(height: 10),
              Text(
                "Jadwal pengukuran telah   ditentukan  oleh surveyor dan pemohon",
                style: TextStyle(
                    color: Color(0xffC3BFBF),
                    fontSize: 12,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              Container(
                height: 1,
                color: Colors.black12,
              ),
              SizedBox(height: 10),
              _actionInfoSurveyor(state)
            ],
          ),
        ),
      ),
    );
  }

  Widget _infoTimePayment(SurveyorEntity surveyor, int remainingTimeToPay) {
    return InkWell(
      onTap: () {},
      child: Padding(
        padding: EdgeInsets.only(left: 24, right: 24, top: 30),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5),
            ),
            boxShadow: [
              BoxShadow(
                  spreadRadius: 0.3, blurRadius: 4, color: Colors.grey[400])
            ],
            color: Colors.white,
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 13, right: 10, top: 10, bottom: 10),
            child: Column(
              children: [
                Row(
                  children: <Widget>[
                    Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                          color: Color(0xffFF719C),
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    SizedBox(width: 5),
                    Expanded(
                        child: Text(
                      "Menunggu Pembayaran",
                      style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff717171)),
                    )),
                    CountdownTimer(
                      widgetBuilder: (_, CurrentRemainingTime time) {
                        if (time == null) {
                          return Text("Waktu Pembaran Habis",
                              style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red));
                        }
                        return Text(
                            (time.days != null)
                                ? sprintf(
                                    "%01i Hari %02i Jam %02i menit %02i detik",
                                    [
                                        time.days ?? 0,
                                        time.hours ?? 0,
                                        time.min ?? 0,
                                        time.sec ?? 0
                                      ])
                                : sprintf("%02i Jam, %02i menit, %02i detik", [
                                    time.hours ?? 0,
                                    time.min ?? 0,
                                    time.sec ?? 0
                                  ]),
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                                color: Colors.red));
                      },
                      endTime: remainingTimeToPay,
                      textStyle: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff717171)),
                      endWidget: Text("Waktu Pembaran Selesai",
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Colors.red)),
                      onEnd: () {},
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Text(
                  "Berkas Anda terverifikasi silahkan pilih \"Lihat Quotation\" untuk melanjutkan transaksi",
                  style: TextStyle(
                      color: Color(0xffC3BFBF),
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10),
                Container(
                  height: 1,
                  color: Colors.black12,
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    CachedNetworkImage(
                      width: 50,
                      imageUrl: surveyor.photoPath ?? "",
                      placeholder: (context, url) => Container(
                        child: Center(
                          child: Container(
                              width: 50,
                              height: 50,
                              child: SpinKitCubeGrid(
                                color: primaryColor,
                                size: 40.0,
                              )),
                        ),
                        width: 50.0,
                        height: 50.0,
                      ),
                      imageBuilder: (context, provider) => Container(
                        width: 50.0,
                        height: 50.0,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: provider,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                          border: Border.all(
                            color: Colors.white,
                            width: 2.0,
                          ),
                        ),
                      ),
                      // cacheManager: _cacheManager,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            surveyor.name ?? "",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 15),
                          ),
                          SizedBox(width: 5),
                          Row(
                            children: [
                              Icon(Icons.star_outlined,
                                  size: 18, color: Colors.amber),
                              Text("4.8")
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(child: SvgPicture.asset("assets/kjsb/phone.svg")),
                    SizedBox(
                      width: 20,
                    ),
                    Container(
                      height: 20,
                      width: 1,
                      color: Colors.black12,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Container(child: SvgPicture.asset("assets/kjsb/chat.svg")),
                    SizedBox(
                      width: 15,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _infoDetailPayment(BuildContext context, KjsbEntity kjsb,
      TransactionKjsbEntity transaction, VirtualAccountEntity payment) {
    return InkWell(
      onTap: () {
        if (kjsb.status == KjsbEntity.waitingPayment)
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Invoice(
                        kjsb: kjsb,
                        transaction: transaction,
                      )));
        else if (kjsb.status == KjsbEntity.paymentGatewayCreated)
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PaymentScreenVa(
                        kjsbEntity: kjsb,
                        transaction: transaction,
                        bankIndex: 1,
                        vaEntity: payment,
                      )));
      },
      child: Padding(
        padding: EdgeInsets.only(left: 24, right: 24, top: 30),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5),
            ),
            boxShadow: [
              BoxShadow(
                  spreadRadius: 0.3, blurRadius: 4, color: Colors.grey[400])
            ],
            color: Colors.white,
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 13, right: 10, top: 10, bottom: 10),
            child: Column(
              children: [
                Row(
                  children: <Widget>[
                    Container(
                      width: 130,
                      child: Text(
                        "Jenis Layanan",
                        style:
                            TextStyle(fontSize: 12, color: Color(0xff717171)),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        kjsb.serviceName ?? "",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff717171)),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  children: <Widget>[
                    Container(
                      width: 130,
                      child: Text(
                        "Jumlah Bidang",
                        style:
                            TextStyle(fontSize: 12, color: Color(0xff717171)),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "1",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff717171)),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  children: <Widget>[
                    Container(
                      width: 130,
                      child: Text(
                        "Total Harga",
                        style:
                            TextStyle(fontSize: 12, color: Color(0xff717171)),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "Rp. ${transaction.totalPriceLabel}",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff717171)),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: 130,
                      child: Text(
                        "Lokasi Tanah",
                        style:
                            TextStyle(fontSize: 12, color: Color(0xff717171)),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        "${kjsb.lokasiTanah})",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff717171)),
                      ),
                    )
                  ],
                ),
                SizedBox(height: 10),
                Container(
                  height: 1,
                  color: Colors.black12,
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        "Lihat Quotation",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 12,
                            color: Color(0xff2CBFCD),
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.black26,
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _sendBoundariesFiles(int numberBorder) {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24, top: 9),
      child: Container(
        decoration: BoxDecoration(
            color: Color(0xffF4F8FC), borderRadius: BorderRadius.circular(10)),
        padding: EdgeInsets.only(left: 20, right: 25, top: 6, bottom: 6),
        child: Column(
          children: [
            Row(
              children: [
                Column(
                  children: [
                    SizedBox(height: 5),
                    SvgPicture.asset(
                      "assets/kjsb/send_boudaries_icon.svg",
                      height: 42,
                      width: 42,
                    )
                  ],
                ),
                SizedBox(width: 10),
                Container(
                  height: 48,
                  color: Color(0xffDFEFFF),
                  width: 1,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Foto Tanda Batas",
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Terkirim",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                                color: Color(0xff44A5A9)),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Jumlah",
                                    style: TextStyle(
                                        fontSize: 10, color: Color(0xff343434)),
                                  ),
                                  Text(
                                    "30 Sept 2021",
                                    style: TextStyle(
                                        fontSize: 10, color: Color(0xff939598)),
                                  ),
                                ],
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    numberBorder.toString(),
                                    style: TextStyle(
                                        fontSize: 10, color: Color(0xff343434)),
                                  ),
                                  Text(
                                    "15:12",
                                    style: TextStyle(
                                        fontSize: 10, color: Color(0xff939598)),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Container(
                              padding: EdgeInsets.only(top: 10),
                              child: Text("Lihat",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                      color: Color(0xff44A5A9)))),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _invoiceInfo(BuildContext context, ScheduledState state) {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24, top: 9),
      child: Container(
        decoration: BoxDecoration(
            color: Color(0xffF4F8FC), borderRadius: BorderRadius.circular(10)),
        padding: EdgeInsets.only(left: 20, right: 25, top: 6, bottom: 6),
        child: Column(
          children: [
            Row(
              children: [
                Column(
                  children: [
                    SizedBox(height: 5),
                    CachedNetworkImage(
                      width: 42,
                      imageUrl:
                          state.detailKjsbEntity.kjsbService.iconUrl ?? "",
                      placeholder: (context, url) => Container(
                        child: Center(
                          child: Container(
                              width: 42,
                              height: 42,
                              child: SpinKitCubeGrid(
                                color: primaryColor,
                                size: 30.0,
                              )),
                        ),
                        width: 42.0,
                        height: 42.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          border: Border.all(
                            color: Colors.white,
                            width: 2.0,
                          ),
                        ),
                      ),
                      // cacheManager: _cacheManager,
                    ),
                  ],
                ),
                SizedBox(width: 10),
                Container(
                  height: 48,
                  color: Color(0xffDFEFFF),
                  width: 1,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            state.detailKjsbEntity.kjsb.serviceName,
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Terbayar",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                                color: Color(0xff44A5A9)),
                          ),
                        ],
                      ),
                      SizedBox(height: 5),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "INVOICE ${state.detailKjsbEntity.kjsb.code}",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 10,
                                        color: Color(0xff44A5A9)),
                                  ),
                                  Text(
                                    state.detailKjsbEntity.virtualAccount
                                        .paymentDateLabel,
                                    style: TextStyle(
                                        fontSize: 10, color: Color(0xff939598)),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => PaymentInvoicePdf(
                                        kjsb: state.detailKjsbEntity.kjsb,
                                      )));
                            },
                            child: Container(
                                child: Text("Lihat",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xff44A5A9)))),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _scheduleInfo() {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24, top: 9),
      child: Container(
        decoration: BoxDecoration(
            color: Color(0xffF4F8FC), borderRadius: BorderRadius.circular(10)),
        padding: EdgeInsets.only(left: 20, right: 25, top: 6, bottom: 6),
        child: Column(
          children: [
            Row(
              children: [
                Column(
                  children: [
                    SizedBox(height: 5),
                    SvgPicture.asset(
                      "assets/kjsb/on_schedule.svg",
                      height: 42,
                      width: 42,
                    )
                  ],
                ),
                SizedBox(width: 10),
                Container(
                  height: 48,
                  color: Color(0xffDFEFFF),
                  width: 1,
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Jadwal Pengukuran",
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Disteujui",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                                color: Color(0xff44A5A9)),
                          ),
                        ],
                      ),
                      SizedBox(height: 5),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "20 Agustus 20201",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 10,
                                        color: Color(0xff44A5A9)),
                                  ),
                                  Text(
                                    "10:30 WIB",
                                    style: TextStyle(
                                        fontSize: 10, color: Color(0xff939598)),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _actionInfoSurveyor(ScheduledState state) {
    return Row(
      children: [
        CachedNetworkImage(
          width: 42,
          imageUrl: state.detailKjsbEntity.surveyor.photoPath ?? "",
          placeholder: (context, url) => Container(
            child: Center(
              child: Container(
                  width: 42,
                  height: 42,
                  child: SpinKitCubeGrid(
                    color: primaryColor,
                    size: 40.0,
                  )),
            ),
            width: 42.0,
            height: 42.0,
          ),
          imageBuilder: (context, provider) => Container(
            width: 42.0,
            height: 42.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: provider,
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.circular(10.0),
              border: Border.all(
                color: Colors.white,
                width: 2.0,
              ),
            ),
          ),
          // cacheManager: _cacheManager,
        ),
        SizedBox(
          width: 5,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                state.detailKjsbEntity.surveyor.name ?? "",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 15),
              ),
              Row(
                children: [
                  Icon(Icons.star_outlined, size: 18, color: Colors.amber),
                  Text("4.8")
                ],
              )
            ],
          ),
        ),
        Container(child: SvgPicture.asset("assets/kjsb/phone.svg")),
        SizedBox(
          width: 20,
        ),
        Container(
          height: 20,
          width: 1,
          color: Colors.black12,
        ),
        SizedBox(
          width: 20,
        ),
        Container(child: SvgPicture.asset("assets/kjsb/chat.svg")),
        SizedBox(
          width: 15,
        ),
      ],
    );
  }
}
