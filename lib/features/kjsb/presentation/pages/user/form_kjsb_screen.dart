import 'package:cached_network_image/cached_network_image.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_service_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/location_entity.dart';
import 'package:get_survey_app/features/kjsb/presentation/pages/user/select_location_screen.dart';
import 'package:get_survey_app/features/kjsb/presentation/provider/kjsb_input_cubit.dart';

import 'file_kjsb_screen.dart';

class FormKJSBScreen extends StatelessWidget {
  KjsbEntity entity;
  final KjsbServiceEntity service;

  FormKJSBScreen({Key key, this.entity, this.service}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    KjsbInputCubit kjsbInputCubit = di<KjsbInputCubit>();
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () {
                Navigator.pop(context, entity != null);
              }),
          centerTitle: true,
          backgroundColor: Color(0xff2CBFCD),
          title: Text(
            "Formulir",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
          )),
      body: BlocProvider(
        create: (BuildContext context) => kjsbInputCubit,
        child: BlocConsumer<KjsbInputCubit, KjsbInputState>(
            listener: (context, state) {
          if (state is KjsbInputSuccess) {
            entity = state.entity;
            kjsbInputCubit.clearInitial();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => FileKJSBScreen(
                          kjsbEntity: state.entity,
                          service: service,
                        )));
          }
        }, builder: (context, state) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _header(),
              SizedBox(height: 20),
              _pageNumber(),
              SizedBox(height: 20),
              createForm(context, kjsbInputCubit, entity),
              SizedBox(height: 10)
            ],
          );
        }),
      ),
    );
  }

  Widget _header() {
    return ExpandablePanel(
      theme: const ExpandableThemeData(
          headerAlignment: ExpandablePanelHeaderAlignment.top,
          tapBodyToCollapse: true,
          hasIcon: false),
      header: Container(
        padding: EdgeInsets.only(top: 5, left: 24, bottom: 5, right: 11),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Detail",
              style: TextStyle(fontSize: 10, color: Color(0xff343434)),
            ),
            Container(
              child: Row(
                children: [
                  Text(
                    "Buka/Tutup",
                    style: TextStyle(fontSize: 10, color: Color(0xff343434)),
                  ),
                  Icon(Icons.arrow_drop_down)
                ],
              ),
            ),
          ],
        ),
      ),
      collapsed: Container(),
      expanded: Container(
        padding: EdgeInsets.only(top: 0, left: 24, bottom: 5, right: 11),
        child: Row(
          children: [
            Column(
              children: [
                SizedBox(height: 5),
                CachedNetworkImage(
                  height: 42,
                  imageUrl: service.iconUrlThumb ?? "",
                  placeholder: (context, url) => Container(
                    child: Center(
                      child: Container(
                          width: 50,
                          height: 50,
                          child: SpinKitCubeGrid(
                            color: primaryColor,
                            size: 40.0,
                          )),
                    ),
                    width: 50.0,
                    height: 50.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(
                        color: Colors.white,
                        width: 2.0,
                      ),
                    ),
                  ),
                  // cacheManager: _cacheManager,
                )
              ],
            ),
            SizedBox(width: 10),
            Container(
              height: 48,
              color: Color(0xff707070),
              width: 1,
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Jenis Layanan",
                    style: TextStyle(fontSize: 12, color: Color(0xff2CBFCD)),
                  ),
                  Text(
                    service.name ?? "",
                    style: TextStyle(fontSize: 10, color: Color(0xff343434)),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      builder: (_, collapsed, expanded) {
        return Expandable(
          collapsed: collapsed,
          expanded: expanded,
          theme: const ExpandableThemeData(crossFadePoint: 0),
        );
      },
    );
  }

  Widget _pageNumber() {
    return Container(
      padding: EdgeInsets.only(left: 80, right: 80),
      child: Column(
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    "1",
                    style: TextStyle(color: Colors.white),
                  ),
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Color(0xff2CBFCD)),
                ),
              ),
              Expanded(
                child: Container(
                  height: 3,
                  color: Color(0xffC6F0F4),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 25),
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    "2",
                    style: TextStyle(color: Color(0xff65B3B5)),
                  ),
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Color(0xffC6F0F4)),
                ),
              )
            ],
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Formulir"),
              Text("Unggah Berkas"),
            ],
          )
        ],
      ),
    );
  }

  Widget createForm(
      BuildContext context, KjsbInputCubit kjsbInputCubit, KjsbEntity entity) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                    spreadRadius: 1.1, blurRadius: 3, color: Colors.grey[200]),
              ]),
          child: Column(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 24, right: 24, top: 26),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        _inputNamaPemohon(kjsbInputCubit),
                        SizedBox(height: 20),
                        _inputNik(kjsbInputCubit),
                        SizedBox(height: 20),
                        _inputPhone(kjsbInputCubit),
                        SizedBox(height: 20),
                        _inputAlamatPemohon(context, kjsbInputCubit),
                        SizedBox(height: 20),
                        _inputEmail(kjsbInputCubit),
                        SizedBox(height: 20),
                        inputLokasiTanah(context, kjsbInputCubit),
                        SizedBox(height: 20),
                        inputLuas(kjsbInputCubit),
                        SizedBox(height: 20),
                        _inputPersetujuan(kjsbInputCubit),
                        StreamBuilder(
                            stream: kjsbInputCubit.defaultStream,
                            builder: (context, snapshotError) {
                              if (entity != null) {
                                context
                                    .read<KjsbInputCubit>()
                                    .loadDraftKjsb(entity);
                              }
                              return SizedBox();
                            })
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20),
              _submitButton(kjsbInputCubit)
            ],
          ),
        ),
      ),
    );
  }

  Widget _inputNamaPemohon(KjsbInputCubit kjsbInputCubit) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              "Nama Pemohonan",
              style: TextStyle(
                fontFamily: "Poppins",
              ),
            ),
            Expanded(child: Container()),
            Text(
              "Pakai data saya",
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Color(0xff2CBFCD)),
            ),
            StreamBuilder(
                stream: kjsbInputCubit.defaultStream,
                builder: (context, snapshot) {
                  return Checkbox(
                    value: snapshot.data ?? false,
                    onChanged: (value) {
                      kjsbInputCubit.setDefaultValue(value);
                    },
                  );
                })
          ],
        ),
        SizedBox(height: 10),
        StreamBuilder(
            stream: kjsbInputCubit.namaStream,
            builder: (context, snapshotError) {
              return StreamBuilder(
                  stream: kjsbInputCubit.defaultNamaStream,
                  builder: (context, snapshotNama) {
                    return TextFormField(
                      controller: snapshotNama.hasData
                          ? TextEditingController(text: snapshotNama.data)
                          : null,
                      onChanged: (text) {
                        kjsbInputCubit.validateNama(text);
                        return text;
                      },
                      keyboardType: TextInputType.name,
                      autofocus: false,
                      style: TextStyle(
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.normal,
                          color: Colors.black),
                      decoration: InputDecoration(
                        errorText: snapshotError.hasError
                            ? snapshotError.error.toString()
                            : null,
                        suffixIcon: Padding(
                          padding: const EdgeInsetsDirectional.only(end: 0),
                          child: Icon(Icons
                              .person_rounded), // myIcon is a 48px-wide widget.
                        ),
                        contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                        hintText: 'Nama Pemohon',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                      ),
                    );
                  });
            })
      ],
    );
  }

  Widget _inputNik(KjsbInputCubit kjsbInputCubit) {
    return StreamBuilder(
        stream: kjsbInputCubit.nikStream,
        builder: (context, snapshotError) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("NIK"),
              SizedBox(height: 10),
              StreamBuilder(
                  stream: kjsbInputCubit,
                  builder: (context, snapshot) {
                    return StreamBuilder(
                        stream: kjsbInputCubit.defaultNikStream,
                        builder: (context, snapshotNik) {
                          return TextFormField(
                            controller: snapshotNik.hasData
                                ? TextEditingController(text: snapshotNik.data)
                                : null,
                            onChanged: (text) {
                              kjsbInputCubit.validateNIK(text);
                              return text;
                            },
                            keyboardType: TextInputType.number,
                            autofocus: false,
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.normal,
                                color: Colors.black),
                            decoration: InputDecoration(
                              errorText: snapshotError.hasError
                                  ? snapshotError.error.toString()
                                  : null,
                              suffixIcon: Padding(
                                padding:
                                    const EdgeInsetsDirectional.only(end: 0),
                                child: Icon(Icons
                                    .picture_in_picture_alt_outlined), // myIcon is a 48px-wide widget.
                              ),
                              contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                              hintText: 'Nomor Identitas Pemohon',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                            ),
                          );
                        });
                  }),
            ],
          );
        });
  }

  Widget _inputPhone(KjsbInputCubit kjsbInputCubit) {
    return StreamBuilder(
        stream: kjsbInputCubit.phoneStream,
        builder: (context, snapshotError) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("No Handphone"),
              SizedBox(height: 10),
              StreamBuilder(
                  stream: kjsbInputCubit,
                  builder: (context, snapshot) {
                    return StreamBuilder(
                        stream: kjsbInputCubit.defaultPhoneStream,
                        builder: (context, snapshotPhone) {
                          return TextFormField(
                            controller: snapshotPhone.hasData
                                ? TextEditingController(
                                    text: snapshotPhone.data)
                                : null,
                            onChanged: (text) {
                              kjsbInputCubit.validatePhone(text);
                              return text;
                            },
                            keyboardType: TextInputType.phone,
                            autofocus: false,
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.normal,
                                color: Colors.black),
                            decoration: InputDecoration(
                              errorText: snapshotError.hasError
                                  ? snapshotError.error.toString()
                                  : null,
                              suffixIcon: Padding(
                                padding:
                                    const EdgeInsetsDirectional.only(end: 0),
                                child: Icon(Icons
                                    .phone), // myIcon is a 48px-wide widget.
                              ),
                              contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                              hintText: 'Nomor Telepon Pemohon',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                            ),
                          );
                        });
                  }),
            ],
          );
        });
  }

  Widget _inputAlamatPemohon(
      BuildContext context, KjsbInputCubit kjsbInputCubit) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text("Alamat Pemohon"),
            Expanded(child: Container()),
            Icon(
              Icons.location_pin,
              color: Colors.red,
            ),
            InkWell(
                onTap: () {
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => SelectLocationScreen()))
                      .then((value) {
                    context
                        .read<KjsbInputCubit>()
                        .loadApplicationLocation((value as LocationEntity));
                  });
                },
                child: Text("Pilih lewat peta"))
          ],
        ),
        SizedBox(height: 10),
        StreamBuilder(
            stream: kjsbInputCubit.alamatStream,
            builder: (context, snapshotError) {
              return StreamBuilder(
                  stream: kjsbInputCubit.defaultAlamatStream,
                  builder: (context, snapshotAlamat) {
                    return TextFormField(
                      controller: snapshotAlamat.hasData
                          ? TextEditingController(text: snapshotAlamat.data)
                          : null,
                      onChanged: (text) {
                        kjsbInputCubit.validateAlamat(text);
                        return text;
                      },
                      keyboardType: TextInputType.text,
                      autofocus: false,
                      style: TextStyle(
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.normal,
                          color: Colors.black),
                      decoration: InputDecoration(
                        errorText: snapshotError.hasError
                            ? snapshotError.error.toString()
                            : null,
                        suffixIcon: Padding(
                          padding: const EdgeInsetsDirectional.only(end: 0),
                          child: Icon(Icons
                              .location_pin), // myIcon is a 48px-wide widget.
                        ),
                        contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                        hintText: 'Masukkan Alamat Pemohon',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                      ),
                    );
                  });
            })
      ],
    );
  }

  Widget _inputEmail(KjsbInputCubit kjsbInputCubit) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Email"),
        SizedBox(height: 10),
        StreamBuilder(
            stream: kjsbInputCubit.emailStream,
            builder: (context, snapshotError) {
              return StreamBuilder(
                  stream: kjsbInputCubit.defaultEmailStream,
                  builder: (context, snapshotEmail) {
                    return TextFormField(
                      controller: snapshotEmail.hasData
                          ? TextEditingController(text: snapshotEmail.data)
                          : null,
                      onChanged: (text) {
                        kjsbInputCubit.validateEmail(text);
                        return text;
                      },
                      keyboardType: TextInputType.emailAddress,
                      autofocus: false,
                      style: TextStyle(
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.normal,
                          color: Colors.black),
                      decoration: InputDecoration(
                        errorText: snapshotError.hasError
                            ? snapshotError.error.toString()
                            : null,
                        suffixIcon: Padding(
                          padding: const EdgeInsetsDirectional.only(end: 0),
                          child: Icon(Icons
                              .email_outlined), // myIcon is a 48px-wide widget.
                        ),
                        contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                        hintText: 'Masukkan Email Pemohon',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                      ),
                    );
                  });
            })
      ],
    );
  }

  Widget inputLokasiTanah(BuildContext context, KjsbInputCubit kjsbInputCubit) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text("Alamat Bidang Tanah"),
            Expanded(child: Container()),
            Icon(
              Icons.location_pin,
              color: Colors.red,
            ),
            InkWell(
                onTap: () {
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SelectLocationScreen()))
                      .then((value) {
                    if (value != null) {
                      context
                          .read<KjsbInputCubit>()
                          .loadLandLocation((value as LocationEntity));
                    }
                  });
                },
                child: Text("Pilih lewat peta"))
          ],
        ),
        SizedBox(height: 10),
        StreamBuilder(
            stream: kjsbInputCubit.lokasiStream,
            builder: (context, snapshotError) {
              return StreamBuilder(
                  stream: kjsbInputCubit.luasStream,
                  builder: (context, snapshot) {
                    return StreamBuilder(
                        stream: kjsbInputCubit.defaultLokasiStream,
                        builder: (context, snapshotLokasi) {
                          return TextFormField(
                            readOnly: true,
                            controller: snapshotLokasi.hasData
                                ? TextEditingController(
                                    text: snapshotLokasi.data)
                                : null,
                            onChanged: (text) {
                              kjsbInputCubit.validateLokasiTanah(text);
                              return text;
                            },
                            keyboardType: TextInputType.text,
                            autofocus: false,
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.normal,
                                color: Colors.black),
                            decoration: InputDecoration(
                              errorText: snapshotError.hasError
                                  ? snapshotError.error.toString()
                                  : null,
                              suffixIcon: Padding(
                                padding:
                                    const EdgeInsetsDirectional.only(end: 0),
                                child: Icon(Icons
                                    .location_pin), // myIcon is a 48px-wide widget.
                              ),
                              contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                              hintText: 'Masukkan Alamat Bidang Tanah',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                            ),
                          );
                        });
                  });
            })
      ],
    );
  }

  Widget inputLuas(KjsbInputCubit kjsbInputCubit) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Luas Tanah"),
        SizedBox(height: 10),
        StreamBuilder(
            stream: kjsbInputCubit.luasStream,
            builder: (context, snapshotError) {
              return StreamBuilder(
                  stream: kjsbInputCubit.defaultLuasStream,
                  builder: (context, snapshotLuas) {
                    return TextFormField(
                      controller: snapshotLuas.hasData
                          ? TextEditingController(
                              text: snapshotLuas.data.toString())
                          : null,
                      onChanged: (text) {
                        kjsbInputCubit.validateLuas(text);
                        return text;
                      },
                      keyboardType: TextInputType.number,
                      autofocus: false,
                      style: TextStyle(
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.normal,
                          color: Colors.black),
                      decoration: InputDecoration(
                        errorText: snapshotError.hasError
                            ? snapshotError.error.toString()
                            : null,
                        suffixIcon: Padding(
                          padding: const EdgeInsetsDirectional.only(end: 0),
                          child: Icon(Icons
                              .aspect_ratio), // myIcon is a 48px-wide widget.
                        ),
                        contentPadding: EdgeInsets.fromLTRB(10, 5, 10, 0),
                        hintText: 'Luas Tanah Sesuai Sertifikat',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                      ),
                    );
                  });
            })
      ],
    );
  }

  Widget _inputPersetujuan(KjsbInputCubit kjsbInputCubit) {
    return Container(
      padding: EdgeInsets.only(left: 0, right: 10, top: 10, bottom: 10),
      decoration: BoxDecoration(
          color: Color(0xffFCE7E4), borderRadius: BorderRadius.circular(10)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          StreamBuilder(
              stream: kjsbInputCubit.agreeStream,
              builder: (context, snapshot) {
                return Checkbox(
                  value: snapshot.data ?? false,
                  onChanged: (agree) {
                    kjsbInputCubit.agree(agree);
                    return agree;
                  },
                );
              }),
          Expanded(
              child: Text(
                  "Saya setuju apabila di kemudian hari terdapat perbedaan luasan tanah, maka saya bersedia untuk membayar sisa luasan tanah tersebut",
                  style: TextStyle(
                      color: Colors.red, fontWeight: FontWeight.w500)))
        ],
      ),
    );
  }

  Widget _submitButton(KjsbInputCubit kjsbInputCubit) {
    return Container(
      height: 46,
      decoration: BoxDecoration(
          color: Color(0xffA8E4EA),
          borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(10),
              bottomLeft: Radius.circular(10))),
      child: StreamBuilder(
          stream: kjsbInputCubit.readyToSubmit,
          builder: (context, snapshot) {
            return StreamBuilder(
                stream: kjsbInputCubit.agreeStream,
                builder: (context, snapshotAgree) {
                  return InkWell(
                    onTap: () {
                      if (snapshot.hasData == true &&
                          snapshotAgree.data == true &&
                          entity == null)
                        context.read<KjsbInputCubit>().addKjsb(service.id);
                      else if (entity != null)
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => FileKJSBScreen(
                                    kjsbEntity: entity, service: service)));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(12),
                            bottomLeft: Radius.circular(12)),
                        color: (snapshot.hasData == true &&
                                snapshotAgree.data == true)
                            ? Color(0xff0c5d65)
                            : Color(0xffA8E4EA),
                      ),
                      height: 30,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Lanjut",
                            style: TextStyle(
                                fontSize: 15,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                  );
                });
          }),
    );
  }
}
