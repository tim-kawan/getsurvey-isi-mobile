import 'package:cached_network_image/cached_network_image.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:expandable/expandable.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_document_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_service_entity.dart';
import 'package:get_survey_app/features/kjsb/presentation/provider/kjsb_input_cubit.dart';
import 'package:get_survey_app/features/kjsb/presentation/routes/kjsb_routes_path.dart';
import 'package:get_survey_app/features/kjsb/presentation/widgets/shimmer_get_files.dart';
import 'package:loader_overlay/loader_overlay.dart';

class FileKJSBScreen extends StatelessWidget {
  final KjsbEntity kjsbEntity;
  List<KjsbDocumentEntity> listDocumentEntity;
  final KjsbServiceEntity service;

  FileKJSBScreen({Key key, this.kjsbEntity, this.service}) : super(key: key) {
    kjsbInputCubit = di<KjsbInputCubit>();
  }

  KjsbInputCubit kjsbInputCubit;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => kjsbInputCubit,
      child: LoaderOverlay(
        useDefaultLoading: false,
        overlayWidget: Center(
          child: SpinKitCubeGrid(
            color: primaryColor,
            size: 50.0,
          ),
        ),
        child: Scaffold(
          appBar: AppBar(
              leading: InkWell(
                onTap: () => Navigator.pop(context),
                child: IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.white),
                ),
              ),
              centerTitle: true,
              backgroundColor: Color(0xff2CBFCD),
              title: Text(
                "Formulir",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              )),
          body: BlocConsumer<KjsbInputCubit, KjsbInputState>(
            listener: (context, state) {
              if (state is DocumentState) {
                listDocumentEntity = state.documents;
              }
              if (state is LoadingState)
                context.loaderOverlay.show();
              else if (state is LoadingState)
                context.loaderOverlay.hide();
              else if (state is UnLoadingState)
                context.loaderOverlay.hide();
              else if (state is UpdateStatusSuccessState) {
                context.loaderOverlay.hide();
                Navigator.of(context).pushNamedAndRemoveUntil(
                    KjsbRoutesPath.ListKjsbRoute,
                    (Route<dynamic> route) => false);
              }
            },
            builder: (context, state) {
              if (state is KjsbInputInitial) {
                context.read<KjsbInputCubit>().loadAllFiles(kjsbEntity.id);
              }
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _header(),
                  SizedBox(height: 20),
                  _pageNumber(),
                  SizedBox(height: 20),
                  _uploadFile(context, state)
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _header() {
    return ExpandablePanel(
      theme: const ExpandableThemeData(
          headerAlignment: ExpandablePanelHeaderAlignment.top,
          tapBodyToCollapse: true,
          hasIcon: false),
      header: Container(
        padding: EdgeInsets.only(top: 5, left: 24, bottom: 5, right: 11),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Detail",
              style: TextStyle(fontSize: 10, color: Color(0xff343434)),
            ),
            Container(
              child: Row(
                children: [
                  Text(
                    "Buka/Tutup",
                    style: TextStyle(fontSize: 10, color: Color(0xff343434)),
                  ),
                  Icon(Icons.arrow_drop_down)
                ],
              ),
            ),
          ],
        ),
      ),
      collapsed: Container(),
      expanded: Container(
        padding: EdgeInsets.only(top: 5, left: 24, bottom: 5, right: 11),
        child: Row(
          children: [
            Column(
              children: [
                SizedBox(height: 5),
                CachedNetworkImage(
                  height: 42,
                  imageUrl: service.iconUrlThumb,
                  placeholder: (context, url) => Container(
                    child: Center(
                      child: Container(
                          width: 50,
                          height: 50,
                          child: SpinKitCubeGrid(
                            color: primaryColor,
                            size: 40.0,
                          )),
                    ),
                    width: 50.0,
                    height: 50.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(
                        color: Colors.white,
                        width: 2.0,
                      ),
                    ),
                  ),
                  // cacheManager: _cacheManager,
                )
              ],
            ),
            SizedBox(width: 10),
            Container(
              height: 48,
              color: Color(0xff707070),
              width: 1,
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Jenis Layanan",
                    style: TextStyle(fontSize: 12, color: Color(0xff2CBFCD)),
                  ),
                  Text(
                    service.name,
                    style: TextStyle(fontSize: 10, color: Color(0xff343434)),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      builder: (_, collapsed, expanded) {
        return Expandable(
          collapsed: collapsed,
          expanded: expanded,
          theme: const ExpandableThemeData(crossFadePoint: 0),
        );
      },
    );
  }

  Widget _pageNumber() {
    return Container(
      padding: EdgeInsets.only(left: 80, right: 80),
      child: Column(
        children: [
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    "1",
                    style: TextStyle(color: Colors.white),
                  ),
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Color(0xff2CBFCD)),
                ),
              ),
              Expanded(
                child: Container(height: 3, color: Color(0xff2CBFCD)),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 25),
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    "2",
                    style: TextStyle(color: Colors.white),
                  ),
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Color(0xff2CBFCD)),
                ),
              )
            ],
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Formulir",
                  style: TextStyle(
                      color: Color(0xff2CBFCD), fontWeight: FontWeight.bold)),
              Text("Unggah Berkas",
                  style: TextStyle(
                      color: Color(0xff2CBFCD), fontWeight: FontWeight.bold)),
            ],
          )
        ],
      ),
    );
  }

  Widget _uploadFile(BuildContext context, KjsbInputState state) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                    spreadRadius: 1.1, blurRadius: 3, color: Colors.grey[200]),
              ]),
          child: Column(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 24, right: 24, top: 26),
                  child: (state is KjsbInputInitial)
                      ? ShimmerWidget()
                      : SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Format (.pdf, .jpg, .png) tidak lebih dari 10MB",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 20),
                              loadAlasHak(context, state),
                              SizedBox(height: 20),
                              loadSuratPermohonanPengukuran(context, state),
                              SizedBox(height: 20),
                              loadSptPBB(context, state),
                              SizedBox(height: 20),
                              loadAJB(context, state),
                              SizedBox(height: 20),
                              loadKTP(context, state),
                              SizedBox(height: 20),
                              loadSuratPelimpahan(context, state),
                              SizedBox(height: 20),
                              loadSuratPermohonan(context, state),
                              SizedBox(height: 20),
                              loadSuratPernyataan(context, state),
                              SizedBox(height: 20),
                              loadSuratKuasa(context, state),
                              SizedBox(height: 20),
                              loadAkta(context, state),
                              SizedBox(height: 20),
                            ],
                          ),
                        ),
                ),
              ),
              SizedBox(height: 20),
              actionButton(context, state),
              SizedBox(height: 5),
            ],
          ),
        ),
      ),
    );
  }

  Future<String> _openFileExplorer() async {
    try {
      return await FilePicker.getFilePath();
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  loadAlasHak(BuildContext context, KjsbInputState state) {
    return StreamBuilder(
        stream: kjsbInputCubit.alasHakStream,
        builder: (context, snapshot) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    "Bukti Alas Hak",
                    style: TextStyle(
                      fontFamily: "Poppins",
                    ),
                  ),
                  Text(
                    " *",
                    style: TextStyle(
                        fontFamily: "Poppins",
                        color: Colors.red,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              SizedBox(height: 11),
              snapshot.hasData
                  ? Stack(
                      children: [
                        Positioned(
                          child: Row(
                            children: [
                              Stack(
                                children: [
                                  Positioned(
                                    child: Container(
                                      width: 60,
                                      height: 60,
                                      child: Image.asset("assets/kjsb/doc.png"),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(width: 10),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(children: [
                                      Text("File"),
                                      SizedBox(width: 10),
                                      Text(
                                          ": ${(snapshot.data as KjsbDocumentEntity).mime}"),
                                    ]),
                                    Row(children: [
                                      Text("Ukuran"),
                                      SizedBox(width: 10),
                                      Text(": 4 MB"),
                                    ])
                                  ]),
                            ],
                          ),
                        ),
                        Positioned(
                            right: 10,
                            top: 0,
                            bottom: 0,
                            child: Icon(Icons.remove_red_eye_sharp))
                      ],
                    )
                  : InkWell(
                      onTap: () async {
                        final path = await _openFileExplorer();
                        context.read<KjsbInputCubit>().uploadFile(
                            path, KjsbDocumentEntity.ALAS_HAK, kjsbEntity.id);
                      },
                      child: DottedBorder(
                        borderType: BorderType.RRect,
                        radius: Radius.circular(12),
                        color: Colors.black,
                        strokeWidth: 1,
                        child: Container(
                          height: 48,
                          width: 48,
                          child: Icon(
                            Icons.add_rounded,
                            size: 21,
                          ),
                        ),
                      ),
                    ),
              SizedBox(height: 15),
              Container(height: 1, color: Color(0xff707070)),
            ],
          );
        });
  }

  loadSuratPermohonanPengukuran(BuildContext context, KjsbInputState state) {
    return StreamBuilder(
        stream: kjsbInputCubit.suratPermohonanPengukuranStream,
        builder: (context, snapshot) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Surat Permohonan Pengukuruan",
                style: TextStyle(
                  fontFamily: "Poppins",
                ),
              ),
              SizedBox(height: 11),
              snapshot.hasData
                  ? Stack(
                      children: [
                        Positioned(
                          child: Row(
                            children: [
                              Stack(
                                children: [
                                  Positioned(
                                    child: Container(
                                      width: 60,
                                      height: 60,
                                      child: Image.asset("assets/kjsb/doc.png"),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(width: 10),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(children: [
                                      Text("File"),
                                      SizedBox(width: 10),
                                      Text(
                                          ": ${(snapshot.data as KjsbDocumentEntity).mime}"),
                                    ]),
                                    Row(children: [
                                      Text("Ukuran"),
                                      SizedBox(width: 10),
                                      Text(": 4 MB"),
                                    ])
                                  ]),
                            ],
                          ),
                        ),
                        Positioned(
                            right: 10,
                            top: 0,
                            bottom: 0,
                            child: Icon(Icons.remove_red_eye_sharp))
                      ],
                    )
                  : InkWell(
                      onTap: () async {
                        final path = await _openFileExplorer();
                        context.read<KjsbInputCubit>().uploadFile(
                            path,
                            KjsbDocumentEntity.PERMOHONAN_PENGUKURAN,
                            kjsbEntity.id);
                      },
                      child: DottedBorder(
                        borderType: BorderType.RRect,
                        radius: Radius.circular(12),
                        color: Colors.black,
                        strokeWidth: 1,
                        child: Container(
                          height: 48,
                          width: 48,
                          child: Icon(
                            Icons.add_rounded,
                            size: 21,
                          ),
                        ),
                      ),
                    ),
              SizedBox(height: 15),
              Container(height: 1, color: Color(0xff707070)),
            ],
          );
        });
  }

  loadSptPBB(BuildContext context, KjsbInputState state) {
    return StreamBuilder(
        stream: kjsbInputCubit.sptPbbStream,
        builder: (context, snapshot) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "SPT Pajak Bumi dan Bangunan",
                style: TextStyle(
                  fontFamily: "Poppins",
                ),
              ),
              SizedBox(height: 11),
              snapshot.hasData
                  ? Stack(
                      children: [
                        Positioned(
                          child: Row(
                            children: [
                              Stack(
                                children: [
                                  Positioned(
                                    child: Container(
                                      width: 60,
                                      height: 60,
                                      child: Image.asset("assets/kjsb/doc.png"),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(width: 10),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(children: [
                                      Text("File"),
                                      SizedBox(width: 10),
                                      Text(
                                          ": ${(snapshot.data as KjsbDocumentEntity).mime}"),
                                    ]),
                                    Row(children: [
                                      Text("Ukuran"),
                                      SizedBox(width: 10),
                                      Text(": 4 MB"),
                                    ])
                                  ]),
                            ],
                          ),
                        ),
                        Positioned(
                            right: 10,
                            top: 0,
                            bottom: 0,
                            child: Icon(Icons.remove_red_eye_sharp))
                      ],
                    )
                  : InkWell(
                      onTap: () async {
                        final path = await _openFileExplorer();
                        context.read<KjsbInputCubit>().uploadFile(
                            path, KjsbDocumentEntity.SPT_PBB, kjsbEntity.id);
                      },
                      child: DottedBorder(
                        borderType: BorderType.RRect,
                        radius: Radius.circular(12),
                        color: Colors.black,
                        strokeWidth: 1,
                        child: Container(
                          height: 48,
                          width: 48,
                          child: Icon(
                            Icons.add_rounded,
                            size: 21,
                          ),
                        ),
                      ),
                    ),
              SizedBox(height: 15),
              Container(height: 1, color: Color(0xff707070)),
            ],
          );
        });
  }

  loadAJB(BuildContext context, KjsbInputState state) {
    return StreamBuilder(
        stream: kjsbInputCubit.aktaJualBeliStream,
        builder: (context, snapshot) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Akta Jual Beli",
                style: TextStyle(
                  fontFamily: "Poppins",
                ),
              ),
              SizedBox(height: 11),
              snapshot.hasData
                  ? Stack(
                      children: [
                        Positioned(
                          child: Row(
                            children: [
                              Stack(
                                children: [
                                  Positioned(
                                    child: Container(
                                      width: 60,
                                      height: 60,
                                      child: Image.asset("assets/kjsb/doc.png"),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(width: 10),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(children: [
                                      Text("File"),
                                      SizedBox(width: 10),
                                      Text(
                                          ": ${(snapshot.data as KjsbDocumentEntity).mime}"),
                                    ]),
                                    Row(children: [
                                      Text("Ukuran"),
                                      SizedBox(width: 10),
                                      Text(": 4 MB"),
                                    ])
                                  ]),
                            ],
                          ),
                        ),
                        Positioned(
                            right: 10,
                            top: 0,
                            bottom: 0,
                            child: Icon(Icons.remove_red_eye_sharp))
                      ],
                    )
                  : InkWell(
                      onTap: () async {
                        final path = await _openFileExplorer();
                        context.read<KjsbInputCubit>().uploadFile(
                            path, KjsbDocumentEntity.AJB, kjsbEntity.id);
                      },
                      child: DottedBorder(
                        borderType: BorderType.RRect,
                        radius: Radius.circular(12),
                        color: Colors.black,
                        strokeWidth: 1,
                        child: Container(
                          height: 48,
                          width: 48,
                          child: Icon(
                            Icons.add_rounded,
                            size: 21,
                          ),
                        ),
                      ),
                    ),
              SizedBox(height: 15),
              Container(height: 1, color: Color(0xff707070)),
            ],
          );
        });
  }

  loadKTP(BuildContext context, KjsbInputState state) {
    return StreamBuilder(
        stream: kjsbInputCubit.ktpStream,
        builder: (context, snapshot) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Identititas KTP/ Identitas Pemohon",
                style: TextStyle(
                  fontFamily: "Poppins",
                ),
              ),
              SizedBox(height: 11),
              snapshot.hasData
                  ? Stack(
                      children: [
                        Positioned(
                          child: Row(
                            children: [
                              Stack(
                                children: [
                                  Positioned(
                                    child: Container(
                                      width: 60,
                                      height: 60,
                                      child: Image.asset("assets/kjsb/doc.png"),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(width: 10),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(children: [
                                      Text("File"),
                                      SizedBox(width: 10),
                                      Text(
                                          ": ${(snapshot.data as KjsbDocumentEntity).mime}"),
                                    ]),
                                    Row(children: [
                                      Text("Ukuran"),
                                      SizedBox(width: 10),
                                      Text(": 4 MB"),
                                    ])
                                  ]),
                            ],
                          ),
                        ),
                        Positioned(
                            right: 10,
                            top: 0,
                            bottom: 0,
                            child: Icon(Icons.remove_red_eye_sharp))
                      ],
                    )
                  : InkWell(
                      onTap: () async {
                        final path = await _openFileExplorer();
                        context.read<KjsbInputCubit>().uploadFile(
                            path, KjsbDocumentEntity.KTP, kjsbEntity.id);
                      },
                      child: DottedBorder(
                        borderType: BorderType.RRect,
                        radius: Radius.circular(12),
                        color: Colors.black,
                        strokeWidth: 1,
                        child: Container(
                          height: 48,
                          width: 48,
                          child: Icon(
                            Icons.add_rounded,
                            size: 21,
                          ),
                        ),
                      ),
                    ),
              SizedBox(height: 15),
              Container(height: 1, color: Color(0xff707070)),
            ],
          );
        });
  }

  loadSuratPelimpahan(BuildContext context, KjsbInputState state) {
    return StreamBuilder(
        stream: kjsbInputCubit.suratPelimpahanStream,
        builder: (context, snapshot) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Surat Pelimpahan Pengukuran dan Pemetaan",
                style: TextStyle(
                  fontFamily: "Poppins",
                ),
              ),
              SizedBox(height: 11),
              snapshot.hasData
                  ? Stack(
                      children: [
                        Positioned(
                          child: Row(
                            children: [
                              Stack(
                                children: [
                                  Positioned(
                                    child: Container(
                                      width: 60,
                                      height: 60,
                                      child: Image.asset("assets/kjsb/doc.png"),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(width: 10),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(children: [
                                      Text("File"),
                                      SizedBox(width: 10),
                                      Text(
                                          ": ${(snapshot.data as KjsbDocumentEntity).mime}"),
                                    ]),
                                    Row(children: [
                                      Text("Ukuran"),
                                      SizedBox(width: 10),
                                      Text(": 4 MB"),
                                    ])
                                  ]),
                            ],
                          ),
                        ),
                        Positioned(
                            right: 10,
                            top: 0,
                            bottom: 0,
                            child: Icon(Icons.remove_red_eye_sharp))
                      ],
                    )
                  : InkWell(
                      onTap: () async {
                        final path = await _openFileExplorer();
                        context.read<KjsbInputCubit>().uploadFile(
                            path,
                            KjsbDocumentEntity.PELIMPAHAN_PENGUKURAN,
                            kjsbEntity.id);
                      },
                      child: DottedBorder(
                        borderType: BorderType.RRect,
                        radius: Radius.circular(12),
                        color: Colors.black,
                        strokeWidth: 1,
                        child: Container(
                          height: 48,
                          width: 48,
                          child: Icon(
                            Icons.add_rounded,
                            size: 21,
                          ),
                        ),
                      ),
                    ),
              SizedBox(height: 15),
              Container(height: 1, color: Color(0xff707070)),
            ],
          );
        });
  }

  loadSuratPermohonan(BuildContext context, KjsbInputState state) {
    return StreamBuilder(
        stream: kjsbInputCubit.suratPermohonanStream,
        builder: (context, snapshot) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Surat Permohonan",
                style: TextStyle(
                  fontFamily: "Poppins",
                ),
              ),
              SizedBox(height: 11),
              snapshot.hasData
                  ? Stack(
                      children: [
                        Positioned(
                          child: Row(
                            children: [
                              Stack(
                                children: [
                                  Positioned(
                                    child: Container(
                                      width: 60,
                                      height: 60,
                                      child: Image.asset("assets/kjsb/doc.png"),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(width: 10),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(children: [
                                      Text("File"),
                                      SizedBox(width: 10),
                                      Text(
                                          ": ${(snapshot.data as KjsbDocumentEntity).mime}"),
                                    ]),
                                    Row(children: [
                                      Text("Ukuran"),
                                      SizedBox(width: 10),
                                      Text(": 4 MB"),
                                    ])
                                  ]),
                            ],
                          ),
                        ),
                        Positioned(
                            right: 10,
                            top: 0,
                            bottom: 0,
                            child: Icon(Icons.remove_red_eye_sharp))
                      ],
                    )
                  : InkWell(
                      onTap: () async {
                        final path = await _openFileExplorer();
                        context.read<KjsbInputCubit>().uploadFile(path,
                            KjsbDocumentEntity.SURAT_PERMOHONAN, kjsbEntity.id);
                      },
                      child: DottedBorder(
                        borderType: BorderType.RRect,
                        radius: Radius.circular(12),
                        color: Colors.black,
                        strokeWidth: 1,
                        child: Container(
                          height: 48,
                          width: 48,
                          child: Icon(
                            Icons.add_rounded,
                            size: 21,
                          ),
                        ),
                      ),
                    ),
              SizedBox(height: 15),
              Container(height: 1, color: Color(0xff707070)),
            ],
          );
        });
  }

  loadSuratPernyataan(BuildContext context, KjsbInputState state) {
    return StreamBuilder(
        stream: kjsbInputCubit.suratPernyataanStream,
        builder: (context, snapshot) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Surat Pernyataan Penguasaan Fisik",
                style: TextStyle(
                  fontFamily: "Poppins",
                ),
              ),
              SizedBox(height: 11),
              snapshot.hasData
                  ? Stack(
                      children: [
                        Positioned(
                          child: Row(
                            children: [
                              Stack(
                                children: [
                                  Positioned(
                                    child: Container(
                                      width: 60,
                                      height: 60,
                                      child: Image.asset("assets/kjsb/doc.png"),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(width: 10),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(children: [
                                      Text("File"),
                                      SizedBox(width: 10),
                                      Text(
                                          ": ${(snapshot.data as KjsbDocumentEntity).mime}"),
                                    ]),
                                    Row(children: [
                                      Text("Ukuran"),
                                      SizedBox(width: 10),
                                      Text(": 4 MB"),
                                    ])
                                  ]),
                            ],
                          ),
                        ),
                        Positioned(
                            right: 10,
                            top: 0,
                            bottom: 0,
                            child: Icon(Icons.remove_red_eye_sharp))
                      ],
                    )
                  : InkWell(
                      onTap: () async {
                        final path = await _openFileExplorer();
                        context.read<KjsbInputCubit>().uploadFile(path,
                            KjsbDocumentEntity.PENGUASAAN_FISIK, kjsbEntity.id);
                      },
                      child: DottedBorder(
                        borderType: BorderType.RRect,
                        radius: Radius.circular(12),
                        color: Colors.black,
                        strokeWidth: 1,
                        child: Container(
                          height: 48,
                          width: 48,
                          child: Icon(
                            Icons.add_rounded,
                            size: 21,
                          ),
                        ),
                      ),
                    ),
              SizedBox(height: 15),
              Container(height: 1, color: Color(0xff707070)),
            ],
          );
        });
  }

  loadSuratKuasa(BuildContext context, KjsbInputState state) {
    return StreamBuilder(
        stream: kjsbInputCubit.suratKuasaStream,
        builder: (context, snapshot) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Surat Kuasa Pemohon",
                style: TextStyle(
                  fontFamily: "Poppins",
                ),
              ),
              SizedBox(height: 11),
              snapshot.hasData
                  ? Stack(
                      children: [
                        Positioned(
                          child: Row(
                            children: [
                              Stack(
                                children: [
                                  Positioned(
                                    child: Container(
                                      width: 60,
                                      height: 60,
                                      child: Image.asset("assets/kjsb/doc.png"),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(width: 10),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(children: [
                                      Text("File"),
                                      SizedBox(width: 10),
                                      Text(
                                          ": ${(snapshot.data as KjsbDocumentEntity).mime}"),
                                    ]),
                                    Row(children: [
                                      Text("Ukuran"),
                                      SizedBox(width: 10),
                                      Text(": 4 MB"),
                                    ])
                                  ]),
                            ],
                          ),
                        ),
                        Positioned(
                            right: 10,
                            top: 0,
                            bottom: 0,
                            child: Icon(Icons.remove_red_eye_sharp))
                      ],
                    )
                  : InkWell(
                      onTap: () async {
                        final path = await _openFileExplorer();
                        context.read<KjsbInputCubit>().uploadFile(path,
                            KjsbDocumentEntity.SURAT_KUASA, kjsbEntity.id);
                      },
                      child: DottedBorder(
                        borderType: BorderType.RRect,
                        radius: Radius.circular(12),
                        color: Colors.black,
                        strokeWidth: 1,
                        child: Container(
                          height: 48,
                          width: 48,
                          child: Icon(
                            Icons.add_rounded,
                            size: 21,
                          ),
                        ),
                      ),
                    ),
              SizedBox(height: 15),
              Container(height: 1, color: Color(0xff707070)),
            ],
          );
        });
  }

  loadAkta(BuildContext context, KjsbInputState state) {
    return StreamBuilder(
        stream: kjsbInputCubit.aktaStream,
        builder: (context, snapshot) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Akta Pendirian dan Pengesahan Badan Hukum",
                style: TextStyle(
                  fontFamily: "Poppins",
                ),
              ),
              SizedBox(height: 11),
              snapshot.hasData
                  ? Stack(
                      children: [
                        Positioned(
                          child: Row(
                            children: [
                              Stack(
                                children: [
                                  Positioned(
                                    child: Container(
                                      width: 60,
                                      height: 60,
                                      child: Image.asset("assets/kjsb/doc.png"),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(width: 10),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(children: [
                                      Text("File"),
                                      SizedBox(width: 10),
                                      Text(
                                          ": ${(snapshot.data as KjsbDocumentEntity).mime}"),
                                    ]),
                                    Row(children: [
                                      Text("Ukuran"),
                                      SizedBox(width: 10),
                                      Text(": 4 MB"),
                                    ])
                                  ]),
                            ],
                          ),
                        ),
                        Positioned(
                            right: 10,
                            top: 0,
                            bottom: 0,
                            child: Icon(Icons.remove_red_eye_sharp))
                      ],
                    )
                  : InkWell(
                      onTap: () async {
                        final path = await _openFileExplorer();
                        context.read<KjsbInputCubit>().uploadFile(path,
                            KjsbDocumentEntity.AKTA_PENDIRIAN, kjsbEntity.id);
                      },
                      child: DottedBorder(
                        borderType: BorderType.RRect,
                        radius: Radius.circular(12),
                        color: Colors.black,
                        strokeWidth: 1,
                        child: Container(
                          height: 48,
                          width: 48,
                          child: Icon(
                            Icons.add_rounded,
                            size: 21,
                          ),
                        ),
                      ),
                    ),
              SizedBox(height: 15),
              Container(height: 1, color: Color(0xff707070)),
            ],
          );
        });
  }

  actionButton(BuildContext context, KjsbInputState state) {
    return StreamBuilder(
        stream: kjsbInputCubit.alasHakStream,
        builder: (context, snapshot) {
          return Container(
            height: 46,
            decoration: BoxDecoration(
                color: snapshot.hasData ? Color(0xff0c5d65) : Color(0xffA8E4EA),
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(10),
                    bottomLeft: Radius.circular(10))),
            child: InkWell(
              onTap: () {
                if (snapshot.hasData)
                  context
                      .read<KjsbInputCubit>()
                      .changeStatusTagging(int.parse(kjsbEntity.id));
              },
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(12),
                      bottomLeft: Radius.circular(12)),
                  color:
                      snapshot.hasData ? Color(0xff0c5d65) : Color(0xffA8E4EA),
                ),
                height: 30,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Lanjut",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}
