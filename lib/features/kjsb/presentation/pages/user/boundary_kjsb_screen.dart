import 'dart:async';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/kjsb/presentation/provider/tagging_cubit.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:location/location.dart';

class BoundaryKJSBScreen extends StatefulWidget {
  final String kjsbId;

  const BoundaryKJSBScreen({Key key, this.kjsbId}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BoundaryKJSBScreen(kjsbId);
}

class _BoundaryKJSBScreen extends State<BoundaryKJSBScreen> {
  final String kjsbId;
  Completer<GoogleMapController> _controller = Completer();
  ScrollController listScrollController = new ScrollController();
  StreamSubscription<LocationData> _streamSubscription;
  LatLng latLng = LatLng(-6.2685497, 107.0496954);
  Set<Marker> _markers = Set();
  BitmapDescriptor sourceIcon;

  _BoundaryKJSBScreen(this.kjsbId);

  @override
  void initState() {
    super.initState();
    _initResource();
    _observeLocation();
  }

  @override
  Widget build(BuildContext context) {
    return LoaderOverlay(
      useDefaultLoading: false,
      overlayWidget: Center(
        child: SpinKitCubeGrid(
          color: primaryColor,
          size: 50.0,
        ),
      ),
      child: BlocProvider(
        create: (context) => di<TaggingCubit>(),
        child: BlocConsumer<TaggingCubit, TaggingState>(
          listener: (context, state) {
            if (state is UploadTaggingSuccessState) {
              context.loaderOverlay.hide();
              Navigator.pop(context, true);
            } else if (state is LoadingUploadTaggingState)
              context.loaderOverlay.show();
            else if (state is UnLoadingUploadTaggingState)
              context.loaderOverlay.hide();
          },
          builder: (context, state) => Scaffold(
            appBar: AppBar(
                leading: IconButton(
                    icon: Icon(Icons.arrow_back, color: Colors.white),
                    onPressed: () {
                      Navigator.pop(context, true);
                    }),
                centerTitle: true,
                backgroundColor: Color(0xff44A5A9),
                title: Text(
                  "Upload Foto Tanda Batas",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                )),
            body: BlocBuilder<TaggingCubit, TaggingState>(
              builder: (context, state) {
                if (state is TaggingInitial)
                  context.read<TaggingCubit>().loadAllTagging(kjsbId);
                if (state is TaggingResultState) {
                  context.loaderOverlay.hide();
                  _markers = state.markers.map((tagging) {
                    return Marker(
                        draggable: true,
                        markerId: MarkerId(tagging.id),
                        position: LatLng(tagging.lat, tagging.lng),
                        icon: sourceIcon,
                        onDragEnd: (value) {
                          context.read<TaggingCubit>().moveMarker(
                              tagging.id, value.latitude, value.longitude);
                        });
                  }).toSet();
                }
                return _body(context);
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _body(BuildContext context) {
    return Stack(
      children: <Widget>[_map(), _action(context)],
    );
  }

  Widget _map() {
    return GoogleMap(
      mapType: MapType.normal,
      initialCameraPosition: CameraPosition(
        target: latLng,
        zoom: 17.4746,
      ),
      myLocationButtonEnabled: true,
      // myLocationEnabled: true,
      markers: _markers,
      zoomControlsEnabled: false,
      padding: EdgeInsets.only(top: 10.0),
      onMapCreated: (GoogleMapController controller) {
        _controller.complete(controller);
      },
    );
  }

  Widget _action(BuildContext context) {
    return Positioned(
      left: 80,
      right: 80,
      bottom: 30,
      child: Column(
        children: [
          Container(
            height: 46,
            child: InkWell(
              onTap: () {
                addImageTagging(context);
              },
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Color(0xff8E99AF),
                ),
                height: 30,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.camera_alt,
                      color: Colors.white,
                    ),
                    SizedBox(width: 10),
                    Text(
                      "Tambah Foto",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 46,
            child: InkWell(
              onTap: () {
                context.read<TaggingCubit>().uploadsTagging(kjsbId);
              },
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Color(0xff40A0A4),
                ),
                height: 30,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Selesai",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    if (_streamSubscription != null) {
      _streamSubscription.cancel();
    }
    super.dispose();
  }

  void _observeLocation() async {
    Location _location = new Location();
    bool _isOn = await _location.requestService();
    PermissionStatus _isAllow = await _location.requestPermission();
    if (_isOn && _isAllow == PermissionStatus.GRANTED) {
      _location.changeSettings(interval: 15000); // reload after 15 seconds
      _streamSubscription =
          _location.onLocationChanged().listen((currentLocation) async {
        latLng = LatLng(currentLocation.latitude, currentLocation.longitude);
        GoogleMapController mapController = await _controller.future;
        mapController
            .animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: latLng,
          zoom: 18.4746,
        )));
      });
    }
  }

  Future addImageTagging(BuildContext context) async {
    final addLatLng = latLng;
    var image = await ImagePicker().getImage(
      source: ImageSource.camera,
      imageQuality: 5,
    );
    context.read<TaggingCubit>().addTagging(image.path, addLatLng, kjsbId);
  }

  void _initResource() async {
    sourceIcon = await _bitmapDescriptorFromSvgAsset(
        context, 'assets/icons/ic_location_me.svg');
  }

  Future<BitmapDescriptor> _bitmapDescriptorFromSvgAsset(
      BuildContext context, String assetName) async {
    var svgString = await DefaultAssetBundle.of(context).loadString(assetName);

    var svgDrawableRoot = await svg.fromSvgString(svgString, null);

    var queryData = MediaQuery.of(context);
    var devicePixelRatio = queryData.devicePixelRatio;
    var width = 48 * devicePixelRatio; // SVG's original width
    var height = 48 * devicePixelRatio; // same thing

    var picture = svgDrawableRoot.toPicture(size: Size(width, height));

    var image = await picture.toImage(width.toInt(), height.toInt());
    var bytes = await image.toByteData(format: ui.ImageByteFormat.png);
    return BitmapDescriptor.fromBytes(bytes.buffer.asUint8List());
  }
}
