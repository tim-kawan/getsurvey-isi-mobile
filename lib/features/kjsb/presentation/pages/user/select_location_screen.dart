import 'dart:async';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/location_entity.dart';
import 'package:get_survey_app/features/kjsb/presentation/provider/kjsb_input_cubit.dart';
import 'package:get_survey_app/features/kjsb/presentation/widgets/shimmer_get_locations.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class SelectLocationScreen extends StatefulWidget {
  final String kjsbId;

  const SelectLocationScreen({Key key, this.kjsbId}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SelectLocationScreen(kjsbId);
}

class _SelectLocationScreen extends State<SelectLocationScreen> {
  final String kjsbId;
  bool hasToLocation = false;
  Completer<GoogleMapController> _controller = Completer();
  ScrollController listScrollController = new ScrollController();
  StreamSubscription<LocationData> _streamSubscription;
  LatLng latLng = LatLng(-6.2685497, 107.0496954);
  Set<Marker> _markers = Set();
  BitmapDescriptor sourceIcon;

  _SelectLocationScreen(this.kjsbId);

  @override
  void initState() {
    super.initState();
    _initResource();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => di<KjsbInputCubit>(),
      child: BlocConsumer<KjsbInputCubit, KjsbInputState>(
        listener: (context, state) {
          if (state is SaveLocationSuccessState) {
            Navigator.pop(context, state.location);
          }
        },
        builder: (context, state) => Scaffold(
          appBar: AppBar(
              leading: IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.white),
                  onPressed: () {
                    Navigator.pop(context, true);
                  }),
              centerTitle: true,
              backgroundColor: Color(0xff44A5A9),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Pilih Lokasi",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ),
                  ),
                  IconButton(
                      icon: Icon(Icons.search_rounded),
                      onPressed: () {
                        context.read<KjsbInputCubit>().searchClick();
                      })
                ],
              )),
          body: BlocBuilder<KjsbInputCubit, KjsbInputState>(
            builder: (context, state) {
              if (state is SearchClickState || state is KjsbInputInitial)
                return _pickLocationContainer(context, state);
              if (state is GetLocationsState)
                return _selectLocationContainer(context, state);
              if (state is LoadingSearchClickState)
                return ShimmerWidget();
              else if (state is SelectedLocationState)
                return _body(context, state);
              else
                return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _body(BuildContext context, SelectedLocationState state) {
    _markers = [
      Marker(
          markerId: MarkerId(state.location.name),
          icon: sourceIcon,
          position: LatLng(state.location.lat, state.location.lng))
    ].toSet();
    zoomTo(state.location);
    return Stack(
      children: <Widget>[_map(), _action(context, state.location)],
    );
  }

  Widget _map() {
    return GoogleMap(
      mapType: MapType.normal,
      initialCameraPosition: CameraPosition(
        target: latLng,
        zoom: 17.4746,
      ),
      myLocationButtonEnabled: true,
      myLocationEnabled: true,
      markers: _markers,
      zoomControlsEnabled: false,
      padding: EdgeInsets.only(top: 10.0),
      onMapCreated: (GoogleMapController controller) {
        _controller.complete(controller);
      },
    );
  }

  Widget _action(BuildContext context, LocationEntity entity) {
    return Positioned(
      left: 0,
      right: 0,
      bottom: 0,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18),
                color: Colors.grey[300],
              ),
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: Row(
                  children: [
                    Icon(
                      Icons.pin_drop,
                      size: 24.0,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        child: Text(entity.address,
                            style: TextStyle(
                              color: Colors.black,
                            ))),
                  ],
                ),
              ),
            ),
          ),
          Container(
            height: 46,
            child: InkWell(
              onTap: () {
                context.read<KjsbInputCubit>().saveLandLocation(entity);
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xff40A0A4),
                ),
                height: 30,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Pilih Lokasi",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    if (_streamSubscription != null) {
      _streamSubscription.cancel();
    }
    super.dispose();
  }

  void _initResource() async {
    sourceIcon = await _bitmapDescriptorFromSvgAsset(
        context, 'assets/icons/ic_location_me.svg');
  }

  Future<BitmapDescriptor> _bitmapDescriptorFromSvgAsset(
      BuildContext context, String assetName) async {
    var svgString = await DefaultAssetBundle.of(context).loadString(assetName);

    var svgDrawableRoot = await svg.fromSvgString(svgString, null);

    var queryData = MediaQuery.of(context);
    var devicePixelRatio = queryData.devicePixelRatio;
    var width = 48 * devicePixelRatio; // SVG's original width
    var height = 48 * devicePixelRatio; // same thing

    var picture = svgDrawableRoot.toPicture(size: Size(width, height));

    var image = await picture.toImage(width.toInt(), height.toInt());
    var bytes = await image.toByteData(format: ui.ImageByteFormat.png);
    return BitmapDescriptor.fromBytes(bytes.buffer.asUint8List());
  }

  Widget _pickLocationContainer(BuildContext context, KjsbInputState state) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                height: 60.0,
                alignment: AlignmentDirectional.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(blurRadius: 10.0, color: Colors.black12)
                    ]),
                padding: EdgeInsets.only(
                    left: 20.0, right: 10.0, top: 0.0, bottom: 0.0),
                child: Theme(
                  data: ThemeData(
                    hintColor: Colors.transparent,
                  ),
                  child: TextFormField(
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        suffixIcon: GestureDetector(
                          onTap: () {
                            context.read<KjsbInputCubit>().findLocation();
                          },
                          child: Container(
                            height: MediaQuery.of(context).size.height,
                            child: Icon(
                              Icons.search,
                              size: 36,
                              color: primaryColor,
                            ),
                          ),
                        ),
                        border: InputBorder.none,
                        labelText: "Pilih Lokasi",
                        labelStyle: TextStyle(
                            fontSize: 15.0,
                            fontFamily: 'Sans',
                            letterSpacing: 0.3,
                            color: primaryColor,
                            fontWeight: FontWeight.w600)),
                    keyboardType: TextInputType.text,
                    onChanged: (value) {
                      context.read<KjsbInputCubit>().validateLocation(value);
                      return value;
                    },
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _selectLocationContainer(
      BuildContext context, GetLocationsState state) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Flexible(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(8, 0, 8, 8),
                  child: ListView.builder(
                      itemCount: state.listlocation.length,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InkWell(
                            onTap: () async {
                              context
                                  .read<KjsbInputCubit>()
                                  .selectLocation(state.listlocation[index]);
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  border:
                                      Border.all(width: 2, color: Colors.grey),
                                  borderRadius: BorderRadius.circular(5.0)),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      state.listlocation[index].name,
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(state.listlocation[index].address),
                                    Row(
                                      children: [
                                        Icon(Icons.location_on_outlined),
                                        Text(state.listlocation[index].lat
                                            .toString()),
                                        Text(","),
                                        Text(state.listlocation[index].lng
                                            .toString()),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      }),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  void zoomTo(LocationEntity location) async {
    GoogleMapController mapController = await _controller.future;
    mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(location.lat, location.lng),
      zoom: 18.4746,
    )));
  }
}
