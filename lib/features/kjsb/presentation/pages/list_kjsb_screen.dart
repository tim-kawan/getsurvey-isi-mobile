import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_service_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/list_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/presentation/pages/surveyor/detail_kjsb_screen.dart'
    as surveyor_detail_kjsb;
import 'package:get_survey_app/features/kjsb/presentation/pages/user/detail_kjsb_screen.dart';
import 'package:get_survey_app/features/kjsb/presentation/pages/user/form_kjsb_screen.dart';
import 'package:get_survey_app/features/kjsb/presentation/provider/get_kjsb_cubit.dart';
import 'package:get_survey_app/features/kjsb/presentation/routes/kjsb_routes_path.dart';
import 'package:get_survey_app/features/kjsb/presentation/widgets/shimmer_get_all_kjsb.dart';
import 'package:no_context_navigation/no_context_navigation.dart';

class ListKjsbScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: BlocProvider(
        create: (BuildContext context) => di<GetKjsbCubit>(),
        child: Scaffold(
          appBar: AppBar(
              leading: IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.white),
                  onPressed: () {
                    Navigator.pop(context, true);
                  }),
              centerTitle: true,
              backgroundColor: Color(0xff2CBFCD),
              title: Text(
                "Berkas Saya",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              )),
          body: Container(
            child: DefaultTabController(
              length: 3,
              child: Column(
                children: [
                  BlocBuilder<GetKjsbCubit, GetKjsbState>(
                    builder: (context, state) {
                      if (state is GetKjsbInitial) {
                        context.read<GetKjsbCubit>().getKjsb(KjsbEntity.draft);
                      }
                      return SizedBox();
                    },
                  ),
                  DecoratedTabBar(
                    tabBar: _tabBar(),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Color(0xffE4E9F2),
                          width: 2.0,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: TabBarView(
                      children: [
                        Container(child: _itemSKJB()),
                        Container(child: _itemSKJB()),
                        Container(child: _itemSKJB())
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          floatingActionButton: BlocBuilder<GetKjsbCubit, GetKjsbState>(
            builder: (context, state) => FloatingActionButton(
              onPressed: () {
                navService.pushNamed(KjsbRoutesPath.CategoriesKjsbRoute);
              },
              backgroundColor: Color(0xff2CBFCD),
              child: Icon(
                Icons.add_rounded,
                color: Colors.white,
                size: 36,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _itemSKJB() {
    return BlocBuilder<GetKjsbCubit, GetKjsbState>(builder: (context, state) {
      if (state is GetKjsbInitial)
        return ShimmerWidget();
      else if (state is GetAllKjsbByType)
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView.builder(
              itemCount: state.listKjsb.length,
              itemBuilder: (context, int key) {
                return InkWell(
                  onTap: () {
                    _navigateTo(context, state.listKjsb[key], state.userId);
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(children: [
                      Container(
                        height: 89,
                        width: 70,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(5),
                            topRight: Radius.circular(5),
                            bottomLeft: Radius.circular(5),
                            bottomRight: Radius.circular(5),
                          ),
                          boxShadow: [
                            BoxShadow(
                                spreadRadius: 1.1,
                                blurRadius: 3,
                                color: Colors.grey[200])
                          ],
                          color: Colors.white,
                        ),
                        child: Center(
                          child: CachedNetworkImage(
                            width: 50,
                            imageUrl: state.listKjsb[key].service.iconUrl ?? "",
                            placeholder: (context, url) => Container(
                              child: Center(
                                child: Container(
                                    width: 50,
                                    height: 50,
                                    child: SpinKitCubeGrid(
                                      color: primaryColor,
                                      size: 40.0,
                                    )),
                              ),
                              width: 50.0,
                              height: 50.0,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                border: Border.all(
                                  color: Colors.white,
                                  width: 2.0,
                                ),
                              ),
                            ),
                            // cacheManager: _cacheManager,
                          ),
                        ),
                      ),
                      Expanded(
                          child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Container(
                          padding: EdgeInsets.all(5),
                          height: 89,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(5),
                              topRight: Radius.circular(5),
                              bottomLeft: Radius.circular(5),
                              bottomRight: Radius.circular(5),
                            ),
                            boxShadow: [
                              BoxShadow(
                                  spreadRadius: 1.1,
                                  blurRadius: 3,
                                  color: Colors.grey[200])
                            ],
                            color: Colors.white,
                          ),
                          child: _description(state.listKjsb[key].kjsb),
                        ),
                      ))
                    ]),
                  ),
                );
              }),
        );
      else
        return Container();
    });
  }

  Widget _tabBar() {
    return TabBar(
      labelColor: Color(0xff00AF5D),
      labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
      unselectedLabelColor: Colors.grey[400],
      unselectedLabelStyle:
          TextStyle(fontWeight: FontWeight.normal, fontSize: 14),
      indicatorSize: TabBarIndicatorSize.tab,
      indicatorColor: Color(0xff00AF5D),
      tabs: [
        Tab(
          child: Text(
            "Permohonan",
          ),
        ),
        Tab(
          child: Text(
            "Proses",
          ),
        ),
        Tab(
          child: Text(
            "Selesai",
          ),
        ),
      ],
    );
  }

  Widget _description(KjsbEntity kjsbEntity) {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 2, bottom: 2),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Row(
                  children: [
                    Text("Get - Layanan Pertanahan",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff0C6164))),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(kjsbEntity.serviceName ?? "",
                        style: TextStyle(fontSize: 12)),
                    Text(kjsbEntity.code,
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff40A0A4))),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    Icon(
                      Icons.person_rounded,
                      size: 12,
                      color: Color(0xffC5CEE0),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 100.0),
                        child: Text(
                          "${kjsbEntity.nama}",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    Icon(
                      Icons.location_pin,
                      size: 12,
                      color: Color(0xffC5CEE0),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 100.0),
                        child: Text(
                          "${kjsbEntity.lokasiTanah}",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: 10),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    SizedBox(width: 2),
                    SvgPicture.asset("assets/kjsb/are.svg",
                        height: 8, width: 8),
                    SizedBox(width: 5),
                    Text(
                      "${kjsbEntity.luasTanah}",
                      style: TextStyle(fontSize: 10),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        Positioned(
            bottom: 4,
            right: 8,
            child: Container(
              width: 80,
              child: Text(
                kjsbEntity.labelStatus(),
                maxLines: 3,
                textAlign: TextAlign.end,
                style: TextStyle(
                    color: Color(0xffF46724),
                    fontSize: 10,
                    fontWeight: FontWeight.bold),
              ),
            ))
      ],
    );
  }

  void _navigateTo(
      BuildContext context, ListKjsbEntity kjsbEntity, int userId) {
    if (kjsbEntity.kjsb.status == KjsbEntity.draft)
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => FormKJSBScreen(
                    entity: kjsbEntity.kjsb,
                    service: KjsbServiceEntity(
                        name: kjsbEntity.service.name,
                        iconUrlThumb: kjsbEntity.service.iconUrl),
                  )));
    else if (userId == kjsbEntity.kjsb.userId)
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => DetailKJSBScreen(
                    kjsbId: kjsbEntity.kjsb.id,
                  ))).then((value) {
        if (value != null && value) {
          context.read<GetKjsbCubit>().getKjsb(KjsbEntity.draft);
        }
      });
    else if (userId != kjsbEntity.kjsb.userId)
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => surveyor_detail_kjsb.DetailKJSBScreen(
                    kjsbId: kjsbEntity.kjsb.id,
                  )));
  }
}

class DecoratedTabBar extends StatelessWidget implements PreferredSizeWidget {
  DecoratedTabBar({@required this.tabBar, @required this.decoration});

  final TabBar tabBar;
  final BoxDecoration decoration;

  @override
  Size get preferredSize => tabBar.preferredSize;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(child: Container(decoration: decoration)),
        tabBar,
      ],
    );
  }
}
