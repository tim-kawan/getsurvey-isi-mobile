import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerWidget extends StatefulWidget {
  const ShimmerWidget({Key key}) : super(key: key);

  @override
  _ShimmerWidgetState createState() => _ShimmerWidgetState();
}

class _ShimmerWidgetState extends State<ShimmerWidget> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 6,
        mainAxisSpacing: 14,
        childAspectRatio: MediaQuery.of(context).size.width /
            (MediaQuery.of(context).size.height / 4),
      ),
      itemCount: 10,
      itemBuilder: (context, int key) {
        return Shimmer.fromColors(
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16.0),
                boxShadow: [
                  BoxShadow(
                      color: backGround.withOpacity(0.1),
                      blurRadius: 0.5,
                      offset: Offset(0, 1))
                ]),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                    padding: const EdgeInsets.only(left: 24.0),
                    child: Container(
                      decoration: BoxDecoration(
                          color: backGround.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(16.0),
                          boxShadow: [
                            BoxShadow(
                                color: backGround.withOpacity(0.1),
                                blurRadius: 0.5,
                                offset: Offset(0, 1))
                          ]),
                      height: 10,
                      width: 50,
                    )),
                Padding(
                    padding: const EdgeInsets.only(right: 25.0),
                    child: Container(
                      decoration: BoxDecoration(
                          color: backGround.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            BoxShadow(
                                color: backGround.withOpacity(0.1),
                                blurRadius: 0.5,
                                offset: Offset(0, 1))
                          ]),
                      height: 50,
                      width: 50,
                    )),
              ],
            ),
          ),
          baseColor: Colors.grey[700],
          highlightColor: Colors.grey[200],
        );
      },
    );
  }
}
