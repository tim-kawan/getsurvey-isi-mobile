import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerWidget extends StatefulWidget {
  const ShimmerWidget({Key key}) : super(key: key);

  @override
  _ShimmerWidgetState createState() => _ShimmerWidgetState();
}

class _ShimmerWidgetState extends State<ShimmerWidget> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 5,
      itemBuilder: (context, int key) {
        return Padding(
          padding: const EdgeInsets.all(10.0),
          child: Shimmer.fromColors(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: backGround.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(16.0),
                      boxShadow: [
                        BoxShadow(
                            color: backGround.withOpacity(0.1),
                            blurRadius: 0.5,
                            offset: Offset(0, 1))
                      ]),
                  height: 10,
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: 70,
                      height: 80,
                      decoration: BoxDecoration(
                          color: backGround.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(16.0),
                          boxShadow: [
                            BoxShadow(
                                color: backGround.withOpacity(0.1),
                                blurRadius: 0.5,
                                offset: Offset(0, 1))
                          ]),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(top: 5),
                        height: 80,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 8.0, right: 0, bottom: 10),
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color:
                                                  backGround.withOpacity(0.1),
                                              borderRadius:
                                                  BorderRadius.circular(16.0),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: backGround
                                                        .withOpacity(0.1),
                                                    blurRadius: 0.5,
                                                    offset: Offset(0, 1))
                                              ]),
                                          height: 10,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 8.0, right: 0, bottom: 10),
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color:
                                                  backGround.withOpacity(0.1),
                                              borderRadius:
                                                  BorderRadius.circular(16.0),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: backGround
                                                        .withOpacity(0.1),
                                                    blurRadius: 0.5,
                                                    offset: Offset(0, 1))
                                              ]),
                                          height: 10,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 18.0, right: 0, bottom: 10),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: backGround.withOpacity(0.1),
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        boxShadow: [
                                          BoxShadow(
                                              color:
                                                  backGround.withOpacity(0.1),
                                              blurRadius: 0.5,
                                              offset: Offset(0, 1))
                                        ]),
                                    height: 15,
                                    width: 30,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            baseColor: Colors.grey[700],
            highlightColor: Colors.grey[200],
          ),
        );
      },
    );
  }
}
