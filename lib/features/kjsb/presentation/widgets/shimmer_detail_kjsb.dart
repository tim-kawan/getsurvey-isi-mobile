import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerWidget extends StatefulWidget {
  const ShimmerWidget({Key key}) : super(key: key);

  @override
  _ShimmerWidgetState createState() => _ShimmerWidgetState();
}

class _ShimmerWidgetState extends State<ShimmerWidget> {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(24, 30, 24, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [

            Expanded(
              child: Container(
                padding: EdgeInsets.only(top: 5),
                height: 80,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    boxShadow: [
                      BoxShadow(
                          color: backGround.withOpacity(0.1),
                          blurRadius: 0.5,
                          offset: Offset(0, 1))
                    ]),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                            color: backGround.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(16.0),
                            boxShadow: [
                              BoxShadow(
                                  color: backGround.withOpacity(0.1),
                                  blurRadius: 0.5,
                                  offset: Offset(0, 1))
                            ]),
                        height: 10,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8),
                      child: Container(
                        decoration: BoxDecoration(
                            color: backGround.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(16.0),
                            boxShadow: [
                              BoxShadow(
                                  color: backGround.withOpacity(0.1),
                                  blurRadius: 0.5,
                                  offset: Offset(0, 1))
                            ]),
                        height: 10,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 8.0, right: 8, bottom: 10),
                      child: Container(
                        decoration: BoxDecoration(
                            color: backGround.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(16.0),
                            boxShadow: [
                              BoxShadow(
                                  color: backGround.withOpacity(0.1),
                                  blurRadius: 0.5,
                                  offset: Offset(0, 1))
                            ]),
                        height: 10,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      baseColor: Colors.grey[700],
      highlightColor: Colors.grey[200],
    );
  }
}

