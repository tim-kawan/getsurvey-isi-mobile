import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerWidget extends StatefulWidget {
  const ShimmerWidget({Key key}) : super(key: key);

  @override
  _ShimmerWidgetState createState() => _ShimmerWidgetState();
}

class _ShimmerWidgetState extends State<ShimmerWidget> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 6,
        mainAxisSpacing: 14,
        childAspectRatio: MediaQuery.of(context).size.width /
            (MediaQuery.of(context).size.height / 1.8),
      ),
      itemCount: 10,
      itemBuilder: (context, int key) {
        return _itemShimmer();
      },
    );
  }

  Widget _itemShimmer() {
    return Shimmer.fromColors(
      child: Padding(
        padding: EdgeInsets.only(left: 3, right: 3, top: 5),
        child: Container(
          child: Padding(
            padding: EdgeInsets.only(left: 0, right: 0, top: 10),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 35,
                        height: 35,
                        decoration: BoxDecoration(
                            color: backGround.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(5.0),
                            boxShadow: [
                              BoxShadow(
                                  color: backGround.withOpacity(0.1),
                                  blurRadius: 0.5,
                                  offset: Offset(0, 1))
                            ]),
                      ),
                      SizedBox(width: 5),
                      Expanded(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                color: backGround.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(16.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: backGround.withOpacity(0.1),
                                      blurRadius: 0.5,
                                      offset: Offset(0, 1))
                                ]),
                            height: 10,
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.star,
                                color: backGround.withOpacity(0.3),
                                size: 16,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                    color: backGround.withOpacity(0.1),
                                    borderRadius: BorderRadius.circular(16.0),
                                    boxShadow: [
                                      BoxShadow(
                                          color: backGround.withOpacity(0.1),
                                          blurRadius: 0.5,
                                          offset: Offset(0, 1))
                                    ]),
                                height: 10,
                                width: 20,
                              ),
                            ],
                          )
                        ],
                      )),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Row(children: [
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Terdaftar",
                              style: TextStyle(fontSize: 8),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  color: backGround.withOpacity(0.1),
                                  borderRadius: BorderRadius.circular(16.0),
                                  boxShadow: [
                                    BoxShadow(
                                        color: backGround.withOpacity(0.1),
                                        blurRadius: 0.5,
                                        offset: Offset(0, 1))
                                  ]),
                              height: 10,
                              width: 50,
                            ),
                          ]),
                      Expanded(child: Container()),
                      Column(children: [
                        Text(
                          "Lisensi",
                          style: TextStyle(fontSize: 8),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: backGround.withOpacity(0.1),
                              borderRadius: BorderRadius.circular(16.0),
                              boxShadow: [
                                BoxShadow(
                                    color: backGround.withOpacity(0.1),
                                    blurRadius: 0.5,
                                    offset: Offset(0, 1))
                              ]),
                          height: 10,
                          width: 10,
                        ),
                      ])
                    ]),
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Row(children: [
                    Expanded(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SvgPicture.asset("assets/kjsb/are.svg",
                                      width: 13, height: 13),
                                  Expanded(child: SizedBox(height: 0)),
                                  Container(
                                    decoration: BoxDecoration(
                                        color: backGround.withOpacity(0.1),
                                        borderRadius:
                                            BorderRadius.circular(16.0),
                                        boxShadow: [
                                          BoxShadow(
                                              color:
                                                  backGround.withOpacity(0.1),
                                              blurRadius: 0.5,
                                              offset: Offset(0, 1))
                                        ]),
                                    height: 10,
                                    width: 40,
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                SvgPicture.asset("assets/kjsb/motocycle.svg"),
                                Expanded(
                                  child: SizedBox(
                                    width: 2,
                                  ),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                      color: backGround.withOpacity(0.1),
                                      borderRadius: BorderRadius.circular(16.0),
                                      boxShadow: [
                                        BoxShadow(
                                            color: backGround.withOpacity(0.1),
                                            blurRadius: 0.5,
                                            offset: Offset(0, 1))
                                      ]),
                                  height: 10,
                                  width: 40,
                                ),
                              ],
                            ),
                          ]),
                    ),
                    SizedBox(width: 2),
                    Container(
                      width: 1,
                      height: 25,
                      color: Colors.brown[100],
                    ),
                    SizedBox(width: 3),
                    Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text(
                                  "Total Harga",
                                  style: TextStyle(fontSize: 8),
                                ),
                                SizedBox(
                                  width: 3,
                                ),
                                Container(
                                  child: Icon(
                                    Icons.info_outlined,
                                    size: 10,
                                    color: Colors.black,
                                  ),
                                )
                              ],
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  color: backGround.withOpacity(0.1),
                                  borderRadius: BorderRadius.circular(16.0),
                                  boxShadow: [
                                    BoxShadow(
                                        color: backGround.withOpacity(0.1),
                                        blurRadius: 0.5,
                                        offset: Offset(0, 1))
                                  ]),
                              height: 10,
                            ),
                          ]),
                    )
                  ]),
                ),
                SizedBox(height: 10),
                Row(children: [
                  Expanded(
                    child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          color: backGround.withOpacity(0.1),
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10))),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(12)),
                          color: backGround.withOpacity(0.2),
                        ),
                        height: 30,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          color: backGround.withOpacity(0.1),
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(10))),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(12),
                          ),
                          color: backGround.withOpacity(0.1),
                        ),
                        height: 30,
                      ),
                    ),
                  ),
                ])
              ],
            ),
          ),
        ),
      ),
      baseColor: Colors.grey[700],
      highlightColor: Colors.grey[100],
    );
  }
}
