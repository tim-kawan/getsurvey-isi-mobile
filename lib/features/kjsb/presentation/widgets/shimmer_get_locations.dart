import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerWidget extends StatefulWidget {
  const ShimmerWidget({Key key}) : super(key: key);

  @override
  _ShimmerWidgetState createState() => _ShimmerWidgetState();
}

class _ShimmerWidgetState extends State<ShimmerWidget> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 10,
      itemBuilder: (context, int key) {
        return Padding(
          padding: const EdgeInsets.all(18.0),
          child: Shimmer.fromColors(
            child: Container(
              height: 90,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    BoxShadow(
                        color: backGround.withOpacity(0.1),
                        blurRadius: 0.5,
                        offset: Offset(0, 1))
                  ]),
              child: Column(
                children: [
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                          color: backGround.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(16.0),
                          boxShadow: [
                            BoxShadow(
                                color: backGround.withOpacity(0.1),
                                blurRadius: 0.5,
                                offset: Offset(0, 1))
                          ]),
                      height: 10,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                          color: backGround.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(16.0),
                          boxShadow: [
                            BoxShadow(
                                color: backGround.withOpacity(0.1),
                                blurRadius: 0.5,
                                offset: Offset(0, 1))
                          ]),
                      height: 10,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                          color: backGround.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(16.0),
                          boxShadow: [
                            BoxShadow(
                                color: backGround.withOpacity(0.1),
                                blurRadius: 0.5,
                                offset: Offset(0, 1))
                          ]),
                      height: 10,
                    ),
                  ),
                ],
              ),
            ),
            baseColor: Colors.grey[700],
            highlightColor: Colors.grey[200],
          ),
        );
      },
    );
  }
}
