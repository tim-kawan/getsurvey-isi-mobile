import 'package:flutter/material.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_service_entity.dart';
import 'package:get_survey_app/features/kjsb/presentation/pages/list_kjsb_screen.dart';
import 'package:get_survey_app/features/kjsb/presentation/pages/user/categories_screen.dart';
import 'package:get_survey_app/features/kjsb/presentation/pages/user/form_kjsb_screen.dart';

import 'kjsb_routes_path.dart';

Route<dynamic> generateKjsbRoute(RouteSettings settings) {
  switch (settings.name) {
    case KjsbRoutesPath.ListKjsbRoute:
      return MaterialPageRoute(builder: (context) => ListKjsbScreen());
    case KjsbRoutesPath.CategoriesKjsbRoute:
      return MaterialPageRoute(builder: (context) => CategoriesScreen());
    case KjsbRoutesPath.FormKjsbRoute:
      return MaterialPageRoute(
          builder: (context) => FormKJSBScreen(
                service: ((settings.arguments as Map)['service']
                    as KjsbServiceEntity),
                entity: ((settings.arguments as Map)['entity'] as KjsbEntity),
              ));
  }
}
