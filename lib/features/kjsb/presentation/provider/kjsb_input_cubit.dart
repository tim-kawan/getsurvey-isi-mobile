import 'package:bloc/bloc.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_document_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_service_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/location_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_document.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_documents.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_kjsb_by_id.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_locations.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_services.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/update_status.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'kjsb_input_state.dart';

class KjsbInputCubit extends Cubit<KjsbInputState> {
  final CreateKJSB createKjsb;
  final GetKJSBById getKjsb;
  final GetServices getServices;
  final GetLocations getLocations;
  final CreateDocument createDocument;
  final UpdateStatus updateStatus;
  final GetDocuments getDocunments;

  double lat;
  double lon;

  KjsbInputCubit(
      {this.getKjsb,
      this.getDocunments,
      this.updateStatus,
      this.createDocument,
      this.getServices,
      this.getLocations,
      this.createKjsb})
      : super(KjsbInputInitial());

  var _defaultController = PublishSubject<bool>();
  var _defaultNamaController = PublishSubject<String>();
  var _defaultNikController = PublishSubject<String>();
  var _defaultPhoneController = PublishSubject<String>();
  var _defaultAlamatController = PublishSubject<String>();
  var _defaultEmailController = PublishSubject<String>();
  var _namaController = BehaviorSubject<String>();
  var _nikController = BehaviorSubject<String>();
  var _phoneController = BehaviorSubject<String>();
  var _alamatPemohonController = BehaviorSubject<String>();
  var _emailController = BehaviorSubject<String>();
  var _lokasiTanahController = BehaviorSubject<String>();
  var _defaultLokasiTanahController = BehaviorSubject<String>();
  var _luasTanahController = BehaviorSubject<String>();
  var _defaultLuasTanahController = BehaviorSubject<String>();
  var _setujuController = BehaviorSubject<bool>();
  var _successController = BehaviorSubject<bool>();
  var _locationNameController = BehaviorSubject<String>();

  ///file
  var _alasHakController = PublishSubject<KjsbDocumentEntity>();
  var _suratPermohonanPengukuranController =
      PublishSubject<KjsbDocumentEntity>();
  var _sptPbbController = PublishSubject<KjsbDocumentEntity>();
  var _aktaJualBeliController = PublishSubject<KjsbDocumentEntity>();
  var _ktpController = PublishSubject<KjsbDocumentEntity>();
  var _suratPelimpahanController = PublishSubject<KjsbDocumentEntity>();
  var _suratPermohonanController = PublishSubject<KjsbDocumentEntity>();
  var _suratPernyataanController = PublishSubject<KjsbDocumentEntity>();
  var _suratKuasaController = PublishSubject<KjsbDocumentEntity>();
  var _aktaController = PublishSubject<KjsbDocumentEntity>();

  Stream<bool> get defaultStream => _defaultController.stream;

  Stream<String> get defaultNamaStream => _defaultNamaController.stream;

  Stream<String> get namaStream => _namaController.stream;

  Stream<String> get defaultNikStream => _defaultNikController.stream;

  Stream<String> get nikStream => _nikController.stream;

  Stream<String> get defaultPhoneStream => _defaultPhoneController.stream;

  Stream<String> get phoneStream => _phoneController.stream;

  Stream<String> get defaultAlamatStream => _defaultAlamatController.stream;

  Stream<String> get alamatStream => _alamatPemohonController.stream;

  Stream<String> get defaultEmailStream => _defaultEmailController.stream;

  Stream<String> get emailStream => _emailController.stream;

  Stream<String> get lokasiStream => _lokasiTanahController.stream;

  Stream<String> get defaultLokasiStream =>
      _defaultLokasiTanahController.stream;

  Stream<String> get luasStream => _luasTanahController.stream;

  Stream<String> get defaultLuasStream => _defaultLuasTanahController.stream;

  Stream<bool> get agreeStream => _setujuController.stream;

  Stream<bool> get success => _successController.stream;

  Stream<String> get locationNameStream => _locationNameController.stream;

  Stream<KjsbDocumentEntity> get alasHakStream => _alasHakController.stream;

  Stream<KjsbDocumentEntity> get suratPermohonanPengukuranStream =>
      _suratPermohonanPengukuranController.stream;

  Stream<KjsbDocumentEntity> get sptPbbStream => _sptPbbController.stream;

  Stream<KjsbDocumentEntity> get aktaJualBeliStream =>
      _aktaJualBeliController.stream;

  Stream<KjsbDocumentEntity> get ktpStream => _ktpController.stream;

  Stream<KjsbDocumentEntity> get suratPelimpahanStream =>
      _suratPelimpahanController.stream;

  Stream<KjsbDocumentEntity> get suratPermohonanStream =>
      _suratPermohonanController.stream;

  Stream<KjsbDocumentEntity> get suratPernyataanStream =>
      _suratPernyataanController.stream;

  Stream<KjsbDocumentEntity> get suratKuasaStream =>
      _suratKuasaController.stream;

  Stream<KjsbDocumentEntity> get aktaStream => _aktaController.stream;

  validatePhone(String text) {
    _defaultPhoneController.sink.add(null);
    if (text.length < 6) {
      _phoneController.sink.addError("No telepon minimal 6 angka");
    } else {
      _phoneController.sink.add(text);
    }
  }

  validateNama(String text) {
    _defaultNamaController.add(null);
    if (text.length < 2) {
      _namaController.sink.addError("Masukkan nama anda dengan lengkap");
    } else {
      _namaController.sink.add(text);
    }
  }

  validateNIK(String text) {
    _defaultNikController.sink.add(null);
    if (text.length < 16) {
      _nikController.sink
          .addError("Nik tidak valid (${16 - text.length} angka lagi)");
    } else if (text.length > 16) {
      _nikController.sink
          .addError("Nik tidak valid (lebih  ${text.length - 16} angka)");
    } else {
      _nikController.sink.add(text);
    }
  }

  validateAlamat(String text) {
    _defaultAlamatController.sink.add(null);
    if (text.length < 2) {
      _alamatPemohonController.sink
          .addError("Masukkan alamat anda dengan lengkap");
    } else {
      _alamatPemohonController.sink.add(text);
    }
  }

  validateEmail(String text) {
    _defaultEmailController.sink.add(null);
    bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(text);
    if (emailValid) {
      _emailController.sink.add(text);
    } else {
      _emailController.sink.addError("Masukkan email yang valid");
    }
  }

  validateLokasiTanah(String text) {
    _defaultLokasiTanahController.sink.add(null);
    if (text.length < 2) {
      _lokasiTanahController.sink
          .addError("Masukkan lokasi tanah anda dengan lengkap");
    } else {
      _lokasiTanahController.sink.add(text);
    }
  }

  validateLuas(String text) {
    _defaultLuasTanahController.sink.add(null);
    if (text.length < 1) {
      _luasTanahController.sink.addError("Masukkan luas tanah");
    } else {
      _luasTanahController.sink.add(text);
    }
  }

  agree(bool agree) {
    _setujuController.sink.add(agree);
  }

  setDefaultValue(bool isDefault) async {
    _defaultController.add(isDefault);
    if (isDefault) {
      getUserData();
    }
  }

  validateLocation(String text) {
    _defaultPhoneController.sink.add(null);
    if (text.length < 4) {
      _locationNameController.sink.addError("No telepon minimal 6 angka");
    } else {
      _locationNameController.sink.add(text);
    }
  }

  Stream<bool> get readyToSubmit => Rx.combineLatest7(
      namaStream,
      nikStream,
      alamatStream,
      lokasiStream,
      phoneStream,
      lokasiStream,
      luasStream,
      (a, b, c, d, e, f, g) => true);

  Future<Stream<bool>> addKjsb(int serviceId) async {
    final params = NewKjsbParams(
        servicesId: serviceId,
        nama: _namaController.value.trim().toString(),
        nik: _nikController.value.trim().toString(),
        phone: _phoneController.value.trim().toString(),
        alamatPemohon: _alamatPemohonController.value.trim().toString(),
        email: _emailController.value.trim().toString(),
        lokasiTanah: _lokasiTanahController.value.trim().toString(),
        luas: int.parse(_luasTanahController.value.toString()),
        lat: lat,
        lon: lon,
        userId: await Session.getValue(Session.USER_ID));
    final result = await createKjsb(params);
    result.fold((fail) => emit(KjsbInputFailure()), (kjsbId) async {
      final kjsb = await getKjsb(kjsbId.toString());
      kjsb.fold((l) => null, (entity) => emit(KjsbInputSuccess(entity.kjsb)));
    });
  }

  Future<void> clearInitial() async {}

  Future<KjsbEntity> loadDraftKjsb(KjsbEntity entity) async {
    _namaController.sink.add(entity.nama);
    _defaultNamaController.sink.add(entity.nama);
    _defaultNikController.sink.add(entity.nik);
    _nikController.sink.add(entity.nik);
    _defaultAlamatController.sink.add(entity.alamatPemohon);
    _alamatPemohonController.sink.add(entity.alamatPemohon);
    _defaultPhoneController.sink.add(entity.phone);
    _phoneController.sink.add(entity.phone);
    _defaultEmailController.sink.add(entity.email);
    _emailController.sink.add(entity.email);
    _defaultLokasiTanahController.sink.add(entity.lokasiTanah);
    _lokasiTanahController.sink.add(entity.lokasiTanah);
    _defaultLuasTanahController.sink.add(entity.luasTanah.toString());
    _luasTanahController.sink.add(entity.luasTanah.toString());
  }

  void getUserData() async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    _defaultNamaController.add(preferences.getString(Session.USER_NAME));
    _namaController.sink.add(preferences.getString(Session.USER_NAME));
    _defaultPhoneController.add(preferences.getString(Session.USER_NO_TELP));
    _phoneController.sink.add(preferences.getString(Session.USER_NO_TELP));
    _defaultAlamatController.add(preferences.getString(Session.ALAMAT));
    _alamatPemohonController.sink.add(preferences.getString(Session.ALAMAT));
    _defaultEmailController.add(preferences.getString(Session.USER_EMAIL));
    _emailController.sink.add(preferences.getString(Session.USER_EMAIL));
  }

  Future<List<KjsbServiceEntity>> getAllServices() async {
    final result = await getServices(NoParams());
    result.fold((fail) => emit(KjsbInputFailure()),
        (listEntity) => emit(KjsbServicesState(listService: listEntity)));
  }

  void searchClick() {
    emit(SearchClickState());
  }

  Future<void> findLocation() async {
    emit(LoadingSearchClickState());
    final result =
        await getLocations(_locationNameController.value.trim().toString());
    result.fold((fail) => emit(KjsbInputFailure()),
        (listEntity) => emit(GetLocationsState(listlocation: listEntity)));
  }

  Future<void> selectLocation(LocationEntity location) async {
    emit(SelectedLocationState(location: location));
  }

  Future<void> saveLandLocation(LocationEntity entity) async {
    emit(SaveLocationSuccessState(entity));
  }

  Future<void> loadLandLocation(LocationEntity entity) async {
    lat = entity.lat;
    lon = entity.lng;
    _defaultLokasiTanahController.sink.add(entity.address);
    _lokasiTanahController.sink.add(entity.address);
  }

  Future<void> loadApplicationLocation(LocationEntity entity) async {
    _defaultAlamatController.sink.add(entity.address);
    _alamatPemohonController.sink.add(entity.address);
  }

  Future<void> loadAllFiles(String kjsbId) async {
    final documents = await getDocunments(kjsbId);
    documents.fold((l) => emit(UnLoadingState()), (document) async {
      emit(UnLoadingState());
      await Future.delayed(Duration(milliseconds: 200));
      emit(LoadingState());
      await Future.delayed(Duration(seconds: 1));
      emit(UnLoadingState());
      _mapDocuments(document);
    });
  }

  Future<void> uploadFile(String filePath, documentType, String kjsbId) async {
    if (filePath.isNotEmpty) {
      emit(LoadingState());
      final params = DocumentParams(
          kjsbId: kjsbId, path: filePath, documentType: documentType);
      final result = await createDocument(params);
      result.fold((l) => emit(UnLoadingState()), (documents) {
        _mapDocuments(documents);
        emit(UnLoadingState());
      });
    }
  }

  void _mapDocuments(List<KjsbDocumentEntity> documents) {
    documents.forEach((element) {
      switch (element.type) {
        case KjsbDocumentEntity.ALAS_HAK:
          _alasHakController.sink.add(element);
          return;
        case KjsbDocumentEntity.PERMOHONAN_PENGUKURAN:
          _suratPermohonanPengukuranController.sink.add(element);
          return;
        case KjsbDocumentEntity.SPT_PBB:
          _sptPbbController.sink.add(element);
          return;
        case KjsbDocumentEntity.AJB:
          _aktaJualBeliController.sink.add(element);
          return;
        case KjsbDocumentEntity.KTP:
          _ktpController.sink.add(element);
          return;
        case KjsbDocumentEntity.PELIMPAHAN_PENGUKURAN:
          _suratPelimpahanController.sink.add(element);
          return;
        case KjsbDocumentEntity.SURAT_PERMOHONAN:
          _suratPermohonanController.sink.add(element);
          return;
        case KjsbDocumentEntity.PENGUASAAN_FISIK:
          _suratPernyataanController.sink.add(element);
          return;
        case KjsbDocumentEntity.SURAT_KUASA:
          _suratKuasaController.sink.add(element);
          return;
        case KjsbDocumentEntity.AKTA_PENDIRIAN:
          _aktaController.sink.add(element);
          return;
      }
    });
  }

  Future<void> changeStatusTagging(int kjsbId) async {
    emit(LoadingState());
    final params = StatusParams(kjsbId, KjsbEntity.taggingLocation);
    final result = await updateStatus(params);
    result.fold((l) => emit(UnLoadingState()), (r) {
      emit(UpdateStatusSuccessState());
    });
  }
}
