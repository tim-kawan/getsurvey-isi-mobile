part of 'kjsb_surveyor_cubit.dart';

@immutable
abstract class KjsbSurveyorState {}

class GetKjsbSurveyorInitial extends KjsbSurveyorState {}

class ResultGetAllKjsbSurveyorState extends KjsbSurveyorState {
  final List<TransactionKjsbEntity> listSurveyor;

  ResultGetAllKjsbSurveyorState(this.listSurveyor);
}

class LoadingState extends DetailKjsbState {}

class UnLoadingState extends KjsbSurveyorState {}

class SelectSurveyorSuccessState extends KjsbSurveyorState {}

class DetailKjsbState extends KjsbSurveyorState {
  final TransactionKjsbEntity kjsbEntity;
  final DetailKjsbEntity detailEntity;
  final int subtractionPanelHeight;

  DetailKjsbState(
      {this.detailEntity, this.kjsbEntity, this.subtractionPanelHeight});
}

class WaitApproveBySurveyorState extends DetailKjsbState {
  final int timeToApprove;

  WaitApproveBySurveyorState(DetailKjsbEntity detailEntity, this.timeToApprove)
      : super(detailEntity: detailEntity, subtractionPanelHeight: 100);
}

class DocumentNotDownloadState extends DetailKjsbState {
  DocumentNotDownloadState(TransactionKjsbEntity kjsbEntity)
      : super(kjsbEntity: kjsbEntity, subtractionPanelHeight: 150);
}

class DownloadDocumentState extends DetailKjsbState {
  DownloadDocumentState(DetailKjsbEntity detailEntity, subtractionPanelHeight)
      : super(detailEntity: detailEntity, subtractionPanelHeight: 200);
}

class VerifyBySurveyorState extends DetailKjsbState {
  final int timeToVerify;

  VerifyBySurveyorState(DetailKjsbEntity detailEntity, this.timeToVerify)
      : super(
            kjsbEntity: detailEntity.transaction,
            detailEntity: detailEntity,
            subtractionPanelHeight: 160);
}

class HasPaidState extends DetailKjsbState {
  HasPaidState(TransactionKjsbEntity kjsbEntity)
      : super(kjsbEntity: kjsbEntity, subtractionPanelHeight: 0);
}

class WaitPaymentState extends DetailKjsbState {
  WaitPaymentState(DetailKjsbEntity detailKjsbEntity)
      : super(
            detailEntity: detailKjsbEntity,
            kjsbEntity: detailKjsbEntity.transaction,
            subtractionPanelHeight: 0);
}

class WaitScheduledState extends DetailKjsbState {
  WaitScheduledState(DetailKjsbEntity detailKjsbEntity)
      : super(detailEntity: detailKjsbEntity, subtractionPanelHeight: 80);
}

class SetScheduledState extends DetailKjsbState {
  SetScheduledState(DetailKjsbEntity detailEntity)
      : super(detailEntity: detailEntity, subtractionPanelHeight: 20);
}

class WaitApproveScheduled extends DetailKjsbState {
  WaitApproveScheduled(DetailKjsbEntity detailKjsbEntity)
      : super(detailEntity: detailKjsbEntity, subtractionPanelHeight: 0);
}
