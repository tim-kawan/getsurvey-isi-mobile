import 'package:bloc/bloc.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/detail_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_schedule_log_dto.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/list_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/approve_kjsb_schedule.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_all_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_kjsb_by_id.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/load_tagging.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

part 'get_kjsb_state.dart';

class GetKjsbCubit extends Cubit<GetKjsbState> {
  final GetAllKJSB getAllKjsb;
  final GetKJSBById getKjsbId;
  final ApproveKJSBSchedule approveKjsbSchedule;
  final LoadTagging loadTagging;

  GetKjsbCubit({
    this.getAllKjsb,
    this.approveKjsbSchedule,
    this.getKjsbId,
    this.loadTagging,
  }) : super(GetKjsbInitial());

  var _agreeKjsbSchedule = PublishSubject<bool>();

  Stream<bool> get agreeKjsbScheduleStream => _agreeKjsbSchedule.stream;

  Future<void> getKjsb(int type) async {
    final userId = await Session.getValue(Session.USER_ID);
    var result = await getAllKjsb(KjsbEntity.draft);
    result.listen((event) {
      event.fold((l) => emit(GetAllKjsbByType(listKjsb: [])),
          (list) => emit(GetAllKjsbByType(listKjsb: list, userId: userId)));
    });
  }

  Future<void> getKjsbById(String id) async {
    emit(LoadingState());
    var result = await getKjsbId(id);
    result.fold((l) => emit(GetKjsbByIdState()), (detailEntity) {
      if (detailEntity.kjsb.status == KjsbEntity.taggingLocation) {
        emit(TakeBoundariesState(detailEntity));
      } else if (detailEntity.kjsb.status == KjsbEntity.findSurveyor) {
        emit(FindSurveyorState(detailEntity, 0));
        countBorder(detailEntity);
      } else if (detailEntity.kjsb.status == KjsbEntity.waitApproveBySurveyor)
        emit(WaitApproveBySurveyorState(detailEntity));
      else if (detailEntity.kjsb.status == KjsbEntity.verifyBySurveyor)
        emit(VerifyBySurveyorState(detailEntity));
      else if (detailEntity.kjsb.status == KjsbEntity.waitingPayment ||
          detailEntity.kjsb.status == KjsbEntity.paymentGatewayCreated) {
        int remainingTimeToPay = getTimeTopay(detailEntity.scheduledLogs);
        emit(WaitPaymentState(detailEntity, remainingTimeToPay));
      } else if (detailEntity.kjsb.status == KjsbEntity.waitScheduled)
        emit(WaitScheduledState(detailEntity));
      else if (detailEntity.kjsb.status == KjsbEntity.waitApproveScheduled)
        emit(WaitApproveScheduledState(detailEntity.kjsb, detailEntity));
      else if (detailEntity.kjsb.status == KjsbEntity.approvedSchedule)
        emit(ScheduledApprovedState(detailEntity.kjsb, detailEntity));
      else if (detailEntity.kjsb.status == KjsbEntity.shareLocation && false)
        emit(ScheduledApprovedState(detailEntity.kjsb, detailEntity));
      else if (detailEntity.kjsb.status == KjsbEntity.shareLocation)
        emit(WaitLiveLocationState(detailEntity));
    });
  }

  Future<void> approveSchedule(int scheduleId) async {
    emit(LoadingState());
    final result = await approveKjsbSchedule(scheduleId);
    result.fold((l) => emit(UnLoadingState()), (success) {
      if (success)
        emit(ApproveScheduledSuccessState());
      else
        emit(UnLoadingState());
    });
  }

  void countBorder(DetailKjsbEntity detailKjsbEntity) async {
    final result = await loadTagging(detailKjsbEntity.kjsb.id);
    result.fold((l) => null,
        (r) => emit(FindSurveyorState(detailKjsbEntity, r.length)));
  }

  int getTimeTopay(List<KjsbScheduleLogEntity> scheduledLogs) {
    int timeToPay = 0;
    scheduledLogs.forEach((element) {
      if (element.type == KjsbEntity.waitingPayment) {
        timeToPay = element.milisecond;
      }
    });
    return timeToPay;
  }

  void agreeWithScheduled(bool agree) {
    _agreeKjsbSchedule.sink.add(agree);
  }
}
