part of 'get_kjsb_cubit.dart';

@immutable
abstract class GetKjsbState {}

class GetKjsbInitial extends GetKjsbState {}

class LoadingState extends GetKjsbState {}

class UnLoadingState extends GetKjsbState {}

class DetailKjsbState extends GetKjsbState {
  final DetailKjsbEntity detailKjsbEntity;

  DetailKjsbState(this.detailKjsbEntity);
}

class TakeBoundariesState extends DetailKjsbState {
  TakeBoundariesState(DetailKjsbEntity detailKjsbEntity)
      : super(detailKjsbEntity);
}

class FindSurveyorState extends DetailKjsbState {
  final int numberBorder;

  FindSurveyorState(DetailKjsbEntity detailKjsbEntity, this.numberBorder)
      : super(detailKjsbEntity);
}

class WaitApproveBySurveyorState extends DetailKjsbState {
  WaitApproveBySurveyorState(DetailKjsbEntity detailKjsbEntity)
      : super(detailKjsbEntity);
}

class VerifyBySurveyorState extends DetailKjsbState {
  VerifyBySurveyorState(DetailKjsbEntity detailKjsbEntity)
      : super(detailKjsbEntity);
}

class HasPaidState extends DetailKjsbState {
  HasPaidState(DetailKjsbEntity detailKjsbEntity) : super(detailKjsbEntity);
}

class ScheduledState extends DetailKjsbState {
  ScheduledState(DetailKjsbEntity detailKjsbEntity) : super(detailKjsbEntity);
}

class WaitScheduledState extends ScheduledState {
  WaitScheduledState(DetailKjsbEntity detailEntity) : super(detailEntity);
}

class WaitApproveScheduledState extends ScheduledState {
  WaitApproveScheduledState(
      KjsbEntity kjsbEntity, DetailKjsbEntity detailEntity)
      : super(detailEntity);
}

class ScheduledApprovedState extends ScheduledState {
  ScheduledApprovedState(KjsbEntity kjsbEntity, DetailKjsbEntity detailEntity)
      : super(detailEntity);
}

class WaitPaymentState extends DetailKjsbState {
  final int remainingTimeToPay;

  WaitPaymentState(DetailKjsbEntity detailKjsbEntity, this.remainingTimeToPay)
      : super(detailKjsbEntity);
}

class GetAllKjsbByType extends GetKjsbState {
  final List<ListKjsbEntity> listKjsb;
  final int userId;

  GetAllKjsbByType({this.userId, this.listKjsb});
}

class GetKjsbByIdState extends GetKjsbState {
  final KjsbEntity kjsb;

  GetKjsbByIdState({this.kjsb});
}

class ApproveScheduledSuccessState extends GetKjsbState {}

class WaitLiveLocationState extends ScheduledState {
  WaitLiveLocationState(DetailKjsbEntity detailEntity) : super(detailEntity);
}
