import 'package:bloc/bloc.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/detail_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_schedule_log_dto.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/transaction_kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/approve_kjsb_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_schedule_kjsb.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_all_kjsb_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_kjsb_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/select_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/verivied_kjsb_surveyor.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sprintf/sprintf.dart';

part 'kjsb_surveyor_state.dart';

class KjsbSurveyorCubit extends Cubit<KjsbSurveyorState> {
  final GetAllKJSBSurveyor getAllKjsbSurveyor;
  final SelectSurveyor selectSurveyor;
  final GetKJSBSurveyor getKjsbSurveyor;
  final ApproveKJSBSurveyor approveKjsbSurveyor;
  final VerifiedKJSBSurveyor verifiedKJSBSurveyor;
  final CreateScheduleKJSB createScheduleKJSB;

  var _dateController = BehaviorSubject<String>();
  var _timeController = BehaviorSubject<String>();

  Stream<String> get dateStream => _dateController.stream;

  Stream<String> get timeStream => _timeController.stream;

  KjsbSurveyorCubit(
      {this.approveKjsbSurveyor,
      this.getKjsbSurveyor,
      this.createScheduleKJSB,
      this.selectSurveyor,
      this.verifiedKJSBSurveyor,
      this.getAllKjsbSurveyor})
      : super(GetKjsbSurveyorInitial());

  Future<void> getSurveyorKjsb(String kjsbId) async {
    final result = await getAllKjsbSurveyor(kjsbId);
    result.fold((fail) => null,
        (listSurveyor) => emit(ResultGetAllKjsbSurveyorState(listSurveyor)));
  }

  Future<void> selectSurveyorKjsb(
      String kjsbId, TransactionKjsbEntity entity) async {
    emit(LoadingState());
    final params = SelectSurveyorParams(
        kjsbId: kjsbId,
        surveyorId: entity.surveyorEntity.id,
        distance: entity.distance,
        totalPrice: entity.totalPrice,
        transportationPrice: entity.transportationPrice);
    final result = await selectSurveyor(params);
    result.fold((fail) => emit(UnLoadingState()),
        (successSelectSurveyor) => emit(SelectSurveyorSuccessState()));
  }

  Future<void> getKjsbById(String id) async {
    var result = await getKjsbSurveyor(id);
    result.fold((fail) => emit(UnLoadingState()), (detailEntity) {
      if (detailEntity.kjsb.status == KjsbEntity.waitApproveBySurveyor)
        emit(WaitApproveBySurveyorState(
            detailEntity, getTimeToApprove(detailEntity.scheduledLogs)));
      else if (detailEntity.kjsb.status == KjsbEntity.verifyBySurveyor)
        emit(VerifyBySurveyorState(
            detailEntity, getTimeToVerifyDocument(detailEntity.scheduledLogs)));
      else if (detailEntity.kjsb.status == KjsbEntity.waitingPayment)
        emit(WaitPaymentState(detailEntity));
      else if (detailEntity.kjsb.status == KjsbEntity.waitScheduled)
        emit(WaitScheduledState(detailEntity));
      else
        emit(DetailKjsbState(
            detailEntity: detailEntity,
            subtractionPanelHeight: 0,
            kjsbEntity: detailEntity.transaction));
    });
  }

  Future<void> approveBySurveyor(String id) async {
    emit(LoadingState());
    var result = await approveKjsbSurveyor(id);
    result.fold((l) => null, (success) async {
      var entity = await getKjsbSurveyor(id);
      entity.fold((l) => emit(UnLoadingState()), (kjsb) {
        if (kjsb.kjsb.status == KjsbEntity.verifyBySurveyor)
          emit(DocumentNotDownloadState(kjsb.transaction));
      });
    });
  }

  Future<void> downloadDocumentKjsb(DetailKjsbEntity entity) async {
    emit(DownloadDocumentState(entity, 100));
    await Future.delayed(Duration(seconds: 5));
    emit(VerifyBySurveyorState(
      entity,
      getTimeToVerifyDocument(entity.scheduledLogs),
    ));
  }

  Future<void> verifyBySurveyor(String id) async {
    emit(LoadingState());
    final result = await verifiedKJSBSurveyor(id);
    result.fold((l) => null, (success) async {
      var entity = await getKjsbSurveyor(id);
      entity.fold((l) => emit(UnLoadingState()), (kjsb) {
        if (kjsb.kjsb.status == KjsbEntity.verifyBySurveyor)
          emit(WaitPaymentState(kjsb));
      });
    });
  }

  Future<void> setScheduled(DetailKjsbEntity entity) async {
    emit(SetScheduledState(entity));
  }

  void setDate(DateTime date) {
    _dateController.sink
        .add(sprintf("%04i-%02i-%02i", [date.year, date.month, date.day]));
  }

  void setTime(DateTime date) {
    _timeController.sink.add(sprintf("%02i:%02i", [date.hour, date.minute]));
  }

  Future<void> createScheduled(KjsbEntity entity) async {
    emit(LoadingState());
    final params = CreateScheduleParams(
      kjsbId: entity.id,
      date: _dateController.value.toString(),
      time: _timeController.value.toString(),
    );
    final result = await createScheduleKJSB(params);
    result.fold((l) => emit(UnLoadingState()), (success) async {
      var kjsbResult = await getKjsbSurveyor(entity.id);
      kjsbResult.fold((l) => emit(UnLoadingState()), (detailEntity) {
        if (detailEntity.kjsb.status == KjsbEntity.waitApproveScheduled)
          emit(WaitApproveScheduled(detailEntity));
      });
    });
  }

  int getTimeToVerifyDocument(List<KjsbScheduleLogEntity> scheduledLogs) {
    int timeToVerify = 0;
    scheduledLogs?.forEach((element) {
      if (element.type == KjsbEntity.verifyBySurveyor) {
        timeToVerify = element.milisecond;
      }
    });
    return timeToVerify;
  }

  int getTimeToApprove(List<KjsbScheduleLogEntity> scheduledLogs) {
    int timeToVerify = 0;
    scheduledLogs?.forEach((element) {
      if (element.type == KjsbEntity.waitApproveBySurveyor) {
        timeToVerify = element.milisecond;
      }
    });
    return timeToVerify;
  }
}
