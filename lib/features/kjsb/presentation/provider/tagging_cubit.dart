import 'package:bloc/bloc.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/tagging_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/create_tagging.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/load_tagging.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/update_status.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/update_tagging.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/upload_tagging.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';
import 'package:uuid/uuid.dart';

part 'tagging_state.dart';

class TaggingCubit extends Cubit<TaggingState> {
  final CreateTagging createTagging;
  final LoadTagging loadTagging;
  final UploadTagging uploadTagging;
  final UpdateTagging updateTagging;
  final UpdateStatus updateStatus;

  TaggingCubit({
    this.updateStatus,
    this.createTagging,
    this.uploadTagging,
    this.loadTagging,
    this.updateTagging,
  }) : super(TaggingInitial());

  Future<void> loadAllTagging(String kjsbId) async {
    final result = await loadTagging(kjsbId);
    result.fold((failure) => null, (value) {
      emit(TaggingResultState((value.map((entity) => entity).toList())));
    });
  }

  Future<void> addTagging(String filePath, LatLng latLng, String kjsbId) async {
    emit(LoadingUploadTaggingState());
    var uuid = Uuid();
    final params = TaggingParams(
      kjsbId: kjsbId,
      id: uuid.v4(),
      filePath: filePath,
      lng: latLng.longitude,
      lat: latLng.latitude,
    );
    final result = await createTagging(params);
    result.fold((failure) => emit(UnLoadingUploadTaggingState()), (value) {
      emit(TaggingResultState((value.map((entity) => entity).toList())));
    });
  }

  Future<void> moveMarker(String id, double latitude, double longitude) async {
    final params = TaggingParams(
      id: id,
      lat: latitude,
      lng: longitude,
    );
    await updateTagging(params);
  }

  Future<void> uploadsTagging(String kjsbId) async {
    emit(LoadingUploadTaggingState());
    final result = await uploadTagging(kjsbId);
    result.fold((fail) => emit(UnLoadingUploadTaggingState()), (r) async {
      if (r) {
        final params = StatusParams(int.parse(kjsbId), KjsbEntity.findSurveyor);
        final status = await updateStatus(params);
        status.fold((fail) => emit(UnLoadingUploadTaggingState()),
            (updated) => emit(UploadTaggingSuccessState()));
      } else
        emit(UnLoadingUploadTaggingState());
    });
  }
}
