part of 'kjsb_input_cubit.dart';

@immutable
abstract class KjsbInputState {}

class KjsbInputInitial extends KjsbInputState {}

class KjsbInputSuccess extends KjsbInputState {
  final KjsbEntity entity;

  KjsbInputSuccess(this.entity);
}

class LoadingState extends KjsbInputState {}

class UnLoadingState extends KjsbInputState {}

class KjsbInputFailure extends KjsbInputState {}

class UpdateStatusSuccessState extends KjsbInputState {}

class KjsbServicesState extends KjsbInputState {
  final List<KjsbServiceEntity> listService;

  KjsbServicesState({this.listService});
}

class GetLocationsState extends KjsbInputState {
  final List<LocationEntity> listlocation;

  GetLocationsState({this.listlocation});
}

class SelectedLocationState extends KjsbInputState {
  final LocationEntity location;

  SelectedLocationState({this.location});
}

class SearchClickState extends KjsbInputState {}

class LoadingSearchClickState extends KjsbInputState {}

class SaveLocationSuccessState extends KjsbInputState {
  final LocationEntity location;

  SaveLocationSuccessState(this.location);
}

class DocumentState extends KjsbInputState {
  final List<KjsbDocumentEntity> documents;

  DocumentState(this.documents);
}
