part of 'tagging_cubit.dart';

@immutable
abstract class TaggingState {}

class TaggingInitial extends TaggingState {}

class TaggingResultState extends TaggingState {
  final List<TaggingEntity> markers;

  TaggingResultState(this.markers);
}

class LoadingUploadTaggingState extends TaggingState {}

class UnLoadingUploadTaggingState extends TaggingState {}

class UploadTaggingSuccessState extends TaggingState {}
