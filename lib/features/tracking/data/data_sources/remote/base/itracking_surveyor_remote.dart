import 'package:get_survey_app/features/tracking/domain/use_cases/tracking_surveyor.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/distance.dart';

abstract class ITrackingSurveyorRemote{
  Future<List<Element>> distanceMatrix(LatLng asal, LatLng tujuan);
  Future<List<dynamic>> searchPlaces(String lokasi);
  Future<bool> trackGoing(TrackingSurveyorParams params);
}