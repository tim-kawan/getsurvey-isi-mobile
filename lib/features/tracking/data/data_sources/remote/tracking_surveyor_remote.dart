import 'dart:convert';

import 'package:get_survey_app/features/tracking/data/data_sources/remote/base/itracking_surveyor_remote.dart';
import 'package:get_survey_app/features/tracking/domain/use_cases/tracking_surveyor.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:google_maps_flutter_platform_interface/src/types/location.dart';
import 'package:google_maps_webservice/directions.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:google_maps_webservice/src/distance.dart';
import 'package:http/http.dart' as http;

class TrackingSurveyorRemote implements ITrackingSurveyorRemote {
  final http.Client client;

  TrackingSurveyorRemote(this.client);

  @override
  Future<List<Element>> distanceMatrix(LatLng asal, LatLng tujuan) async {
    final distanceMatrix =
        new GoogleDistanceMatrix(apiKey: RestAPI.GOOGLE_API_KEY);
    List<Element> distanceValues = [];
    DistanceResponse distanceResponse = await distanceMatrix
        .distanceWithLocation([
      Location(asal.latitude, asal.longitude)
    ], [
      Location(tujuan.latitude, tujuan.longitude)
    ], travelMode: TravelMode.driving, languageCode: "id");

    if (distanceResponse.isOkay) {
      distanceResponse.results.forEach((element) {
        element.elements.forEach((inner) {
          distanceValues.add(inner);
        });
      });
    }

    return distanceValues;
  }

  @override
  Future<List> searchPlaces(String lokasi) async {
    final places = new GoogleMapsPlaces(apiKey: RestAPI.GOOGLE_API_KEY);
    PlacesSearchResponse placesResponse = await places.searchByText(lokasi);
    List results = [];

    if (placesResponse.isOkay) {
      placesResponse.results.forEach((element) {
        results.add({
          "name": element.name,
          "address": element.formattedAddress,
          "lat": element.geometry.location.lat,
          "lng": element.geometry.location.lng,
        });
      });
    }

    return results;
  }

  @override
  Future<bool> trackGoing(TrackingSurveyorParams params) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response =
        await client.post(Uri.parse("${RestAPI.API_URL}/irk/mapping/going"),
            headers: {
              'Content-Type': 'application/json',
              'Authorization': authToken,
            },
            body: json.encode({
              "irk": params.idIrk,
              "done": params.status,
              "aktualLat": params.aktualLat,
              "aktualLng": params.aktualLong
            }));
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
