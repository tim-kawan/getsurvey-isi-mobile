class TrackingSurveyorEntity{
  final double latSurveyor;
  final double longSurveyor;
  final double latPermohon;
  final double longPermohon;

  TrackingSurveyorEntity(this.latSurveyor, this.longSurveyor, this.latPermohon, this.longPermohon);

}