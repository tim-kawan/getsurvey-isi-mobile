import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/tracking/domain/entities/tracking_surveyor_entity.dart';

class TrackingSurveyor extends UseCase<List<TrackingSurveyorEntity>, String>{
  @override
  Future<Either<Failure, List<TrackingSurveyorEntity>>> call(String params) async{
    return Right([
      TrackingSurveyorEntity(0.0, 0.0, 0.0, 0.0)
    ]
    );
  }

}

class TrackingSurveyorParams{
  final int idIrk;
  final bool status;
  final double aktualLat;
  final double aktualLong;

  TrackingSurveyorParams(
      {this.idIrk, this.status, this.aktualLat, this.aktualLong});

  TrackingSurveyorParams setFromEntity()=> TrackingSurveyorParams(
    status: status,
    idIrk: idIrk,
    aktualLat: aktualLat,
    aktualLong: aktualLong
  );

}