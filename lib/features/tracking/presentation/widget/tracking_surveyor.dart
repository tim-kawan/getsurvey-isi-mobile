import 'dart:async';
import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/service/MapsAPI.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:slidable_button/slidable_button.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class TrackingLocationSurveyor extends StatefulWidget {
  final double lng;
  final double lat;
  final String namaPemilik;
  final String alamatTanah;
  final int id;

  const TrackingLocationSurveyor(
      {Key key,
      this.lng,
      this.lat,
      this.id,
      this.namaPemilik,
      this.alamatTanah})
      : super(key: key);

  @override
  _TrackingLocationSurveyorState createState() =>
      _TrackingLocationSurveyorState();
}

class _TrackingLocationSurveyorState extends State<TrackingLocationSurveyor> {
  final Completer<GoogleMapController> _controller = Completer();
  final DefaultCacheManager _cacheManager = new DefaultCacheManager();
  BitmapDescriptor sourceIcon, destinationIcon;
  Set<Marker> _markers = Set();
  Set<Polyline> _polylines = Set();
  StreamSubscription<LocationData> _streamSubscription;
  LatLng latLng = LatLng(-6.2685497, 107.0496954);
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  bool statusMulai = false;
  String jarak = "0 km", waktu = "0 detik";
  PanelController _pc = new PanelController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tracking Lokasi"),
        centerTitle: true,
        backgroundColor: primaryColor,
      ),
      body: Stack(
        children: <Widget>[
          _body(),
          statusMulai
              ? Positioned(
                  top: 20,
                  left: 5,
                  child: InkWell(
                    onTap: () {
                      MapsAPI.openUrlMaps(
                          latLng, LatLng(widget.lat, widget.lng));
                    },
                    child: CircleAvatar(
                      backgroundColor: Colors.white,
                      child: Icon(
                        Icons.assistant_navigation,
                        size: 40,
                        color: Colors.red,
                      ),
                    ),
                  ),
                )
              : Container(),
          SlidingUpPanel(
            controller: _pc,
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            panelBuilder: (sc) => _panelDone(sc),
            maxHeight: 250,
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    if (_streamSubscription != null) {
      _streamSubscription.cancel();
    }

    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _doHere();
  }

  Widget _body() {
    return GoogleMap(
      mapType: MapType.normal,
      initialCameraPosition: CameraPosition(
        target: latLng,
        zoom: 14.4746,
      ),
      myLocationButtonEnabled: true,
      myLocationEnabled: true,
      markers: _markers,
      polylines: _polylines,
      onMapCreated: (GoogleMapController controller) {
        _controller.complete(controller);
      },
    );
  }

  _doHere() async {
    await _cacheManager.emptyCache();
    Location _location = new Location();
    bool _isOn = await _location.requestService();
    PermissionStatus _isAllow = await _location.requestPermission();
    sourceIcon = await _bitmapDescriptorFromSvgAsset(
        context, 'assets/icons/ic_surveyor_map.svg');
    destinationIcon = await _bitmapDescriptorFromSvgAsset(
        context, 'assets/icons/ic_location_me.svg');
    var tracked = await RestAPI.trackGet(widget.id);
    statusMulai = tracked != null;
    setState(() {});

    if (_isOn && _isAllow == PermissionStatus.GRANTED) {
      _location.changeSettings(interval: 15000); // reload after 15 seconds
      _streamSubscription =
          _location.onLocationChanged().listen((currentLocation) async {
        latLng = LatLng(currentLocation.latitude, currentLocation.longitude);
        GoogleMapController mapController = await _controller.future;
        mapController
            .animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: latLng,
          zoom: 14.4746,
        )));
        await RestAPI.trackGoing(widget.id, latLng, false);
        setState(() {});
        _setMapPins();
        _setPolylines();
      });
    }
  }

  void _setMapPins() {
    _markers.clear();
    // source pin
    _markers.add(Marker(
      markerId: MarkerId("sourcePin"),
      position: latLng,
      icon: sourceIcon,
    ));
    // destination pin
    _markers.add(Marker(
      markerId: MarkerId("destPin"),
      position: LatLng(widget.lat, widget.lng),
      icon: destinationIcon,
    ));
    setState(() {});
  }

  _setPolylines() async {
    PolylineResult geometry = await polylinePoints?.getRouteBetweenCoordinates(
      RestAPI.GOOGLE_API_KEY,
      PointLatLng(latLng.latitude, latLng.longitude),
      PointLatLng(widget.lat, widget.lng),
      travelMode: TravelMode.driving,
    );
    var disDur =
        await MapsAPI.distanceMatrix(latLng, LatLng(widget.lat, widget.lng));
    _polylines.clear();
    polylineCoordinates.clear();

    disDur.forEach((element) {
      jarak = element.distance.text;
      waktu = element.duration.text;
    });

    geometry.points.forEach((element) {
      polylineCoordinates.add(LatLng(element.latitude, element.longitude));
    });

    _polylines.add(Polyline(
        polylineId: PolylineId("${widget.id}"),
        width: 3,
        color: Color.fromARGB(255, 40, 122, 198),
        points: polylineCoordinates));
    setState(() {});
  }

  Future<BitmapDescriptor> _bitmapDescriptorFromSvgAsset(
      BuildContext context, String assetName) async {
    var svgString = await DefaultAssetBundle.of(context).loadString(assetName);

    var svgDrawableRoot = await svg.fromSvgString(svgString, null);

    var queryData = MediaQuery.of(context);
    var devicePixelRatio = queryData.devicePixelRatio;
    var width = 48 * devicePixelRatio; // SVG's original width
    var height = 48 * devicePixelRatio; // same thing

    var picture = svgDrawableRoot.toPicture(size: Size(width, height));

    var image = await picture.toImage(width.toInt(), height.toInt());
    var bytes = await image.toByteData(format: ui.ImageByteFormat.png);
    return BitmapDescriptor.fromBytes(bytes.buffer.asUint8List());
  }

  void showAlertDialog(BuildContext context) {}
  Widget _panel(ScrollController sc) {
    return MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView(controller: sc, children: <Widget>[
          SizedBox(
            height: 12.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 30,
                height: 5,
                decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.all(Radius.circular(12.0))),
              ),
            ],
          ),
          SizedBox(
            height: 18.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Apakah Anda sudah sampai?",
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 24.0,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          Text(
            "Geser tombol berbentuk arah panah jika Anda sudah tiba ditujuan.",
            style: TextStyle(
              fontWeight: FontWeight.normal,
              color: Color(0xff7E7E7E),
              fontSize: 16.0,
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          SlidableButton(
              color: Color(0xffAEE9EB),
              width: MediaQuery.of(context).size.width / 1,
              height: 49,
              // color: Theme.of(context).accentColor.withOpacity(0.5),
              buttonColor: Color(0xff00C045),
              dismissible: false,
              borderRadius: BorderRadius.circular(5),
              label: Center(
                  child: Icon(
                    Icons.arrow_forward,
                    color: Colors.white,
                  )),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Sudah Tiba",
                        style: TextStyle(
                            fontSize: 19,
                            fontWeight: FontWeight.bold,
                            color: Colors.white))
                  ],
                ),
              ),
              onChanged: (position) {
                print("A");
              }),
        ]));
  }

  Widget _panelDone(ScrollController sc) {
    return MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView(controller: sc, children: <Widget>[
          SizedBox(
            height: 12.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 30,
                height: 5,
                decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.all(Radius.circular(12.0))),
              ),
            ],
          ),
          SizedBox(
            height: 18.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Pengukuran Selesai",
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 24.0,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          Text(
            "Geser tombol berbentuk arah panah jika pengukuran telah selesai dilakukan.",
            style: TextStyle(
              fontWeight: FontWeight.normal,
              color: Color(0xff7E7E7E),
              fontSize: 16.0,
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          SlidableButton(
              color: Color(0xffAEE9EB),
              width: MediaQuery.of(context).size.width / 1,
              height: 49,
              // color: Theme.of(context).accentColor.withOpacity(0.5),
              buttonColor: Color(0xff00C045),
              dismissible: false,
              borderRadius: BorderRadius.circular(5),
              label: Center(
                  child: Icon(
                    Icons.arrow_forward,
                    color: Colors.white,
                  )),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Selesai Mengukur",
                        style: TextStyle(
                            fontSize: 19,
                            fontWeight: FontWeight.bold,
                            color: Colors.white))
                  ],
                ),
              ),
              onChanged: (position) {
                print("A");
              }),
        ]));
  }
}
