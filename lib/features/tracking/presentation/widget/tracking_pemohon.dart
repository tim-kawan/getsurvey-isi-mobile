import 'dart:async';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/service/MapsAPI.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:slidable_button/slidable_button.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class TrackingLocationPemohon extends StatefulWidget {
  final double lng;
  final double lat;
  final String namaPemilik;
  final String alamatTanah;
  final int id;

  const TrackingLocationPemohon(
      {Key key,
      this.lng,
      this.lat,
      this.id,
      this.namaPemilik,
      this.alamatTanah})
      : super(key: key);

  @override
  _TrackingLocationPemohonState createState() =>
      _TrackingLocationPemohonState();
}

class _TrackingLocationPemohonState extends State<TrackingLocationPemohon> {
  final Completer<GoogleMapController> _controller = Completer();
  final DefaultCacheManager _cacheManager = new DefaultCacheManager();
  BitmapDescriptor sourceIcon, destinationIcon;
  Set<Marker> _markers = Set();
  Set<Polyline> _polylines = Set();
  LatLng latLng = LatLng(-6.2685497, 107.0496954);
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  String jarak = "0 km", waktu = "0 detik";
  Timer timer;
  PanelController _pc = new PanelController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Tracking Lokasi"),
          centerTitle: true,
          backgroundColor: primaryColor,
        ),
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: [
                Column(
                  children: [
                    Expanded(flex: 5, child: _body()),
                    Expanded(
                      flex: 4,
                      child: Column(
                        children: [
                          Expanded(
                            flex: 2,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 10),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(15),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 1,
                                      blurRadius: 3,
                                      offset: Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ],
                                ),
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      left: 13, right: 10, top: 10, bottom: 10),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            height: 10,
                                            width: 10,
                                            decoration: BoxDecoration(
                                                color: Color(0xff11CC76),
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                          ),
                                          SizedBox(width: 5),
                                          Expanded(
                                              child: Text(
                                            "Surveyor lagi jalan ke tempat Anda",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold,
                                                color: Color(0xff717171)),
                                          )),
                                        ],
                                      ),
                                      SizedBox(height: 10),
                                      Text(
                                        "Harap menghadirkan tetangga dan saksi ukur (kades dan 2 perangkat Desa/Kel)",
                                        style: TextStyle(
                                            color: Color(0xffC3BFBF),
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      SizedBox(height: 10),
                                      Container(
                                        height: 1,
                                        color: Colors.black12,
                                      ),
                                      SizedBox(height: 10),
                                      Row(
                                        children: [
                                          Image.asset(
                                            "assets/logo.png",
                                            height: 40,
                                            width: 40,
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Zaenal Abidin",
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style:
                                                      TextStyle(fontSize: 15),
                                                ),
                                                Row(
                                                  children: [
                                                    Icon(Icons.star_outlined,
                                                        size: 18,
                                                        color: Colors.amber),
                                                    Text("4.8")
                                                  ],
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(
                                              child: SvgPicture.asset(
                                                  "assets/kjsb/phone.svg")),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Container(
                                            height: 20,
                                            width: 1,
                                            color: Colors.black12,
                                          ),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Container(
                                              child: SvgPicture.asset(
                                                  "assets/kjsb/chat.svg")),
                                          SizedBox(
                                            width: 15,
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 8,
                      child: ListView(
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 10),
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(
                                        left: 13, right: 10, top: 10, bottom: 10),
                                    decoration: BoxDecoration(
                                        color: Color(0xffF4F8FC),
                                        borderRadius: BorderRadius.circular(10)),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            Expanded(
                                                flex: 3,
                                                child: Row(
                                                  children: [
                                                    SvgPicture.asset(
                                                        "assets/kjsb/tanda_batas.svg"),
                                                    SizedBox(width: 20),
                                                    Container(
                                                      color: Colors.brown,
                                                      height: 49,
                                                      width: 1,
                                                    ),
                                                  ],
                                                )),
                                            Expanded(
                                              flex: 5,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Foto Tanda Batas",
                                                    style: TextStyle(
                                                        fontSize: 14,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                        "Jumlah",
                                                        style: TextStyle(
                                                            fontSize: 13),
                                                      ),
                                                      Text(
                                                        "4",
                                                        style: TextStyle(
                                                            fontSize: 13),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                        "30 sept 2021",
                                                        style: TextStyle(
                                                            fontSize: 9,
                                                            color:
                                                                Colors.grey[800]),
                                                      ),
                                                      Text(
                                                        "15:00",
                                                        style: TextStyle(
                                                            fontSize: 9,
                                                            color:
                                                                Colors.grey[800]),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                                flex: 2,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Text(
                                                      "Terkirim",
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: primaryColor,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    SizedBox(
                                                      height: 20,
                                                    ),
                                                    Text(
                                                      "Lihat",
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: primaryColor,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    )
                                                  ],
                                                )),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(
                                        left: 13, right: 10, top: 10, bottom: 10),
                                    decoration: BoxDecoration(
                                        color: Color(0xffF4F8FC),
                                        borderRadius: BorderRadius.circular(10)),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            Expanded(
                                                flex: 3,
                                                child: Row(
                                                  children: [
                                                    SvgPicture.asset(
                                                        "assets/kjsb/jadwal.svg"),
                                                    SizedBox(width: 20),
                                                    Container(
                                                      color: Colors.brown,
                                                      height: 49,
                                                      width: 1,
                                                    ),
                                                  ],
                                                )),
                                            Expanded(
                                              flex: 5,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Jadwal Pengukuran",
                                                    style: TextStyle(
                                                        fontSize: 14,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                        "Mulai",
                                                        style: TextStyle(
                                                            fontSize: 13),
                                                      ),
                                                      Text(
                                                        "Selesai",
                                                        style: TextStyle(
                                                            fontSize: 13),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                        "05 Okt 2021",
                                                        style: TextStyle(
                                                            fontSize: 9,
                                                            color:
                                                                Colors.grey[800]),
                                                      ),
                                                      Text(
                                                        "13 Okt 2021",
                                                        style: TextStyle(
                                                            fontSize: 9,
                                                            color:
                                                                Colors.grey[800]),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                                flex: 2,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Text(
                                                      "Disetujui",
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: primaryColor,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    SizedBox(
                                                      height: 20,
                                                    ),
                                                    Text(
                                                      "Lihat",
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: primaryColor,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    )
                                                  ],
                                                )),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(
                                        left: 13, right: 10, top: 10, bottom: 10),
                                    decoration: BoxDecoration(
                                        color: Color(0xffF4F8FC),
                                        borderRadius: BorderRadius.circular(10)),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            Expanded(
                                                flex: 3,
                                                child: Row(
                                                  children: [
                                                    SvgPicture.asset(
                                                        "assets/kjsb/map.svg"),
                                                    SizedBox(width: 20),
                                                    Container(
                                                      color: Colors.brown,
                                                      height: 49,
                                                      width: 1,
                                                    ),
                                                  ],
                                                )),
                                            Expanded(
                                              flex: 5,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Mengetahui Luas",
                                                    style: TextStyle(
                                                        fontSize: 14,
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Text(
                                                    "INVOICE #GS292301",
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        color: primaryColor),
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                        "30 Sept 2021",
                                                        style: TextStyle(
                                                            fontSize: 9,
                                                            color:
                                                                Colors.grey[800]),
                                                      ),
                                                      Text(
                                                        "15:00",
                                                        style: TextStyle(
                                                            fontSize: 9,
                                                            color:
                                                                Colors.grey[800]),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                                flex: 2,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Text(
                                                      "Terbayar",
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: primaryColor,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    SizedBox(
                                                      height: 20,
                                                    ),
                                                    Text(
                                                      "Lihat",
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: primaryColor,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    )
                                                  ],
                                                )),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(
                                        left: 13, right: 10, top: 10, bottom: 10),
                                    decoration: BoxDecoration(
                                        color: Color(0xffFCBEBE),
                                        borderRadius: BorderRadius.circular(10)),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                            flex: 1,
                                            child: SvgPicture.asset(
                                                "assets/kjsb/warning.svg")),
                                        Expanded(
                                          flex: 9,
                                          child: Text(
                                            "PERHATIAN! Waspada terhadap pihak-pihak yang mengatasnamakan Get-Survey untuk bertransaksi diluar aplikasi Get-Survey. Pastikan Anda mentransfer dana hanya ke rekening resmi milik PT. Get-Survey",
                                            style: TextStyle(
                                                color: Color(0xff002741)),
                                            textAlign: ui.TextAlign.justify,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 120,
                            )
                          ],
                        ),
                      ),
                  ],
                ),
                SlidingUpPanel(
                  controller: _pc,
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  panelBuilder: (sc) => _panel(sc),
                  maxHeight: 250,
                ),
              ],
            ),
          ),
        ));
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _doHere();
  }

  Widget _body() {
    return GoogleMap(
      mapType: MapType.normal,
      initialCameraPosition: CameraPosition(
        target: latLng,
        zoom: 14.4746,
      ),
      myLocationButtonEnabled: false,
      myLocationEnabled: false,
      markers: _markers,
      polylines: _polylines,
      onMapCreated: (GoogleMapController controller) {
        _controller.complete(controller);
      },
    );
  }

  _doHere() async {
    await _cacheManager.emptyCache();
    sourceIcon = await _bitmapDescriptorFromSvgAsset(
        context, 'assets/icons/ic_surveyor_map.svg');
    destinationIcon = await _bitmapDescriptorFromSvgAsset(
        context, 'assets/icons/ic_location_me.svg');
    GoogleMapController mapController = await _controller.future;
    var tracked = await RestAPI.trackGet(widget.id);

    if (tracked != null) {
      latLng = LatLng(tracked["aktualLat"], tracked["aktualLng"]);
      mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: latLng,
        zoom: 14.4746,
      )));
      _setMapPins();
      _setPolylines();
    }

    setState(() {});

    await Timer.periodic(Duration(seconds: 15), (_timer) async {
      var tracked = await RestAPI.trackGet(widget.id);
      timer = _timer;
      setState(() {});

      if (tracked != null) {
        latLng = LatLng(tracked["aktualLat"], tracked["aktualLng"]);
        mapController
            .animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: latLng,
          zoom: 14.4746,
        )));
        setState(() {});
        _setMapPins();
        _setPolylines();
      }
    });
  }

  void _setMapPins() {
    _markers.clear();
    // source pin
    _markers.add(Marker(
      markerId: MarkerId("sourcePin"),
      position: latLng,
      icon: sourceIcon,
    ));
    // destination pin
    _markers.add(Marker(
      markerId: MarkerId("destPin"),
      position: LatLng(widget.lat, widget.lng),
      icon: destinationIcon,
    ));
    setState(() {});
  }

  _setPolylines() async {
    PolylineResult geometry = await polylinePoints?.getRouteBetweenCoordinates(
      RestAPI.GOOGLE_API_KEY,
      PointLatLng(latLng.latitude, latLng.longitude),
      PointLatLng(widget.lat, widget.lng),
      travelMode: TravelMode.driving,
    );
    var disDur =
        await MapsAPI.distanceMatrix(latLng, LatLng(widget.lat, widget.lng));
    _polylines.clear();
    polylineCoordinates.clear();

    disDur.forEach((element) {
      jarak = element.distance.text;
      waktu = element.duration.text;
    });

    geometry.points.forEach((element) {
      polylineCoordinates.add(LatLng(element.latitude, element.longitude));
    });

    _polylines.add(Polyline(
        polylineId: PolylineId("${widget.id}"),
        width: 3,
        color: Color.fromARGB(255, 40, 122, 198),
        points: polylineCoordinates));
    setState(() {});
  }

  Future<BitmapDescriptor> _bitmapDescriptorFromSvgAsset(
      BuildContext context, String assetName) async {
    var svgString = await DefaultAssetBundle.of(context).loadString(assetName);

    var svgDrawableRoot = await svg.fromSvgString(svgString, null);

    var queryData = MediaQuery.of(context);
    var devicePixelRatio = queryData.devicePixelRatio;
    var width = 48 * devicePixelRatio; // SVG's original width
    var height = 48 * devicePixelRatio; // same thing

    var picture = svgDrawableRoot.toPicture(size: Size(width, height));

    var image = await picture.toImage(width.toInt(), height.toInt());
    var bytes = await image.toByteData(format: ui.ImageByteFormat.png);
    return BitmapDescriptor.fromBytes(bytes.buffer.asUint8List());
  }

  Widget _panel(ScrollController sc) {
    return MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView(controller: sc, children: <Widget>[
          SizedBox(
            height: 12.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 30,
                height: 5,
                decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.all(Radius.circular(12.0))),
              ),
            ],
          ),
          SizedBox(
            height: 18.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Apakah Surveyor sudah tiba?",
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 24.0,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          SlidableButton(
              color: Color(0xffFFA7B6),
              width: MediaQuery.of(context).size.width / 1,
              height: 49,
              // color: Theme.of(context).accentColor.withOpacity(0.5),
              buttonColor: Color(0xffF25F5F),
              dismissible: false,
              borderRadius: BorderRadius.circular(5),
              label: Center(
                  child: Icon(
                Icons.arrow_forward,
                color: Colors.white,
              )),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Belum Tiba",
                        style: TextStyle(
                            fontSize: 19,
                            fontWeight: FontWeight.bold,
                            color: Colors.white))
                  ],
                ),
              ),
              onChanged: (position) {
                print("A");
              }),
          SizedBox(height: 10),
          SlidableButton(
              color: Color(0xffAEE9EB),
              width: MediaQuery.of(context).size.width / 1,
              height: 49,
              // color: Theme.of(context).accentColor.withOpacity(0.5),
              buttonColor: Color(0xff00C045),
              dismissible: false,
              borderRadius: BorderRadius.circular(5),
              label: Center(
                  child: Icon(
                Icons.arrow_forward,
                color: Colors.white,
              )),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Sudah Tiba",
                        style: TextStyle(
                            fontSize: 19,
                            fontWeight: FontWeight.bold,
                            color: Colors.white))
                  ],
                ),
              ),
              onChanged: (position) {
                print("A");
              }),
        ]));
  }
}
