part of 'tracking_surveyor_cubit.dart';

abstract class TrackingSurveyorState extends Equatable {
  const TrackingSurveyorState();
}

class TrackingSurveyorInitial extends TrackingSurveyorState {
  @override
  List<Object> get props => [];
}
