import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'tracking_surveyor_state.dart';

class TrackingSurveyorCubit extends Cubit<TrackingSurveyorState> {
  TrackingSurveyorCubit() : super(TrackingSurveyorInitial());
}
