import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get_survey_app/account/UpgradeToSurveyors.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/presentation/provider/surveyor_tools_cubit.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class SurveyorToolScreen extends StatefulWidget {
  @override
  _SurveyorToolScreenState createState() => _SurveyorToolScreenState();
}

class _SurveyorToolScreenState extends State<SurveyorToolScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _merekCtrl = TextEditingController();
  TextEditingController _nomerSerictrl = TextEditingController();

  String _selectedProdYear;
  String _selectedTipe;
  String _selectedMerk;
  bool _isLoading = false;

  String _fileName = "Format (jpeg,jpg, png) tidak lebih dari 10MB";
  String _path;
  File _image;

  final List<String> _listTipe = [
    "GNSS Geodetic",
    "Total Station",
    "Distometer"
  ];

  final List<String> _listMerk = [
    "Topcon",
    "Nikon",
    "Leica",
    "Geomax",
    "Sokkia",
    "Chcnav",
    "South",
    "Hi-target",
    "Trimble",
    "Produk Lain(isi)"
  ];

  final List<String> _listProdYear = [
    '1990',
    '1991',
    '1992',
    '1993',
    '1994',
    '1995',
    '1996',
    '1997',
    '1998',
    '1999',
    '2000',
    '2001',
    '2002',
    '2003',
    '2004',
    '2005',
    '2006',
    '2007',
    '2008',
    '2009',
    '2010',
    '2011',
    '2012',
    '2013',
    '2014',
    '2015',
    '2016',
    '2017',
    '2018',
    '2019',
    '2020',
    '2021'
  ];

  SurveyorToolsCubit surveyorToolsCubit = di<SurveyorToolsCubit>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
            color: Colors.white,
          ),
          backgroundColor: primaryColor,
          title: Text(
            "Tambah Alat",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ),
        body: LoaderOverlay(
          useDefaultLoading: false,
          overlayWidget: Center(
            child: SpinKitCubeGrid(
              color: primaryColor,
              size: 50.0,
            ),
          ),
          child: BlocProvider(
            create: (context) => surveyorToolsCubit,
            child: BlocConsumer<SurveyorToolsCubit, SurveyorToolsState>(
              listener: (context, state) {
                if (state is LoadingState) {
                  context.loaderOverlay.show();
                } else if (state is FailureState) {
                  context.loaderOverlay.hide();
                  Component.showAppNotification(
                      context, "Gagal!!", "Cobalah beberapa saat lagi.");
                } else if (state is SuccessState) {
                  context.loaderOverlay.hide();
                  Navigator.of(context).pop(state.surveyorToolParams);
                  Component.showAppNotification(
                      context, "Berhasil!!", "yeessssss.....");
                }
              },
              builder: (context, state) {
                return ModalProgressHUD(
                  child: _body(surveyorToolsCubit),
                  inAsyncCall: _isLoading,
                );
              },
            ),
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          child: StreamBuilder(
              stream: surveyorToolsCubit.cekForm,
              builder: (context, snapshot) {
                return InkWell(
                  onTap: () {
                    if (snapshot.hasData == true) {
                      surveyorToolsCubit.createSurveyorTools();
                    }
                  },
                  child: Container(
                    height: 50.0,
                    decoration: BoxDecoration(
                      color: snapshot.hasData == true
                          ? Color(0xff0c5d65)
                          : Color(0xffA8E4EA),
                    ),
                    child: Center(
                      child: Text(
                        "Tambah",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: "Sans",
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                );
              }),
        ),
      ),
    );
  }

  Widget _body(SurveyorToolsCubit surveyorToolsCubit) {
    return BlocProvider(
      create: (context) => surveyorToolsCubit,
      child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: 'Jenis Produk',
                            style: TextStyle(
                                color: Color((0xff667589)),
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                            children: const <TextSpan>[
                              TextSpan(
                                  text: '*',
                                  style: TextStyle(color: Colors.red)),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        StreamBuilder(
                            stream: surveyorToolsCubit.jenisProdukStream,
                            builder: (context, snapshot) {
                              return Container(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                    // underline: Text(snapshot.hasError
                                    //     ? snapshot.error.toString()
                                    //     : null),
                                    isExpanded: true,
                                    hint: Text('Pilih jenis produk'),
                                    value: _selectedTipe,
                                    onChanged: (newValue) {
                                      _selectedTipe = newValue;
                                      surveyorToolsCubit
                                          .validateJenisProduk(newValue);
                                      return newValue;
                                    },
                                    items: _listTipe.map((category) {
                                      return DropdownMenuItem(
                                        child: new Text(category),
                                        value: category,
                                      );
                                    }).toList(),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 1, color: primaryColor),
                                    borderRadius: BorderRadius.circular(14)),
                              );
                            })
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: 'Merk/Brand Produk',
                            style: TextStyle(
                                color: Color((0xff667589)),
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                            children: const <TextSpan>[
                              TextSpan(
                                  text: '*',
                                  style: TextStyle(color: Colors.red)),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        StreamBuilder(
                            stream: surveyorToolsCubit.merkProdukStream,
                            builder: (context, snapshot) {
                              return Container(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                    isExpanded: true,
                                    hint: Text('Pilih'),
                                    // Not necessary for Option 1
                                    value: _selectedMerk,
                                    onChanged: (newValue) {
                                      _selectedMerk = newValue;
                                      surveyorToolsCubit
                                          .validateMerkProduk(newValue);
                                      return newValue;
                                    },
                                    items: _listMerk.map((category) {
                                      return DropdownMenuItem(
                                        child: new Text(category),
                                        value: category,
                                      );
                                    }).toList(),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 1, color: primaryColor),
                                    borderRadius: BorderRadius.circular(14)),
                              );
                            }),
                      ],
                    ),
                  ),
                  (_selectedMerk == "Produk Lain(isi)")
                      ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            autofocus: true,
                            controller: _merekCtrl,
                            textCapitalization: TextCapitalization.sentences,
                            decoration: const InputDecoration(
                              labelText: 'Merk/Brand',
                            ),
                          ),
                        )
                      : Container(),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: 'Nomor Seri',
                            style: TextStyle(
                                color: Color((0xff667589)),
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                            children: const <TextSpan>[
                              TextSpan(
                                  text: '*',
                                  style: TextStyle(color: Colors.red)),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        StreamBuilder(
                            stream: surveyorToolsCubit.nomorSeriStream,
                            builder: (context, snapshot) {
                              return Container(
                                height: 60.0,
                                alignment: AlignmentDirectional.center,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(14.0),
                                    color: Colors.white,
                                    border: Border.all(
                                        color: primaryColor, width: 1)),
                                padding: EdgeInsets.only(
                                    left: 20.0,
                                    right: 30.0,
                                    top: 0.0,
                                    bottom: 0.0),
                                child: Theme(
                                    data: ThemeData(
                                      hintColor: Colors.transparent,
                                    ),
                                    child: TextFormField(
                                        decoration: InputDecoration(
                                            errorText: snapshot.hasError
                                                ? snapshot.error.toString()
                                                : null,
                                            border: InputBorder.none,
                                            hintText:
                                                "Masukkan Nomor seri produk",
                                            hintStyle: TextStyle(
                                              fontSize: 12.0,
                                              fontFamily: 'Sans',
                                              letterSpacing: 0.3,
                                              color: Colors.grey,
                                            )),
                                        onChanged: (text) {
                                          surveyorToolsCubit
                                              .validateNmorSeri(text);
                                          return text;
                                        })),
                              );
                            }),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                          text: TextSpan(
                            text: 'Tahun Produksi',
                            style: TextStyle(
                                color: Color((0xff667589)),
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                            children: const <TextSpan>[
                              TextSpan(
                                  text: '*',
                                  style: TextStyle(color: Colors.red)),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        StreamBuilder(
                            stream: surveyorToolsCubit.tahunProduksiStream,
                            builder: (context, snapshot) {
                              return Container(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                    isExpanded: true,
                                    hint: Text('Pilih'),
                                    // Not necessary for Option 1
                                    value: _selectedProdYear,
                                    onChanged: (newValue) {
                                      _selectedProdYear = newValue;
                                      surveyorToolsCubit
                                          .validateTahunProduk(newValue);
                                      return newValue;
                                    },
                                    items: _listProdYear.map((category) {
                                      return DropdownMenuItem(
                                        child: new Text(category),
                                        value: category,
                                      );
                                    }).toList(),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 1, color: primaryColor),
                                    borderRadius: BorderRadius.circular(14)),
                              );
                            }),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: StreamBuilder(
                        stream: surveyorToolsCubit.unggahFileStream,
                        builder: (context, snapshot) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'Unggah File Sertifikat',
                                  style: TextStyle(
                                      color: Color((0xff667589)),
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                InkWell(
                                  onTap: () {
                                    _showPicker(context);
                                  },
                                  child: StreamBuilder(
                                    stream: surveyorToolsCubit.unggahFileStream,
                                    builder: (context, snapshot) {
                                      return Container(
                                        padding: const EdgeInsets.all(5.0),
                                        child: snapshot.hasData
                                            ? Image.file(
                                                File(snapshot.data),
                                                width: 70,
                                                height: 70,
                                                fit: BoxFit.cover,
                                              )
                                            : Container(
                                                width: 70,
                                                height: 70,
                                                child: Icon(
                                                  Icons.add,
                                                  color: Colors.grey[800],
                                                ),
                                              ),
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.grey, width: 1)),
                                      );
                                    }
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  _fileName,
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w700),
                                ),
                              ],
                            ),
                          );
                        }),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          )),
    );
  }

  Future<String> getImageGaleri() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 60,
    );
    return image.path;
  }

  Future<String> _openCamera() async {
    try {
      var image = await ImagePicker.pickImage(
        source: ImageSource.camera,
        imageQuality: 60,
      );
      return image.path;
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  Future<File> _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () async {
                        final imagePath = await getImageGaleri();
                        surveyorToolsCubit.selectPhoto(imagePath);
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () async {
                      final filePath = await _openCamera();
                      surveyorToolsCubit.selectPhoto(filePath);
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }
}
