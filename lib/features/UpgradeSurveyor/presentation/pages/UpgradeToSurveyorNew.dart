import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get_survey_app/account/AddItem.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/entities/upgrade_surveyor_data_diri_entity.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_surveyor_tools.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/presentation/pages/SurveyorTool.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/presentation/provider/upgrade_surveyor_cubit.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/location_entity.dart';
import 'package:get_survey_app/features/kjsb/presentation/pages/user/select_location_screen.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/model/Alat.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:intl/intl.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:loader_overlay/loader_overlay.dart';

class UpgradeToSurveyorNew extends StatefulWidget {
  @override
  _UpgradeToSurveyorNewState createState() => _UpgradeToSurveyorNewState();
}

class _UpgradeToSurveyorNewState extends State<UpgradeToSurveyorNew> {
  final form1 = GlobalKey<FormState>();
  final form2 = GlobalKey<FormState>();

  int _currentStep = 0;
  StepperType stepperType = StepperType.horizontal;
  bool _isLoading = false;
  String tanggalLahir = DateFormat('yyyy-MM-dd').format(DateTime.now());
  String _selectedSurveyor;
  String _fileName = 'Pilih Berkas Lisensi PDF, JPG/JPEG, PNG';
  String _path;
  String failedFile;
  bool aktifValue = false;
  bool cekAtmUser = false;
  List<SurveyorToolParams> listAlat = [];
  String _selectedJenjang;
  String _selectedBank;
  int tipe = 0;

  double long;
  double lat;
  String alamat;

  String nama;
  String email;
  String noTelp;
  int jenisKelamin;

  List<String> _kategoriSurveyor = [
    'Surveyor IRK (Informasi Rencana Kota)',
    'Surveyor SKB (Layanan Pertanahan)',
  ];

  List<String> _listBank = [
    'BNI',
    'BCA',
    'MANDIRI',
    'BRI',
    'DANAMON',
    'PERMATA',
    'CIMB NIAGA',
  ];

  List<String> _jenjangPendidikan = [
    'SMP',
    'SMA',
    'D3',
    'S1',
    'S2',
    'S3',
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getUser();
  }

  UpgradeSurveyorCubit upgradeSurveyorCubit = di<UpgradeSurveyorCubit>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (context) => BtnNavigation(
                            atIndex: 3,
                          )),
                  (Route<dynamic> route) => false);
            },
            color: Colors.white,
          ),
          backgroundColor: primaryColor,
          title: Text(
            "Form Upgrade Surveyor",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ),
        body: LoaderOverlay(
          useDefaultLoading: false,
          overlayWidget: Center(
            child: SpinKitCubeGrid(
              color: primaryColor,
              size: 50.0,
            ),
          ),
          child: BlocProvider(
            create: (context) => upgradeSurveyorCubit,
            child: BlocConsumer<UpgradeSurveyorCubit, UpgradeSurveyorState>(
              listener: (context, state) {
                if (state is LoadingState) {
                  context.loaderOverlay.show();
                } else if (state is SuccessState) {
                  context.loaderOverlay.hide();
                  continued();
                } else if (state is FailureState) {
                  context.loaderOverlay.hide();
                  Component.showAppNotification(
                      context, "Gagal!!", "Cobalah beberapa saat lagi.");
                } else if (state is SuccessDocumentState) {
                  context.loaderOverlay.hide();
                  Component.showAppNotification(
                      context, "Berhasil!!", "Upload Document Surveyor");
                }
              },
              builder: (context, state) {
                if(state is ListTooltState){
                  listAlat = state.params;
                }
                return Container(
                  child: Column(
                    children: [
                      Expanded(
                        child: Stepper(
                          type: stepperType,
                          physics: ScrollPhysics(),
                          currentStep: _currentStep,
                          onStepTapped: (step) => tapped(step),
                          controlsBuilder: (context,
                              {onStepContinue, onStepCancel}) {
                            return SizedBox();
                          },
                          // onStepContinue: continued,
                          // onStepCancel: cancel,
                          steps: <Step>[
                            Step(
                              title: new Text('Data Diri'),
                              content: Form(
                                key: form1,
                                child: Column(
                                  children: <Widget>[
                                    textFieldHaveValue(
                                      context,
                                      "Nama",
                                      nama.toString().toUpperCase(),
                                    ),
                                    textFieldHaveValue(
                                      context,
                                      "Email",
                                      email.toString(),
                                    ),
                                    textFieldHaveValue(
                                      context,
                                      "Jenis Kelamin",
                                      jenisKelamin ==
                                              UpgradeSurveyorEntity
                                                  .JENIS_KELAMIN_WANITA
                                          ? "Wanita"
                                          : "Pria",
                                    ),
                                    textFieldHaveValue(
                                      context,
                                      "No Telephone",
                                      noTelp.toString(),
                                    ),
                                    StreamBuilder(
                                        stream: upgradeSurveyorCubit.nikStream,
                                        builder: (context, snapshot) {
                                          return textField(
                                              context,
                                              "No Kartu Identitas(KTP)*",
                                              "Masukkan no identitas Anda",
                                              // snapshot.hasData
                                              //     ? TextEditingController(
                                              //         text: snapshot.data)
                                              //     : null,
                                              false,
                                              upgradeSurveyorCubit.validateNIK,
                                              errorText: snapshot.hasError
                                                  ? snapshot.error.toString()
                                                  : null);
                                        }),
                                    Container(
                                      child: Row(
                                        children: [
                                          Expanded(
                                            flex: 3,
                                            child: StreamBuilder(
                                                stream: upgradeSurveyorCubit
                                                    .tempatLahirStream,
                                                builder: (context, snapshot) {
                                                  return textField(
                                                      context,
                                                      "Tempat Lahir*",
                                                      "Tempat Kelahiran",
                                                      false,
                                                      upgradeSurveyorCubit
                                                          .validateTempatLahir,
                                                      errorText:
                                                          snapshot.hasError
                                                              ? snapshot.error
                                                                  .toString()
                                                              : null);
                                                }),
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 20.0, left: 10),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Tanggal Lahir",
                                                    style: TextStyle(
                                                        color: primaryColor,
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  StreamBuilder(
                                                      stream: upgradeSurveyorCubit
                                                          .tanggalLahirStream,
                                                      builder:
                                                          (context, snapshot) {
                                                        return InkWell(
                                                          onTap: () {
                                                            DatePicker.showDatePicker(
                                                                context,
                                                                theme:
                                                                    DatePickerTheme(
                                                                  containerHeight:
                                                                      210.0,
                                                                ),
                                                                showTitleActions:
                                                                    true,
                                                                minTime: DateTime(
                                                                    1950,
                                                                    01,
                                                                    01),
                                                                maxTime:
                                                                    DateTime
                                                                        .now(),
                                                                onConfirm:
                                                                    (date) {
                                                              String
                                                                  formattedDate =
                                                                  DateFormat(
                                                                          'yyyy-MM-dd')
                                                                      .format(
                                                                          date);
                                                              tanggalLahir =
                                                                  formattedDate;
                                                              upgradeSurveyorCubit
                                                                  .validateTanggalLahir(
                                                                      tanggalLahir);
                                                              setState(() {});
                                                            },
                                                                currentTime:
                                                                    DateTime(
                                                                        1988,
                                                                        05,
                                                                        21),
                                                                locale:
                                                                    LocaleType
                                                                        .id);
                                                          },
                                                          child: Container(
                                                            height: 60.0,
                                                            child: Center(
                                                              child: Text(
                                                                tanggalLahir,
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .black,
                                                                    fontSize:
                                                                        14.0),
                                                              ),
                                                            ),
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            14),
                                                                border: Border.all(
                                                                    width: 1,
                                                                    color: Colors
                                                                        .grey)),
                                                          ),
                                                        );
                                                      }),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    StreamBuilder(
                                        stream: upgradeSurveyorCubit
                                            .defaultAlamatStream,
                                        builder: (context, snapshot) {
                                          return textField(
                                              context,
                                              "Alamat Lengkap*",
                                              "Masukkan alamat lengkap ",
                                              true,
                                              upgradeSurveyorCubit
                                                  .validateAlamat,
                                              textEditingController:
                                                  snapshot.hasData
                                                      ? TextEditingController(
                                                          text: snapshot.data)
                                                      : null,
                                              errorText: snapshot.hasError
                                                  ? snapshot.error.toString()
                                                  : null);
                                        }),
                                    StreamBuilder(
                                        stream: upgradeSurveyorCubit
                                            .jurusanPendidikanStream,
                                        builder: (context, snapshot) {
                                          return textField(
                                              context,
                                              "Jurusan Pendidikan*",
                                              "Masukkan jurusan pendidikan ",
                                              false,
                                              upgradeSurveyorCubit
                                                  .validateJurusanPendidikan,
                                              errorText: snapshot.hasError
                                                  ? snapshot.error.toString()
                                                  : null);
                                        }),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        "Jenjang Pendidikan Terakhir",
                                        style: TextStyle(
                                            color: primaryColor,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    StreamBuilder(
                                        stream: upgradeSurveyorCubit
                                            .selectedJenjangStream,
                                        builder: (context, snapshot) {
                                          return Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 10),
                                            child: DropdownButtonHideUnderline(
                                              child: DropdownButton(
                                                isExpanded: true,
                                                hint: Text('Pilih'),
                                                // Not necessary for Option 1
                                                value: _selectedJenjang,
                                                onChanged: (newValue) {
                                                  _selectedJenjang = newValue;
                                                  upgradeSurveyorCubit
                                                      .validateJenjang(
                                                          newValue);
                                                  return newValue;
                                                },
                                                items: _jenjangPendidikan
                                                    .map((category) {
                                                  return DropdownMenuItem(
                                                    child: new Text(category),
                                                    value: category,
                                                  );
                                                }).toList(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 1,
                                                    color: primaryColor),
                                                borderRadius:
                                                    BorderRadius.circular(14)),
                                          );
                                        }),
                                    StreamBuilder(
                                        stream: upgradeSurveyorCubit
                                            .perguruanTinggiStream,
                                        builder: (context, snapshot) {
                                          return textField(
                                              context,
                                              "Perguruan Tinggi",
                                              "Masukkan nama perguruan tinggi ",
                                              false,
                                              upgradeSurveyorCubit
                                                  .validatePerguruanTinggi,
                                              errorText: snapshot.hasError
                                                  ? snapshot.error.toString()
                                                  : null);
                                        }),
                                  ],
                                ),
                              ),
                              isActive: _currentStep >= 0,
                              state: _currentStep >= 0
                                  ? StepState.complete
                                  : StepState.disabled,
                            ),
                            Step(
                              title: new Text('Keanggotaan'),
                              content: Form(
                                key: form2,
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        "Mengajukan Sebagai*",
                                        style: TextStyle(
                                            color: primaryColor,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    StreamBuilder(
                                        stream: upgradeSurveyorCubit
                                            .kategoriSurveyorStream,
                                        builder: (context, snapshot) {
                                          return Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 10),
                                            child: DropdownButtonHideUnderline(
                                              child: DropdownButton(
                                                isExpanded: true,
                                                hint: Text(
                                                    'Pilih Jenis Surveyor'),
                                                // Not necessary for Option 1
                                                value: _selectedSurveyor,
                                                onChanged: (newValue) {
                                                  _selectedSurveyor = newValue;
                                                  if (_selectedSurveyor ==
                                                      "Surveyor IRK (Informasi Rencana Kota)") {
                                                    tipe = UpgradeSurveyorEntity
                                                        .SURVEYOR_IRK;
                                                  } else {
                                                    tipe = UpgradeSurveyorEntity
                                                        .SURVEYOR_SKB;
                                                  }
                                                  upgradeSurveyorCubit
                                                      .setkategori(tipe);
                                                  setState(() {});
                                                },
                                                items: _kategoriSurveyor
                                                    .map((category) {
                                                  return DropdownMenuItem(
                                                    child: new Text(category),
                                                    value: category,
                                                  );
                                                }).toList(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 1,
                                                    color: primaryColor),
                                                borderRadius:
                                                    BorderRadius.circular(14)),
                                          );
                                        }),
                                    tipeSurveyor(
                                        context, tipe, upgradeSurveyorCubit)
                                  ],
                                ),
                              ),
                              isActive: _currentStep >= 0,
                              state: _currentStep >= 1
                                  ? StepState.complete
                                  : StepState.disabled,
                            ),
                            Step(
                              title: new Text('Berkas'),
                              content: Container(
                                height: MediaQuery.of(context).size.height,
                                width: MediaQuery.of(context).size.width,
                                child: Column(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 3,
                                      child: StreamBuilder(
                                          stream: upgradeSurveyorCubit
                                              .unggahDocumentStream,
                                          builder: (context, snapshot) {
                                            return Container(
                                              child: Column(
                                                children: [
                                                  Text(
                                                    "Unggah File Sertifikat*",
                                                    style: TextStyle(
                                                        color: primaryColor4,
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  SizedBox(
                                                      child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      InkWell(
                                                        onTap:
                                                            _openFileExplorer,
                                                        child: Container(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(10.0),
                                                          child: _fileName ==
                                                                  '*Pilih Berkas Lisensi PDF, JPG/JPEG, PNG'
                                                              ? Icon(
                                                                  Icons.add,
                                                                  color: Colors
                                                                      .grey,
                                                                  size: 70,
                                                                )
                                                              : Icon(
                                                                  Icons
                                                                      .file_present,
                                                                  color: Colors
                                                                      .red,
                                                                  size: 70,
                                                                ),
                                                          decoration: BoxDecoration(
                                                              border: Border.all(
                                                                  color: Colors
                                                                      .grey,
                                                                  width: 1)),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 10,
                                                      ),
                                                      Text(
                                                        _fileName,
                                                        style: TextStyle(
                                                            color: Colors.grey,
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w700),
                                                      ),
                                                      SizedBox(
                                                        height: 20,
                                                      ),
                                                      Padding(
                                                        padding: (aktifValue ==
                                                                false)
                                                            ? EdgeInsets
                                                                .fromLTRB(
                                                                    8, 0, 8, 15)
                                                            : EdgeInsets
                                                                .fromLTRB(
                                                                    8, 0, 8, 4),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: <Widget>[
                                                            Expanded(
                                                              flex: 2,
                                                              child: Checkbox(
                                                                value:
                                                                    aktifValue,
                                                                activeColor:
                                                                    primaryColor,
                                                                onChanged: (bool
                                                                    value) {
                                                                  setState(() {
                                                                    aktifValue =
                                                                        value;
                                                                  });
                                                                },
                                                              ),
                                                            ),
                                                            Expanded(
                                                                flex: 16,
                                                                child: Text(
                                                                    "Memiliki Peralatan Survey GPS/ Total Station")),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  )),
                                                ],
                                              ),
                                            );
                                          }),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            200, 20, 0, 30),
                                        child: InkWell(
                                          child: Container(
                                            color: Colors.red,
                                            child: Center(
                                                child: Text(
                                              "Tambah Alat",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            )),
                                          ),
                                          onTap: () {
                                            Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            SurveyorToolScreen()))
                                                .then((value) {
                                              if (value != null) {
                                                upgradeSurveyorCubit.addTools(
                                                    value
                                                        as SurveyorToolParams);
                                              }
                                            });
                                          },
                                        ),
                                      ),
                                      flex: 1,
                                    ),
                                    Expanded(
                                        flex: 4,
                                        child: ListView.builder(
                                            itemCount: listAlat.length,
                                            itemBuilder: (context, index) {
                                              return Padding(
                                                padding:
                                                    const EdgeInsets.all(2.0),
                                                child: Container(
                                                  height: 120,
                                                  child: Row(
                                                    children: [
                                                      Expanded(
                                                        flex: 3,
                                                        child: Container(
                                                          decoration:
                                                              BoxDecoration(
                                                            image: DecorationImage(
                                                                image: FileImage(
                                                                    File(listAlat[
                                                                    index]
                                                                        .uploadFile)),
                                                                fit: BoxFit
                                                                    .cover),
                                                            color: Colors.red,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10),
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        flex: 3,
                                                        child: Container(
                                                          padding: EdgeInsets
                                                              .fromLTRB(
                                                                  3, 3, 0, 3),
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceAround,
                                                            children: [
                                                              Text(
                                                                listAlat[index].tipe,
                                                                style: TextStyle(
                                                                    color:
                                                                        primaryGreyText),
                                                              ),
                                                              Text(
                                                                listAlat[index].merek,
                                                                style: TextStyle(
                                                                    color:
                                                                        primaryGreyText),
                                                              ),
                                                              Text(
                                                                listAlat[index].nomerSeri,
                                                                style: TextStyle(
                                                                    color:
                                                                        primaryGreyText),
                                                              ),
                                                              Text(
                                                                listAlat[index].tahunProduksi,
                                                                style: TextStyle(
                                                                    color:
                                                                        primaryGreyText),
                                                              ),
                                                            ],
                                                          ),
                                                          decoration:
                                                              BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10),
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        flex: 4,
                                                        child: Container(
                                                          padding: EdgeInsets
                                                              .fromLTRB(
                                                                  0, 3, 3, 3),
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceAround,
                                                            children: [
                                                              Text(
                                                                ": ${listAlat[index].tipe}",
                                                                maxLines: 1,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                style: TextStyle(
                                                                    color:
                                                                        primaryGreyText),
                                                              ),
                                                              Text(
                                                                ": ${listAlat[index].merek}",
                                                                maxLines: 1,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                style: TextStyle(
                                                                    color:
                                                                        primaryGreyText),
                                                              ),
                                                              Text(
                                                                ": ${listAlat[index].nomerSeri}",
                                                                maxLines: 1,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                style: TextStyle(
                                                                    color:
                                                                        primaryGreyText),
                                                              ),
                                                              Text(
                                                                ": ${listAlat[index].tahunProduksi}",
                                                                maxLines: 1,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                style: TextStyle(
                                                                    color:
                                                                        primaryGreyText),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10)),
                                                ),
                                              );
                                            }))
                                  ],
                                ),
                              ),
                              isActive: _currentStep >= 0,
                              state: _currentStep >= 2
                                  ? StepState.complete
                                  : StepState.disabled,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
        bottomNavigationBar: containerStepper(context, _currentStep));
  }

  Widget containerStepper(BuildContext context, int currentStepper) {
    Widget container = Container();
    if (currentStepper == 0) {
      container = StreamBuilder(
          stream: upgradeSurveyorCubit.nextFormSubmit,
          builder: (context, snapshot) {
            return InkWell(
              onTap: () {
                if (snapshot.hasData == true) {
                  upgradeSurveyorCubit.upgradeSurveyor();
                }
                // continued();
              },
              child: Container(
                height: 50,
                decoration: BoxDecoration(
                  color: snapshot.hasData == true
                      ? Color(0xff0c5d65)
                      : Color(0xffA8E4EA),
                ),
                child: Center(
                  child: Text(
                    "Selanjutnya",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
              ),
            );
          });
    } else if (currentStepper == 1) {
      container = StreamBuilder(
          stream: upgradeSurveyorCubit.readyToSubmit,
          builder: (context, snapshotKeanggotaan) {
            return InkWell(
              onTap: () {
                if (snapshotKeanggotaan.hasData == true) {
                  upgradeSurveyorCubit.updateSurveyor();
                }
                // continued();
              },
              child: Container(
                height: 50,
                decoration: BoxDecoration(
                    color: snapshotKeanggotaan.hasData == true
                        ? Color(0xff0c5d65)
                        : Color(0xffA8E4EA)),
                child: Center(
                  child: Text(
                    "Selanjutnya",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
              ),
            );
          });
    } else {
      container = InkWell(
          onTap: () async {
            upgradeSurveyorCubit.finishCreateSurveyor();
          },
          child: Container(
            height: 50,
            decoration: BoxDecoration(color: primaryColor),
            child: Center(
              child: Text(
                "Kirirm data",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
            ),
          ));
    }
    return container;
  }

  tapped(int step) {
    setState(() => _currentStep = step);
  }

  continued() {
    _currentStep < 2 ? setState(() => _currentStep += 1) : null;
  }

  cancel() {
    _currentStep > 0 ? setState(() => _currentStep -= 1) : null;
  }

  Widget tipeSurveyor(BuildContext context, int tipe,
      UpgradeSurveyorCubit upgradeSurveyorCubit) {
    Widget container = Container();
    if (tipe == 1) {
      return Column(
        children: [
          StreamBuilder(
              stream: upgradeSurveyorCubit.noLisensiISIStream,
              builder: (context, snapshot) {
                return textField(
                    context,
                    "No. Anggota ISI* ",
                    "Masukkan nomor anggota isi",
                    false,
                    upgradeSurveyorCubit.validateNoLisesnsiISI,
                    errorText:
                        snapshot.hasError ? snapshot.error.toString() : null);
              }),
          Padding(
              padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
              child: RichText(
                text: TextSpan(
                  text: 'Daftarkan diri Anda ',
                  style: TextStyle(color: Colors.grey),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'disini',
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        color: Colors.green,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () =>
                            launch('http://www.isi.or.id/keanggotaan/daftar/'),
                    ),
                    TextSpan(
                        text:
                            ' jika Anda belum terdaftar sebagai anggota ikatan surveyor indonesia (isi)'),
                  ],
                ),
              )),
          StreamBuilder(
              stream: upgradeSurveyorCubit.noSertipikatKompetensiISIStream,
              builder: (context, snapshot) {
                return textField(
                    context,
                    "No. Sertipikat Kompetensi ISI*",
                    "Masukkan nomor sertipikat kompetensi",
                    false,
                    upgradeSurveyorCubit.validateNoSertipikatKompetensiISI,
                    icon: true,
                    img: "assets/medali/kompetensi.png",
                    errorText:
                        snapshot.hasError ? snapshot.error.toString() : null);
              }),
          StreamBuilder(
              stream: upgradeSurveyorCubit.noSkaSktStream,
              builder: (context, snapshot) {
                return textField(
                    context,
                    "No. SKA/SKT*",
                    "Masukkan nomor ska/skt",
                    false,
                    upgradeSurveyorCubit.validateNoSkaSkt,
                    icon: true,
                    img: "assets/medali/ska.png",
                    errorText:
                        snapshot.hasError ? snapshot.error.toString() : null);
              }),
          StreamBuilder(
              stream: upgradeSurveyorCubit.noLisensiKadastralStream,
              builder: (context, snapshot) {
                return textField(
                    context,
                    "No. Lisensi Kadastral*",
                    "Masukkan nomor Lisensi Kadastral",
                    false,
                    upgradeSurveyorCubit.validateNoLisensiKadastral,
                    icon: true,
                    errorText:
                        snapshot.hasError ? snapshot.error.toString() : null,
                    img: "assets/medali/kadastral.png");
              }),
          StreamBuilder(
              stream: upgradeSurveyorCubit.noLisensiIrkStream,
              builder: (context, snapshot) {
                return textField(
                    context,
                    "No. Lisensi IRK*",
                    "Masukkan nomor Lisensi IRK",
                    false,
                    upgradeSurveyorCubit.validateNoLisensiIRK,
                    icon: true,
                    errorText:
                        snapshot.hasError ? snapshot.error.toString() : null,
                    img: "assets/medali/irk.png");
              }),
          SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Akun Bank*",
              style: TextStyle(
                  color: primaryColor,
                  fontSize: 14,
                  fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          StreamBuilder(
              stream: upgradeSurveyorCubit.namabankStream,
              builder: (context, snapshot) {
                return Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(
                      isExpanded: true,
                      hint: Text('Pilih Bank'),
                      // Not necessary for Option 1
                      value: _selectedBank,
                      onChanged: (newValue) {
                        setState(() {
                          _selectedBank = newValue;
                          print(newValue);
                          upgradeSurveyorCubit.setAkunBank(newValue);
                        });
                      },
                      items: _listBank.map((category) {
                        return DropdownMenuItem(
                          child: new Text(category),
                          value: category,
                        );
                      }).toList(),
                    ),
                  ),
                  decoration: BoxDecoration(
                      border: Border.all(width: 1, color: primaryColor),
                      borderRadius: BorderRadius.circular(14)),
                );
              }),
          SizedBox(
            height: 20,
          ),
          Text(
            "Nomor Rekening*",
            style: TextStyle(
                color: primaryColor, fontSize: 14, fontWeight: FontWeight.w500),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 60.0,
            alignment: AlignmentDirectional.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14.0),
                color: Colors.white,
                border: Border.all(color: primaryColor, width: 1)),
            padding:
                EdgeInsets.only(left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
            child: Theme(
              data: ThemeData(
                hintColor: Colors.transparent,
              ),
              child: Row(
                children: [
                  Expanded(
                    flex: 4,
                    child: StreamBuilder(
                        stream: upgradeSurveyorCubit.noRekeningStream,
                        builder: (context, snapshot) {
                          return TextFormField(
                            decoration: InputDecoration(
                                errorText: snapshot.hasError
                                    ? snapshot.error.toString()
                                    : null,
                                border: InputBorder.none,
                                hintText: "Masukkan nomor rekening",
                                hintStyle: TextStyle(
                                  fontSize: 12.0,
                                  fontFamily: 'Sans',
                                  letterSpacing: 0.3,
                                  color: Colors.grey,
                                )),
                            onChanged: (text) {
                              upgradeSurveyorCubit.validateNoRekening(text);
                              return text;
                            },
                          );
                        }),
                  ),
                  Expanded(
                      flex: 1,
                      child: InkWell(
                          onTap: () {
                            cekAtmUser = true;
                            setState(() {});
                          },
                          child: Text(
                            "Periksa",
                            style: TextStyle(color: primaryColor),
                          )))
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          cekAtmUser == false
              ? Container()
              : Container(
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xffB2E8ED),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Aufa Ahdi",
                        style: TextStyle(fontWeight: FontWeight.w500),
                      ),
                      CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Center(
                              child: IconButton(
                                  icon: Icon(
                                    Icons.check,
                                    color: Color(0xffB2E8ED),
                                  ),
                                  onPressed: () {})))
                    ],
                  ))
        ],
      );
    } else if (tipe == 2) {
      container = Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          StreamBuilder(
              stream: upgradeSurveyorCubit.noLisensiSKBStream,
              builder: (context, snapshot) {
                return textField(
                  context,
                  "No. Lisensi SKB* ",
                  "Masukkan nomor lisensi skb",
                  false,
                  upgradeSurveyorCubit.validateNoLisesnsiSKB,
                  errorText:
                      snapshot.hasError ? snapshot.error.toString() : null,
                );
              }),
          Padding(
              padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
              child: RichText(
                text: TextSpan(
                  text: 'Daftarkan diri Anda ',
                  style: TextStyle(color: Colors.grey),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'disini',
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        color: Colors.green,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () =>
                            launch('http://www.isi.or.id/keanggotaan/daftar/'),
                    ),
                    TextSpan(
                        text:
                            ' jika Anda belum terdaftar sebagai surveyor kadaster berlisensi(SKB)'),
                  ],
                ),
              )),
          StreamBuilder(
              stream: upgradeSurveyorCubit.noSertipikatKompetensiISIStream,
              builder: (context, snapshot) {
                return textField(
                    context,
                    "No. Sertipikat Kompetensi ISI*",
                    "Masukkan nomor sertipikat kompetensi",
                    false,
                    upgradeSurveyorCubit.validateNoSertipikatKompetensiISI,
                    icon: true,
                    errorText:
                        snapshot.hasError ? snapshot.error.toString() : null,
                    img: "assets/medali/kompetensi.png");
              }),
          StreamBuilder(
              stream: upgradeSurveyorCubit.noSkaSktStream,
              builder: (context, snapshot) {
                return textField(
                    context,
                    "No. SKA/SKT*",
                    "Masukkan nomor ska/skt",
                    false,
                    upgradeSurveyorCubit.validateNoSkaSkt,
                    icon: true,
                    errorText:
                        snapshot.hasError ? snapshot.error.toString() : null,
                    img: "assets/medali/ska.png");
              }),
          StreamBuilder(
              stream: upgradeSurveyorCubit.noLisensiKadastralStream,
              builder: (context, snapshot) {
                return textField(
                    context,
                    "No. Lisensi Kadastral*",
                    "Masukkan nomor Lisensi Kadastral",
                    false,
                    upgradeSurveyorCubit.validateNoLisensiKadastral,
                    icon: true,
                    errorText:
                        snapshot.hasError ? snapshot.error.toString() : null,
                    img: "assets/medali/kadastral.png");
              }),
          StreamBuilder(
              stream: upgradeSurveyorCubit.noLisensiIrkStream,
              builder: (context, snapshot) {
                return textField(
                    context,
                    "No. Lisensi IRK*",
                    "Masukkan nomor Lisensi IRK",
                    false,
                    upgradeSurveyorCubit.validateNoLisensiIRK,
                    icon: true,
                    errorText:
                        snapshot.hasError ? snapshot.error.toString() : null,
                    img: "assets/medali/irk.png");
              }),
          SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Akun Bank*",
              style: TextStyle(
                  color: primaryColor,
                  fontSize: 14,
                  fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          StreamBuilder(
              stream: upgradeSurveyorCubit.namabankStream,
              builder: (context, snapshot) {
                return Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(
                      isExpanded: true,
                      hint: Text('Pilih Bank'),
                      // Not necessary for Option 1
                      value: _selectedBank,
                      onChanged: (newValue) {
                        setState(() {
                          _selectedBank = newValue;
                          print(newValue);
                          upgradeSurveyorCubit.setAkunBank(newValue);
                        });
                      },
                      items: _listBank.map((category) {
                        return DropdownMenuItem(
                          child: new Text(category),
                          value: category,
                        );
                      }).toList(),
                    ),
                  ),
                  decoration: BoxDecoration(
                      border: Border.all(width: 1, color: primaryColor),
                      borderRadius: BorderRadius.circular(14)),
                );
              }),
          SizedBox(
            height: 20,
          ),
          Text(
            "Nomor Rekening*",
            style: TextStyle(
                color: primaryColor, fontSize: 14, fontWeight: FontWeight.w500),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 60.0,
            alignment: AlignmentDirectional.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14.0),
                color: Colors.white,
                border: Border.all(color: primaryColor, width: 1)),
            padding:
                EdgeInsets.only(left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
            child: Theme(
              data: ThemeData(
                hintColor: Colors.transparent,
              ),
              child: Row(
                children: [
                  Expanded(
                    flex: 4,
                    child: StreamBuilder(
                        stream: upgradeSurveyorCubit.noRekeningStream,
                        builder: (context, snapshot) {
                          return TextFormField(
                            decoration: InputDecoration(
                                errorText: snapshot.hasError
                                    ? snapshot.error.toString()
                                    : null,
                                border: InputBorder.none,
                                hintText: "Masukkan nomor rekening",
                                hintStyle: TextStyle(
                                  fontSize: 12.0,
                                  fontFamily: 'Sans',
                                  letterSpacing: 0.3,
                                  color: Colors.grey,
                                )),
                            onChanged: (text) {
                              upgradeSurveyorCubit.validateNoRekening(text);
                              return text;
                            },
                          );
                        }),
                  ),
                  Expanded(
                      flex: 1,
                      child: InkWell(
                          onTap: () {
                            cekAtmUser = true;
                            setState(() {});
                          },
                          child: Text(
                            "Periksa",
                            style: TextStyle(color: primaryColor),
                          )))
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          cekAtmUser == false
              ? Container()
              : Container(
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xffB2E8ED),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Aufa Ahdi",
                        style: TextStyle(fontWeight: FontWeight.w500),
                      ),
                      CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Center(
                              child: IconButton(
                                  icon: Icon(
                                    Icons.check,
                                    color: Color(0xffB2E8ED),
                                  ),
                                  onPressed: () {})))
                    ],
                  ))
        ],
      );
    } else {
      container = Container();
    }
    return container;
  }

  Widget textField(BuildContext context, String label, String hint, bool onTap,
      void Function(String x) myVoidCallback,
      {bool icon: false,
      String errorText,
      String img,
      TextEditingController textEditingController}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 20,
        ),
        Text(
          label,
          style: TextStyle(
              color: primaryColor, fontSize: 14, fontWeight: FontWeight.w500),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          height: 60.0,
          alignment: AlignmentDirectional.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14.0),
              color: Colors.white,
              border: Border.all(color: primaryColor, width: 1)),
          padding:
              EdgeInsets.only(left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
          child: Theme(
            data: ThemeData(
              hintColor: Colors.transparent,
            ),
            child: TextFormField(
              controller: textEditingController,
              onTap: onTap
                  ? () {
                      Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => SelectLocationScreen()))
                          .then((value) {
                        print((value as LocationEntity).lat);
                        upgradeSurveyorCubit
                            .loadLandLocation((value as LocationEntity));
                      });
                    }
                  : null,
              decoration: icon == false
                  ? InputDecoration(
                      errorText: errorText,
                      border: InputBorder.none,
                      hintText: hint,
                      hintStyle: TextStyle(
                        fontSize: 12.0,
                        fontFamily: 'Sans',
                        letterSpacing: 0.3,
                        color: Colors.grey,
                      ))
                  : InputDecoration(
                      errorText: errorText,
                      border: InputBorder.none,
                      hintText: hint,
                      suffixIcon: Image.asset(
                        img,
                        cacheHeight: 30,
                      ),
                      hintStyle: TextStyle(
                        fontSize: 12.0,
                        fontFamily: 'Sans',
                        letterSpacing: 0.3,
                        color: Colors.grey,
                      )),
              onChanged: (text) {
                myVoidCallback(text);
                return text;
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget textFieldHaveValue(BuildContext context, String label, String hint) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 20,
        ),
        Text(
          label,
          style: TextStyle(
              color: primaryColor, fontSize: 14, fontWeight: FontWeight.w500),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          height: 60.0,
          alignment: AlignmentDirectional.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14.0),
              color: Colors.white,
              border: Border.all(color: primaryColor, width: 1)),
          padding:
              EdgeInsets.only(left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
          child: Theme(
            data: ThemeData(
              hintColor: Colors.transparent,
            ),
            child: TextFormField(
              enabled: false,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: hint,
                  hintStyle: TextStyle(
                    fontSize: 12.0,
                    fontFamily: 'Sans',
                    letterSpacing: 0.3,
                    color: Colors.grey,
                  )),
            ),
          ),
        ),
      ],
    );
  }

  void _openFileExplorer() async {
    _fileName = 'Pilih Berkas Lisensi PDF, JPG/JPEG, PNG';
    failedFile = "";
    try {
      _path = await FilePicker.getFilePath();
      if (_path != null) {
        if (_path.toLowerCase().contains(".pdf") == false &&
            _path.toLowerCase().contains(".png") == false &&
            _path.toLowerCase().contains(".jpg") == false &&
            _path.toLowerCase().contains(".jpeg") == false) {
          failedFile =
              "Gagal!! Pastikan file anda memilkki extension seperti contoh( .pdf, .png, .jpeg, .jpg) dibelakang nama file anda, ubah nama file dan sesuaikan dengan format file anda";
          _path = null;
          _fileName = "Gagal Memproses Berkas!";
        } else {
          _fileName = (_path.toLowerCase().contains("pdf") ||
                  _path.toLowerCase().contains("png") ||
                  _path.toLowerCase().contains("jpg") ||
                  _path.toLowerCase().contains("jpeg")
              ? _path.split('/').last
              : "Pilih Berkas Lisensi PDF, JPG/JPEG, PNG");
        }
      } else {
        _fileName = "Gagal Memproses Berkas!";
        _path = null;
      }
      upgradeSurveyorCubit.selectDocument(_path);
      upgradeSurveyorCubit.documentSurveyor();
      setState(() {});
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  _getUser() async {
    nama = await Session.getValue(Session.USER_NAME);
    noTelp = await Session.getValue(Session.USER_NO_TELP);
    email = await Session.getValue(Session.USER_EMAIL);
    jenisKelamin = await Session.getValue(Session.JENIS_KELAMIN);
    long = await Session.getValue("longitude");
    lat = await Session.getValue("latitude");
    alamat = await Session.getValue("alamats");
    setState(() {});
  }

  Future<dynamic> _onSubmit(
    int id,
    int nik,
    String tempatLahir,
    String tanggalLahir,
    String kotaTempatTinggal,
    String alamatLengkap,
    double lat,
    double long,
    String noAnggota,
    String noSka,
    String sebagai,
    String noLisensIrki,
    String noLisensiKadastral,
    String jenjangPendidikan,
    String jurusanPendidikan,
    String perguruanTinggi,
    String daftar,
    String nolisensiKompetensi,
    BuildContext context,
  ) async {
    if (_selectedSurveyor == null) {
      sebagai = "Surveyor";
    }

    bool isUpgraded = await RestAPI.doUpgrade(
        id: id,
        nik: nik,
        tempatLahir: tempatLahir,
        tanggalLahir: tanggalLahir,
        long: long,
        lat: lat,
        noAnggota: noAnggota,
        noLisensiIrk: noLisensIrki,
        noLisensiKadastral: noLisensiKadastral,
        noLisensiKompetensi: nolisensiKompetensi,
        noSka: noSka,
        alamatLengkap: alamatLengkap,
        daftar: daftar,
        jenjangPendidikan: jenjangPendidikan,
        jurusanPendidikan: jurusanPendidikan,
        kotaTempatTinggal: kotaTempatTinggal,
        perguruanTinggi: perguruanTinggi,
        sebagai: sebagai);

    if (isUpgraded) {
      if (_path != null) {
        if (_path.toLowerCase().contains("pdf") ||
            _path.toLowerCase().contains("png") ||
            _path.toLowerCase().contains("jpg") ||
            _path.toLowerCase().contains("jpeg")) {
          String tipe = "jpg";

          if (_path.toLowerCase().contains("pdf")) {
            tipe = "pdf";
          } else if (_path.toLowerCase().contains("png")) {
            tipe = "png";
          }

          var upload = await RestAPI.doUpload(id, _path, tipe);
          print(upload);
          Alert(
            context: context,
            image: Image.asset(
              "assets/alert/check.png",
              color: Colors.red,
            ),
            type: AlertType.success,
            title: "Berhasil",
            desc:
                "Data Sedang Dalam Proses Pengajuan Silahkan Menunggu Notifikasi Dari email!! ",
            closeIcon: Container(),
            onWillPopActive: true,
            buttons: [
              DialogButton(
                child: Text(
                  "Kembali",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () async {
                  await Session.doClear();
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => BtnNavigation(
                                atIndex: 2,
                              )));
                },
                width: 120,
              )
            ],
          ).show();
        }
      } else {
        Alert(
          context: context,
          image: Image.asset(
            "assets/alert/check.png",
            color: Colors.red,
          ),
          type: AlertType.success,
          title: "Berhasil",
          desc:
              "Data Sedang Dalam Proses Pengajuan Silahkan Menunggu Notifikasi Dari email!! ",
          closeIcon: Container(),
          onWillPopActive: true,
          buttons: [
            DialogButton(
              child: Text(
                "Kembali",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () async {
                await Session.doClear();
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => BtnNavigation(
                              atIndex: 2,
                            )));
              },
              width: 120,
            )
          ],
        ).show();
      }
    } else {
      Component.showAppNotification(
        context,
        "Gagal!",
        "Proses pengajuan menjadi Surveyor gagal, Pastikan semua data terisi dengan benar!",
      );
    }
  }
}
