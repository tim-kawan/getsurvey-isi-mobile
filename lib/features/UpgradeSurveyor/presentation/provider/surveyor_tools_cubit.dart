import 'package:bloc/bloc.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_surveyor_tools.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

part 'surveyor_tools_state.dart';

class SurveyorToolsCubit extends Cubit<SurveyorToolsState> {
  final CreateSurveyorTool createSurveyorTool;

  SurveyorToolsCubit({this.createSurveyorTool}) : super(SurveyorToolsInitial());

  var _jenisProdukController = BehaviorSubject<String>();
  var _merkProdukController = BehaviorSubject<String>();
  var _nomorSeriController = BehaviorSubject<String>();
  var _tahunProduksiController = BehaviorSubject<String>();
  var _unggahFileController = BehaviorSubject<String>();

  Stream<String> get jenisProdukStream => _jenisProdukController.stream;

  Stream<String> get merkProdukStream => _merkProdukController.stream;

  Stream<String> get nomorSeriStream => _nomorSeriController.stream;

  Stream<String> get tahunProduksiStream => _tahunProduksiController.stream;

  Stream<String> get unggahFileStream => _unggahFileController.stream;

  validateNmorSeri(String text) {
    if (text.length < 2) {
      _nomorSeriController.sink.addError("Inputan Belum Sesuai");
    } else {
      _nomorSeriController.sink.add(text);
    }
  }

  validateJenisProduk(String text) {
    if (text == null) {
      _jenisProdukController.sink.addError("Pilih salah satu");
    } else {
      _jenisProdukController.sink.add(text);
    }
  }

  validateMerkProduk(String text) {
    if (text == null) {
      _merkProdukController.sink.addError("Pilih salah satu");
    } else {
      _merkProdukController.sink.add(text);
    }
  }

  validateTahunProduk(String text) {
    if (text == null) {
      _tahunProduksiController.sink.addError("Pilih salah satu");
    } else {
      _tahunProduksiController.sink.add(text);
    }
  }

  selectPhoto(String text) {
    if (text.isEmpty) {
      _unggahFileController.sink.addError("Pilih salah satu");
    } else {
      _unggahFileController.sink.add(text);
    }
  }

  Stream<bool> get cekForm => Rx.combineLatest5(
      jenisProdukStream,
      merkProdukStream,
      nomorSeriStream,
      tahunProduksiStream,
      unggahFileStream,
      (a, b, c, d, e) => true);

  Future<Stream<bool>> createSurveyorTools() async {
    emit(LoadingState());
    final params = SurveyorToolParams(
        tipe: _jenisProdukController.value.trim().toString(),
        merek: _merkProdukController.value.trim().toString(),
        nomerSeri: _nomorSeriController.value.trim().toString(),
        tahunProduksi: _tahunProduksiController.value.trim().toString(),
        uploadFile: _unggahFileController.value.trim().toString());
    final result = await createSurveyorTool(params);
    result.fold((failure) => emit(FailureState()), (succes) {
      if (succes) {
        emit(SuccessState(params));
      } else {
        emit(FailureState());
      }
    });
  }
}
