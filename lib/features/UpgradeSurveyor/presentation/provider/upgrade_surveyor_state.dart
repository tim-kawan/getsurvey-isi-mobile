part of 'upgrade_surveyor_cubit.dart';

@immutable
abstract class UpgradeSurveyorState {}

class UpgradeSurveyorInitial extends UpgradeSurveyorState {}
class UpgradeSurveyorFailed extends UpgradeSurveyorState {}
class SaveLocationSuccessState extends UpgradeSurveyorState {
  final LocationEntity location;

  SaveLocationSuccessState(this.location);
}
class GetLocationsState extends UpgradeSurveyorState {
  final List<LocationEntity> listlocation;

  GetLocationsState({this.listlocation});
}

class SelectedLocationState extends UpgradeSurveyorState {
  final LocationEntity location;

  SelectedLocationState({this.location});
}

class SearchClickState extends UpgradeSurveyorState {}

class LoadingSearchClickState extends UpgradeSurveyorState {}
class LoadingState extends UpgradeSurveyorState {}
class FailureState extends UpgradeSurveyorState {}
class SuccessDocumentState extends UpgradeSurveyorState {}
class ListTooltState extends UpgradeSurveyorState {
  final List<SurveyorToolParams> params;

  ListTooltState(this.params);
}
class SuccessState extends UpgradeSurveyorState {
}

