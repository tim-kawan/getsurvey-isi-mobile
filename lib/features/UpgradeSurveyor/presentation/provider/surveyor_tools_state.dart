part of 'surveyor_tools_cubit.dart';

@immutable
abstract class SurveyorToolsState {}

class SurveyorToolsInitial extends SurveyorToolsState {}
class FailureState extends SurveyorToolsState {}
class SuccessState extends SurveyorToolsState {
  final SurveyorToolParams surveyorToolParams;

  SuccessState(this.surveyorToolParams);

}
class LoadingState extends SurveyorToolsState {}
