import 'package:bloc/bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/entities/upgrade_surveyor_data_diri_entity.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_finish_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_surveyor_tools.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_update_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_upgrade_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/document_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/location_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/surveyor_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_locations.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_services.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

part 'upgrade_surveyor_state.dart';

class UpgradeSurveyorCubit extends Cubit<UpgradeSurveyorState> {
  final CreateUpgradeSurveyor createUpgradeSurveyor;
  final CreateUpdateSurveyor createUpdateSurveyor;
  final UpgardeSurveyorDocument document;
  final GetLocations getAddress;
  final GetServices getServices;
  final GetLocations getLocations;
  final CreateFinishSurveyor finishSurveyor;

  double lat;
  double lon;
  List<SurveyorToolParams> params = [];
  UpgradeSurveyorCubit(
      {this.getAddress,
      this.createUpgradeSurveyor,
      this.getLocations,
      this.getServices,
      this.createUpdateSurveyor,
      this.document,
      this.finishSurveyor})
      : super(UpgradeSurveyorInitial());

  var _defaultAlamatController = PublishSubject<String>();
  var _nameController = BehaviorSubject<String>();
  var _emailController = BehaviorSubject<String>();
  var _jenisKelaminController = BehaviorSubject<int>();
  var _phoneNumberController = BehaviorSubject<String>();
  var _noLisensiSKBController = BehaviorSubject<String>();
  var _noLisensiISIController = BehaviorSubject<String>();
  var _tempatLahirController = BehaviorSubject<String>();
  var _tanggalLahirController = BehaviorSubject<String>();
  var _nikController = BehaviorSubject<String>();
  var _jurusanPendidikanController = BehaviorSubject<String>();
  var _perguruanTinggiController = BehaviorSubject<String>();
  var _noSertipikatKompetensiISIController = BehaviorSubject<String>();
  var _noLisensiKadastralController = BehaviorSubject<String>();
  var _noSkaSktController = BehaviorSubject<String>();
  var _noLisensiIrkController = BehaviorSubject<String>();
  var _kotaTempatTinggalController = BehaviorSubject<String>();
  var _alamatController = BehaviorSubject<String>();
  var _noRekeningController = BehaviorSubject<String>();
  var _selectedJenjangController = BehaviorSubject<String>();
  var _namaBankController = BehaviorSubject<String>();
  var _kategoriSurveyorController = BehaviorSubject<String>();
  var _noSkaController = BehaviorSubject<String>();
  var _unggahDocumentController = BehaviorSubject<String>();

  Stream<String> get defaultAlamatStream => _defaultAlamatController.stream;
  Stream<String> get nameStream => _nameController.stream;
  Stream<String> get emailStream => _emailController.stream;
  Stream<int> get jenisKelaminStream => _jenisKelaminController.stream;
  Stream<String> get phoneNumberStream => _phoneNumberController.stream;
  Stream<String> get noLisensiSKBStream => _noLisensiSKBController.stream;
  Stream<String> get noLisensiISIStream => _noLisensiISIController.stream;
  Stream<String> get tempatLahirStream => _tempatLahirController.stream;
  Stream<String> get tanggalLahirStream => _tanggalLahirController.stream;
  Stream<String> get nikStream => _nikController.stream;
  Stream<String> get jurusanPendidikanStream =>
      _jurusanPendidikanController.stream;
  Stream<String> get perguruanTinggiStream => _perguruanTinggiController.stream;
  Stream<String> get noSertipikatKompetensiISIStream =>
      _noSertipikatKompetensiISIController.stream;
  Stream<String> get noLisensiKadastralStream =>
      _noLisensiKadastralController.stream;
  Stream<String> get noSkaSktStream => _noSkaSktController.stream;
  Stream<String> get noLisensiIrkStream => _noLisensiIrkController.stream;
  Stream<String> get kotaTempatTinggalStream =>
      _kotaTempatTinggalController.stream;
  Stream<String> get alamatStream => _alamatController.stream;
  Stream<String> get noRekeningStream => _noRekeningController.stream;
  Stream<String> get selectedJenjangStream => _selectedJenjangController.stream;
  Stream<String> get namabankStream => _namaBankController.stream;
  Stream<String> get kategoriSurveyorStream =>
      _kategoriSurveyorController.stream;
  Stream<String> get noSkaStream => _noSkaController.stream;
  Stream<String> get unggahDocumentStream => _unggahDocumentController.stream;

  validateNIK(String text) {
    _nikController.sink.add(null);
    if (text.length < 16) {
      _nikController.sink
          .addError("Nik tidak valid (${16 - text.length} angka lagi)");
    } else if (text.length > 16) {
      _nikController.sink
          .addError("Nik tidak valid (lebih  ${text.length - 16} angka)");
    } else {
      _nikController.sink.add(text);
    }
  }

  validateJenjang(String text) {
    if (text == null) {
      _selectedJenjangController.sink.addError("Pilih salah satu");
    } else {
      _selectedJenjangController.sink.add(text);
    }
  }

  validateNamaBank(String text) {
    if (text == null) {
      _namaBankController.sink.addError("Pilih salah satu");
    } else {
      _namaBankController.sink.add(text);
    }
  }

  validateAlamat(String text) async {
    _defaultAlamatController.sink.add(null);
    if (text.length < 2) {
      _alamatController.sink.addError("Inputan Belum Sesuai");
    } else {
      _alamatController.sink.add(text);

      Geolocator _geolocator = Geolocator()..forceAndroidLocationManager = true;
      List<Placemark> _geocoder = [];
      _geocoder = await _geolocator.placemarkFromCoordinates(
        lat,
        lon,
      );
      print(_geocoder[0].thoroughfare);
    }
  }

  validateKotaTempatTinggal(String text) {
    if (text.length < 2) {
      _kotaTempatTinggalController.sink.addError("Inputan Belum Sesuai");
    } else {
      _kotaTempatTinggalController.sink.add(text);
    }
  }

  validateTanggalLahir(String text) {
    if (text.length == null) {
      _tanggalLahirController.sink.addError("Inputan Belum Sesuai");
    } else {
      _tanggalLahirController.sink.add(text);
    }
  }

  validateJurusanPendidikan(String text) {
    if (text.length < 2) {
      _jurusanPendidikanController.sink.addError("Inputan Belum Sesuai");
    } else {
      _jurusanPendidikanController.sink.add(text);
    }
  }

  validatePerguruanTinggi(String text) {
    if (text.length < 2) {
      _perguruanTinggiController.sink.addError("Inputan Belum Sesuai");
    } else {
      _perguruanTinggiController.sink.add(text);
    }
  }

  validateTempatLahir(String text) {
    if (text.length < 2) {
      _tempatLahirController.sink.addError("Inputan Belum Sesuai");
    } else {
      _tempatLahirController.sink.add(text);
    }
  }

  validateNoLisesnsiSKB(String text) {
    if (text.length < 2) {
      _noLisensiSKBController.sink.addError("Inputan Belum Sesuai");
    } else {
      _noLisensiSKBController.sink.add(text);
    }
  }

  validateNoLisesnsiISI(String text) {
    if (text.length < 2) {
      _noLisensiISIController.sink.addError("Inputan Belum Sesuai");
    } else {
      _noLisensiISIController.sink.add(text);
    }
  }

  validateNoSertipikatKompetensiISI(String text) {
    _noSertipikatKompetensiISIController.sink.add(null);
    if (text.length < 2) {
      _noSertipikatKompetensiISIController.sink
          .addError("Inputan Belum Sesuai");
    } else {
      _noSertipikatKompetensiISIController.sink.add(text);
    }
  }

  validateNoSkaSkt(String text) {
    _noSkaSktController.sink.add(null);
    if (text.length < 2) {
      _noSkaSktController.sink.addError("Inputan Belum Sesuai");
    } else {
      _noSkaSktController.sink.add(text);
    }
  }

  validateNoLisensiKadastral(String text) {
    _noLisensiKadastralController.sink.add(null);
    if (text.length < 2) {
      _noLisensiKadastralController.sink.addError("Inputan Belum Sesuai");
    } else {
      _noLisensiKadastralController.sink.add(text);
    }
  }

  validateNoLisensiIRK(String text) {
    _noLisensiIrkController.sink.add(null);
    if (text.length < 2) {
      _noLisensiIrkController.sink.addError("Inputan Belum Sesuai");
    } else {
      _noLisensiIrkController.sink.add(text);
    }
  }

  validateNoRekening(String text) {
    _noRekeningController.sink.add(null);
    if (text.length < 2) {
      _noRekeningController.sink.addError("Inputan Belum Sesuai");
    } else {
      _noRekeningController.sink.add(text);
    }
  }

  validateLocation(String text) {
    _defaultAlamatController.sink.add(null);
    if (text.length < 4) {
      _alamatController.sink.addError("No telepon minimal 6 angka");
    } else {
      _alamatController.sink.add(text);
    }
  }

  setkategori(int value) {
    if (value == UpgradeSurveyorEntity.SURVEYOR_IRK) {
      _noLisensiSKBController.sink.add("-");
      _kategoriSurveyorController.sink
          .add(UpgradeSurveyorEntity.SURVEYOR_IRK.toString());
    } else {
      _noLisensiIrkController.sink.add("-");
      _kategoriSurveyorController.sink
          .add(UpgradeSurveyorEntity.SURVEYOR_SKB.toString());
    }
  }

  selectDocument(String text) {
    if (text.isEmpty) {
      _unggahDocumentController.sink.addError("Pilih salah satu");
    } else {
      _unggahDocumentController.sink.add(text);
    }
  }

  void searchClick() {
    emit(SearchClickState());
  }

  Future<void> selectLocation(LocationEntity location) async {
    emit(SelectedLocationState(location: location));
  }

  Future<void> findLocation() async {
    emit(LoadingSearchClickState());
    final result = await getAddress(_alamatController.value.trim().toString());
    result.fold((fail) => emit(UpgradeSurveyorFailed()),
        (listEntity) => emit(GetLocationsState(listlocation: listEntity)));
  }

  Future<void> saveLandLocation(LocationEntity entity) async {
    emit(SaveLocationSuccessState(entity));
  }

  Future<void> loadLandLocation(LocationEntity entity) async {
    lat = entity.lat;
    lon = entity.lng;
    _defaultAlamatController.sink.add(entity.address);
    _alamatController.sink.add(entity.address);
    _cariKotaTempatTinggal(lat, lon);
  }

  Future<void> loadApplicationLocation(LocationEntity entity) async {
    _alamatController.sink.add(entity.address);
  }

  Stream<bool> get nextFormSubmit => Rx.combineLatest7(
      nikStream,
      tempatLahirStream,
      alamatStream,
      kotaTempatTinggalStream,
      jurusanPendidikanStream,
      perguruanTinggiStream,
      selectedJenjangStream,
      (a, b, c, d, e, f, g) => true);

  Stream<bool> get readyToSubmit => Rx.combineLatest9(
      kategoriSurveyorStream,
      noLisensiISIStream,
      noLisensiSKBStream,
      noSertipikatKompetensiISIStream,
      noSkaSktStream,
      noLisensiKadastralStream,
      noLisensiIrkStream,
      namabankStream,
      noRekeningStream,
      (a, b, c, d, e, f, g, h, i) => true);

  Future<Stream<bool>> upgradeSurveyor() async {
    emit(LoadingState());
    String nama = await Session.getValue(Session.USER_NAME);
    String noTelp = await Session.getValue(Session.USER_NO_TELP);
    String email = await Session.getValue(Session.USER_EMAIL);
    int jenisKelamin = await Session.getValue(Session.JENIS_KELAMIN);
    final params = UpgradeSurveyorParams(
        long: lon.toString(),
        lat: lat.toString(),
        nik: _nikController.value.trim().toString(),
        alamatTempatTinggal: _alamatController.value.trim().toString(),
        email: email,
        jenisKelamin: jenisKelamin,
        jenjangPendidikan: _selectedJenjangController.value.trim().toString(),
        jurusanPendidikan: _jurusanPendidikanController.value.trim().toString(),
        kotaTempatTinggal: _kotaTempatTinggalController.value.trim().toString(),
        namaLengkap: nama,
        noHp: noTelp,
        perguruanTinggi: _perguruanTinggiController.value.trim().toString(),
        tanggalLahir: _tanggalLahirController.value.toString(),
        tempatLahir: _tempatLahirController.value.trim().toString());

    final result = await createUpgradeSurveyor(params);
    result.fold((failure) => emit(FailureState()), (succes) {
      if (succes) {
        emit(SuccessState());
      } else {
        emit(FailureState());
      }
    });
  }

  Future<Stream<bool>> updateSurveyor() async {
    emit(LoadingState());
    final params = UpdateSurveyorParams(
        sebagai: _kategoriSurveyorController.value.trim().toString(),
        akunBank: _namaBankController.value.trim().toString(),
        noLisensiKadastral: _noLisensiKadastralController.value.toString(),
        noLisensiIrk: _noLisensiIrkController.value.trim().toString(),
        noSkaSkt: _noSkaSktController.value.trim().toString(),
        noLinsensiSkb: _noLisensiSKBController.value.trim().toString(),
        noRek: _noRekeningController.value.trim().toString(),
        noSertifikatKompetensi:
            _noSertipikatKompetensiISIController.value.trim().toString());
    final result = await createUpdateSurveyor(params);
    result.fold((failure) => emit(FailureState()), (succes) {
      if (succes) {
        emit(SuccessState());
      } else {
        emit(FailureState());
      }
    });
  }

  Future<Stream<bool>> finishCreateSurveyor() async {
    emit(LoadingState());
    final params = UpgradeSurveyorFinishparams(
      sebagai: _kategoriSurveyorController.value.trim().toString(),
    );
    final result = await finishSurveyor(params);
    result.fold((failure) => emit(FailureState()), (succes) {
      if (succes) {
        emit(SuccessState());
      } else {
        emit(FailureState());
      }
    });
  }

  Future<Stream<bool>> documentSurveyor() async {
    emit(LoadingState());
    print(_kategoriSurveyorController.value.trim().toString());
    print(_unggahDocumentController.value.trim().toString());
    final params = UpgradeSurveyorDocumentParams(
        sebagai: _kategoriSurveyorController.value.trim().toString(),
        file: _unggahDocumentController.value.trim().toString());
    final result = await document(params);
    result.fold((failure) => emit(FailureState()), (succes) {
      if (succes) {
        emit(SuccessDocumentState());
      } else {
        emit(FailureState());
      }
    });
  }

  void setAkunBank(String akun) {
    _namaBankController.sink.add(akun);
  }

  void _cariKotaTempatTinggal(double lat, double lon) async {
    Geolocator _geolocator = Geolocator()..forceAndroidLocationManager = true;
    List<Placemark> _geocoder = [];
    _geocoder = await _geolocator.placemarkFromCoordinates(
      lat,
      lon,
    );
    print(_geocoder[0].thoroughfare);
    _kotaTempatTinggalController.sink.add(_geocoder[0].subAdministrativeArea);
  }

  void addTools(SurveyorToolParams value) {
    params.add(value);
    emit(ListTooltState(params));
  }
}
