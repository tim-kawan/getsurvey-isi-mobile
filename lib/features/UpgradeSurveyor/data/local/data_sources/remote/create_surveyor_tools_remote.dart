import 'dart:convert';
import 'dart:io';

import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/base/icreate_surveyor_tool_remote.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_surveyor_tools.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'package:path/path.dart';

class CreateSurveyorToolRemote implements IcreateSurveyorToolRemote {
  final http.Client client;

  CreateSurveyorToolRemote(this.client);

  @override
  Future<bool> createSurveyorTool(SurveyorToolParams params) async {
    // TODO: implement createSurveyorTool
    var request = new http.MultipartRequest(
        "POST", Uri.parse("${RestAPI.API_URL}/survey-tool/skb"));
    final authToken = await Session.getValue(Session.AUTH_TOKEN);

    Map<String, String> headers = {
      "Authorization": authToken,
      "Content-type": "multipart/form-data"
    };
    final file = File(params.uploadFile);
    final mimeType = lookupMimeType(params.uploadFile).split("/");
    request.files.add(
      http.MultipartFile(
        'upload_file',
        file.readAsBytes().asStream(),
        file.lengthSync(),
        contentType: MediaType(mimeType[0], mimeType[1]),
        filename: basename(params.uploadFile),
      ),
    );
    request.headers.addAll(headers);
    request.fields.addAll({
      "tipe": params.tipe,
      "merek": params.merek,
      "nomer_seri": params.nomerSeri,
      "tahun_produksi": params.tahunProduksi,
    });
    final response = await request.send();
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
