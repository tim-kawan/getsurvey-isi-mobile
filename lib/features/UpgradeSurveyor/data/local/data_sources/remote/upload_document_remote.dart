import 'dart:convert';
import 'dart:io';

import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/base/iupload_document_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/document_surveyor.dart';
import 'package:http/http.dart' as http;
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'package:path/path.dart';

class UploadDocumentSurveyorRemote implements IUploadDocumentSurveyorRemote {
  final http.Client client;

  UploadDocumentSurveyorRemote(this.client);
  @override
  Future<bool> uploadDocumentSurveyor(
      UpgradeSurveyorDocumentParams params) async {
    var request = new http.MultipartRequest(
        "POST", Uri.parse("${RestAPI.API_URL}/user-public/upgrade-surveyor-skb/upload"));
    final authToken = await Session.getValue(Session.AUTH_TOKEN);

    Map<String, String> headers = {
      "Authorization": authToken,
      "Content-type": "multipart/form-data"
    };
    final file = File(params.file);
    final mimeType = lookupMimeType(params.file).split("/");
    request.files.add(
      http.MultipartFile(
        'upload_file',
        file.readAsBytes().asStream(),
        file.lengthSync(),
        contentType: MediaType(mimeType[0], mimeType[1]),
        filename: basename(params.file),
      ),
    );
    request.headers.addAll(headers);
    request.fields.addAll({"sebagai": params.sebagai});
    final response = await request.send();
    print(response.statusCode);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
