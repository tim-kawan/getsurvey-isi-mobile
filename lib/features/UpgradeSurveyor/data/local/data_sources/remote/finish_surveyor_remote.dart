import 'dart:convert';

import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/base/ifinish_surveyor_remote.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_finish_surveyor.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:http/http.dart' as http;

class FinishSurveyorRemote implements IFinishSurevyroRemote {
  final http.Client client;

  FinishSurveyorRemote(this.client);

  @override
  Future<bool> finishSurveyor(UpgradeSurveyorFinishparams params) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.post(
        Uri.parse("${RestAPI.API_URL}/user-public/upgrade-surveyor/finish"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authToken,
        },
        body: json.encode({
          "sebagai": params.sebagai,
        }));
    print(response.statusCode);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
