import 'dart:convert';

import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/base/iupgrade_surveyor_remote.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_update_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_upgrade_surveyor.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:http/http.dart' as http;

class UpgradeSurveyorRemote implements IUpgradeSurveyorRemote {
  final http.Client client;

  UpgradeSurveyorRemote(this.client);

  @override
  Future<bool> createUpgradeSurveyor(UpgradeSurveyorParams params) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    print(authToken);
    var response = await client.post(
        Uri.parse("${RestAPI.API_URL}/user-public/upgrade-surveyor-skb"),
        headers: {'Content-Type': 'application/json','Authorization': authToken},
        body: json.encode({
          "nama_lengkap" : params.namaLengkap,
          "email" : params.email,
          "jenis_kelamin" : params.jenisKelamin,
          "no_hp" : params.noHp,
          "tempat_lahir" : params.tempatLahir,
          "tanggal_lahir" : params.tanggalLahir,
          "nik" : params.nik,
          "kota_tempat_tinggal" : params.kotaTempatTinggal,
          "alamat_tempat_tinggal" : params.alamatTempatTinggal,
          "lat" : params.lat,
          "long" : params.long,
          "jurusan_pendidikan" : params.jurusanPendidikan,
          "jenjang_pendidikan" : params.jenjangPendidikan,
          "perguruan_tinggi" : params.perguruanTinggi
        }));
    print(response.statusCode);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<bool> surveyorDocument(String document) async{
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.post(
        Uri.parse("${RestAPI.API_URL}/user-public/upgrade-surveyor-skb/upload"),
        headers: {'Content-Type': 'application/json','Authorization': authToken,},
        body: json.encode({
          "sebagai": "1",
          "upload_file" : "file.jpg"
        }));
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<bool> createUpdateSurveyor(UpdateSurveyorParams params) async{
    // TODO: implement createUpdateSurveyor
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.post(
        Uri.parse("${RestAPI.API_URL}/user-public/upgrade-skb-keanggotaan"),
        headers: {'Content-Type': 'application/json','Authorization': authToken,},
        body: json.encode({
          "sebagai": params.sebagai,
          "no_linsensi_skb": params.noLinsensiSkb,
          "no_sertifikat_kompetensi": params.noSertifikatKompetensi,
          "no_ska_skt": params.noSkaSkt,
          "no_lisensi_kadastral": params.noLisensiKadastral,
          "no_lisensi_irk": params.noLisensiIrk,
          "akun_bank": params.akunBank,
          "no_rek": params.noRek
        }));
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }


}
