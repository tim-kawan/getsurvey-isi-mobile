import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_update_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_upgrade_surveyor.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/dto/location_dto.dart';


abstract class IUpgradeSurveyorRemote{
  Future<bool> createUpgradeSurveyor(UpgradeSurveyorParams params);
  Future<bool> surveyorDocument(String document);
  Future<bool> createUpdateSurveyor(UpdateSurveyorParams params);
}