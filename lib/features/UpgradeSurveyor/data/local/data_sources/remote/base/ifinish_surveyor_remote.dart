import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_finish_surveyor.dart';

abstract class IFinishSurevyroRemote{
  Future<bool> finishSurveyor(UpgradeSurveyorFinishparams params);
}