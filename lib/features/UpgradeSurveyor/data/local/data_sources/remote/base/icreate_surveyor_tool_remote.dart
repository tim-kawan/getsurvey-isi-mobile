import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_surveyor_tools.dart';

abstract class IcreateSurveyorToolRemote{
  Future<bool> createSurveyorTool(SurveyorToolParams params);
}