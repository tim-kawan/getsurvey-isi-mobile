import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/document_surveyor.dart';

abstract class IUploadDocumentSurveyorRemote{
  Future<bool> uploadDocumentSurveyor(UpgradeSurveyorDocumentParams params);
}