import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/network/network_info.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/base/iupload_document_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/repositories/iupgrade_surveyor_document_repository.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/document_surveyor.dart';

class UploadDocumntSurveyorRepository
    implements IUploadurveyorDocumentRepository {
  final NetworkInfo networkInfo;
  final IUploadDocumentSurveyorRemote iUploadDocumentSurveyorRemote;

  UploadDocumntSurveyorRepository(
      {this.networkInfo, this.iUploadDocumentSurveyorRemote});

  @override
  Future<Either<Failure, bool>> uploadFileUpgradeSurveyor(
      UpgradeSurveyorDocumentParams params) async {
    if (await networkInfo.isConnected) {
      try {

            await iUploadDocumentSurveyorRemote.uploadDocumentSurveyor(params);
        return Right(true);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }
}
