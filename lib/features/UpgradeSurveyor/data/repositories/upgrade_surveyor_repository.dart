import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/network/network_info.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/base/iupgrade_surveyor_remote.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/repositories/iupgrade_surveyor_repository.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_update_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_upgrade_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/location_entity.dart';

class UpgradeSurveyorRepository implements IUpgradeSurveyorRepository{
  final NetworkInfo networkInfo;
  final IUpgradeSurveyorRemote iUpgradeSurveyorRemote;

  UpgradeSurveyorRepository(
      {this.networkInfo,
      this.iUpgradeSurveyorRemote});

  @override
  Future<Either<Failure, bool>> upgradeSurveyorDataDiri(UpgradeSurveyorParams params) async{
    if (await networkInfo.isConnected) {
      try {
        await iUpgradeSurveyorRemote.createUpgradeSurveyor(params);
        return Right(true);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> upgradeSurveyorDataKeanggotaan(UpdateSurveyorParams params) async{
    if (await networkInfo.isConnected) {
      try {
        await iUpgradeSurveyorRemote.createUpdateSurveyor(params);
        return Right(true);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }

}