import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/network/network_info.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/base/ifinish_surveyor_remote.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/repositories/ifinish_surveyor_repository.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_finish_surveyor.dart';

class FinishSurveyorRepository extends IFinishSurveyorRepository{
  final NetworkInfo networkInfo;
  final IFinishSurevyroRemote iFinishSurevyroRemote;

  FinishSurveyorRepository({this.networkInfo, this.iFinishSurevyroRemote});

  @override
  Future<Either<Failure, bool>> finishSurveyor(UpgradeSurveyorFinishparams params) async{
    if (await networkInfo.isConnected) {
      try {
        await iFinishSurevyroRemote.finishSurveyor(params);
        return Right(true);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }


}