import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/network/network_info.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/base/icreate_surveyor_tool_remote.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/repositories/icreate_surveyor_tools_repository.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_surveyor_tools.dart';

class CreateSurveyorToolRepository extends ICreateSurveyorToolsRepository{
  final NetworkInfo networkInfo;
  final IcreateSurveyorToolRemote icreateSurveyorToolRemote;

  CreateSurveyorToolRepository(
      {this.networkInfo, this.icreateSurveyorToolRemote});

  @override
  Future<Either<Failure, bool>> createData(SurveyorToolParams params) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await icreateSurveyorToolRemote.createSurveyorTool(params);
        return Right(result);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }}