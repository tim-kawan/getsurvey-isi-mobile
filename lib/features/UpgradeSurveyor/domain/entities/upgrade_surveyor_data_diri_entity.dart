class UpgradeSurveyorEntity {
  final String namaLengkap;
  final String email;
  final int jenisKelamin;
  final String noHp;
  final String tempatLahir;
  final String tanggalLahir;
  final String nik;
  final String kotaTempatTinggal;
  final String alamatTempatTinggal;
  final String lat;
  final String long;
  final String jurusanPendidikan;
  final String jenjangPendidikan;
  final String perguruanTinggi;

  static const SURVEYOR_IRK =1;
  static const SURVEYOR_SKB =2;

  static const JENIS_KELAMIN_PRIA = 1;
  static const JENIS_KELAMIN_WANITA = 0;

  UpgradeSurveyorEntity(
      {this.namaLengkap,
      this.email,
      this.jenisKelamin,
      this.noHp,
      this.tempatLahir,
      this.tanggalLahir,
      this.nik,
      this.kotaTempatTinggal,
      this.alamatTempatTinggal,
      this.lat,
      this.long,
      this.jurusanPendidikan,
      this.jenjangPendidikan,
      this.perguruanTinggi});

}


