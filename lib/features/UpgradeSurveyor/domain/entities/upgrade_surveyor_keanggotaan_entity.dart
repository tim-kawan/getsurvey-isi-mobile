class UpdateSurveyorKeanggotaanEntity {
  final String sebagai;
  final String noLinsensiSkb;
  final String noSertifikatKompetensi;
  final String noSkaSkt;
  final String noLisensiKadastral;
  final String noLisensiIrk;
  final String akunBank;
  final String noRek;

  UpdateSurveyorKeanggotaanEntity(
      {this.sebagai,
      this.noLinsensiSkb,
      this.noSertifikatKompetensi,
      this.noSkaSkt,
      this.noLisensiKadastral,
      this.noLisensiIrk,
      this.akunBank,
      this.noRek});
}
