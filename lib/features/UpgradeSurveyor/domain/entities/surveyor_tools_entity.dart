class SurveyorToolEntity {
  final String tipe;
  final String merek;
  final String nomerSeri;
  final String tahunProduksi;
  final String userSurveyorId;
  final String uploadFile;

  SurveyorToolEntity(
      {this.tipe,
      this.merek,
      this.nomerSeri,
      this.tahunProduksi,
      this.userSurveyorId,
      this.uploadFile});
}