import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_finish_surveyor.dart';

abstract class IFinishSurveyorRepository{
  Future<Either<Failure, bool>> finishSurveyor(UpgradeSurveyorFinishparams params);
}