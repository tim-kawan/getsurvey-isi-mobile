import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_surveyor_tools.dart';

abstract class ICreateSurveyorToolsRepository{
  Future<Either<Failure, bool>> createData(SurveyorToolParams params);
}