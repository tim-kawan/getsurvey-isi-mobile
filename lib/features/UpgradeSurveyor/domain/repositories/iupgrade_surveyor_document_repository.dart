import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/document_surveyor.dart';

abstract class IUploadurveyorDocumentRepository{
  Future<Either<Failure, bool>> uploadFileUpgradeSurveyor(UpgradeSurveyorDocumentParams params);
}