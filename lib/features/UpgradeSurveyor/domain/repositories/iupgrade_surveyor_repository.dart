import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_update_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_upgrade_surveyor.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/location_entity.dart';

abstract class IUpgradeSurveyorRepository{
  Future<Either<Failure, bool>> upgradeSurveyorDataDiri(UpgradeSurveyorParams params);
  Future<Either<Failure, bool>> upgradeSurveyorDataKeanggotaan(UpdateSurveyorParams params);
}