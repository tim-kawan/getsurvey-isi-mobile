import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/repositories/iupgrade_surveyor_repository.dart';

class CreateUpdateSurveyor extends UseCase<bool, UpdateSurveyorParams> {
  final IUpgradeSurveyorRepository _surveyorRepository;

  CreateUpdateSurveyor(this._surveyorRepository);

  @override
  Future<Either<Failure, bool>> call(UpdateSurveyorParams params) {
    return _surveyorRepository.upgradeSurveyorDataKeanggotaan(params);
  }
}

class UpdateSurveyorParams {
  final String sebagai;
  final String noLinsensiSkb;
  final String noSertifikatKompetensi;
  final String noSkaSkt;
  final String noLisensiKadastral;
  final String noLisensiIrk;
  final String akunBank;
  final String noRek;

  UpdateSurveyorParams(
      {this.sebagai,
      this.noLinsensiSkb,
      this.noSertifikatKompetensi,
      this.noSkaSkt,
      this.noLisensiKadastral,
      this.noLisensiIrk,
      this.akunBank,
      this.noRek});

  UpdateSurveyorParams setFromEntity() => UpdateSurveyorParams(
      sebagai: sebagai,
      noLinsensiSkb: noLinsensiSkb,
      noSertifikatKompetensi: noSertifikatKompetensi,
      noSkaSkt: noSkaSkt,
      noLisensiKadastral: noLisensiKadastral,
      noLisensiIrk: noLisensiIrk,
      akunBank: akunBank,
      noRek: noRek);

  @override
  // TODO: implement props
  List<Object> get props => [];
}

