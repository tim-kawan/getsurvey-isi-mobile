import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/repositories/iupgrade_surveyor_document_repository.dart';

class UpgardeSurveyorDocument
    extends UseCase<bool, UpgradeSurveyorDocumentParams> {
  final IUploadurveyorDocumentRepository iupgradeSurveyor;

  UpgardeSurveyorDocument({this.iupgradeSurveyor});

  @override
  Future<Either<Failure, bool>> call(
      UpgradeSurveyorDocumentParams params) async {
    return await iupgradeSurveyor.uploadFileUpgradeSurveyor(params);
  }
}

class UpgradeSurveyorDocumentParams {
  final String sebagai;
  final String file;

  UpgradeSurveyorDocumentParams({this.sebagai, this.file});
}
