import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/repositories/ifinish_surveyor_repository.dart';

class CreateFinishSurveyor extends UseCase<bool, UpgradeSurveyorFinishparams>{
  final IFinishSurveyorRepository iFinishSurveyorRepository;

  CreateFinishSurveyor({this.iFinishSurveyorRepository});

  @override
  Future<Either<Failure, bool>> call(UpgradeSurveyorFinishparams params) async{
    // TODO: implement call
    return await iFinishSurveyorRepository.finishSurveyor(params);
  }


}

class UpgradeSurveyorFinishparams {
  final String sebagai;

  UpgradeSurveyorFinishparams({this.sebagai});
}