import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/repositories/icreate_surveyor_tools_repository.dart';

class CreateSurveyorTool extends UseCase<bool, SurveyorToolParams> {
  final ICreateSurveyorToolsRepository iSurveyorTools;

  CreateSurveyorTool(this.iSurveyorTools);

  @override
  Future<Either<Failure, bool>> call(SurveyorToolParams params) async {
    return await iSurveyorTools.createData(params);
  }
}

class SurveyorToolParams {
  final String tipe;
  final String merek;
  final String nomerSeri;
  final String tahunProduksi;
  final String uploadFile;

  SurveyorToolParams(
      {this.tipe,
      this.merek,
      this.nomerSeri,
      this.tahunProduksi,
      this.uploadFile});
}
