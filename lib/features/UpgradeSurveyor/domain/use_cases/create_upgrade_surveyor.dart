import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/repositories/iupgrade_surveyor_repository.dart';

class CreateUpgradeSurveyor extends UseCase<bool, UpgradeSurveyorParams> {
  final IUpgradeSurveyorRepository _surveyorRepository;

  CreateUpgradeSurveyor(this._surveyorRepository);

  @override
  Future<Either<Failure, bool>> call(UpgradeSurveyorParams params) {
    return _surveyorRepository.upgradeSurveyorDataDiri(params);
  }
}

class UpgradeSurveyorParams extends Equatable {
  final String namaLengkap;
  final String email;
  final int jenisKelamin;
  final String noHp;
  final String tempatLahir;
  final String tanggalLahir;
  final String nik;
  final String kotaTempatTinggal;
  final String alamatTempatTinggal;
  final String lat;
  final String long;
  final String jurusanPendidikan;
  final String jenjangPendidikan;
  final String perguruanTinggi;

  UpgradeSurveyorParams(
      {this.namaLengkap,
      this.email,
      this.jenisKelamin,
      this.noHp,
      this.tempatLahir,
      this.tanggalLahir,
      this.nik,
      this.kotaTempatTinggal,
      this.alamatTempatTinggal,
      this.lat,
      this.long,
      this.jurusanPendidikan,
      this.jenjangPendidikan,
      this.perguruanTinggi});

  UpgradeSurveyorParams setFromEntity() => UpgradeSurveyorParams(
      namaLengkap: namaLengkap,
      tempatLahir: tempatLahir,
      tanggalLahir: tanggalLahir,
      perguruanTinggi: perguruanTinggi,
      noHp: noHp,
      kotaTempatTinggal: kotaTempatTinggal,
      jurusanPendidikan: jurusanPendidikan,
      jenjangPendidikan: jenjangPendidikan,
      jenisKelamin: jenisKelamin,
      email: email,
      alamatTempatTinggal: alamatTempatTinggal,
      nik: nik,
      lat: lat,
      long: long,);

  @override
  // TODO: implement props
  List<Object> get props => [];
}
