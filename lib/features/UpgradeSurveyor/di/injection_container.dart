import 'package:get_it/get_it.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/base/icreate_surveyor_tool_remote.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/base/ifinish_surveyor_remote.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/base/iupgrade_surveyor_remote.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/base/iupload_document_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/create_surveyor_tools_remote.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/finish_surveyor_remote.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/upgrade_surveyor_remote.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/local/data_sources/remote/upload_document_remote.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/repositories/create_surveyor_tool_repository.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/repositories/finish_surveyor_repository.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/repositories/upgrade_surveyor_repository.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/data/repositories/upload_document_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/repositories/icreate_surveyor_tools_repository.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/repositories/ifinish_surveyor_repository.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/repositories/iupgrade_surveyor_document_repository.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/repositories/iupgrade_surveyor_repository.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_finish_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_surveyor_tools.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_update_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/create_upgrade_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/domain/use_cases/document_surveyor.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/presentation/provider/surveyor_tools_cubit.dart';
import 'package:get_survey_app/features/UpgradeSurveyor/presentation/provider/upgrade_surveyor_cubit.dart';

Future<void> initUpgradeSurveyor(GetIt di) async {
  //bloc
  di.registerFactory(() => UpgradeSurveyorCubit(
      createUpgradeSurveyor: di(),
      createUpdateSurveyor: di(),
      document: di(),
      finishSurveyor: di()));
  di.registerFactory(() => SurveyorToolsCubit(createSurveyorTool: di()));

  //usecase
  di.registerLazySingleton(() => CreateUpgradeSurveyor(di()));
  di.registerLazySingleton(() => CreateSurveyorTool(di()));
  di.registerLazySingleton(() => CreateUpdateSurveyor(di()));
  di.registerLazySingleton(
      () => UpgardeSurveyorDocument(iupgradeSurveyor: di()));
  di.registerLazySingleton(
      () => CreateFinishSurveyor(iFinishSurveyorRepository: di()));

  di.registerLazySingleton<IUpgradeSurveyorRepository>(
      () => UpgradeSurveyorRepository(
            networkInfo: di(),
            iUpgradeSurveyorRemote: di(),
          ));
  di.registerLazySingleton<IUploadurveyorDocumentRepository>(
      () => UploadDocumntSurveyorRepository(
            iUploadDocumentSurveyorRemote: di(),
            networkInfo: di(),
          ));
  di.registerLazySingleton<ICreateSurveyorToolsRepository>(() =>
      CreateSurveyorToolRepository(
          networkInfo: di(), icreateSurveyorToolRemote: di()));
  di.registerLazySingleton<IFinishSurveyorRepository>(() =>
      FinishSurveyorRepository(networkInfo: di(), iFinishSurevyroRemote: di()));

  //remote
  di.registerLazySingleton<IUpgradeSurveyorRemote>(
      () => UpgradeSurveyorRemote(di()));
  di.registerLazySingleton<IUploadDocumentSurveyorRemote>(
      () => UploadDocumentSurveyorRemote(di()));
  di.registerLazySingleton<IcreateSurveyorToolRemote>(
      () => CreateSurveyorToolRemote(di()));
  di.registerLazySingleton<IFinishSurevyroRemote>(
      () => FinishSurveyorRemote(di()));
}
