import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:get_survey_app/core/network/network_info.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/base/kjsb_remote.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/remote/kjsb_remote.dart';
import 'package:get_survey_app/features/voting/data/data_sources/remotes/voting_remote.dart';
import 'package:get_survey_app/features/voting/data/repositories/data_sources/remotes/voting_remote.dart';
import 'package:get_survey_app/features/voting/data/repositories/voting_repository.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/check_voter_status.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/fetch_kandidat.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/get_vote_result.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/repositories/voting_repository.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/submit_kandidat.dart';
import 'package:get_survey_app/features/voting/presentation/cubit/voting_cubit.dart';
import 'package:http/http.dart' as http;

Future<void> initVoting(GetIt di) async {
  // Bloc
  di.registerFactory(() => VotingCubit(di(), di(), di(), di()));

  // Use cases
  di.registerLazySingleton(() => FetchKandidat(di()));
  di.registerLazySingleton(() => VoteKandidat(di()));
  di.registerLazySingleton(() => VoterStatus(di()));
  di.registerLazySingleton(() => GetVoteResult(di()));

  // Repository
  di.registerLazySingleton<IVotingRepository>(
    () => VotingRepository(networkInfo: di(), votingRemote: di()),
  );

  // Data sources
  di.registerLazySingleton<IVotingRemote>(() => VotingRemote(di()));
  di.registerLazySingleton<IKjsbRemote>(() => KjsbRemote(di()));

  //! Core
  di.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(di()));

  //! External
  di.registerLazySingleton(() => http.Client());
  di.registerLazySingleton(() => DataConnectionChecker());
}
