import 'dart:convert';

import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/features/voting/data/data_sources/remotes/entities/kandidat_dto.dart';
import 'package:get_survey_app/features/voting/data/repositories/data_sources/remotes/voting_remote.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/submit_kandidat.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:http/http.dart' as http;

import 'entities/result_dto.dart';
import 'entities/valid_voter.dart';

class VotingRemote extends IVotingRemote {
  final http.Client client;

  VotingRemote(this.client);

  @override
  Future<List<KandidatDto>> fetchKandidat(String type) async {
    final response = await client
        .get(Uri.parse("${RestAPI.API_URL}/vote/candidates/$type"), headers: {
      'Content-Type': 'application/json',
    });

    if (response.statusCode == 200) {
      return (json.decode(response.body)['data'] as List)
          .map((kandidat) => KandidatDto.fromJson(kandidat))
          .toList();
    } else {
      throw ServerException(response.body.toString(), response.statusCode);
    }
  }

  @override
  Future<bool> voteKandidat(KanidatParams kanidatParams) async {
    final response =
        await client.post(Uri.parse("${RestAPI.API_URL}/vote/submit"),
            headers: {'Content-Type': 'application/json'},
            body: json.encode({
              "user_id": kanidatParams.userId,
              "kandidat_ketua_id": kanidatParams.ketua,
              "kandidat_wakil1_id": kanidatParams.wakil1,
              "kandidat_wakil2_id": kanidatParams.wakil2,
              "kandidat_wakil3_id": kanidatParams.wakil3,
            }));

    if (response.statusCode == 200) {
      return true;
    } else {
      throw ServerException(response.body.toString(), response.statusCode);
    }
  }

  @override
  Future<ValidVoterDto> validateVoter(int userId) async {
    final response = await client.get(
        Uri.parse("${RestAPI.API_URL}/vote/validate/${userId}"),
        headers: {'Content-Type': 'application/json'});
    if (response.statusCode == 200) {
      return ValidVoterDto.fromJson(json.decode(response.body));
    } else {
      throw ServerException(response.body.toString(), response.statusCode);
    }
  }

  @override
  Future<VoteResultDto> getResultVote(int userId) async {
    final response = await client.get(
        Uri.parse("${RestAPI.API_URL}/vote/result/${userId}"),
        headers: {'Content-Type': 'application/json'});
    if (response.statusCode == 200) {
      final wakil = json.decode(response.body)['data']['wakil'] as List;
      return VoteResultDto(
          ketua:
              KandidatDto.fromJson(json.decode(response.body)['data']['ketua']),
          wakilKetua: wakil.map((wakil) => KandidatDto.fromJson(wakil)).toList());
    } else {
      throw ServerException(response.body.toString(), response.statusCode);
    }
  }
}
