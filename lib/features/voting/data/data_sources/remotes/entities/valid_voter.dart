import 'package:get_survey_app/features/voting/domain/entities/valid_voter_entity.dart';

class ValidVoterDto {
  final String title;
  final bool isVlid;
  final String startEnd;
  final bool hasVote;
  final bool hasOpen;
  final double timeToVote;

  ValidVoterDto(
      {this.title,
      this.startEnd,
      this.hasOpen,
      this.timeToVote,
      this.isVlid,
      this.hasVote});

  factory ValidVoterDto.fromJson(Map<String, dynamic> json) {
    return ValidVoterDto(
        title: json["data"]["vote_session"]["event"],
        isVlid: json["valid"] ?? false,
        hasVote: json["data"]["is_vote"],
        hasOpen: json["data"]["vote_session"]["open"],
        timeToVote: json["data"]["vote_session"]["time"],
        startEnd: json["data"]["vote_session"]["start_end"]);
  }

  ValidVoterEntity toEntity() => ValidVoterEntity(
      timeToVote: this.timeToVote.toInt(),
      hasVote: this.hasVote,
      isValid: this.isVlid,
      hasOpen: this.hasOpen,
      title: this.title,
      startEnd: this.startEnd);
}
