import 'package:get_survey_app/features/voting/data/data_sources/remotes/entities/kandidat_dto.dart';
import 'package:get_survey_app/features/voting/domain/entities/result_dto.dart';

class VoteResultDto {
  final KandidatDto ketua;
  final List<KandidatDto> wakilKetua;

  VoteResultDto({this.ketua, this.wakilKetua});

  VoteResultEntity toEntity() => VoteResultEntity(
      ketua: ketua.toEntity(), wakilKetua: wakilKetua.map((e) => e.toEntity()).toList());
}
