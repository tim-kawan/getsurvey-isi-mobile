import 'package:get_survey_app/features/voting/domain/entities/kandidat_entity.dart';

class KandidatDto {
  final int id;
  final String name;
  final String location;
  final String photo;
  final String videoUrl;

  KandidatDto({
    this.id,
    this.name,
    this.location,
    this.photo,
    this.videoUrl,
  });

  factory KandidatDto.fromJson(Map<String, dynamic> json) {
    return KandidatDto(
      id: json["id"],
      name: json["name"] ?? json["nama"],
      location: json["location"],
      photo: json["avatar"],
      videoUrl: json["video_url"],
    );
  }

  KandidatEntity toEntity() => KandidatEntity(
      id: id,
      name: name,
      location: this.location,
      photo: this.photo,
      isLast: false,
      videoUrl: this.videoUrl);
}
