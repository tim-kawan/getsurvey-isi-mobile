import 'package:get_survey_app/features/voting/data/data_sources/remotes/entities/kandidat_dto.dart';
import 'package:get_survey_app/features/voting/data/data_sources/remotes/entities/result_dto.dart';
import 'package:get_survey_app/features/voting/data/data_sources/remotes/entities/valid_voter.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/submit_kandidat.dart';

abstract class IVotingRemote {
  Future<List<KandidatDto>> fetchKandidat(String type);
  Future<bool> voteKandidat(KanidatParams kanidatParams);
  Future<ValidVoterDto> validateVoter(int userId);
  Future<VoteResultDto> getResultVote(int userId);
}
