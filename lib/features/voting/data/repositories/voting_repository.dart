import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/network/network_info.dart';
import 'package:get_survey_app/features/voting/data/repositories/data_sources/remotes/voting_remote.dart';
import 'package:get_survey_app/features/voting/domain/entities/kandidat_entity.dart';
import 'package:get_survey_app/features/voting/domain/entities/result_dto.dart';
import 'package:get_survey_app/features/voting/domain/entities/valid_voter_entity.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/repositories/voting_repository.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/submit_kandidat.dart';

class VotingRepository extends IVotingRepository {
  final NetworkInfo networkInfo;
  final IVotingRemote votingRemote;

  VotingRepository({this.networkInfo, this.votingRemote});

  @override
  Future<Either<Failure, List<KandidatEntity>>> fetchKandidat(
      String type) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await votingRemote.fetchKandidat(type);
        final data = result.map((kandidat) => kandidat.toEntity()).toList();
        return Right(data);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> voteKandidat(KanidatParams request) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await votingRemote.voteKandidat(request);
        return Right(result);
      } on ServerException catch (e) {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, ValidVoterEntity>> validateVoter(int userId) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await votingRemote.validateVoter(userId);
        return Right(result.toEntity());
      } on ServerException catch (e) {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure,VoteResultEntity>> getResultVote(int userId) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await votingRemote.getResultVote(userId);
        return Right(result.toEntity());
      } on ServerException catch (e) {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }
}
