import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/voting/domain/entities/result_dto.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/repositories/voting_repository.dart';

class GetVoteResult implements UseCase<VoteResultEntity, int> {
  final IVotingRepository repository;

  GetVoteResult(this.repository);

  @override
  Future<Either<Failure, VoteResultEntity>> call(int userId) async {
    return await repository.getResultVote(userId);
  }
}
