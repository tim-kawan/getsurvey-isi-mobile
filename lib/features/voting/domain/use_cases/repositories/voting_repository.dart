import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/features/voting/domain/entities/kandidat_entity.dart';
import 'package:get_survey_app/features/voting/domain/entities/result_dto.dart';
import 'package:get_survey_app/features/voting/domain/entities/valid_voter_entity.dart';

import '../submit_kandidat.dart';

abstract class IVotingRepository {
  Future<Either<Failure, List<KandidatEntity>>> fetchKandidat(String type);
  Future<Either<Failure, bool>> voteKandidat(KanidatParams request);
  Future<Either<Failure, ValidVoterEntity>>  validateVoter(int userId);
  Future<Either<Failure,VoteResultEntity>> getResultVote(int userId);
}
