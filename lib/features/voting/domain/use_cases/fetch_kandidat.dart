import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/voting/domain/entities/kandidat_entity.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/repositories/voting_repository.dart';

class FetchKandidat implements UseCase<List<KandidatEntity>, Params> {
  final IVotingRepository repository;

  FetchKandidat(this.repository);

  @override
  Future<Either<Failure, List<KandidatEntity>>> call(Params params) async {
    return await repository.fetchKandidat(params.type);
  }
}

class Params extends Equatable {
  final String type;

  Params(this.type);

  @override
  List<Object> get props => [];
}
