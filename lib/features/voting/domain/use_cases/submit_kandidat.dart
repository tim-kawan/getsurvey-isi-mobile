import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/repositories/voting_repository.dart';

class VoteKandidat implements UseCase<bool, KanidatParams> {
  final IVotingRepository repository;

  VoteKandidat(this.repository);

  @override
  Future<Either<Failure, bool>> call(KanidatParams params) async {
    return await repository.voteKandidat(params);
  }
}

class KanidatParams extends Equatable {
  final int ketua;
  final int wakil1;
  final int wakil2;
  final int wakil3;
  final userId;

  KanidatParams(this.ketua, this.wakil1, this.wakil2, this.wakil3, this.userId);

  @override
  List<Object> get props => [ketua, wakil1, wakil2, wakil3];
}
