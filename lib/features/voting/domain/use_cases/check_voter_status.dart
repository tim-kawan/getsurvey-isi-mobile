import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/voting/domain/entities/valid_voter_entity.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/repositories/voting_repository.dart';

class VoterStatus implements UseCase<ValidVoterEntity, int> {
  final IVotingRepository repository;

  VoterStatus(this.repository);

  @override
  Future<Either<Failure, ValidVoterEntity>> call(int userId) async {
    return await repository.validateVoter(userId);
  }
}
