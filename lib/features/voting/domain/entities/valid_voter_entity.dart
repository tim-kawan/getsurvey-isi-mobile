class ValidVoterEntity {
  final String title;
  final int timeToVote;
  final bool isValid;
  final bool hasVote;
  final bool hasOpen;
  final String startEnd;

  ValidVoterEntity({
    this.title,
    this.timeToVote,
    this.hasOpen,
    this.isValid,
    this.hasVote,
    this.startEnd,
  });
}
