import 'package:get_survey_app/features/voting/domain/entities/kandidat_entity.dart';

class VoteResultEntity {
  final KandidatEntity ketua;
  final List<KandidatEntity> wakilKetua;

  VoteResultEntity({this.ketua, this.wakilKetua});
}
