class KandidatEntity {
  final int id;
  final String name;
  final String location;
  final dynamic photo;
  final String videoUrl;
  final bool isSelected;
  final bool isLast;

  KandidatEntity(
      {this.id,
      this.name,
      this.location,
      this.photo,
      this.videoUrl,
      this.isSelected,
      this.isLast});
}

List<KandidatEntity> listLastKandidat = [
  KandidatEntity(isLast: true),
  KandidatEntity(isLast: true),
];
