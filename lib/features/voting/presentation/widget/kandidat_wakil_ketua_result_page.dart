import 'package:flutter/material.dart';
import 'package:get_survey_app/features/voting/domain/entities/kandidat_entity.dart';

import 'kandidat_wakil_ketua_item.dart';
import 'kandidat_wakil_ketua_result_item.dart';

class KandidatWakilKetuaResultPage extends StatelessWidget {
  final List<KandidatEntity> listKandidatEntity;

  const KandidatWakilKetuaResultPage(
      {Key key, @required this.listKandidatEntity})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Padding(
          padding:
              const EdgeInsets.only(left: 35, bottom: 0, right: 35, top: 10),
          child: Container(
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 20,
                  childAspectRatio: MediaQuery.of(context).size.width /
                      (MediaQuery.of(context).size.height / 1.4),
                  mainAxisSpacing: 0),
              itemCount:
                  listKandidatEntity != null ? listKandidatEntity.length : 0,
              itemBuilder: (context, int key) {
                return KandidatWakilKetuaResultItem(
                  index: key,
                  kandidatEntity: listKandidatEntity[key],
                );
              },
            ),
          ),
        )
      ],
    );
  }
}
