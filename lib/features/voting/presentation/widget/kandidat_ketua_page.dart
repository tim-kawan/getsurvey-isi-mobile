import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_survey_app/features/voting/domain/entities/kandidat_entity.dart';
import 'package:get_survey_app/features/voting/presentation/cubit/voting_cubit.dart';

import 'kandidat_ketua_item.dart';

class KandidatKetuaPage extends StatelessWidget {
  final List<KandidatEntity> listKandidatEntity;
  final int selectedKandidat;
  final bool hasScrollKetuaUntilBottom;

  const KandidatKetuaPage(
      {Key key,
      @required this.listKandidatEntity,
      this.selectedKandidat,
      this.hasScrollKetuaUntilBottom})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScrollController controller = ScrollController();
    void onScrollKandidat() {
      double maxScroll = controller.position.maxScrollExtent;
      double currentScroll = controller.position.pixels;
      if (!hasScrollKetuaUntilBottom &&
          (currentScroll.toInt() > (maxScroll.toInt() - 150))) {
        BlocProvider.of<VotingCubit>(context).hasScrollKetuaUntilBottom();
      }
    }

    controller.addListener(onScrollKandidat);
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 40, right: 40, top: 5),
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xff1E3F0F6),
            ),
            child: Row(
              children: [
                (hasScrollKetuaUntilBottom)
                    ? Icon(
                        Icons.check_circle_outline,
                        color: Colors.green,
                        size: 24.0,
                        semanticLabel:
                            'Text to announce in accessibility modes',
                      )
                    : Icon(
                        Icons.warning_amber_rounded,
                        color: Colors.red,
                        size: 24.0,
                        semanticLabel:
                            'Text to announce in accessibility modes',
                      ),
                SizedBox(width: 10),
                Expanded(
                  child: Text(
                    "Anda dapat memilih Calon Ketua Umum, setelah scroll kebawah hingga selesai",
                    style: TextStyle(
                        fontSize: 14,
                        color: hasScrollKetuaUntilBottom
                            ? Colors.green
                            : Colors.red),
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding:
              const EdgeInsets.only(left: 35, bottom: 0, right: 35, top: 75),
          child: Container(
            child: GridView.builder(
              controller: controller,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 10,
                  childAspectRatio: MediaQuery.of(context).size.width /
                      (MediaQuery.of(context).size.height / 1.4),
                  mainAxisSpacing: 0),
              itemCount:
                  listKandidatEntity != null ? listKandidatEntity.length : 0,
              itemBuilder: (context, int key) {
                return KandidatKetuaItem(
                  index: key,
                  kandidatEntity: listKandidatEntity[key],
                  selectedKandidat: selectedKandidat,
                  hasScrollKetuaUntilBottom: hasScrollKetuaUntilBottom,
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
