import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_survey_app/features/voting/presentation/const/colors.dart';
import 'package:get_survey_app/features/voting/presentation/pages/voting_result_screen.dart';

class HasVotingMenu extends StatelessWidget {
  const HasVotingMenu({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Container(
      width: 200.0,
      height: 80.0,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(blurRadius: 5.0, color: Colors.black12),
          ]),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 70.0,
            child: SvgPicture.asset(
              'assets/voting/ballot.svg',
              width: 38,
              height: 40,
            ),
          ),
          Container(
              width: 2,
              decoration: BoxDecoration(
                color: primaryLine,
              )),
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => VotingResultScreen()),
              );
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.6,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ConstrainedBox(
                      constraints: BoxConstraints(
                          maxWidth: screenSize.width * 0.7, maxHeight: 30),
                      child: Text(
                        'Pemilihan Calon Ketua dan Wakil Ketua',
                        style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.bold,
                          fontFamily: "BS",
                          letterSpacing: 0.6,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Text(
                          'Anda telah melakukan voting',
                          maxLines: 1,
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.green,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
