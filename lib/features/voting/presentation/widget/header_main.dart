import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Header extends StatelessWidget {
  final String title;
  final bool loading;

  const Header({Key key, this.title, this.loading}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: SvgPicture.asset(
            'assets/voting/bg_voting.svg',
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 25),
          child: Center(
            child: Container(
              child: Column(
                children: <Widget>[
                  Text(
                    "Voting",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(14.0),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          child: Center(
                            child: Text(
                              title,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 12,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          height: 57.0,
                          width: 190.0,
                          decoration: BoxDecoration(
                            color: const Color(0x007c94b6),
                            border: Border.all(
                              color: Colors.white,
                              width: 2,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Text(
                    "27 Oktober 2021",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 13,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
