import 'package:flutter/cupertino.dart';
import 'package:flutter_shimmer/flutter_shimmer.dart';

class Shimmer extends StatefulWidget {
  const Shimmer({Key key}) : super(key: key);

  @override
  _ShimmerState createState() => _ShimmerState();
}

class _ShimmerState extends State<Shimmer> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(children: [
          for (int i = 0; i < 10; i++)
            VideoShimmer(
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
            ),
        ]),
      ),
    );
  }
}
