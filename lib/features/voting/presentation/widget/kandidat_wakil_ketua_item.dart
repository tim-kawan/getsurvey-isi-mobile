import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/features/voting/domain/entities/kandidat_entity.dart';
import 'package:get_survey_app/features/voting/presentation/cubit/voting_cubit.dart';
import 'package:url_launcher/url_launcher.dart';

class KandidatWakilKetuaItem extends StatelessWidget {
  final int index;
  final KandidatEntity kandidatEntity;
  final List<int> selectedKandidat;
  final bool hasScrollWakilKetuaUntilBottom;

  KandidatWakilKetuaItem(
      {this.index,
      this.kandidatEntity,
      this.selectedKandidat,
      this.hasScrollWakilKetuaUntilBottom});

  @override
  Widget build(BuildContext context) {
    return (kandidatEntity.isLast)
        ? Container()
        : Container(
            child: Stack(
              children: [
                Positioned(
                  bottom: 10,
                  left: 3,
                  right: 3,
                  child: Container(
                    height: 110,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      ),
                      boxShadow: [
                        BoxShadow(
                            spreadRadius: 2,
                            blurRadius: 3,
                            color: Colors.grey[300])
                      ],
                      color: Colors.white,
                    ),
                  ),
                ),
                Positioned(
                    bottom: 37,
                    left: 0,
                    right: 0,
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                              kandidatEntity.name,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: primaryBlack,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          SizedBox(height: 5),
                          Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.location_on,
                                      color: primaryColor4,
                                      size: 12,
                                    ),
                                    SizedBox(width: 2),
                                    Container(
                                      width:
                                          MediaQuery.of(context).size.height /
                                              10,
                                      child: Text(
                                        kandidatEntity.location,
                                        style: TextStyle(
                                          color: primaryBlack,
                                          fontSize: 11,
                                        ),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Positioned(
                                right: 0,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      right: 8.0, left: 8.0),
                                  child: Container(
                                    child: InkWell(
                                      onTap: () {
                                        openYoutubeProfile(
                                            kandidatEntity.videoUrl);
                                      },
                                      child: Container(
                                        child: Row(
                                          children: [
                                            SvgPicture.asset(
                                              "assets/voting/youtube.svg",
                                              width: 18,
                                            ),
                                            SizedBox(
                                              width: 6,
                                            ),
                                            Text(
                                              "Lihat",
                                              style: TextStyle(
                                                color: primaryBlack,
                                                fontSize: 11,
                                              ),
                                              maxLines: 1,
                                              overflow: TextOverflow.clip,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )),
                Positioned(
                  left: 1,
                  right: 1,
                  bottom: 0,
                  child: InkWell(
                    onTap: () {
                      if (selectedKandidat.contains(kandidatEntity.id) &&
                          hasScrollWakilKetuaUntilBottom) {
                        BlocProvider.of<VotingCubit>(context)
                            .cancelSelectedKandidatWakilKetua(
                                kandidatEntity.id);
                      } else if (selectedKandidat.length < 3 &&
                          hasScrollWakilKetuaUntilBottom) {
                        BlocProvider.of<VotingCubit>(context)
                            .selectKandidatWakilKetua(kandidatEntity.id);
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(12),
                            bottomLeft: Radius.circular(12)),
                        color: (selectedKandidat.contains(kandidatEntity.id))
                            ? Color(0xffF36E6E)
                            : Color(0xff338B8E),
                      ),
                      height: 30,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                              (selectedKandidat.contains(kandidatEntity.id))
                                  ? "assets/voting/cancel.svg"
                                  : "assets/voting/select.svg",
                              height:
                                  (selectedKandidat.contains(kandidatEntity.id))
                                      ? 10
                                      : 20),
                          SizedBox(width: 5),
                          Text(
                              (selectedKandidat.contains(kandidatEntity.id))
                                  ? "Batal"
                                  : "Pilih",
                              style: TextStyle(color: Colors.white))
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 80,
                  left: 10,
                  right: 10,
                  child: Center(
                    child: CachedNetworkImage(
                      imageUrl: kandidatEntity.photo,
                      placeholder: (context, url) => Container(
                        child: Center(
                          child: Container(
                              width: 50,
                              height: 50,
                              child: CircularProgressIndicator()),
                        ),
                        width: 125.0,
                        height: 125.0,
                        decoration: BoxDecoration(
                          color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10.0),
                          border: Border.all(
                            color: Colors.white,
                            width: 2.0,
                          ),
                        ),
                      ),
                      // cacheManager: _cacheManager,
                      imageBuilder: (context, provider) => Container(
                        width: 125.0,
                        height: 125.0,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: provider,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                          border: Border.all(
                            color: Colors.white,
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
  }
}

void openYoutubeProfile(String videoUrl) async {
  var urllaunchable =
      await canLaunch(videoUrl); //canLaunch is from url_launcher package
  if (urllaunchable) {
    await launch(videoUrl); //launch is from url_launcher package to launch URL
  } else {
    print("URL can't be launched.");
  }
}
