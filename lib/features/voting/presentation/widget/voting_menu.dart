import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_countdown_timer/index.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_survey_app/features/voting/presentation/const/colors.dart';
import 'package:get_survey_app/features/voting/presentation/cubit/voting_cubit.dart';
import 'package:get_survey_app/features/voting/presentation/pages/kandidat_voting_screen.dart';

class VotingMenu extends StatefulWidget {
  final int timeToFinish;

  const VotingMenu({Key key, this.timeToFinish}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _VotingMenu(timeToFinish);
}

class _VotingMenu extends State<VotingMenu> {
  final int timeToFinish;

  _VotingMenu(this.timeToFinish);

  @override
  Widget build(BuildContext context) {
    int endTime =
        DateTime.now().millisecondsSinceEpoch + 1000 *  timeToFinish;
    Size screenSize = MediaQuery.of(context).size;
    return BlocBuilder<VotingCubit, VotingState>(
      builder: (context, state) {
        return Column(
          children: [
            Padding(
                padding: const EdgeInsets.only(left: 6.0, right: 6.0),
                child: Container(
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xff1E3F0F6),
                  ),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.warning_amber_rounded,
                          color: Colors.grey,
                          size: 24.0,
                          semanticLabel:
                              'Text to announce in accessibility modes',
                        ),
                        SizedBox(width: 10),
                        Expanded(
                            child: Text(
                                "Silakan memilih dengan seksama. Setelah Anda melakukan Voting, pilihan Anda tidak dapat dirubah.")),
                      ]),
                )),
            SizedBox(width: 200, height: 20),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => KandidatVotingScreen()),
                ).then((value) => (value)
                    ? context.read<VotingCubit>().reloadVote()
                    : () {});
              },
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(blurRadius: 5.0, color: Colors.black12),
                    ]),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 70.0,
                      child: SvgPicture.asset(
                        'assets/voting/ballot.svg',
                        width: 38,
                        height: 40,
                      ),
                    ),
                    Container(
                        width: 2,
                        height: 75,
                        decoration: BoxDecoration(
                          color: primaryLine,
                        )),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.6,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            ConstrainedBox(
                              constraints: BoxConstraints(
                                  maxWidth: screenSize.width * 0.7,
                                  maxHeight: 30),
                              child: Text(
                                'Pemilihan Calon Ketua dan Wakil Ketua',
                                style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "BS",
                                  letterSpacing: 0.6,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Waktu Tersisa',
                                  maxLines: 1,
                                  style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.black,
                                  ),
                                ),
                                CountdownTimer(
                                  endTime: endTime,
                                  textStyle: TextStyle(color: Colors.red),
                                  endWidget: Text("Selesai",
                                      style: TextStyle(color: Colors.red)),
                                  onEnd: () {
                                    context
                                        .read<VotingCubit>()
                                        .reloadVote();
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
