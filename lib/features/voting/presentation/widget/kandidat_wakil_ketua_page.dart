import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_survey_app/features/voting/domain/entities/kandidat_entity.dart';
import 'package:get_survey_app/features/voting/presentation/cubit/voting_cubit.dart';

import 'kandidat_wakil_ketua_item.dart';

class KandidatWakilKetuaPage extends StatelessWidget {
  final List<KandidatEntity> listKandidatEntity;
  final List<int> selectedKandidat;
  final bool hasScrollWakilKetuaUntilBottom;

  const KandidatWakilKetuaPage(
      {Key key,
      @required this.listKandidatEntity,
      this.selectedKandidat,
      this.hasScrollWakilKetuaUntilBottom})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScrollController _controller = ScrollController();
    void onScrollKandidat() {
      double maxScroll = _controller.position.maxScrollExtent;
      double currentScroll = _controller.position.pixels;
      if (!hasScrollWakilKetuaUntilBottom &&
          (currentScroll.toInt() > (maxScroll.toInt() - 150))) {
        BlocProvider.of<VotingCubit>(context)
            .hasScrollWakilKetuaUntilBottom();
      }
    }

    _controller.addListener(onScrollKandidat);
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 40, right: 40, top: 10),
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xff1E3F0F6),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                (hasScrollWakilKetuaUntilBottom)
                    ? Icon(
                        Icons.check_circle_outline,
                        color: Colors.green,
                        size: 24.0,
                        semanticLabel:
                            'Text to announce in accessibility modes',
                      )
                    : Icon(
                        Icons.warning_amber_rounded,
                        color: Colors.red,
                        size: 24.0,
                        semanticLabel:
                            'Text to announce in accessibility modes',
                      ),
                SizedBox(width: 10),
                Expanded(
                  child: Text(
                    "Anda dapat memilih Calon Wakil Ketua Umum, setelah scroll kebawah hingga selesai",
                    style: TextStyle(fontSize: 14, color: hasScrollWakilKetuaUntilBottom
                        ? Colors.green
                        : Colors.red),
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding:
              const EdgeInsets.only(left: 35, bottom: 0, right: 35, top: 75),
          child: Container(
            child: GridView.builder(
              controller: _controller,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 20,
                  childAspectRatio: MediaQuery.of(context).size.width /
                      (MediaQuery.of(context).size.height / 1.4),
                  mainAxisSpacing: 0),
              itemCount:
                  listKandidatEntity != null ? listKandidatEntity.length : 0,
              itemBuilder: (context, int key) {
                return KandidatWakilKetuaItem(
                    index: key,
                    kandidatEntity: listKandidatEntity[key],
                    selectedKandidat: selectedKandidat,
                    hasScrollWakilKetuaUntilBottom:
                        hasScrollWakilKetuaUntilBottom);
              },
            ),
          ),
        )
      ],
    );
  }
}
