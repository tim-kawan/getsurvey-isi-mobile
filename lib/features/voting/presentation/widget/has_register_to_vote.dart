import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_survey_app/features/voting/presentation/cubit/voting_cubit.dart';

class HasRegisterToVote extends StatefulWidget {
  final startEndVote;

  const HasRegisterToVote({Key key, this.startEndVote}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HasRegisterToVote(startEndVote);
}

class _HasRegisterToVote extends State<HasRegisterToVote> {
  final startEndVote;

  _HasRegisterToVote(this.startEndVote);

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return BlocBuilder<VotingCubit, VotingState>(
      builder: (context, state) {
        return Column(
          children: [
            Padding(
                padding: const EdgeInsets.only(left: 6.0, right: 6.0),
                child: Container(
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xff1E3F0F6),
                  ),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.info_outline_rounded,
                          color: Colors.blue,
                          size: 24.0,
                          semanticLabel:
                              'Text to announce in accessibility modes',
                        ),
                        SizedBox(width: 10),
                        Expanded(
                          child: Text(
                            "Selamat, Anda sudah masuk dalam peserta yang akan melakukan voting."
                            "Waktu voting akan dilakukan pada hari  ${startEndVote} WIB."
                            "\n\nTerima Kasih.",
                            style: TextStyle(color: Colors.blue),
                          ),
                        ),
                      ]),
                )),
            SizedBox(width: 200, height: 20),
          ],
        );
      },
    );
  }
}
