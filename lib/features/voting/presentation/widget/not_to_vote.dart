import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_survey_app/features/voting/presentation/cubit/voting_cubit.dart';

class NotToVote extends StatefulWidget {
  const NotToVote({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _NotToVote();
}

class _NotToVote extends State<NotToVote> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<VotingCubit, VotingState>(
      builder: (context, state) {
        return Column(
          children: [
            Padding(
                padding: const EdgeInsets.only(left: 6.0, right: 6.0),
                child: Container(
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xff1E3F0F6),
                  ),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.warning_amber_outlined,
                          color: Colors.red,
                          size: 24.0,
                          semanticLabel:
                              'Text to announce in accessibility modes',
                        ),
                        SizedBox(width: 10),
                        Expanded(
                          child: Text(
                            "Mohon maaf waktu voting sudah berakhir. Anda tidak tidak dapat melakukan voting.\n\nTerima Kasih.",
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                      ]),
                )),
            SizedBox(width: 200, height: 20),
          ],
        );
      },
    );
  }
}
