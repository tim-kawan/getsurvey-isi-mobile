import 'package:bloc/bloc.dart';
import 'package:get_survey_app/features/voting/domain/entities/kandidat_entity.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/check_voter_status.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/fetch_kandidat.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/get_vote_result.dart';
import 'package:get_survey_app/features/voting/domain/use_cases/submit_kandidat.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:meta/meta.dart';

part 'voting_state.dart';

class VotingCubit extends Cubit<VotingState> {
  final FetchKandidat fetchKandidatUseCase;
  final VoteKandidat submitKandidat;
  final VoterStatus voterStatus;
  final GetVoteResult getVoteResult;
  int page = 1;
  String keySearch = '';

  VotingCubit(this.fetchKandidatUseCase, this.submitKandidat, this.voterStatus,
      this.getVoteResult)
      : super(KandidatInitial());

  Future<void> selectKandidatKetua(int kandidatId) async {
    emit(SelectKandidatState(kandidatId));
  }

  Future<void> selectKandidatWakilKetua(int kandidatId) async {
    emit(SelectKandidatWakilKetuaState(kandidatId));
  }

  Future<void> cancelSelectedKandidatKetua() async {
    emit(CancelSelectedKandidatKetuaState());
  }

  Future<void> cancelSelectedKandidatWakilKetua(int kandidatId) async {
    emit(CancelKandidatWakilKetuaState(kandidatId));
  }

  Future<void> fetchKandidat() async {
    List<KandidatEntity> kandidatKetua = [];
    final listKandidatKetua = await fetchKandidatUseCase(Params("ketua"));
    listKandidatKetua.fold((failure) => emit(FailureFetchListState()),
        (result) {
      kandidatKetua = result + listLastKandidat;
    });
    final listKandidatWakil = await fetchKandidatUseCase(Params("wakil"));
    listKandidatWakil.fold((l) => emit(FailureFetchListState()), (result) {
      emit(FetchKandidatListState(
          listKetuaKandidatEntity: kandidatKetua,
          listWakilKandidatEntity: result + listLastKandidat));
    });
  }

  Future<void> fetchVoteResults() async {
    final userId = await Session.getValue(Session.USER_ID);
    final userName = await Session.getValue(Session.USER_NAME);
    final result = await getVoteResult(userId);
    result.fold((l) => emit(FailureFetchListState()), (result) {
      emit(FetchKandidatListState(
          listKetuaKandidatEntity: [result.ketua],
          listWakilKandidatEntity: result.wakilKetua,
          userName: userName));
    });
  }

  Future<void> submitVoting(
      int ketuaSelected, List<int> wakilKetuaSelected) async {
    final userId = await Session.getValue(Session.USER_ID);
    final kandidatParam = KanidatParams(ketuaSelected, wakilKetuaSelected[0],
        wakilKetuaSelected[1], wakilKetuaSelected[2], userId);
    emit(Loading());
    final result = await submitKandidat(kandidatParam);
    result.fold((l) => null, (result) {
      if (result) emit(VoteSuccess());
    });
  }

  Future<void> checkUserStatusVote() async {
    final userId = await Session.getValue(Session.USER_ID);
    final result = await voterStatus(userId);
    result.fold((failure) => emit(RegisterVoterStatus("")), (result) {
      if (result.timeToVote < 1 && result.hasVote == false) {
        emit(NotVoteState(result.title));
      } else if (result.isValid == false)
        emit(RegisterVoterStatus(result.title));
      else if (result.hasVote)
        emit(HasVoteStatus(result.title));
      else if (result.hasOpen)
        emit(HasVoteOpen(result.timeToVote, result.title));
      else if (result.isValid) emit(WaitToVote(result.startEnd, result.title));
    });
  }

  Future<void> checkUserStatusOnVote() async {
    final userId = await Session.getValue(Session.USER_ID);
    final result = await voterStatus(userId);
    result.fold((failure) => emit(RegisterVoterStatus("")), (result) {
      if (result.hasOpen) {
        emit(HasVoteOpen(result.timeToVote, result.title));
        fetchKandidat();
      } else if (result.isValid) emit(ValidVoterStatus());
    });
  }

  Future<void> reloadVote() async {
    emit(Loading());
    checkUserStatusVote();
  }

  Future<void> hasScrollKetuaUntilBottom() async {
    emit(HasScrollKetuaUntilBottom());
  }

  Future<void> hasScrollWakilKetuaUntilBottom() {
    emit(HasScrollWakilKetuaUntilBottom());
  }
}
