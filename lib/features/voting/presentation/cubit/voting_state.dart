part of 'voting_cubit.dart';

@immutable
abstract class VotingState {}

class KandidatInitial extends VotingState {}

class Loading extends VotingState {}

class FetchKandidatListState extends VotingState {
  final List<KandidatEntity> listKetuaKandidatEntity;
  final List<KandidatEntity> listWakilKandidatEntity;
  final String userName;

  FetchKandidatListState(
      {this.listKetuaKandidatEntity,
      this.listWakilKandidatEntity,
      this.userName = ""});
}

class FailureFetchListState extends VotingState {}

class SelectKandidatState extends VotingState {
  final int ketuaSelected;

  SelectKandidatState(this.ketuaSelected);
}

class SelectKandidatWakilKetuaState extends VotingState {
  final int wakilKetuaSelected;

  SelectKandidatWakilKetuaState(this.wakilKetuaSelected);
}

class CancelSelectedKandidatKetuaState extends VotingState {}

class CancelSelectedKandidatWakilKetuaState extends VotingState {}

class CancelKandidatWakilKetuaState extends VotingState {
  final int wakilKetuaSelected;

  CancelKandidatWakilKetuaState(this.wakilKetuaSelected);
}

class HasScrollKetuaUntilBottom extends VotingState {}

class HasScrollWakilKetuaUntilBottom extends VotingState {}

class VoteSuccess extends VotingState {}

class ValidVoterStatus extends VotingState {}

class VoterStatusState extends VotingState {
  final String title;

  VoterStatusState(this.title);
}

class RegisterVoterStatus extends VoterStatusState {
  final String title;

  RegisterVoterStatus(this.title) : super(title);
}

class WaitToVote extends VoterStatusState {
  final String startEndVoting;
  final String title;

  WaitToVote(this.startEndVoting, this.title) : super(title);
}

class HasVoteStatus extends VoterStatusState {
  final String title;

  HasVoteStatus(this.title) : super(title);
}

class NotVoteState extends VoterStatusState {
  final String title;

  NotVoteState(this.title) : super(title);
}

class HasVoteOpen extends VoterStatusState {
  final String title;
  final int timeToVote;

  HasVoteOpen(this.timeToVote, this.title) : super(title);
}
