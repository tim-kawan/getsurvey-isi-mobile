import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_shimmer/flutter_shimmer.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/voting/domain/entities/kandidat_entity.dart';
import 'package:get_survey_app/features/voting/presentation/cubit/voting_cubit.dart';
import 'package:get_survey_app/features/voting/presentation/widget/kandidat_ketua_page.dart';
import 'package:get_survey_app/features/voting/presentation/widget/kandidat_wakil_ketua_page.dart';
import 'package:get_survey_app/features/voting/presentation/widget/shimmer.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class KandidatVotingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _KandidatVotingScreenState();
}

class _KandidatVotingScreenState extends State<KandidatVotingScreen> {
  List<KandidatEntity> listKandidatEntity;
  List<KandidatEntity> listWakilKandidatEntity;
  int ketuaSelected;
  List<int> wakilKetuaSelected = [];
  bool hasScrollKetuaUntilBottom = true;
  bool hasScrollWakilKetuaUntilBottom = true;

  int totalKetua = 0;
  int totalWakil = 0;
  bool isAllKandidatSelected = false;

  @override
  Widget build(BuildContext context) {
    int endTime = 0;
    return BlocProvider(
      create: (_) => di<VotingCubit>(),
      child: Scaffold(
        appBar: AppBar(
            actions: <Widget>[],
            leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pop(context, true);
                }),
            centerTitle: false,
            backgroundColor: primaryColor,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Voting",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
                BlocBuilder<VotingCubit, VotingState>(
                  builder: (context, state) {
                    if (state is HasVoteOpen) {
                      endTime = DateTime.now().millisecondsSinceEpoch +
                          1000 * state.timeToVote;
                      return Text("");
                    } else if (endTime > 0) {
                      return CountdownTimer(
                        endTime: endTime,
                        textStyle: TextStyle(color: Colors.white, fontSize: 18),
                        endWidget: Text("Selesai",
                            style:
                                TextStyle(color: Colors.white, fontSize: 14)),
                        onEnd: () {
                          Navigator.pop(context, true);
                        },
                      );
                    } else
                      return Text("");
                  },
                ),
              ],
            )),
        body: Stack(
          children: [
            Column(
              children: <Widget>[
                Expanded(
                  child: DefaultTabController(
                      length: 2,
                      child: Container(
                        color: Colors.white10,
                        child: Column(
                          children: <Widget>[
                            TabBar(
                              labelColor: Colors.black,
                              labelStyle: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 12),
                              unselectedLabelColor: Colors.grey[400],
                              unselectedLabelStyle: TextStyle(
                                  fontWeight: FontWeight.normal, fontSize: 12),
                              indicatorSize: TabBarIndicatorSize.tab,
                              indicatorColor: Color(0xff04777F),
                              tabs: <Widget>[
                                Tab(
                                  icon: SvgPicture.asset(
                                      "assets/voting/ketua.svg",
                                      width: 30,
                                      height: 30),
                                  child: Text("Calon Ketua",
                                      style: TextStyle(fontSize: 16)),
                                ),
                                Tab(
                                  icon: SvgPicture.asset(
                                      "assets/voting/wakil.svg",
                                      width: 30,
                                      height: 30),
                                  child: Text("Calon Wakil Ketua",
                                      style: TextStyle(fontSize: 16)),
                                ),
                              ],
                            ),
                            BlocConsumer<VotingCubit, VotingState>(
                              listener: (context, state) {
                                observeState(state);
                              },
                              builder: (context, state) {
                                if (state is KandidatInitial) {
                                  loadKandidat(context);
                                }
                                return Expanded(
                                  child: TabBarView(
                                    children: <Widget>[
                                      _loadWidgetStateKetua(state),
                                      _loadWidgetStateWakilKetua(state)
                                    ],
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      )),
                ),
              ],
            ),
            Positioned(
              left: 10,
              right: 10,
              bottom: 20,
              child: Container(
                padding: EdgeInsets.only(right: 20, left: 20),
                height: 73,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(32),
                    boxShadow: [
                      BoxShadow(
                          spreadRadius: 2,
                          blurRadius: 5,
                          color: Colors.grey[400])
                    ]),
                child: BlocBuilder<VotingCubit, VotingState>(
                  builder: (context, state) {
                    if (!(state is Loading)) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Container(
                                child: Row(
                              children: [
                                SvgPicture.asset("assets/voting/vote.svg"),
                                SizedBox(
                                  width: 19,
                                ),
                                Expanded(
                                  child: Text(
                                    "Anda sudah memilih Calon: \nKetua ${totalKetua}/1, Wakil Ketua ($totalWakil/3)",
                                    style: TextStyle(fontSize: 14),
                                  ),
                                ),
                              ],
                            )),
                          ),
                          Container(
                            child: OutlinedButton(
                              onPressed: () {
                                if (isAllKandidatSelected) showAlert(context);
                              },
                              child: Text("Vote"),
                              style: ButtonStyle(
                                foregroundColor: (isAllKandidatSelected)
                                    ? MaterialStateProperty.all<Color>(
                                        Colors.white)
                                    : MaterialStateProperty.all<Color>(
                                        Colors.grey),
                                backgroundColor: (isAllKandidatSelected)
                                    ? MaterialStateProperty.all<Color>(
                                        Color(0xff36CFD5))
                                    : MaterialStateProperty.all<Color>(
                                        Colors.grey[300]),
                              ),
                            ),
                          ),
                        ],
                      );
                    } else
                      return ListTileShimmer(
                        isDisabledButton: true,
                        padding: EdgeInsets.only(left: 0, right: 0),
                      );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showAlert(BuildContext context) {
    Alert(
      context: context,
      image: Image.asset(
        "assets/alert/warning.png",
        color: Colors.yellow,
      ),
      type: AlertType.info,
      title: "Perhatian",
      desc:
          "Silakan memilih dengan seksama. Setelah Anda melakukan Voting, pilihan Anda tidak dapat dirubah!",
      closeIcon: Container(),
      onWillPopActive: true,
      buttons: [
        DialogButton(
          color: Colors.redAccent,
          child: Text(
            "Batal",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        ),
        DialogButton(
          child: Text(
            "Vote",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => submitVoting(context),
          width: 120,
        )
      ],
    ).show();
  }

  void loadKandidat(BuildContext context) async {
    final loginCubit = context.read<VotingCubit>();
    await loginCubit.checkUserStatusOnVote();
  }

  void submitVoting(BuildContext context) async {
    final loginCubit = context.read<VotingCubit>();
    Navigator.pop(context);
    await loginCubit.submitVoting(ketuaSelected, wakilKetuaSelected);
  }

  _loadWidgetStateKetua(state) {
    if (state is KandidatInitial || state is Loading || state is HasVoteOpen)
      return Shimmer();
    else
      return KandidatKetuaPage(
        listKandidatEntity: listKandidatEntity,
        selectedKandidat: ketuaSelected,
        hasScrollKetuaUntilBottom: hasScrollKetuaUntilBottom,
      );
  }

  _loadWidgetStateWakilKetua(VotingState state) {
    if (state is KandidatInitial || state is Loading || state is HasVoteOpen)
      return Shimmer();
    else
      return KandidatWakilKetuaPage(
        listKandidatEntity: listWakilKandidatEntity,
        selectedKandidat: wakilKetuaSelected,
        hasScrollWakilKetuaUntilBottom: hasScrollWakilKetuaUntilBottom,
      );
  }

  void observeState(VotingState state) {
    if (state is FetchKandidatListState) {
      listKandidatEntity = state.listKetuaKandidatEntity;
      listWakilKandidatEntity = state.listWakilKandidatEntity;
    } else if (state is HasScrollKetuaUntilBottom) {
      hasScrollKetuaUntilBottom = true;
    } else if (state is HasScrollWakilKetuaUntilBottom) {
      hasScrollWakilKetuaUntilBottom = true;
    } else if (state is SelectKandidatState) {
      ketuaSelected = state.ketuaSelected;
    } else if (state is CancelSelectedKandidatKetuaState) {
      ketuaSelected = null;
    } else if (state is SelectKandidatWakilKetuaState)
      wakilKetuaSelected.add(state.wakilKetuaSelected);
    else if (state is CancelKandidatWakilKetuaState)
      wakilKetuaSelected.remove(state.wakilKetuaSelected);
    else if (state is HasScrollKetuaUntilBottom)
      hasScrollKetuaUntilBottom = true;
    else if (state is VoteSuccess) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Anda berhasil melakukan voting"),
      ));
      Navigator.pop(context, true);
    }

    if (!(state is Loading)) {
      totalWakil = wakilKetuaSelected.length;
      totalKetua = (ketuaSelected != null) ? 1 : 0;
      isAllKandidatSelected = totalWakil > 2 && ketuaSelected != null;
    }
  }
}
