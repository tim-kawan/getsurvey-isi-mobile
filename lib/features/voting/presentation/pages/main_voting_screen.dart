import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_shimmer/flutter_shimmer.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/voting/presentation/cubit/voting_cubit.dart';
import 'package:get_survey_app/features/voting/presentation/widget/has_register_to_vote.dart';
import 'package:get_survey_app/features/voting/presentation/widget/has_vote_menu.dart';
import 'package:get_survey_app/features/voting/presentation/widget/not_to_vote.dart';
import 'package:get_survey_app/features/voting/presentation/widget/register_voting_menu.dart';
import 'package:get_survey_app/features/voting/presentation/widget/voting_menu.dart';

import '../widget/header_main.dart';

class MainVotingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MainVotingScreenState();
}

class _MainVotingScreenState extends State<MainVotingScreen> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocProvider(
        create: (_) => di<VotingCubit>(),
        child: ListView(
          children: <Widget>[
            BlocBuilder<VotingCubit, VotingState>(builder: (context, state) {
              return _buildHeader(state);
            }),
            Transform.translate(
              offset: Offset(0.0, -(height * 0.3 - height * 0.28)),
              child: Container(
                width: width,
                padding: EdgeInsets.only(top: 10),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20))),
                child: DefaultTabController(
                    length: 1,
                    child: Container(
                      height: 10,
                    )),
              ),
            ),
            SizedBox(height: 30),
            BlocBuilder<VotingCubit, VotingState>(
              builder: (context, state) {
                if (state is KandidatInitial) {
                  context.read<VotingCubit>().checkUserStatusVote();
                  return ProfileShimmer(isRectBox: true);
                } else if (state is Loading) {
                  return ProfileShimmer(isRectBox: true);
                } else if (state is HasVoteStatus) {
                  return Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 0, horizontal: 16),
                      child: HasVotingMenu());
                } else if (state is NotVoteState) {
                  return Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 0, horizontal: 16),
                      child: NotToVote());
                } else if (state is RegisterVoterStatus) {
                  return Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 0, horizontal: 16),
                      child: RegisterVotingMenu());
                } else if (state is WaitToVote) {
                  return Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 0, horizontal: 16),
                      child: HasRegisterToVote(
                          startEndVote: state.startEndVoting));
                } else if (state is HasVoteOpen) {
                  return Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 0, horizontal: 16),
                      child: VotingMenu(timeToFinish: state.timeToVote));
                } else
                  return Container();
              },
            ),
            SizedBox(height: 40),
          ],
        ),
      ),
    );
  }

  Widget _buildHeader(VotingState state) {
    if (state is VoterStatusState) {
      return Header(title: state.title.replaceAll("\\n", "\n"));
    } else
      return Header(
        title: "",
      );
  }
}
