import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/voting/domain/entities/kandidat_entity.dart';
import 'package:get_survey_app/features/voting/presentation/cubit/voting_cubit.dart';
import 'package:get_survey_app/features/voting/presentation/widget/kandidat_ketua_result_page.dart';
import 'package:get_survey_app/features/voting/presentation/widget/kandidat_wakil_ketua_result_page.dart';
import 'package:get_survey_app/features/voting/presentation/widget/shimmer.dart';

class VotingResultScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => Voting_ResultScreenState();
}

class Voting_ResultScreenState extends State<VotingResultScreen> {
  List<KandidatEntity> listKandidatEntity;
  List<KandidatEntity> listWakilKandidatEntity;
  String fullName = "...";

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => di<VotingCubit>(),
      child: Scaffold(
        appBar: AppBar(
            actions: <Widget>[],
            leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pop(context, true);
                }),
            centerTitle: true,
            backgroundColor: primaryColor,
            title: BlocBuilder<VotingCubit, VotingState>(
              builder: (context, state) {
                return Text(
                  "Pilihan ${(state is FetchKandidatListState) ? state.userName : ''}",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                );
              },
            )),
        body: Stack(
          children: [
            Column(
              children: <Widget>[
                Expanded(
                  child: DefaultTabController(
                      length: 2,
                      child: Container(
                        color: Colors.white10,
                        child: Column(
                          children: <Widget>[
                            TabBar(
                              labelColor: Colors.black,
                              labelStyle: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 12),
                              unselectedLabelColor: Colors.grey[400],
                              unselectedLabelStyle: TextStyle(
                                  fontWeight: FontWeight.normal, fontSize: 12),
                              indicatorSize: TabBarIndicatorSize.tab,
                              indicatorColor: Color(0xff04777F),
                              tabs: <Widget>[
                                Tab(
                                  icon: SvgPicture.asset(
                                      "assets/voting/ketua.svg",
                                      width: 30,
                                      height: 30),
                                  child: Text("Calon Ketua",
                                      style: TextStyle(fontSize: 16)),
                                ),
                                Tab(
                                  icon: SvgPicture.asset(
                                      "assets/voting/wakil.svg",
                                      width: 30,
                                      height: 30),
                                  child: Text("Calon Wakil Ketua",
                                      style: TextStyle(fontSize: 16)),
                                ),
                              ],
                            ),
                            BlocConsumer<VotingCubit, VotingState>(
                              listener: (context, state) {
                                observeState(state);
                              },
                              builder: (context, state) {
                                if (state is KandidatInitial) {
                                  context
                                      .read<VotingCubit>()
                                      .fetchVoteResults();
                                }
                                return Expanded(
                                  child: TabBarView(
                                    children: <Widget>[
                                      _loadWidgetStateKetua(state),
                                      _loadWidgetStateWakilKetua(state)
                                    ],
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      )),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _loadWidgetStateKetua(state) {
    if (state is KandidatInitial || state is Loading || state is HasVoteOpen)
      return Shimmer();
    else
      return KandidatKetuaResultPage(
        listKandidatEntity: listKandidatEntity,
      );
  }

  _loadWidgetStateWakilKetua(VotingState state) {
    if (state is KandidatInitial || state is Loading || state is HasVoteOpen)
      return Shimmer();
    else
      return KandidatWakilKetuaResultPage(
        listKandidatEntity: listWakilKandidatEntity,
      );
  }

  void observeState(VotingState state) {
    if (state is FetchKandidatListState) {
      listKandidatEntity = state.listKetuaKandidatEntity;
      listWakilKandidatEntity = state.listWakilKandidatEntity;
    }
  }
}
