import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/features/payment/domain/entities/virtual_account_entity.dart';
import 'package:get_survey_app/features/payment/domain/use_cases/generate_va.dart';

abstract class IKjsbPaymentRepository {
  Future<Either<Failure, bool>> payKjsb(String kjsbId);

  Future<Either<Failure, VirtualAccountEntity>> generateVa(
      GenerateVaParam param);

  Future<Either<Failure, String>> viewIncoice(String kjsbId);
}
