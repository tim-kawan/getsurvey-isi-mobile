import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/payment/domain/repositories/ikjsb_payment_repository.dart';

class PayKjsb extends UseCase<bool, String> {
  final IKjsbPaymentRepository kjsbPaymentRepository;

  PayKjsb({this.kjsbPaymentRepository});

  @override
  Future<Either<Failure, bool>> call(String kjsbId) async {
    return kjsbPaymentRepository.payKjsb(kjsbId);
  }
}
