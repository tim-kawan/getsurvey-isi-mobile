import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/payment/domain/entities/virtual_account_entity.dart';
import 'package:get_survey_app/features/payment/domain/repositories/ikjsb_payment_repository.dart';

class GenerateVa extends UseCase<VirtualAccountEntity, GenerateVaParam> {
  final IKjsbPaymentRepository kjsbPaymentRepository;

  GenerateVa({this.kjsbPaymentRepository});

  @override
  Future<Either<Failure, VirtualAccountEntity>> call(GenerateVaParam param) async {
    return kjsbPaymentRepository.generateVa(param);
  }
}

class GenerateVaParam {
  final int transactionId;
  final String bankCode;

  GenerateVaParam({this.transactionId, this.bankCode});
}
