import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/usecases/usecase.dart';
import 'package:get_survey_app/features/payment/domain/repositories/ikjsb_payment_repository.dart';

class ViewInvoice extends UseCase<String, String> {
  final IKjsbPaymentRepository kjsbPaymentRepository;

  ViewInvoice({this.kjsbPaymentRepository});

  @override
  Future<Either<Failure, String>> call(String kjsbId) async {
    return kjsbPaymentRepository.viewIncoice(kjsbId);
  }
}
