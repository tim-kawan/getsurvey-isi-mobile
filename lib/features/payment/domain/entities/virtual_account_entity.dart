class VirtualAccountEntity {
  VirtualAccountEntity({
    this.id,
    this.skbTransactionsId,
    this.type,
    this.gatwayCode,
    this.amount,
    this.status,
    this.statusVa,
    this.externalId,
    this.paymentId,
    this.paymentDate,
    this.expirationDate,
    this.bankCode,
    this.accountNumber,
    this.accountNumberLabel,
    this.createdAt,
    this.paymentDateLabel,
    this.updatedAt,
  });

  int id;
  int skbTransactionsId;
  String type;
  String gatwayCode;
  String amount;
  String status;
  String statusVa;
  String externalId;
  String paymentId;
  String paymentDate;
  String expirationDate;
  String bankCode;
  String accountNumber;
  String accountNumberLabel;
  String createdAt;
  String paymentDateLabel;
  String updatedAt;

  static String bankCodeIndex(int index) {
    final code = ['BNI', 'BCA', 'BRI', 'MANDIRI'];
    return code[index];
  }

  static const String PENDING = "PENDING";
  static const String ACTIVE = "ACTIVE";

  static const PAYMENT_PENDING = "PENDING";
  static const PAYMENT_PAID = "PAID";

  String statusLabel() {
    if (this.status == PAYMENT_PENDING)
      return "Menunggu Pembayaran";
    else if (this.status == PAYMENT_PAID)
      return "Sudah Terbayar";
    else
      return "Tidak Diketahui";
  }
}
