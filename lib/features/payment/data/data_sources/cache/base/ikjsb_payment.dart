abstract class IKjsbPayment {
  Future<bool> payKjsb(String kjsbId);
}
