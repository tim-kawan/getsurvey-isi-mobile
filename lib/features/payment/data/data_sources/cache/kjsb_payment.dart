import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/features/kjsb/data/data_sources/cache/db/kjsb_provider.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:sqflite/sqflite.dart';

import 'base/ikjsb_payment.dart';

class KjsbPayment extends IKjsbPayment {
  final Future<Database> database;

  KjsbPayment({this.database});

  @override
  Future<bool> payKjsb(String kjsbId) async {
    try {
      final db = await database;
      String whereString = '${KjsbProvider.COLUMN_ID} = ?';
      List<dynamic> whereArguments = [kjsbId];
      await db.update(
          KjsbProvider.TABLE_KJSB,
          {
            KjsbProvider.COLUMN_STATUS: KjsbEntity.hasPaid,
          },
          where: whereString,
          whereArgs: whereArguments);
      return true;
    } catch (Exception) {
      CacheException();
    }
  }
}
