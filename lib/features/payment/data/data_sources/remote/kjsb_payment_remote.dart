import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/features/payment/data/data_sources/remote/base/kjsb_payment_remote.dart';
import 'package:get_survey_app/features/payment/data/data_sources/remote/dto/virtual_account_dto.dart';
import 'package:get_survey_app/features/payment/domain/use_cases/generate_va.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

class KjsbPaymentRemote extends IKjsbPaymentRemote {
  final http.Client client;

  KjsbPaymentRemote(this.client);

  @override
  Future<VirtualAccountDto> generateVa(GenerateVaParam param) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.post(
        Uri.parse("${RestAPI.API_URL}/skb/payment/create-va"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authToken
        },
        body: json.encode({
          "skb_transactions_id": param.transactionId,
          "type": "va",
          "bank_code": param.bankCode
        }));
    if (response.statusCode == 200) {
      return VirtualAccountDto.fromJson(json.decode(response.body)['data']);
    } else {
      ServerException(response.body.toString(), response.statusCode);
    }
  }

  @override
  Future<String> viewIncoice(String kjsbId) async {
    final authToken = await Session.getValue(Session.AUTH_TOKEN);
    var response = await client.get(
        Uri.parse("${RestAPI.API_URL}/skb/submission/invoice/$kjsbId"),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authToken
        });
    if (response.statusCode == 200) {
      var url = json.decode(response.body)['data']['link'];
      final filename = url.substring(url.lastIndexOf("/") + 1);
      String dir = (await getApplicationDocumentsDirectory()).path;
      File file = new File('$dir/$filename');
      if (await file.exists()) {
        return file.path;
      } else {
        var request = await HttpClient().getUrl(Uri.parse(url));
        var response = await request.close();
        var bytes = await consolidateHttpClientResponseBytes(response);
        await file.writeAsBytes(bytes);
        return file.path;
      }
    } else {
      ServerException(response.body.toString(), response.statusCode);
    }
  }
}
