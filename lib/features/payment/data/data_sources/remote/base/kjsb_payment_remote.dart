import 'package:get_survey_app/features/payment/data/data_sources/remote/dto/virtual_account_dto.dart';
import 'package:get_survey_app/features/payment/domain/use_cases/generate_va.dart';

abstract class IKjsbPaymentRemote {
  Future<VirtualAccountDto> generateVa(GenerateVaParam param);
  Future<String> viewIncoice(String kjsbId);
}
