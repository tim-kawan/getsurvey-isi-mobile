import 'package:get_survey_app/features/payment/domain/entities/virtual_account_entity.dart';
import 'package:intl/intl.dart';

class VirtualAccountDto {
  VirtualAccountDto({
    this.id,
    this.skbTransactionsId,
    this.type,
    this.gatwayCode,
    this.amount,
    this.status,
    this.statusVa,
    this.externalId,
    this.paymentId,
    this.paymentDate,
    this.expirationDate,
    this.bankCode,
    this.accountNumber,
    this.createdAt,
    this.updatedAt,
  });

  VirtualAccountDto.fromJson(dynamic json) {
    id = json['id'];
    skbTransactionsId = json['skb_transactions_id'];
    type = json['type'];
    gatwayCode = json['gatway_code'];
    amount = json['amount'];
    status = json['status'];
    statusVa = json['status_va'];
    externalId = json['external_id'];
    paymentId = json['payment_id'];
    paymentDate = json['payment_date'];
    expirationDate = json['expiration_date'];
    bankCode = json['bank_code'];
    accountNumber = json['account_number'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  int id;
  int skbTransactionsId;
  String type;
  String gatwayCode;
  String amount;
  String status;
  String statusVa;
  String externalId;
  dynamic paymentId;
  String paymentDate;
  String expirationDate;
  String bankCode;
  String accountNumber;
  String createdAt;
  String updatedAt;

  VirtualAccountEntity toEntity() => VirtualAccountEntity(
      id: id,
      status: status,
      amount: amount,
      paymentDateLabel:
          (paymentDate != null) ? formatDate(strToDate(paymentDate)) : "",
      accountNumber: accountNumber,
      accountNumberLabel: splitAccountNumber());

  String splitAccountNumber() {
    int index = 0;
    String accountNumberLabel = "";
    for (int i = 4; i < accountNumber.length; i += 4) {
      accountNumberLabel = accountNumberLabel +
          ((i == 4) ? "" : "-") +
          accountNumber.substring(index, i);
      index = i;
    }
    if (index < accountNumber.length)
      accountNumberLabel = accountNumberLabel +
          "-" +
          accountNumber.substring(index, accountNumber.length);
    return accountNumberLabel;
  }

  DateTime strToDate(String date) => DateTime.parse(date);

  String formatDate(DateTime date) {
    return DateFormat("dd MMM yyyy").format(date);
  }
}
