import 'package:dartz/dartz.dart';
import 'package:get_survey_app/core/error/exceptions.dart';
import 'package:get_survey_app/core/error/failures.dart';
import 'package:get_survey_app/core/network/network_info.dart';
import 'package:get_survey_app/features/payment/data/data_sources/cache/base/ikjsb_payment.dart';
import 'package:get_survey_app/features/payment/data/data_sources/remote/base/kjsb_payment_remote.dart';
import 'package:get_survey_app/features/payment/domain/entities/virtual_account_entity.dart';
import 'package:get_survey_app/features/payment/domain/repositories/ikjsb_payment_repository.dart';
import 'package:get_survey_app/features/payment/domain/use_cases/generate_va.dart';

class KjsbPaymentRepository extends IKjsbPaymentRepository {
  final NetworkInfo networkInfo;
  final IKjsbPayment kjsbPaymentCache;
  final IKjsbPaymentRemote kjsbPaymentRemote;

  KjsbPaymentRepository(
      {this.networkInfo, this.kjsbPaymentCache, this.kjsbPaymentRemote});

  @override
  Future<Either<Failure, bool>> payKjsb(String kjsbId) async {
    try {
      return Right(await kjsbPaymentCache.payKjsb(kjsbId));
    } on CacheException {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, VirtualAccountEntity>> generateVa(GenerateVaParam param) async {
    if (await networkInfo.isConnected) {
      final remoteResult = await kjsbPaymentRemote.generateVa(param);
      return Right(remoteResult.toEntity());
    } else
      return Left(ServerFailure());
  }

  @override
  Future<Either<Failure, String>> viewIncoice(String kjsbId) async{
    if (await networkInfo.isConnected) {
      final remoteResult = await kjsbPaymentRemote.viewIncoice(kjsbId);
      return Right(remoteResult);
    } else
      return Left(ServerFailure());
  }


}
