import 'package:get_it/get_it.dart';
import 'package:get_survey_app/features/payment/data/data_sources/cache/base/ikjsb_payment.dart';
import 'package:get_survey_app/features/payment/data/data_sources/cache/kjsb_payment.dart';
import 'package:get_survey_app/features/payment/data/data_sources/remote/base/kjsb_payment_remote.dart';
import 'package:get_survey_app/features/payment/data/data_sources/remote/kjsb_payment_remote.dart';
import 'package:get_survey_app/features/payment/data/repositories/kjsb_payment_repository.dart';
import 'package:get_survey_app/features/payment/domain/repositories/ikjsb_payment_repository.dart';
import 'package:get_survey_app/features/payment/domain/use_cases/generate_va.dart';
import 'package:get_survey_app/features/payment/domain/use_cases/pay_kjsb.dart';
import 'package:get_survey_app/features/payment/domain/use_cases/view_invoice.dart';
import 'package:get_survey_app/features/payment/presentation/manager/kjsb_payment_cubit.dart';

Future<void> initKjsbPayment(GetIt di) async {
  // Bloc
  di.registerFactory(() => KjsbPaymentCubit(
      payKjsb: di(), generateVa: di(), getKjsbById: di(), viewIncoice: di()));

  // Use cases
  di.registerLazySingleton(() => PayKjsb(kjsbPaymentRepository: di()));
  di.registerLazySingleton(() => GenerateVa(kjsbPaymentRepository: di()));
  di.registerLazySingleton(() => ViewInvoice(kjsbPaymentRepository: di()));

  di.registerLazySingleton<IKjsbPaymentRepository>(
    () => KjsbPaymentRepository(
        networkInfo: di(), kjsbPaymentCache: di(), kjsbPaymentRemote: di()),
  );

  //remote
  di.registerLazySingleton<IKjsbPaymentRemote>(() => KjsbPaymentRemote(di()));

  /// cache
  di.registerLazySingleton<IKjsbPayment>(() => KjsbPayment(database: di()));
}
