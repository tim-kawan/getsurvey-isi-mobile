import 'package:bloc/bloc.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_schedule_log_dto.dart';
import 'package:get_survey_app/features/kjsb/domain/use_cases/get_kjsb_by_id.dart';
import 'package:get_survey_app/features/payment/domain/entities/virtual_account_entity.dart';
import 'package:get_survey_app/features/payment/domain/use_cases/generate_va.dart';
import 'package:get_survey_app/features/payment/domain/use_cases/pay_kjsb.dart';
import 'package:get_survey_app/features/payment/domain/use_cases/view_invoice.dart';
import 'package:meta/meta.dart';

part 'kjsb_payment_state.dart';

class KjsbPaymentCubit extends Cubit<KjsbPaymentState> {
  final PayKjsb payKjsb;
  final GenerateVa generateVa;
  final GetKJSBById getKjsbById;
  final ViewInvoice viewIncoice;
  int bankIndex = -1;

  KjsbPaymentCubit(
      {this.payKjsb, this.generateVa, this.viewIncoice, this.getKjsbById})
      : super(KjsbPaymentInitial());

  Future<void> payKjsbByUser(kjsbId) async {
    final result = await payKjsb(kjsbId);
    result.fold((l) => null, (r) => emit(KjsbPaymentSuccessState()));
  }

  Future<void> generateVaEvent(int transactionId) async {
    emit(LoadingState());
    final param = GenerateVaParam(
        bankCode: VirtualAccountEntity.bankCodeIndex(bankIndex),
        transactionId: transactionId);
    final result = await generateVa(param);
    result.fold((failure) => emit(GenerateVaFailState()),
        (vaEntity) => emit(GenerateVaSuccessState(vaEntity)));
  }

  void selectVirtualAccount(int selectedIndex) {
    bankIndex = selectedIndex;
    emit(SelectedPaymentMethodState(selectedIndex, -1));
  }

  void selectBankAccount(int selectedIndex) {
    emit(SelectedPaymentMethodState(-1, selectedIndex));
  }

  void selectDescriptionPayment() {
    emit(SelectDescriptionPaymentState());
  }

  Future<void> getKjsbByIdEvent(String id) async {
    emit(LoadingState());
    var result = await getKjsbById(id);
    result.fold((l) => null, (detailEntity) {
      emit(KjsbPaymentTimeState(getTimeTopay(detailEntity.scheduledLogs)));
    });
  }

  Future<void> getStatusPayment(String id) async {
    emit(LoadingState());
    var result = await getKjsbById(id);
    result.fold((l) => null, (detailEntity) {
      emit(KjsbPaymentStatusState(
          status: detailEntity.virtualAccount.status,
          statusLabel: detailEntity.virtualAccount.statusLabel()));
    });
  }

  int getTimeTopay(List<KjsbScheduleLogEntity> scheduledLogs) {
    int timeToPay = 0;
    scheduledLogs.forEach((element) {
      if (element.type == KjsbEntity.paymentGatewayCreated) {
        timeToPay = element.milisecond;
      }
    });
    return timeToPay;
  }

  Future<void> viewIncoiceEvent(String kjsbId) async {
    emit(LoadingState());
    var result = await viewIncoice(kjsbId);
    result.fold((l) => null, (filePath) {
      emit(ViewInvoiceState(filePath));
    });
  }
}
