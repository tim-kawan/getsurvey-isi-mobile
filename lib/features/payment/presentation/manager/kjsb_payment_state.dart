part of 'kjsb_payment_cubit.dart';

@immutable
abstract class KjsbPaymentState {}

class KjsbPaymentInitial extends KjsbPaymentState {}

class SelectedPaymentMethodState extends KjsbPaymentState {
  final int virtualAccountIndex;
  final int manuanlBankAccountIndex;

  SelectedPaymentMethodState(
      this.virtualAccountIndex, this.manuanlBankAccountIndex);
}

class KjsbPaymentSuccessState extends KjsbPaymentState {}

class KjsbPaymentTimeState extends KjsbPaymentState {
  final int remainingTimeToPay;

  KjsbPaymentTimeState(this.remainingTimeToPay);
}

class KjsbPaymentStatusState extends KjsbPaymentState {
  final String statusLabel;
  final String status;

  KjsbPaymentStatusState({this.statusLabel, this.status});
}

class SelectDescriptionPaymentState extends KjsbPaymentState {}

class GenerateVaSuccessState extends KjsbPaymentState {
  final VirtualAccountEntity vaEntity;

  GenerateVaSuccessState(this.vaEntity);
}

class GenerateVaFailState extends KjsbPaymentState {}

class LoadingState extends KjsbPaymentState {}

class UnLoadingState extends KjsbPaymentState {}

class ViewInvoiceState extends KjsbPaymentState {
  final String filePath;

  ViewInvoiceState(this.filePath);
}
