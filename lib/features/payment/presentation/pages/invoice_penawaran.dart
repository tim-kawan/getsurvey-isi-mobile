import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/transaction_kjsb_entity.dart';
import 'package:get_survey_app/features/payment/presentation/pages/payment_screen_main.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Invoice extends StatefulWidget {
  final KjsbEntity kjsb;
  final TransactionKjsbEntity transaction;

  const Invoice({Key key, this.kjsb, this.transaction}) : super(key: key);

  @override
  _InvoiceState createState() => _InvoiceState(kjsb, transaction);
}

class _InvoiceState extends State<Invoice> {
  bool aktifValue = false;
  final KjsbEntity kjsb;
  final TransactionKjsbEntity transaction;

  _InvoiceState(this.kjsb, this.transaction);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pop(context);
          },
          color: Colors.white,
        ),
        backgroundColor: primaryColor,
        title: Text(
          "Form Penawaran",
          style: TextStyle(
              color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SvgPicture.asset(
                    'assets/warning.svg',
                    height: 30,
                    width: 30,
                  ),
                  Text(
                    "Periksa Form Penawaran Sebelum \nMelanjutkan Pembayaran",
                    style: TextStyle(color: Color(0xffFF4848), fontSize: 16),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: 342.14,
                width: 225.5,
                child: WebView(
                  initialUrl:
                      'https://getsurvey.id/view/irk/invoice/8uLqFh5nGtF2wmw',
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Checkbox(
                    value: aktifValue,
                    onChanged: (bool value) {
                      setState(() {
                        aktifValue = value;
                      });
                    },
                  ),
                  Text(
                      "Saya telah membaca dan menyetujui \nPenawaran Kerja di atas"),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    notePenawaran(context, "1.",
                        "Getsurveyor akan menggunakan cakupan pekerjaan yang dicantumkan di penawaran kerja sebagai bukti bila terjadi pembatalan order."),
                    notePenawaran(context, "2.",
                        "Akun anda akan di banned bila anda menghilang dan tidak dapat dihubungi."),
                    notePenawaran(context, "3.",
                        "Getsurveyor dapat mengakses informasi yang dicantumkan di penawaran kerja bila di perlukan.")
                  ],
                ),
                decoration: BoxDecoration(
                    color: Color(0xffF5F5F5),
                    borderRadius: BorderRadius.circular(15)),
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: aktifValue
          ? InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => PaymentScreenMain(
                          kjsb: kjsb, transaction: transaction,
                        )));
              },
              child: Container(
                height: 50,
                decoration: BoxDecoration(
                    color: aktifValue ? primaryColor : primaryColor4),
                child: Center(
                  child: Text(
                    "Pembayaran",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
              ),
            )
          : Stack(
              children: [
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                      color: aktifValue ? primaryColor : primaryColor4),
                  child: Center(
                    child: Text(
                      "Pembayaran",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  color: Colors.white.withOpacity(0.8),
                )
              ],
            ),
    );
  }

  Widget notePenawaran(BuildContext context, String number, String desc) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 1,
            child: Text(number, style: TextStyle(fontSize: 12)),
          ),
          Expanded(
              flex: 9,
              child: Text(
                desc,
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 12),
              ))
        ],
      ),
    );
  }
}
