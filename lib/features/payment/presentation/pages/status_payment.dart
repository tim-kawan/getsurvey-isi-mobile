import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';

class StatusPayment extends StatefulWidget {
  @override
  _StatusPaymentState createState() => _StatusPaymentState();
}

class _StatusPaymentState extends State<StatusPayment> {
  bool statusPayment = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              Navigator.of(context).pop();
            },
            color: Colors.white,
          ),
          backgroundColor: primaryColor,
          title: Text(
            "Status payment",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 80,
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 3,
                        offset: Offset(0, 2), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Total Tagihan",
                                style: TextStyle(
                                    fontWeight: FontWeight.w400, fontSize: 16),
                              ),
                              Text(
                                "Rp.5.000.000",
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xffFF719C),
                                    fontSize: 16),
                              )
                            ],
                          ),
                        ),
                      ),
                      // Expanded(
                      //   flex: 2,
                      //   child: Container(
                      //     child: Column(
                      //       crossAxisAlignment: CrossAxisAlignment.end,
                      //       mainAxisAlignment: MainAxisAlignment.spaceAround,
                      //       children: [
                      //         Text(
                      //           "23:58",
                      //           style: TextStyle(
                      //               color: Color(0xffF41E4F),
                      //               fontWeight: FontWeight.w700),
                      //         ),
                      //         Text(
                      //           "Menunggu konfirmasi",
                      //           style: TextStyle(
                      //               color: Color(0xffFFBC50),
                      //               fontWeight: FontWeight.w300),
                      //         ),
                      //       ],
                      //     ),
                      //   ),
                      // )
                      Expanded(
                        flex: 2,
                        child: Container(
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              "Terbayar",
                              style: TextStyle(
                                  color: primaryColor,
                                  fontWeight: FontWeight.w700),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 3,
                        offset: Offset(0, 2), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Expanded(
                            flex: 2,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  "Rekening Pengirim",
                                  style: TextStyle(color: Colors.grey),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Text(
                                  "Detail Rekening",
                                  style: TextStyle(
                                      color: Colors.grey.withOpacity(0.5)),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Image.asset(
                                  'assets/bni.png',
                                  height: 15,
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Text(
                                  "12345678",
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Text(
                                  "Aufa Ahdi",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            color: Colors.grey.withOpacity(0.5),
                            height: 100,
                            width: 1,
                          ),
                          Expanded(
                            flex: 2,
                            child: Column(
                              children: [
                                Text(
                                  "Rekening Tujuan",
                                  style: TextStyle(color: Colors.grey),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Text(
                                  "Detail Rekening",
                                  style: TextStyle(
                                      color: Colors.grey.withOpacity(0.5)),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Image.asset(
                                  'assets/bni.png',
                                  height: 15,
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Text(
                                  "151 0001 972",
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Text(
                                  "Ikatan Surveyor Indonesia",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        color: Colors.grey.withOpacity(0.5),
                        height: 1,
                        width: MediaQuery.of(context).size.width,
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 30, vertical: 20),
                          child: Text(
                            "Transfer sebelum Kamis, 30 Sept 2021 Pukul 21:00",
                            style:
                                TextStyle(color: Colors.grey.withOpacity(0.5)),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  color: Colors.grey.withOpacity(0.5),
                  height: 1,
                  width: MediaQuery.of(context).size.width,
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "Unggah bukti pembayaran",
                  style: TextStyle(color: Colors.grey, fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 15,
                ),
                InkWell(
                  // onTap: _openFileExplorer,
                  child: Container(
                    padding: const EdgeInsets.all(10.0),
                    child:
                    // _fileName ==
                    //     '*Pilih Berkas Lisensi PDF, JPG/JPEG, PNG'
                    //     ?
                    Icon(
                      Icons.add,
                      color: Colors.grey,
                      size: 50,
                    ),
                    //     : Icon(
                    //   Icons.file_present,
                    //   color: Colors.red,
                    //   size: 70,
                    // ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border:
                        Border.all(color: Colors.grey, width: 1)),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "*Unggah bukti pembayaran untuk proses \n  verifikasi lebih cepat",
                  style: TextStyle(color: Colors.grey, fontSize: 12),
                ),
              ],
            ),
          ),
        ));
  }
}
