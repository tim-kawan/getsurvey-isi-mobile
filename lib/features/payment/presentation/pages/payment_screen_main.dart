import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/transaction_kjsb_entity.dart';
import 'package:get_survey_app/features/payment/presentation/manager/kjsb_payment_cubit.dart';
import 'package:get_survey_app/features/payment/presentation/pages/payment_screen_va.dart';
import 'package:loader_overlay/loader_overlay.dart';

class PaymentScreenMain extends StatefulWidget {
  final KjsbEntity kjsb;
  final TransactionKjsbEntity transaction;

  const PaymentScreenMain({Key key, this.kjsb, this.transaction})
      : super(key: key);

  @override
  _PaymentScreenMainState createState() =>
      _PaymentScreenMainState(kjsb, transaction);
}

class _PaymentScreenMainState extends State<PaymentScreenMain> {
  final KjsbEntity kjsb;
  final TransactionKjsbEntity transaction;

  int virtualAccountIndex = -1;
  int manualBankAccountIndex = -1;

  _PaymentScreenMainState(this.kjsb, this.transaction);

  List<String> _listBankName = [
    'BNI Virtual Account',
    'BCA Virtual Account',
    'Briva',
    'Mandiri Virtual Account',
  ];
  List<String> _listBank = [
    'assets/payment/bni.png',
    'assets/payment/bca.png',
    'assets/payment/bri.jpeg',
    'assets/payment/mandiri.jpg',
  ];

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => di<KjsbPaymentCubit>(),
        child: LoaderOverlay(
          useDefaultLoading: false,
          overlayWidget: Center(
            child: SpinKitCubeGrid(
              color: primaryColor,
              size: 50.0,
            ),
          ),
          child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () {
                  Navigator.of(context).pop();
                },
                color: Colors.white,
              ),
              backgroundColor: primaryColor,
              title: Text(
                "Pembayaran",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
            body: BlocConsumer<KjsbPaymentCubit, KjsbPaymentState>(
                listener: (context, state) {
                  if (state is LoadingState)
                    context.loaderOverlay.show();
                  else if (state is UnLoadingState ||
                      state is GenerateVaFailState)
                    context.loaderOverlay.hide();
                  if (state is GenerateVaSuccessState) {
                    context.loaderOverlay.hide();
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PaymentScreenVa(
                                  transaction: transaction,
                                  vaEntity: state.vaEntity,
                                  bankIndex: virtualAccountIndex,
                                  kjsbEntity: kjsb,
                                )));
                  }
                },
                builder: (context, state) =>
                    _selectPaymentMethod(context, state)),
            bottomNavigationBar:
                BlocBuilder<KjsbPaymentCubit, KjsbPaymentState>(
              builder: (context, state) {
                return InkWell(
                  onTap: () {
                    if (state is SelectedPaymentMethodState)
                      context
                          .read<KjsbPaymentCubit>()
                          .selectDescriptionPayment();
                    else if (state is SelectDescriptionPaymentState)
                      context
                          .read<KjsbPaymentCubit>()
                          .generateVaEvent(transaction.id);
                  },
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(color: primaryColor),
                    child: Center(
                      child: Text(
                        "Lanjutkan Transaksi",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ));
  }

  Widget _splitter() {
    return Container(
      height: 5,
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: Color(0xDE8E4E4),
            width: 1.0,
          ),
        ),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xD707070),
            Colors.white10,
          ],
        ),
      ),
    );
  }

  _createVirtualAccount(BuildContext context, int selectedIndex) {
    return Column(children: _itemVirtualAccount(context, selectedIndex));
  }

  _listBankAccount(BuildContext context, int selectedIndex) {
    return Column(children: _itemBankAccount(context, selectedIndex));
  }

  List<Widget> _itemVirtualAccount(BuildContext context, int selectedIndex) {
    List<Widget> list = [];
    _listBankName.asMap().forEach((index, element) {
      list.add(
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 7),
          child: InkWell(
            onTap: () {
              context.read<KjsbPaymentCubit>().selectVirtualAccount(index);
            },
            child: Container(
              child: Row(
                children: [
                  Icon(Icons.check_circle,
                      color: (index == selectedIndex)
                          ? primaryColor
                          : Colors.grey[200]),
                  SizedBox(width: 5),
                  Expanded(
                    child: Text(element),
                  ),
                  Image.asset(
                    _listBank[index],
                    height: 16,
                  ),
                  Icon(Icons.navigate_next_rounded),
                ],
              ),
            ),
          ),
        ),
      );
    });
    return list;
  }

  List<Widget> _itemBankAccount(BuildContext context, selectedIndex) {
    List<Widget> list = [];
    _listBankName.asMap().forEach((index, element) {
      list.add(
        InkWell(
          onTap: () {
            context.read<KjsbPaymentCubit>().selectBankAccount(index);
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 7),
            child: Container(
              child: Row(
                children: [
                  Icon(Icons.check_circle,
                      color: (index == selectedIndex)
                          ? primaryColor
                          : Colors.grey[200]),
                  SizedBox(width: 5),
                  Expanded(
                    child: Text(element),
                  ),
                  Image.asset(
                    _listBank[index],
                    height: 16,
                  ),
                  Icon(Icons.navigate_next_rounded),
                ],
              ),
            ),
          ),
        ),
      );
    });
    return list;
  }

  _header() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 15),
      child: Container(
        height: 80,
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 3,
              offset: Offset(0, 2), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          children: [
            Expanded(
              flex: 3,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Total Tagihan",
                      style:
                          TextStyle(fontWeight: FontWeight.w400, fontSize: 16),
                    ),
                    Text(
                      "Rp. ${transaction.totalPriceLabel}",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          color: Color(0xffFF719C),
                          fontSize: 16),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _paymentMethod() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: BlocBuilder<KjsbPaymentCubit, KjsbPaymentState>(
        builder: (context, state) {
          if (state is SelectedPaymentMethodState) {
            virtualAccountIndex = state.virtualAccountIndex;
            manualBankAccountIndex = state.manuanlBankAccountIndex;
          }

          return Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 3,
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Pilih Metode Pembayaran",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  _splitter(),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Transfer Virtual Account",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  _createVirtualAccount(context, virtualAccountIndex),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Transfer Bank Manual",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  _listBankAccount(context, manualBankAccountIndex)
                ],
              ));
        },
      ),
    );
  }

  _descriptionPaymentMethod(BuildContext context, index) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: BlocBuilder<KjsbPaymentCubit, KjsbPaymentState>(
        builder: (context, state) {
          if (state is SelectedPaymentMethodState) {
            virtualAccountIndex = state.virtualAccountIndex;
            manualBankAccountIndex = state.manuanlBankAccountIndex;
          }

          return Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 3,
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 24),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          _listBankName[index],
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Image.asset(
                          _listBank[index],
                          height: 16,
                        ),
                      ],
                    ),
                  ),
                  _splitter(),
                  SizedBox(height: 10),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 24),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 3.0),
                          child: Icon(
                            Icons.circle,
                            color: primaryColor,
                            size: 10,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: Text(
                                "Dapatkan kode pembayaran setelah menekan tombol \"Lanjutkan Transaksi\" "))
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 24),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 3.0),
                          child: Icon(
                            Icons.circle,
                            color: primaryColor,
                            size: 10,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: Text(
                                "Tidak disarankan bayar melalui bank lain agar transaksi dapat diproses tanpa kendala"))
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                ],
              ));
        },
      ),
    );
  }

  _selectPaymentMethod(BuildContext context, KjsbPaymentState state) {
    return Container(
        child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _header(),
          (state is SelectDescriptionPaymentState)
              ? Container()
              : Text(
                  "Metode Pembayaran",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
          (state is SelectDescriptionPaymentState)
              ? _descriptionPaymentMethod(context, virtualAccountIndex)
              : _paymentMethod()
        ],
      ),
    ));
  }
}
