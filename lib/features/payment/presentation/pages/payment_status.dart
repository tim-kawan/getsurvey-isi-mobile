import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/transaction_kjsb_entity.dart';
import 'package:get_survey_app/features/payment/domain/entities/virtual_account_entity.dart';
import 'package:get_survey_app/features/payment/presentation/manager/kjsb_payment_cubit.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:shimmer/shimmer.dart';

class PaymentStatus extends StatefulWidget {
  final int bankIndex;
  final TransactionKjsbEntity transaction;
  final KjsbEntity kjsbEntity;
  final VirtualAccountEntity vaEntity;

  const PaymentStatus(
      {Key key,
      this.transaction,
      this.vaEntity,
      this.bankIndex,
      this.kjsbEntity})
      : super(key: key);

  @override
  _PaymentStatusState createState() =>
      _PaymentStatusState(transaction, vaEntity, bankIndex, kjsbEntity);
}

class _PaymentStatusState extends State<PaymentStatus> {
  final TransactionKjsbEntity transaction;
  final VirtualAccountEntity vaEntity;
  final int bankIndex;
  final KjsbEntity kjsbEntity;

  int virtualAccountIndex = 1;
  int manualBankAccountIndex = 1;

  _PaymentStatusState(
      this.transaction, this.vaEntity, this.bankIndex, this.kjsbEntity);

  List<String> _listBankName = [
    'BNI Virtual Account',
    'BCA Virtual Account',
    'Briva',
    'Mandiri Virtual Account',
  ];
  List<String> _listBank = [
    'assets/payment/bni.png',
    'assets/payment/bca.png',
    'assets/payment/bri.jpeg',
    'assets/payment/mandiri.jpg',
  ];

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => di<KjsbPaymentCubit>(),
        child: LoaderOverlay(
          useDefaultLoading: false,
          overlayWidget: Center(
            child: SpinKitCubeGrid(
              color: primaryColor,
              size: 50.0,
            ),
          ),
          child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () {
                  Navigator.of(context).pop();
                },
                color: Colors.white,
              ),
              backgroundColor: primaryColor,
              title: Text(
                "Pembayaran",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
            body: BlocConsumer<KjsbPaymentCubit, KjsbPaymentState>(
                listener: (context, state) {
              if (state is KjsbPaymentInitial) {
                context
                    .read<KjsbPaymentCubit>()
                    .getStatusPayment(kjsbEntity.id);
              }
            }, builder: (context, state) {
              if (state is KjsbPaymentInitial) {
                context
                    .read<KjsbPaymentCubit>()
                    .getStatusPayment(kjsbEntity.id);
              }
              return _selectPaymentMethod(context, state);
            }),
          ),
        ));
  }

  Widget _splitter() {
    return Container(
      height: 5,
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: Color(0xDE8E4E4),
            width: 1.0,
          ),
        ),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xD707070),
            Colors.white10,
          ],
        ),
      ),
    );
  }

  _descriptionPaymentMethod(BuildContext context, index) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: BlocBuilder<KjsbPaymentCubit, KjsbPaymentState>(
        builder: (context, state) {
          return Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 3,
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  (state is KjsbPaymentStatusState)
                      ? _header(state.statusLabel)
                      : _header(""),
                  _splitter(),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 24),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          _listBankName[bankIndex],
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Image.asset(
                          _listBank[bankIndex],
                          height: 16,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    child: Text("Nomor Virtual Account",
                        style: TextStyle(fontSize: 10)),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("${vaEntity.accountNumberLabel}",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 24),
                    child: Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Total Pembayaran",
                                style:
                                    TextStyle(fontSize: 10, color: Colors.red)),
                            Text("Rp. ${transaction.totalPriceLabel}",
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            SizedBox(height: 10),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ));
        },
      ),
    );
  }

  _selectPaymentMethod(BuildContext context, KjsbPaymentState state) {
    return Container(
        child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [_descriptionPaymentMethod(context, virtualAccountIndex)],
      ),
    ));
  }

  _header(String statusLabel) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 24),
        child: Row(
          children: [
            Container(
              child: CachedNetworkImage(
                width: 50,
                imageUrl: kjsbEntity.iconUrl,
                placeholder: (context, url) => Container(
                  child: Center(
                    child: Container(
                        width: 50,
                        height: 50,
                        child: SpinKitCubeGrid(
                          color: primaryColor,
                          size: 40.0,
                        )),
                  ),
                  width: 50.0,
                  height: 50.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(
                      color: Colors.white,
                      width: 2.0,
                    ),
                  ),
                ),
                // cacheManager: _cacheManager,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(kjsbEntity.code),
                  Text(kjsbEntity.serviceName),
                  statusLabel.isNotEmpty
                      ? Text(
                          statusLabel,
                          style: TextStyle(
                              color: Colors.amber, fontWeight: FontWeight.bold),
                        )
                      : Container(
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[700],
                            highlightColor: Colors.grey[200],
                            child: Container(
                              decoration: BoxDecoration(
                                  color: backGround.withOpacity(0.1),
                                  borderRadius: BorderRadius.circular(16.0),
                                  boxShadow: [
                                    BoxShadow(
                                        color: backGround.withOpacity(0.1),
                                        blurRadius: 0.5,
                                        offset: Offset(0, 1))
                                  ]),
                              height: 20,
                            ),
                          ),
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
