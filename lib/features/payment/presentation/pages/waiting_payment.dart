import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/features/payment/presentation/pages/status_payment.dart';

class WaitingPayment extends StatefulWidget {
  @override
  _WaitingPaymentState createState() => _WaitingPaymentState();
}

class _WaitingPaymentState extends State<WaitingPayment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
          color: Colors.white,
        ),
        backgroundColor: primaryColor,
        title: Text(
          "Menunggu Pembayaran",
          style: TextStyle(
              color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          child: Column(
            children: [
              Container(
                height: 80,
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 3,
                      offset: Offset(0, 2), // changes position of shadow
                    ),
                  ],
                ),
                child: Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Transfer Sebelum",
                              style: TextStyle(
                                  fontWeight: FontWeight.w400, fontSize: 16),
                            ),
                            Text(
                              "Kamis, 30 Sept 2021(21:00)",
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffFF719C),
                                  fontSize: 16),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Center(
                        child: Container(
                          child: Text(
                            "23:58",
                            style: TextStyle(
                                color: primaryColor,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                child: Column(
                  children: [
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Transfer Bank",
                            style: TextStyle(fontWeight: FontWeight.w400),
                          ),
                          Image.asset(
                            "assets/bni.png",
                            height: 15,
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 1,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.grey,
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "No. Rekening",
                            style: TextStyle(color: Colors.grey),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "151 00001 972",
                            style: TextStyle(fontWeight: FontWeight.w700),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text("Ikatan Surveyor Indonesia"),
                          SizedBox(
                            height: 3,
                          ),
                          Container(
                            height: 1,
                            width: MediaQuery.of(context).size.width,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Total yang harus dibayar",
                            style: TextStyle(color: Colors.grey),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Rp. 5.000.568",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          Container(
                            height: 1,
                            width: MediaQuery.of(context).size.width,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Unggah bukti pembayaran",
                            style: TextStyle(color: Colors.grey),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          InkWell(
                            // onTap: _openFileExplorer,
                            child: Container(
                              padding: const EdgeInsets.all(10.0),
                              child:
                                  // _fileName ==
                                  //     '*Pilih Berkas Lisensi PDF, JPG/JPEG, PNG'
                                  //     ?
                                  Icon(
                                Icons.add,
                                color: Colors.grey,
                                size: 50,
                              ),
                              //     : Icon(
                              //   Icons.file_present,
                              //   color: Colors.red,
                              //   size: 70,
                              // ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border:
                                      Border.all(color: Colors.grey, width: 1)),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "*Unggah bukti pembayaran untuk proses \n  verifikasi lebih cepat",
                            style: TextStyle(color: Colors.grey),
                          ),
                          noteWidget(context,
                              'Transfer total pembayaran di ikuti dengan kode unik 3 digit terakhir'),
                          noteWidget(context,
                              'Tidak disarankan transfer melalui LLG/Kliring/SKBNI'),
                          noteWidget(context,
                              'Pembayaran dapat dilakukan melalui ATM/ Mobile Banking/ Internet Banking / SMS Banking / Bank Terdekat'),
                          noteWidget(context,
                              'Lakukan pembayaran sesuai dengan nomer rekening yang telah panda daftarkan sebelumnya'),
                          noteWidget(context,
                              'Pembayaran akan diverifikasi oleh Admin GetSurveyor. Verifikasi paling lambat 1 x 24 jam')
                        ],
                      ),
                    )
                  ],
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 3,
                      offset: Offset(0, 2), // changes position of shadow
                    ),
                  ],
                ),
              )
              // Align(
              //   alignment: Alignment.centerLeft,
              //   child: Text(
              //     "Metode Pembayaran",
              //     style: TextStyle(
              //         color: primaryColor,
              //         fontSize: 14,
              //         fontWeight: FontWeight.w500),
              //   ),
              // ),
              // SizedBox(
              //   height: 10,
              // ),
              // Container(
              //   padding: EdgeInsets.symmetric(horizontal: 10),
              //   child: DropdownButtonHideUnderline(
              //     child: DropdownButton(
              //       isExpanded: true,
              //       hint: Text('Pilih Metode Pembayaran'),
              //       // Not necessary for Option 1
              //       value: _selectedBank,
              //       onChanged: (newValue) {
              //         setState(() {
              //           _selectedBank = newValue;
              //         });
              //       },
              //       items: _listBank.map((category) {
              //         return DropdownMenuItem(
              //           child: new Text(category),
              //           value: category,
              //         );
              //       }).toList(),
              //     ),
              //   ),
              //   decoration: BoxDecoration(
              //       border: Border.all(width: 1, color: primaryColor),
              //       borderRadius: BorderRadius.circular(14)),
              // ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: InkWell(
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => StatusPayment()));
        },
        child: Container(
          height: 50,
          decoration: BoxDecoration(color: primaryColor),
          child: Center(
            child: Text(
              "Cek Status Pembayaran",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
          ),
        ),
      ),
    );
  }

  Widget noteWidget(BuildContext context, title) {
    return Container(
      height: 50,
      child: Row(
        children: [
          Expanded(
              flex: 1,
              child: Center(
                child: Container(
                  height: 10,
                  width: 10,
                  decoration: BoxDecoration(
                      color: primaryColor,
                      borderRadius: BorderRadius.circular(10)),
                ),
              )),
          Expanded(
              flex: 6,
              child: Container(
                child: Text(title),
              ))
        ],
      ),
    );
  }
}
