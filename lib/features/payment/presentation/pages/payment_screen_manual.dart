import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/payment/presentation/manager/kjsb_payment_cubit.dart';
import 'package:get_survey_app/features/payment/presentation/pages/waiting_payment.dart';

class PaymentScreen extends StatefulWidget {
  final String kjsbId;

  const PaymentScreen({Key key, this.kjsbId}) : super(key: key);

  @override
  _PaymentScreenState createState() => _PaymentScreenState(kjsbId);
}

class _PaymentScreenState extends State<PaymentScreen> {
  final String kjsbId;
  TextEditingController _noRekeningCtrl = TextEditingController();
  TextEditingController _namaPemilikRekening = TextEditingController();

  String _selectedBank;
  List<String> _listBank = [
    'BNI',
    'BCA',
    'MANDIRI',
    'BRI',
    'DANAMON',
    'PERMATA',
    'CIMB NIAGA',
  ];

  _PaymentScreenState(this.kjsbId);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => di<KjsbPaymentCubit>(),
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              Navigator.of(context).pop();
            },
            color: Colors.white,
          ),
          backgroundColor: primaryColor,
          title: Text(
            "Pembayaran",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ),
        body: SingleChildScrollView(
          child: BlocConsumer<KjsbPaymentCubit, KjsbPaymentState>(
            listener: (context, state) {
              if (state is KjsbPaymentSuccessState)
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => WaitingPayment()));
            },
            builder: (context, state) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              child: Column(
                children: [
                  Container(
                    height: 80,
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 1,
                          blurRadius: 3,
                          offset: Offset(0, 2), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 3,
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Total Tagihan",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 16),
                                ),
                                Text(
                                  "Rp.5.000.000",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      color: Color(0xffFF719C),
                                      fontSize: 16),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            child: Center(
                              child: Text(
                                "Detail",
                                style: TextStyle(
                                    color: primaryColor,
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 15, vertical: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Transfer Manual",
                                style: TextStyle(fontWeight: FontWeight.w400),
                              ),
                              Image.asset(
                                "assets/bni.png",
                                height: 15,
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: 1,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.grey,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 15, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("No. Rekening"),
                              TextFormField(
                                controller: _noRekeningCtrl,
                                decoration: InputDecoration(
                                    hintText: "Contoh : 12345678",
                                    hintStyle: TextStyle(
                                      fontSize: 15.0,
                                      fontFamily: 'Sans',
                                      letterSpacing: 0.3,
                                      color: Colors.grey,
                                    )),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text("Nama Pemilik Bank"),
                              TextFormField(
                                controller: _namaPemilikRekening,
                                decoration: InputDecoration(
                                    hintText: "Contoh : Tester",
                                    hintStyle: TextStyle(
                                      fontSize: 15.0,
                                      fontFamily: 'Sans',
                                      letterSpacing: 0.3,
                                      color: Colors.grey,
                                    )),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                height: 50,
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 1,
                                        child: Center(
                                          child: Container(
                                            height: 15,
                                            width: 15,
                                            decoration: BoxDecoration(
                                                color: primaryColor,
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                          ),
                                        )),
                                    Expanded(
                                        flex: 4,
                                        child: Container(
                                          child: Text(
                                              "Masukkan data diatas sesuai buku tabungan Anda"),
                                        ))
                                  ],
                                ),
                              ),
                              Container(
                                height: 50,
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 1,
                                        child: Center(
                                          child: Container(
                                            height: 15,
                                            width: 15,
                                            decoration: BoxDecoration(
                                                color: primaryColor,
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                          ),
                                        )),
                                    Expanded(
                                        flex: 4,
                                        child: Container(
                                          child: Text(
                                              'Cek kembali data diatas sebelum menekan "Lanjutkan Transaksi"'),
                                        ))
                                  ],
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 1,
                          blurRadius: 3,
                          offset: Offset(0, 2), // changes position of shadow
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: BlocBuilder<KjsbPaymentCubit, KjsbPaymentState>(
          builder: (context, state) {
            return InkWell(
              onTap: () {
                context.read<KjsbPaymentCubit>().payKjsbByUser(kjsbId);
              },
              child: Container(
                height: 50,
                decoration: BoxDecoration(color: primaryColor),
                child: Center(
                  child: Text(
                    "Lanjutkan Transaksi",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
