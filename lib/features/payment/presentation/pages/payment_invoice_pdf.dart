import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/payment/presentation/manager/kjsb_payment_cubit.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:pdf_viewer_2/pdfviewer_scaffold.dart';

class PaymentInvoicePdf extends StatelessWidget {
  final KjsbEntity kjsb;

  const PaymentInvoicePdf({Key key, this.kjsb}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => di<KjsbPaymentCubit>(),
        child: LoaderOverlay(
          useDefaultLoading: false,
          overlayWidget: Center(
            child: SpinKitCubeGrid(
              color: primaryColor,
              size: 50.0,
            ),
          ),
          child: BlocConsumer<KjsbPaymentCubit, KjsbPaymentState>(
              listener: (context, state) {},
              builder: (context, state) {
                if (state is KjsbPaymentInitial)
                  context.read<KjsbPaymentCubit>().viewIncoiceEvent(kjsb.id);
                return (state is ViewInvoiceState)
                    ? PDFScreen(state.filePath, kjsb.code)
                    : Scaffold(
                        appBar: AppBar(
                          title: Text(kjsb.code),
                        ),
                        body: Container(
                          child: Center(
                              child: Center(
                            child: SpinKitCubeGrid(
                              color: primaryColor,
                              size: 50.0,
                            ),
                          )),
                        ));
              }),
        ));
  }
}

class PDFScreen extends StatelessWidget {
  String pathPDF = "";
  String title = "";

  PDFScreen(this.pathPDF, this.title);

  @override
  Widget build(BuildContext context) {
    return PDFViewerScaffold(
        appBar: AppBar(
          title: Text(title),
          actions: <Widget>[],
        ),
        path: pathPDF);
  }
}
