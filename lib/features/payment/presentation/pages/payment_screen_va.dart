import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/core/di/injection_container.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/kjsb_entity.dart';
import 'package:get_survey_app/features/kjsb/domain/entities/transaction_kjsb_entity.dart';
import 'package:get_survey_app/features/payment/domain/entities/virtual_account_entity.dart';
import 'package:get_survey_app/features/payment/presentation/manager/kjsb_payment_cubit.dart';
import 'package:get_survey_app/features/payment/presentation/pages/payment_status.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sprintf/sprintf.dart';

class PaymentScreenVa extends StatefulWidget {
  final int bankIndex;
  final TransactionKjsbEntity transaction;
  final KjsbEntity kjsbEntity;
  final VirtualAccountEntity vaEntity;

  const PaymentScreenVa(
      {Key key,
      this.transaction,
      this.vaEntity,
      this.bankIndex,
      this.kjsbEntity})
      : super(key: key);

  @override
  _PaymentScreenVaState createState() =>
      _PaymentScreenVaState(transaction, vaEntity, bankIndex, kjsbEntity);
}

class _PaymentScreenVaState extends State<PaymentScreenVa> {
  final TransactionKjsbEntity transaction;
  final VirtualAccountEntity vaEntity;
  final int bankIndex;
  final KjsbEntity kjsbEntity;

  int virtualAccountIndex = 1;
  int manualBankAccountIndex = 1;

  _PaymentScreenVaState(
      this.transaction, this.vaEntity, this.bankIndex, this.kjsbEntity);

  List<String> _listBankName = [
    'BNI Virtual Account',
    'BCA Virtual Account',
    'Briva',
    'Mandiri Virtual Account',
  ];
  List<String> _listBank = [
    'assets/payment/bni.png',
    'assets/payment/bca.png',
    'assets/payment/bri.jpeg',
    'assets/payment/mandiri.jpg',
  ];

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => di<KjsbPaymentCubit>(),
        child: LoaderOverlay(
          useDefaultLoading: false,
          overlayWidget: Center(
            child: SpinKitCubeGrid(
              color: primaryColor,
              size: 50.0,
            ),
          ),
          child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () {
                  Navigator.of(context).pop();
                },
                color: Colors.white,
              ),
              backgroundColor: primaryColor,
              title: Text(
                "Pembayaran",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
            body: BlocConsumer<KjsbPaymentCubit, KjsbPaymentState>(
                listener: (context, state) {
              if (state is KjsbPaymentInitial) {
                context
                    .read<KjsbPaymentCubit>()
                    .getKjsbByIdEvent(kjsbEntity.id);
              }
            }, builder: (context, state) {
              if (state is KjsbPaymentInitial) {
                context
                    .read<KjsbPaymentCubit>()
                    .getKjsbByIdEvent(kjsbEntity.id);
              }
              return _selectPaymentMethod(context, state);
            }),
            bottomNavigationBar:
                BlocBuilder<KjsbPaymentCubit, KjsbPaymentState>(
              builder: (context, state) {
                return InkWell(
                  onTap: () {
                    Navigator.push(context,
                    MaterialPageRoute(
                      builder: (context) => PaymentStatus(
                        vaEntity: vaEntity,
                        bankIndex: bankIndex,
                        transaction: transaction,
                        kjsbEntity: kjsbEntity,
                      )
                    ));
                  },
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(color: primaryColor),
                    child: Center(
                      child: Text(
                        "Cek Status Pembayaran",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ));
  }

  Widget _splitter() {
    return Container(
      height: 5,
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: Color(0xDE8E4E4),
            width: 1.0,
          ),
        ),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xD707070),
            Colors.white10,
          ],
        ),
      ),
    );
  }

  _header(KjsbPaymentState state) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 15),
      child: Container(
        height: 80,
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 3,
              offset: Offset(0, 2), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          children: [
            Expanded(
              flex: 3,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Sisa Waktu Pembayaran",
                      style:
                          TextStyle(fontWeight: FontWeight.w400, fontSize: 16),
                    ),
                    (state is  KjsbPaymentTimeState) ? CountdownTimer(
                      widgetBuilder: (_, CurrentRemainingTime time) {
                        if (time == null) {
                          return Text('Waktu Pembayaran Habis',
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Color(0xffFF719C),
                                fontSize: 16),);
                        }
                        return Text(
                            (time.days != null)
                                ? sprintf(
                                "%01i Hari %02i Jam %02i menit %02i detik",
                                [
                                  time.days ?? 0,
                                  time.hours ?? 0,
                                  time.min ?? 0,
                                  time.sec ?? 0
                                ])
                                : sprintf("%02i Jam, %02i menit, %02i detik", [
                              time.hours ?? 0,
                              time.min ?? 0,
                              time.sec ?? 0
                            ]),
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Colors.red));
                      },
                      endTime: state.remainingTimeToPay,
                      textStyle: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff717171)),
                      endWidget: Text("Selesai",
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Color(0xff717171))),
                      onEnd: () {},
                    ) : Shimmer.fromColors(
                      baseColor: Colors.grey[700],
                      highlightColor: Colors.grey[200],
                      child: Container(
                        decoration: BoxDecoration(
                            color: backGround.withOpacity(0.1),
                            borderRadius: BorderRadius.circular(16.0),
                            boxShadow: [
                              BoxShadow(
                                  color: backGround.withOpacity(0.1),
                                  blurRadius: 0.5,
                                  offset: Offset(0, 1))
                            ]),
                        height: 20,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _descriptionPaymentMethod(BuildContext context, index) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: BlocBuilder<KjsbPaymentCubit, KjsbPaymentState>(
        builder: (context, state) {
          return Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 3,
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 24),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          _listBankName[bankIndex],
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Image.asset(
                          _listBank[bankIndex],
                          height: 16,
                        ),
                      ],
                    ),
                  ),
                  _splitter(),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 24),
                    child: Text("Nomor Virtual Account",
                        style: TextStyle(fontSize: 10)),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 24),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("${vaEntity.accountNumberLabel}",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        InkWell(
                          onTap: () {
                            Clipboard.setData(ClipboardData(text: vaEntity.accountNumber));
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text("Nomor Virtual Account telah dicopy"),
                            ));
                          },
                          child: Row(
                            children: [
                              Text("Salin",
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: primaryColor,
                                      fontWeight: FontWeight.bold)),
                              Icon(
                                Icons.content_copy,
                                color: primaryColor,
                                size: 15,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  _splitter(),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 24),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Total Pembayaran",
                            style: TextStyle(fontSize: 10)),
                        SizedBox(height: 10),
                        Text("Rp. ${transaction.totalPriceLabel}",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        SizedBox(height: 10),
                      ],
                    ),
                  ),
                ],
              ));
        },
      ),
    );
  }

  _selectPaymentMethod(BuildContext context, KjsbPaymentState state) {
    return Container(
        child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _header(state),
          _descriptionPaymentMethod(context, virtualAccountIndex)
        ],
      ),
    ));
  }
}
