import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_survey_app/chat/Chat.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/maintenance/resources/ApiProvider.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:indonesia/indonesia.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class DetailProductScreen extends StatefulWidget {
  final int id;
  final String tipe;
  final dynamic rate;

  DetailProductScreen({
    Key key,
    this.id,
    this.tipe,
    this.rate,
  }) : super(key: key);

  @override
  _DetailProductScreenState createState() => _DetailProductScreenState();
}

class _DetailProductScreenState extends State<DetailProductScreen> {
  static var _customTextStyle = TextStyle(
    color: Colors.black,
    fontFamily: "Gotik",
    fontSize: 17.0,
    fontWeight: FontWeight.w800,
  );

  /// Custom Text for Header title
  static var _subHeaderCustomStyle = TextStyle(
      color: Colors.black54,
      fontWeight: FontWeight.w700,
      fontFamily: "Gotik",
      fontSize: 16.0);

  /// Custom Text for Detail title
  static var _detailText = TextStyle(
      fontFamily: "Gotik",
      color: Colors.black54,
      letterSpacing: 0.3,
      wordSpacing: 0.5);

  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();

  String status;
  String rating = "";
  Position userLocation;
  String locationText = "Selamat datang...";
  String name = "";
  String price = "";
  String surveyor = "";
  String desc = "";
  dynamic imageItem;
  var item;
  bool _isLoading = false;
  double long;

  @override
  Widget build(BuildContext context) {
    var smallestDimension = MediaQuery.of(context).size.shortestSide;
    return Scaffold(
      key: _key,
      appBar: AppBar(
        elevation: 0.5,
        centerTitle: true,
        backgroundColor: primaryColor,
        leading: IconButton(
            color: Colors.white,
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text(
          "Produk Detail",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.white,
            fontSize: 17.0,
            fontFamily: "Gotik",
          ),
        ),
      ),
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        child: Column(
          children: <Widget>[
            Flexible(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    /// Header image slider
                    Container(
                      height:smallestDimension>600? 350:300,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                        image: imageItem,
                      )),
                    ),

                    Container(
                      decoration:
                          BoxDecoration(color: Colors.white, boxShadow: [
                        BoxShadow(
                          color: Color(0xFF656565).withOpacity(0.15),
                          blurRadius: 1.0,
                          spreadRadius: 0.2,
                        )
                      ]),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0, top: 10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              name,
                              style: _customTextStyle,
                            ),
                            Padding(padding: EdgeInsets.only(top: 5.0)),
                            Text(
                              price,
                              style: TextStyle(
                                color: Colors.red,
                                fontFamily: "Gotik",
                                fontSize: 16.0,
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(top: 10.0)),
                            Container(
                              width: MediaQuery.of(context).size.width / 5,
                              decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(15.0)),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      rating,
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      width: 3.0,
                                    ),
                                    Icon(
                                      Icons.star,
                                      color: Colors.white,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(top: 10.0)),
                            Divider(
                              color: Colors.black12,
                              height: 1.0,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 10.0, bottom: 10.0),
                              child: Padding(
                                padding: const EdgeInsets.only(right: 15.0),
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      "Surveyor  : ",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16.0,
                                      ),
                                    ),
                                    Text(
                                      surveyor.toUpperCase(),
                                      style: TextStyle(
                                        color: Colors.orange,
                                        fontSize: 16.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),

                    /// Background white for description
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration:
                            BoxDecoration(color: Colors.white, boxShadow: [
                          BoxShadow(
                            color: Color(0xFF656565).withOpacity(0.15),
                            blurRadius: 1.0,
                            spreadRadius: 0.2,
                          )
                        ]),
                        child: Padding(
                          padding: EdgeInsets.only(top: 20.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left: 20.0),
                                child: Text(
                                  "Keterangan",
                                  style: _subHeaderCustomStyle,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 15.0,
                                    right: 20.0,
                                    bottom: 10.0,
                                    left: 20.0),
                                child: Text(desc, style: _detailText),
                              ),
                              SizedBox(
                                height: 20.0,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: (status == "Surveyor")
          ? BottomAppBar()
          : BottomAppBar(
              child: Builder(
                builder: (context) => InkWell(
                  onTap: () => _doChat(),
                  child: Container(
                    height: 50.0,
                    decoration: BoxDecoration(
                      color: primaryColor,
                    ),
                    child: Center(
                      child: Text(
                        "Chat Penyedia Jasa",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: "Sans",
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget iconsRate(int rateProduk) {
    List<Widget> widget = [];
    for (int i = 1; i <= rateProduk; i++) {
      widget.add(Icon(
        Icons.star,
        color: Colors.green,
      ));
    }
    for (int i = 5 - rateProduk; i > 0; i--) {
      widget.add(Icon(
        Icons.star_border,
        color: Colors.grey[700],
      ));
    }
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: widget,
      ),
    );
  }

  @override
  void initState() {
    Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .asStream()
        .listen((data) {
      _loadPlace(data);
      userLocation = data;
      setState(() {});
    });
    super.initState();
    imageItem = AssetImage("assets/logo.png");
    _initData();
  }

  _doChat() async {
    int idUser = await Session.getValue(Session.USER_ID);

    if (idUser == null) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => BtnNavigation(
            atIndex: 2,
          ),
        ),
      );
    } else {
      var room;
      var listRoom = await RestAPI.seeChatRoom(idUser);
      bool isNew = true;

      listRoom.forEach((element) async {
        if (element[widget.tipe == "item" ? "produk" : "jasa"] == widget.id &&
            element["status"] != "Agreed") {
          room = element;
          isNew = false;
        }
      });

      if (room == null) {
        if (widget.tipe == "item") {
          room = await RestAPI.bookRoom(idUser, produk: widget.id);
        } else {
          room = await RestAPI.bookRoom(idUser, jasa: widget.id);
        }

        await RestAPI.joinRoom(item['user_surveyor']["id"], room["kode"]);

        if (isNew) {
          await RestAPI.doChat(
            room["kode"],
            item['user_surveyor']["id"],
            "Hallo pengguna GetSurvey, silahkan mengisi formulir berikut terlebih dahulu: \nNama Lengkap : \nAsal : \nLokasi Bidang Dimana : \nUntuk Keperluan :  \n\nUntuk keperluan pengukuran IRK silahkan melalui menu Get informasi rencana kota di halaman awal aplikasi. \nTerima Kasih..",
            forMe: item['user_surveyor']["id"],
          );
        }
      }

      if (room != null) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ChatScreen(
              room: room,
              chatSistem: true,
            ),
          ),
        );
      }
    }
  }

  void _initData() async {
    setState(() => _isLoading = true);
    int statusSurveyor = await Session.getValue(Session.USER_LANJUT);

    item = widget.tipe == "item"
        ? await RestAPII.getProdukWithId(id: widget.id)
        : await RestAPII.getServiceWithId(id: widget.id);
    name = item['nama'];
    desc = item['deskripsi'];
    surveyor = item['user_surveyor']['user']['nama'];
    price = item['harga'] == null ? "" : rupiah(item['harga']);


      imageItem =NetworkImage(item["product_photos"][0]['foto']);

    if (widget.rate == null) {
      rating = item['rates'].toString().length == 1
          ? "${item['rates'].toString()}.0"
          : item['rates'].toString().substring(0, 3);
    } else {
      rating = widget.rate.toString().length == 1
          ? "${widget.rate.toString()}.0"
          : widget.rate.toString().substring(0, 3);
    }

    if (statusSurveyor == 1 || statusSurveyor == 2) {
      status = "Surveyor";
    } else {
      status = "User";
    }

    _isLoading = false;
    setState(() {});
  }

  _loadPlace(Position loc) async {
    int idUser = await Session.getValue(Session.USER_ID);
    Geolocator()
        .placemarkFromCoordinates(loc.latitude, loc.longitude)
        .asStream()
        .listen((result) {
      setState(() {
        if (result.isNotEmpty) {
          RestAPI.doLogHit(
              idUser.toString(),
              loc.longitude.toString(),
              loc.latitude.toString(),
              DateFormat("dd MMM yyyy HH:mm:ss").format(DateTime.now()),
              item['nama']);
          var lc = result.first;
          locationText =
              "${lc.subLocality}, ${lc.locality.isEmpty ? lc.administrativeArea : lc.locality}";
        } else {
          locationText = "Lokasi tidak ditemukan...";
        }
      });
    }).onError((error) {
      locationText = "Gagal mendapatkan lokasi....";
    });
  }
}
