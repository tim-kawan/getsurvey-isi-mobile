import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:get_survey_app/component/ImageUtils.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/model/ProductService.dart';
import 'package:get_survey_app/product/AddProduct.dart';
import 'package:get_survey_app/product/DetailProductScreen.dart';
import 'package:get_survey_app/product/EditProduct.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/ServiceGetData.dart';
import 'package:indonesia/indonesia.dart';
import 'package:shimmer/shimmer.dart';

class ListProductScreen extends StatefulWidget {
  final int id;
  final int addItem;
  final int editItem;

  ListProductScreen({this.addItem, this.id, this.editItem, Key key})
      : super(key: key);

  @override
  _ListProductScreenState createState() => _ListProductScreenState();
}

class _ListProductScreenState extends State<ListProductScreen> {
  bool _isLoading = true;
  List<ProductService> listProductByUser = [];
  List<ProductService> listServiceByUser = [];
  final DefaultCacheManager _cacheManager = new DefaultCacheManager();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[300],
        appBar: AppBar(
          backgroundColor: primaryColor,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              }),
          title: Text(
            widget.addItem == 1 ? "Daftar Jasa" : "Daftar Produk",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
        ),
        body: widget.editItem == 1
            ? Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: widget.addItem == 1
                    ? (_isLoading)
                        ? Shimmer.fromColors(
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                "Tunggu Sebentar...",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22.5,
                                ),
                              ),
                            ),
                            baseColor: Colors.grey[700],
                            highlightColor: Colors.grey[200],
                          )
                        : ListView.builder(
                            itemCount: listServiceByUser.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DetailProductScreen(
                                                  id: listServiceByUser[index]
                                                      .id,
                                                  tipe: "work",
                                                )));
                                  },
                                  child: Container(
                                      height: 100,
                                      width: MediaQuery.of(context).size.width /
                                          1.5,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10.0)),
                                      child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Center(
                                                child: CachedNetworkImage(
                                                  imageUrl:
                                                  listServiceByUser[index].productPhotos[0].foto,
                                                  placeholder: (context, url) =>
                                                      Container(
                                                    width: 70.0,
                                                    height: 70.0,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                        image:
                                                            ImageUtils.APP_LOGO,
                                                        fit: BoxFit.cover,
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              80.0),
                                                      border: Border.all(
                                                        color: Colors.white,
                                                        width: 2.0,
                                                      ),
                                                    ),
                                                  ),
                                                  cacheManager: _cacheManager,
                                                  imageBuilder:
                                                      (context, provider) =>
                                                          Container(
                                                    width: 70.0,
                                                    height: 70.0,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                        image: provider,
                                                        fit: BoxFit.cover,
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              80.0),
                                                      border: Border.all(
                                                        color: Colors.white,
                                                        width: 2.0,
                                                      ),
                                                    ),
                                                  ),
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          Container(
                                                    width: 70.0,
                                                    height: 70.0,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                        image: NetworkImage(
                                                            "${RestAPI.FOTO_URL}/pp_0.jpg"),
                                                        fit: BoxFit.cover,
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              80.0),
                                                      border: Border.all(
                                                        color: Colors.white,
                                                        width: 2.0,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Flexible(
                                                      child: Container(
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width /
                                                            1.5,
                                                        child: Text(
                                                          listServiceByUser[
                                                                  index]
                                                              .nama
                                                              .toUpperCase(),
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          maxLines: 2,
                                                          style: TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 20.0),
                                                      child: Center(
                                                        child: InkWell(
                                                          onTap: () {
                                                            Navigator.push(
                                                                context,
                                                                MaterialPageRoute(
                                                                    builder:
                                                                        (context) =>
                                                                            EditProductScreen(
                                                                              addItem: 1,
                                                                              nameProduct: listServiceByUser[index].nama,
                                                                              deskripsi: listServiceByUser[index].deskripsi,
                                                                              productPhotos: listServiceByUser[index].productPhotos,
                                                                              idProduct: listServiceByUser[index].id,
                                                                            )));
                                                          },
                                                          child: Container(
                                                            height: 30.0,
                                                            width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width /
                                                                3,
                                                            child: Center(
                                                              child: Text(
                                                                "Edit Data",
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                            ),
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            8.0),
                                                                color:
                                                                    Colors.red),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ))),
                                ),
                              );
                            })
                    : (_isLoading)
                        ? Shimmer.fromColors(
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                "Tunggu Sebentar...",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22.5,
                                ),
                              ),
                            ),
                            baseColor: Colors.grey[700],
                            highlightColor: Colors.grey[200],
                          )
                        : ListView.builder(
                            itemCount: listProductByUser.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DetailProductScreen(
                                                  id: listProductByUser[index]
                                                      .id,
                                                  tipe: "item",
                                                )));
                                  },
                                  child: Container(
                                      height: 150.0,
                                      width: MediaQuery.of(context).size.width /
                                          1.5,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10.0)),
                                      child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Center(
                                                child: CachedNetworkImage(
                                                  imageUrl:
                                                      listProductByUser[index].productPhotos[0].foto,
                                                  placeholder: (context, url) =>
                                                      Container(
                                                    width: 70.0,
                                                    height: 70.0,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                        image:
                                                            ImageUtils.APP_LOGO,
                                                        fit: BoxFit.cover,
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              80.0),
                                                      border: Border.all(
                                                        color: Colors.white,
                                                        width: 2.0,
                                                      ),
                                                    ),
                                                  ),
                                                  cacheManager: _cacheManager,
                                                  imageBuilder:
                                                      (context, provider) =>
                                                          Container(
                                                    width: 70.0,
                                                    height: 70.0,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                        image: provider,
                                                        fit: BoxFit.cover,
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              80.0),
                                                      border: Border.all(
                                                        color: Colors.white,
                                                        width: 2.0,
                                                      ),
                                                    ),
                                                  ),
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          Container(
                                                    width: 70.0,
                                                    height: 70.0,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                        image: NetworkImage(
                                                            "${RestAPI.FOTO_URL}/pp_0.jpg"),
                                                        fit: BoxFit.cover,
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              80.0),
                                                      border: Border.all(
                                                        color: Colors.white,
                                                        width: 2.0,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Flexible(
                                                      child: Container(
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width /
                                                            1.5,
                                                        child: Text(
                                                          listProductByUser[
                                                                  index]
                                                              .nama
                                                              .toUpperCase(),
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          maxLines: 2,
                                                          style: TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Text(
                                                      rupiah(listProductByUser[
                                                              index]
                                                          .harga),
                                                      style: TextStyle(
                                                          color: Colors.black),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 15.0),
                                                      child: Center(
                                                        child: InkWell(
                                                          onTap: () {
                                                            Navigator.push(
                                                                context,
                                                                MaterialPageRoute(
                                                                    builder:
                                                                        (context) =>
                                                                            EditProductScreen(
                                                                              harga: listProductByUser[index].harga,
                                                                              nameProduct: listProductByUser[index].nama,
                                                                              deskripsi: listProductByUser[index].deskripsi,
                                                                              addItem: 2,
                                                                              productPhotos: listProductByUser[index].productPhotos,
                                                                              idProduct: listProductByUser[index].id,
                                                                            )));
                                                          },
                                                          child: Container(
                                                            height: 30.0,
                                                            width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width /
                                                                3,
                                                            child: Center(
                                                              child: Text(
                                                                "Edit Data",
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                            ),
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            8.0),
                                                                color:
                                                                    Colors.red),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ))),
                                ),
                              );
                            }),
              )
            : Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: widget.addItem == 1
                    ? (_isLoading)
                        ? Shimmer.fromColors(
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                "Tunggu Sebentar...",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22.5,
                                ),
                              ),
                            ),
                            baseColor: Colors.grey[700],
                            highlightColor: Colors.grey[200],
                          )
                        : ListView.builder(
                            itemCount: listServiceByUser.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DetailProductScreen(
                                                  id: listServiceByUser[index]
                                                      .id,
                                                  tipe: "work",
                                                )));
                                  },
                                  child: Container(
                                      height: 100,
                                      width: MediaQuery.of(context).size.width /
                                          1.5,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10.0)),
                                      child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              CachedNetworkImage(
                                                imageUrl:
                                                listServiceByUser[index].productPhotos[0].foto,
                                                placeholder: (context, url) =>
                                                    Container(
                                                  width: 70.0,
                                                  height: 70.0,
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image:
                                                          ImageUtils.APP_LOGO,
                                                      fit: BoxFit.cover,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            80.0),
                                                    border: Border.all(
                                                      color: Colors.white,
                                                      width: 2.0,
                                                    ),
                                                  ),
                                                ),
                                                cacheManager: _cacheManager,
                                                imageBuilder:
                                                    (context, provider) =>
                                                        Container(
                                                  width: 70.0,
                                                  height: 70.0,
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image: provider,
                                                      fit: BoxFit.cover,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            80.0),
                                                    border: Border.all(
                                                      color: Colors.white,
                                                      width: 2.0,
                                                    ),
                                                  ),
                                                ),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        Container(
                                                  width: 70.0,
                                                  height: 70.0,
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image: NetworkImage(
                                                          "${RestAPI.FOTO_URL}/pp_0.jpg"),
                                                      fit: BoxFit.cover,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            80.0),
                                                    border: Border.all(
                                                      color: Colors.white,
                                                      width: 2.0,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Flexible(
                                                      child: Container(
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width /
                                                            1.5,
                                                        child: Text(
                                                          listServiceByUser[
                                                                  index]
                                                              .nama
                                                              .toUpperCase(),
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          maxLines: 2,
                                                          style: TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ))),
                                ),
                              );
                            })
                    : (_isLoading)
                        ? Shimmer.fromColors(
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                "Tunggu Sebentar...",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22.5,
                                ),
                              ),
                            ),
                            baseColor: Colors.grey[700],
                            highlightColor: Colors.grey[200],
                          )
                        : ListView.builder(
                            itemCount: listProductByUser.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DetailProductScreen(
                                                  id: listProductByUser[index]
                                                      .id,
                                                  tipe: "item",
                                                )));
                                  },
                                  child: Container(
                                      height: 100,
                                      width: MediaQuery.of(context).size.width /
                                          1.5,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10.0)),
                                      child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              CachedNetworkImage(
                                                imageUrl:
                                                    "${RestAPI.FOTO_URL}/gambar1_p${listProductByUser[index].id}.jpg",
                                                placeholder: (context, url) =>
                                                    Container(
                                                  width: 70.0,
                                                  height: 70.0,
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image:
                                                          ImageUtils.APP_LOGO,
                                                      fit: BoxFit.cover,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            80.0),
                                                    border: Border.all(
                                                      color: Colors.white,
                                                      width: 2.0,
                                                    ),
                                                  ),
                                                ),
                                                cacheManager: _cacheManager,
                                                imageBuilder:
                                                    (context, provider) =>
                                                        Container(
                                                  width: 70.0,
                                                  height: 70.0,
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image: provider,
                                                      fit: BoxFit.cover,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            80.0),
                                                    border: Border.all(
                                                      color: Colors.white,
                                                      width: 2.0,
                                                    ),
                                                  ),
                                                ),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        Container(
                                                  width: 70.0,
                                                  height: 70.0,
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image: NetworkImage(
                                                          "${RestAPI.FOTO_URL}/pp_0.jpg"),
                                                      fit: BoxFit.cover,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            80.0),
                                                    border: Border.all(
                                                      color: Colors.white,
                                                      width: 2.0,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Flexible(
                                                      child: Container(
                                                        width: MediaQuery.of(
                                                                    context)
                                                                .size
                                                                .width /
                                                            1.5,
                                                        child: Text(
                                                          listProductByUser[
                                                                  index]
                                                              .nama
                                                              .toUpperCase(),
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          maxLines: 2,
                                                          style: TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    Text(
                                                      rupiah(listProductByUser[
                                                              index]
                                                          .harga),
                                                      style: TextStyle(
                                                          color: Colors.black),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ))),
                                ),
                              );
                            }),
              ),
        floatingActionButton: widget.id != null
            ? FloatingActionButton(
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddProductScreen(
                                addItem: widget.addItem,
                              )));
                })
            : Container());
  }

  @override
  void initState() {
    super.initState();
    _initData();
  }

  _getListProductByUser() async {
    await API.getListProductByUser(widget.id).then((response) async {
      var result = await json.decode(response.body);
      result['list'].forEach((product) {
        listProductByUser.add(ProductService.fromJson(product));
      });
      setState(() {});
    });
  }

  _getListServicesByUser() async {
    await API.getListServicesByUser(widget.id).then((response) async {
      var result = await json.decode(response.body);

      result['list'].forEach((product) {
        listServiceByUser.add(ProductService.fromJson(product));
      });
      setState(() {});
    });
  }

  void _initData() async {
    await _cacheManager.emptyCache();

    if (widget.addItem == 1) {
      await _getListServicesByUser();
    } else {
      await _getListProductByUser();
    }

    setState(() {
      _isLoading = false;
    });
  }
}
