import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/model/ProductService.dart';
import 'package:get_survey_app/service/ServicePostData.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class EditProductScreen extends StatefulWidget {
  final int addItem;
  final int editItem;
  final int idProduct;
  final List<ProductPhotos> productPhotos;
  final String nameProduct;
  final int harga;
  final String deskripsi;

  EditProductScreen(
      {this.addItem,
      this.deskripsi,
      this.harga,
      this.idProduct,
      this.nameProduct,
      this.productPhotos,
      this.editItem,
      Key key})
      : super(key: key);

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  TextEditingController _namabarangController = TextEditingController();
  TextEditingController _hargaBarangController = TextEditingController();
  TextEditingController _deskripsiBarangController = TextEditingController();
  final DefaultCacheManager _cacheManager = new DefaultCacheManager();
  final _formKey = GlobalKey<FormState>();
  File _image;
  File _image1;
  File _image2;
  bool _isLoading = false;
  int itemAddId;
  String nama;
  String noImg =
      'https://storage.googleapis.com/getsurvey-c2c51.appspot.com/banners/banner-no-image.jpg?GoogleAccessId=firebase-adminsdk-r8syc@getsurvey-c2c51.iam.gserviceaccount.com&Expires=1662037539&Signature=TEdrvd93jY4xIzH+JR9W7MVl8stP9S6qWR3GsxCMDshzBzAkydc4PNZrrWQvsw0ifNkX7lsGNdLN6ltb8RHTLXBaqIjxNVDoBMEncUYqVVJ584Jn/DoFCpp3f2oeC4plMW8+sCnZOcB+um9ZVmSxuu008HOOG+drrHeFYDmF/YMuzH5NS7ZtjHR/fPjkgnzyn1CZiDboglgzeSmnoPBOJKHviGJIBjp2MI5WSdovRShDCE+m1QY6ncleyBM178RTNkHIZiNAAVQ6VcSqkNdvsnOG3VrHkhX4dEC8TMnHyg+LDSM2qnRicLXSZtBb9kz4jzY7b1toK6To0kbzYaAvyg==';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: primaryColor,
          centerTitle: true,
          title: Text(
            itemAddId == 1 ? "Edit Jasa" : "Edit Product",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        backgroundColor: Colors.grey[200],
        body: ModalProgressHUD(
            inAsyncCall: _isLoading,
            child: itemAddId == 1 ? _bodyJasa() : _bodyProduct()));
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 60,
    );

    setState(() {
      _image = image;
    });
  }

  Future getImage1() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 60,
    );

    setState(() {
      _image1 = image;
    });
  }

  Future getImage2() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 60,
    );

    setState(() {
      _image2 = image;
    });
  }

  @override
  void initState() {
    super.initState();
    _cacheManager.emptyCache();
    itemAddId = widget.addItem;
    _namabarangController.text = widget.nameProduct;
    _hargaBarangController.text = widget.harga.toString();
    _deskripsiBarangController.text = widget.deskripsi;
  }

  Widget _addPhotoProduct(BuildContext context) {
    return InkWell(
      onTap: getImage,
      child: Container(
        height: 150.0,
        width: MediaQuery.of(context).size.width / 4,
        child: _image == null
            ? _doImgCheck(widget.productPhotos, 1)
            : Image.file(
                _image,
                fit: BoxFit.cover,
              ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(width: 2.0, color: Colors.grey),
            color: Colors.white),
      ),
    );
  }

  Widget _addPhotoProduct1(BuildContext context) {
    return InkWell(
      onTap: getImage1,
      child: Container(
        height: 150.0,
        width: MediaQuery.of(context).size.width / 4,
        child: _image1 == null
            ? _doImgCheck(widget.productPhotos, 2)
            : Image.file(
                _image1,
                fit: BoxFit.cover,
              ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(width: 2.0, color: Colors.grey),
            color: Colors.white),
      ),
    );
  }

  Widget _addPhotoProduct2(BuildContext context) {
    return InkWell(
      onTap: getImage2,
      child: Container(
        height: 150.0,
        width: MediaQuery.of(context).size.width / 4,
        child: _image2 == null
            ? _doImgCheck(widget.productPhotos, 3)
            : Image.file(
                _image2,
                fit: BoxFit.cover,
              ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(width: 2.0, color: Colors.grey),
            color: Colors.white),
      ),
    );
  }

  Widget _bodyJasa() {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height +
            MediaQuery.of(context).size.height / 4,
        child: Column(children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                _addPhotoProduct(context),
                _addPhotoProduct1(context),
                _addPhotoProduct2(context),
              ],
            ),
          ),
          _formAddJasa(context)
        ]),
      ),
    );
  }

  Widget _bodyProduct() {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height +
            MediaQuery.of(context).size.height / 4,
        child: Column(children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                _addPhotoProduct(context),
                _addPhotoProduct1(context),
                _addPhotoProduct2(context),
              ],
            ),
          ),
          _formAddProduct(context)
        ]),
      ),
    );
  }

  _doImgCheck(List<ProductPhotos> photos, int index) {
    return CachedNetworkImage(
      imageUrl: photos.length>=index?photos[index-1].foto:noImg,
      cacheManager: _cacheManager,
      placeholder: (context, url) => Icon(
        Icons.add_a_photo,
        color: primaryColor,
        size: 50.0,
      ),
      errorWidget: (context, url, error) => Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.add_a_photo,
            color: primaryColor,
            size: 50.0,
          ),
          SizedBox(
            height: 5.0,
          ),
          Text(
            itemAddId == 1 ? "Edit Foto Jasa" : "Edit Foto Produk",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: primaryColor,
                fontSize: 17.0,
                fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

  Future<void> _doSendImage(
    File image,
    int i,
    int produk,
    int jasa,
    int ke,
    BuildContext context,
  ) async {
    if (image != null) {
      await _onSubmitFoto(
        base64.encode(image.readAsBytesSync()),
        produk,
        jasa,
        (widget.productPhotos.length > i) ? widget.productPhotos[i].id : 0,
        ke,
        context,
      );
    }
  }

  Widget _formAddJasa(BuildContext context) {
    return Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _namabarangController,
                  maxLines: 3,
                  decoration: const InputDecoration(
                      hintText: 'contoh : Alat Ukur', labelText: 'Nama Jasa'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Wajib di Isi';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _deskripsiBarangController,
                  maxLines: 5,
                  decoration: const InputDecoration(
                    labelText: "Deskripsi Jasa",
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Wajib di Isi';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 30.0, right: 30.0, top: 20.0, bottom: 20.0),
                child: Builder(
                  builder: (context) => InkWell(
                    onTap: () async {
                      if (_formKey.currentState.validate()) {
                        int userId = await Session.getValue(Session.USER_ID);
                        setState(() {
                          _isLoading = true;
                        });

                        await _onSubmitJasa(
                            _namabarangController.text,
                            _deskripsiBarangController.text,
                            userId,
                            widget.idProduct,
                            _image,
                            _image1,
                            _image2,
                            context);
                      }
                    },
                    child: Container(
                      height: 60.0,
                      width: MediaQuery.of(context).size.width,
                      child: Center(
                        child: Text(
                          "Edit Jasa",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      decoration: BoxDecoration(
                          color: primaryColor,
                          borderRadius: BorderRadius.circular(10.0)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Widget _formAddProduct(BuildContext context) {
    return Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _namabarangController,
                  decoration: const InputDecoration(
                      hintText: 'contoh : Alat Ukur', labelText: 'Nama Produk'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Wajib di Isi';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _hargaBarangController,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(
                      hintText: 'contoh : 2000000', labelText: "Harga Barang"),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Wajib di Isi';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _deskripsiBarangController,
                  maxLines: 5,
                  decoration: const InputDecoration(
                    labelText: "Keterangan Produk",
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Wajib di Isi';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 30.0, right: 30.0, top: 20.0, bottom: 20.0),
                child: Builder(
                  builder: (context) => InkWell(
                    onTap: () async {
                      if (_formKey.currentState.validate()) {
                        int userId = await Session.getValue(Session.USER_ID);
                        setState(() {
                          _isLoading = true;
                        });
                        await _onSubmitProduct(
                            _namabarangController.text,
                            int.parse(_hargaBarangController.text),
                            widget.idProduct,
                            _deskripsiBarangController.text,
                            userId,
                            _image,
                            _image1,
                            _image2,
                            context);
                      }
                    },
                    child: Container(
                      height: 60.0,
                      width: MediaQuery.of(context).size.width,
                      child: Center(
                        child: Text(
                          "Edit Produk",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      decoration: BoxDecoration(
                          color: primaryColor,
                          borderRadius: BorderRadius.circular(10.0)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Future<dynamic> _onSubmitFoto(
    String foto,
    int produk,
    int jasa,
    int id,
    int ke,
    BuildContext context,
  ) async {
    if (id > 0) {
      await editGaleries(foto, produk, jasa, id, ke);
    } else {
      await addGaleries(foto, produk, jasa, ke);
    }
  }

  Future<dynamic> _onSubmitJasa(
    String nama,
    String deskripsi,
    int userId,
    int id,
    image,
    image1,
    image2,
    BuildContext context,
  ) async {
    var response = await editJasa(nama, deskripsi, userId, id);
    if (response.statusCode == 200) {
      await _doSendImage(image, 0, null, id, 1, context);
      await _doSendImage(image1, 1, null, id, 2, context);
      await _doSendImage(image2, 2, null, id, 3, context);

      setState(() {
        _isLoading = false;
      });

      Alert(
        context: context,
        image: Image.asset(
          "assets/alert/check.png",
          color: Colors.red,
        ),
        type: AlertType.success,
        title: "Berhasil",
        desc: "Edit Jasa berhasil dirubah!!",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Kembali",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () => Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => BtnNavigation(
                          atIndex: 0,
                        ))),
            width: 120,
          )
        ],
      ).show();
    } else {
      setState(() {
        _isLoading = false;
      });
      Component.showAppNotification(context, "Salah Inputan",
          "Data yang anda Inputkan salah.. Cek lagi inputan yang Anda Masukkan!!");
    }
  }

  Future<dynamic> _onSubmitProduct(
      String nama,
      int harga,
      int id,
      String deskripsi,
      int userId,
      image,
      image1,
      image2,
      BuildContext context) async {
    var response = await editProduct(nama, harga, deskripsi, userId, id);
    if (response.statusCode == 200 && jsonDecode(response.body)['status']) {
      await _doSendImage(image, 0, id, null, 1, context);
      await _doSendImage(image1, 1, id, null, 2, context);
      await _doSendImage(image2, 2, id, null, 3, context);

      setState(() {
        _isLoading = false;
      });
      Alert(
        context: context,
        image: Image.asset(
          "assets/alert/check.png",
          color: Colors.red,
        ),
        type: AlertType.success,
        title: "Berhasil",
        desc: "Edit Produk berhasil dirubah!!",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Kembali",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () => Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => BtnNavigation(
                          atIndex: 0,
                        ))),
            width: 120,
          )
        ],
      ).show();
    } else {
      setState(() {
        _isLoading = false;
      });
      Component.showAppNotification(context, "Salah Inputan",
          "Data yang anda Inputkan salah.. Cek lagi inputan yang Anda Masukkan!!");
    }
  }
}
