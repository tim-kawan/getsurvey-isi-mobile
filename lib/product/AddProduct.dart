import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/service/ServicePostData.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class AddProductScreen extends StatefulWidget {
  final int addItem;

  AddProductScreen({this.addItem, Key key}) : super(key: key);

  @override
  _AddProductScreenState createState() => _AddProductScreenState();
}

class _AddProductScreenState extends State<AddProductScreen> {
  TextEditingController _namabarangController = TextEditingController();
  TextEditingController _hargaBarangController = TextEditingController();
  TextEditingController _deskripsiBarangController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  File _image;
  File _image1;
  File _image2;
  bool _isLoading = false;
  int itemAddId;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: primaryColor,
          centerTitle: true,
          title: Text(
            itemAddId == 1 ? "Tambah Jasa" : "Tambah Produk",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        backgroundColor: Colors.grey[200],
        body: ModalProgressHUD(
            inAsyncCall: _isLoading,
            child: itemAddId == 1 ? _bodyJasa() : _bodyProduct()));
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 60,
    );

    setState(() {
      _image = image;
    });
  }

  Future getImage1() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 60,
    );

    setState(() {
      _image1 = image;
    });
  }

  Future getImage2() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 60,
    );

    setState(() {
      _image2 = image;
    });
  }

  @override
  void initState() {
    super.initState();
    itemAddId = widget.addItem;
  }

  Widget _addPhotoProduct(BuildContext context) {
    return InkWell(
      onTap: getImage,
      child: Container(
        height: 150.0,
        width: MediaQuery.of(context).size.width / 4,
        child: _image == null
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.add_a_photo,
                    color: primaryColor,
                    size: 50.0,
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    itemAddId == 1 ? "Add Photo Jasa" : "Add Photo Product",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: primaryColor,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              )
            : Image.file(
                _image,
                fit: BoxFit.cover,
              ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(width: 2.0, color: Colors.grey),
            color: Colors.white),
      ),
    );
  }

  Widget _addPhotoProduct1(BuildContext context) {
    return InkWell(
      onTap: getImage1,
      child: Container(
        height: 150.0,
        width: MediaQuery.of(context).size.width / 4,
        child: _image1 == null
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.add_a_photo,
                    color: primaryColor,
                    size: 50.0,
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    itemAddId == 1 ? "Add Photo Jasa" : "Add Photo Produk",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: primaryColor,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              )
            : Image.file(
                _image1,
                fit: BoxFit.cover,
              ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(width: 2.0, color: Colors.grey),
            color: Colors.white),
      ),
    );
  }

  Widget _addPhotoProduct2(BuildContext context) {
    return InkWell(
      onTap: getImage2,
      child: Container(
        height: 150.0,
        width: MediaQuery.of(context).size.width / 4,
        child: _image2 == null
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.add_a_photo,
                    color: primaryColor,
                    size: 50.0,
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    itemAddId == 1 ? "Tambah Foto Jasa" : "Tambah Foto Produk",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: primaryColor,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              )
            : Image.file(
                _image2,
                fit: BoxFit.cover,
              ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(width: 2.0, color: Colors.grey),
            color: Colors.white),
      ),
    );
  }

  Widget _bodyJasa() {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height +
            MediaQuery.of(context).size.height / 4,
        child: Column(children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                _addPhotoProduct(context),
                _addPhotoProduct1(context),
                _addPhotoProduct2(context),
              ],
            ),
          ),
          _formAddJasa(context)
        ]),
      ),
    );
  }

  Widget _bodyProduct() {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height +
            MediaQuery.of(context).size.height / 4,
        child: Column(children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                _addPhotoProduct(context),
                _addPhotoProduct1(context),
                _addPhotoProduct2(context),
              ],
            ),
          ),
          _formAddProduct(context)
        ]),
      ),
    );
  }

  Widget _formAddJasa(BuildContext context) {
    return Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _namabarangController,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: const InputDecoration(
                      hintText: 'contoh : Alat Ukur', labelText: 'Nama Jasa'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Wajib di Isi';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _deskripsiBarangController,
                  maxLines: 5,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: const InputDecoration(
                    labelText: "Keterangan Jasa",
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Wajib di Isi';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 30.0, right: 30.0, top: 20.0, bottom: 20.0),
                child: Builder(
                  builder: (context) => InkWell(
                    onTap: () async {
                      if (_formKey.currentState.validate()) {
                        int userId = await Session.getValue(Session.USER_ID);
                        setState(() {
                          _isLoading = true;
                        });
                        await _onSubmitJasa(
                            _namabarangController.text,
                            _deskripsiBarangController.text,
                            userId,
                            _image,
                            _image1,
                            _image2,
                            context);
                      }
                    },
                    child: Container(
                      height: 60.0,
                      width: MediaQuery.of(context).size.width,
                      child: Center(
                        child: Text(
                          "Tambah Jasa",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      decoration: BoxDecoration(
                          color: primaryColor,
                          borderRadius: BorderRadius.circular(10.0)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Widget _formAddProduct(BuildContext context) {
    return Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _namabarangController,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: const InputDecoration(
                      hintText: 'contoh : Alat Ukur', labelText: 'Nama Produk'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Wajib di Isi';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _hargaBarangController,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(
                      hintText: 'contoh : 2000000', labelText: "Harga Barang"),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Wajib di Isi';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _deskripsiBarangController,
                  maxLines: 5,
                  textCapitalization: TextCapitalization.sentences,
                  decoration: const InputDecoration(
                    labelText: "Keterangan Produk",
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Wajib di Isi';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 30.0, right: 30.0, top: 20.0, bottom: 20.0),
                child: Builder(
                  builder: (context) => InkWell(
                    onTap: () async {
                      if (_formKey.currentState.validate()) {
                        int userId = await Session.getValue(Session.USER_ID);
                        setState(() {
                          _isLoading = true;
                        });
                        await _onSubmitProduct(
                            _namabarangController.text,
                            int.parse(_hargaBarangController.text),
                            _deskripsiBarangController.text,
                            userId,
                            _image,
                            _image1,
                            _image2,
                            context);
                      }
                    },
                    child: Container(
                      height: 60.0,
                      width: MediaQuery.of(context).size.width,
                      child: Center(
                        child: Text(
                          "Tambah Produk",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      decoration: BoxDecoration(
                          color: primaryColor,
                          borderRadius: BorderRadius.circular(10.0)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Future<dynamic> _onSubmitFoto(
    String foto,
    int produk,
    int jasa,
    int ke,
    BuildContext context,
  ) async {
    await addGaleries(foto, produk, jasa, ke);
  }

  Future<dynamic> _onSubmitJasa(
    String nama,
    String deskripsi,
    int userId,
    image,
    image1,
    image2,
    BuildContext context,
  ) async {
    var response = await addJasa(nama, deskripsi, userId);
    if (response.statusCode == 200 &&
        jsonDecode(response.body)['status'] == true) {
      int idItem = jsonDecode(response.body)['id'];
      if (_image != null) {
        await _onSubmitFoto(
            base64.encode(image.readAsBytesSync()), null, idItem, 1, context);
      }
      if (_image1 != null) {
        await _onSubmitFoto(
            base64.encode(image1.readAsBytesSync()), null, idItem, 2, context);
      }
      if (_image2 != null) {
        await _onSubmitFoto(
            base64.encode(image2.readAsBytesSync()), null, idItem, 3, context);
      }
      setState(() {
        _isLoading = false;
      });
      Alert(
        context: context,
        image: Image.asset(
          "assets/alert/check.png",
          color: Colors.green,
        ),
        type: AlertType.success,
        title: "Berhasil",
        desc: "Jasa Anda telah di tambahkan",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Kembali",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () => Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (context) => BtnNavigation(
                      atIndex: 0,
                    )),
                    (Route<dynamic> route) => false),
            width: 120,
          )
        ],
      ).show();
    } else {
      setState(() {
        _isLoading = false;
      });
      Component.showAppNotification(context, "Salah Inputan",
          "Data yang anda Inputkan salah.. Cek lagi inputan yang Anda Masukkan!!");
    }
  }

  Future<dynamic> _onSubmitProduct(String nama, int harga, String deskripsi,
      int userId, image, image1, image2, BuildContext context) async {
    var response = await addProduct(nama, harga, deskripsi, userId);
    if (response.statusCode == 200 && jsonDecode(response.body)['status']) {
      int idItem = jsonDecode(response.body)['id'];
      if (_image != null) {
        await _onSubmitFoto(
            base64.encode(image.readAsBytesSync()), idItem, null, 1, context);
      }
      if (_image1 != null) {
        await _onSubmitFoto(
            base64.encode(image1.readAsBytesSync()), idItem, null, 2, context);
      }
      if (_image2 != null) {
        await _onSubmitFoto(
            base64.encode(image2.readAsBytesSync()), idItem, null, 3, context);
      }
      setState(() {
        _isLoading = false;
      });
      Alert(
        context: context,
        image: Image.asset(
          "assets/alert/check.png",
          color: Colors.green,
        ),
        type: AlertType.success,
        title: "Berhasil",
        desc: "Produk Anda telah di tambahkan",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Kembali",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () => Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (context) => BtnNavigation(
                      atIndex: 0,
                    )),
                    (Route<dynamic> route) => false),
            width: 120,
          )
        ],
      ).show();
    } else {
      setState(() {
        _isLoading = false;
      });
      Component.showAppNotification(context, "Salah Inputan",
          "Data yang anda Inputkan salah.. Cek lagi inputan yang Anda Masukkan!!");
    }
  }
}
