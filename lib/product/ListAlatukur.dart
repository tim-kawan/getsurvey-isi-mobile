import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/SearchScreen.dart';
import 'package:get_survey_app/model/ProductService.dart';
import 'package:get_survey_app/product/DetailProductScreen.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:indonesia/indonesia.dart';
import 'package:shimmer/shimmer.dart';

class ListAlatUkurScreen extends StatefulWidget {
  final int choseList;

  ListAlatUkurScreen({this.choseList, Key key}) : super(key: key);

  @override
  _ListAlatUkurScreenState createState() => _ListAlatUkurScreenState();
}

class _ListAlatUkurScreenState extends State<ListAlatUkurScreen> {
  bool _isLoading = true;

  List<ProductService> listProduct = [];
  List<ProductService> listService = [];

  @override
  Widget build(BuildContext context) {
    var listChoseProduct = Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: widget.choseList == 1
          ? (_isLoading)
              ? Shimmer.fromColors(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "Tunggu Sebentar...",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 22.5,
                      ),
                    ),
                  ),
                  baseColor: Colors.grey[700],
                  highlightColor: Colors.grey[200],
                )
              : listProduct.isNotEmpty
                  ? GridView.count(
                      crossAxisCount: 2,
                      mainAxisSpacing: 10,
                      childAspectRatio: 0.70,
                      children: List.generate(listProduct.length, (index) {
                        return Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            DetailProductScreen(
                                              id: listProduct[index].id,
                                              tipe: "item",
                                            )));
                              },
                              child: Container(
                                padding: EdgeInsets.all(5),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Container(
                                            width: 120.0,
                                            height: 120.0,
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                image: listProduct[index]
                                                            .productPhotos
                                                            .isEmpty ||
                                                        listService[index]
                                                                .productPhotos[
                                                                    0]
                                                                .foto ==
                                                            null
                                                    ? AssetImage(
                                                        'assets/logo.png')
                                                    : NetworkImage(
                                                        listProduct[index]
                                                            .productPhotos[0]
                                                            .foto,
                                                      ),
                                                fit: BoxFit.cover,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(20.0),
                                            ),
                                          ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text(
                                      listProduct[index].nama.toUpperCase(),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text(
                                      listProduct[index].harga == null
                                          ? ""
                                          : rupiah(listProduct[index].harga),
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  ],
                                ),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10.0)),
                              )),
                        );
                      }))
                  : Container(
                      child: Center(
                        child: Text(
                          "Tidak Ada Data",
                          style: TextStyle(color: Colors.black, fontSize: 19),
                        ),
                      ),
                    )
          : (_isLoading)
              ? Shimmer.fromColors(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      "Tunggu Sebentar...",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 22.5,
                      ),
                    ),
                  ),
                  baseColor: Colors.grey[700],
                  highlightColor: Colors.grey[200],
                )
              : listService.isNotEmpty
                  ? GridView.count(
                      crossAxisCount: 2,
                      childAspectRatio: 0.75,
                      children: List.generate(listService.length, (index) {
                        return Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            DetailProductScreen(
                                              id: listService[index].id,
                                              tipe: "work",
                                            )));
                              },
                              child: Container(
                                padding: EdgeInsets.all(5),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      height: 10,
                                    ), Container(
                                            width: 120.0,
                                            height: 120.0,
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                image: listService[index]
                                                            .productPhotos
                                                            .isEmpty ||
                                                        listService[index]
                                                                .productPhotos[
                                                                    0]
                                                                .foto ==
                                                            null
                                                    ? AssetImage(
                                                        'assets/logo.png')
                                                    : NetworkImage(
                                                        listService[index]
                                                            .productPhotos[0]
                                                            .foto,
                                                      ),
                                                fit: BoxFit.cover,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(20.0),
                                            ),
                                          ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text(
                                      listService[index].nama.toUpperCase(),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 8,
                                    ),
                                    Text(
                                      listService[index].harga == null
                                          ? ""
                                          : rupiah(listService[index].harga),
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  ],
                                ),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10.0)),
                              )),
                        );
                      }))
                  : Container(
                      child: Center(
                        child: Text(
                          "Tidak Ada Data",
                          style: TextStyle(color: Colors.black, fontSize: 19),
                        ),
                      ),
                    ),
    );

    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SearchScreen()));
              })
        ],
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
        centerTitle: true,
        backgroundColor: primaryColor,
        title: Text(
          widget.choseList == 1 ? "Daftar Produk" : "Daftar Jasa ",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(5.0),
        child: listChoseProduct,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _initData();
  }

  void _initData() async {
    List produk = [];
    List service = [];
    try {
      produk = await RestAPI.seeProducts();
      service = await RestAPI.seeServices();
    } catch (_) {}

    produk.forEach((data) {
      listProduct.add(ProductService.fromJson(data));
    });
    service.forEach((data) {
      listService.add(ProductService.fromJson(data));
    });
    setState(() {});
    setState(() => _isLoading = false);
  }
}
