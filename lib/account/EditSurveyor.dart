import 'dart:convert';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/ServiceGetData.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class EditSurveyorScreen extends StatefulWidget {
  final String noAngggota;
  final String noLisesnsi;
  final String noSka;
  final String jenjangPendidikan;
  final String jurusanPendidikan;
  final String sekolah;
  final int update;

  EditSurveyorScreen(
      {this.jenjangPendidikan,
      this.jurusanPendidikan,
      this.noLisesnsi,
      this.noAngggota,
      this.noSka,
      this.update,
      this.sekolah});

  @override
  _EditSurveyorScreenState createState() => _EditSurveyorScreenState();
}

class _EditSurveyorScreenState extends State<EditSurveyorScreen> {
  TextEditingController _noAnggotaController = TextEditingController();
  TextEditingController _noKtp = TextEditingController();
  TextEditingController _noLisensiIrkController = TextEditingController();
  TextEditingController _noSkaController = TextEditingController();
  TextEditingController _noLisensiKompentensiController =
      TextEditingController();
  TextEditingController _noKadastral = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  bool _isLoading = false;
  String _fileName = 'Pilih Berkas Lisensi PDF, JPG/JPEG, PNG';

  String _path;
  String failedFile;

  int maxLengthKtp = 16;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: primaryColor,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              }),
          centerTitle: true,
          title: Text(
            "Edit Data Surveyor",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          )),
      body: ModalProgressHUD(inAsyncCall: _isLoading, child: _bodyupdate()),
      bottomNavigationBar: BottomAppBar(
        child: Builder(
          builder: (context) => InkWell(
            onTap: () async {
              if (_formKey.currentState.validate()) {
                setState(() {
                  _isLoading = true;
                });
                int idUser = await Session.getValue(Session.USER_ID);
                await _onSubmit(
                    context,
                    idUser,
                    _noKtp.text,
                    _noAnggotaController.text,
                    _noLisensiKompentensiController.text,
                    _noSkaController.text,
                    _noKadastral.text,
                    _noLisensiIrkController.text);
                setState(() {
                  _isLoading = false;
                });
              }
            },
            child: Container(
              height: 50.0,
              decoration: BoxDecoration(
                color: primaryColor,
              ),
              child: Center(
                child: Text(
                  "Edit Data",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: "Sans",
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _getListSurveyor();
  }

  Widget _bodyupdate() {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Text(
                "NIK",
                style: TextStyle(
                    color: primaryColor4,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 60.0,
                alignment: AlignmentDirectional.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(14.0),
                    color: Colors.white,
                    border: Border.all(color: primaryColor4, width: 1)),
                padding: EdgeInsets.only(
                    left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
                child: Theme(
                  data: ThemeData(
                    hintColor: Colors.transparent,
                  ),
                  child: TextFormField(
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(20),
                    ],
                    controller: _noKtp,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Contoh : 2/xxx/ST/xxxx",
                        hintStyle: TextStyle(
                          fontSize: 12.0,
                          fontFamily: 'Sans',
                          letterSpacing: 0.3,
                          color: Colors.grey,
                        )),
                    validator: (value) {
                      if (value.isEmpty) {
                        setState(() {
                          _isLoading = false;
                        });
                        return 'Wajib di Isi';
                      } else if (value.length < maxLengthKtp) {
                        setState(() {
                          _isLoading = false;
                        });
                        return 'No Ktp Minimal 16 karakter';
                      }
                      return null;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "No Anggota",
                style: TextStyle(
                    color: primaryColor4,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 60.0,
                alignment: AlignmentDirectional.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(14.0),
                    color: Colors.white,
                    border: Border.all(color: primaryColor4, width: 1)),
                padding: EdgeInsets.only(
                    left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
                child: Theme(
                  data: ThemeData(
                    hintColor: Colors.transparent,
                  ),
                  child: TextFormField(
                    controller: _noAnggotaController,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Contoh : 2/xxx/ST/xxxx",
                        hintStyle: TextStyle(
                          fontSize: 12.0,
                          fontFamily: 'Sans',
                          letterSpacing: 0.3,
                          color: Colors.grey,
                        )),
                    validator: (value) {
                      if (value.isEmpty) {
                        setState(() {
                          _isLoading = false;
                        });
                        return "Data Tidak Boleh Kosong!!";
                      }
                      return null;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "No Lisensi",
                style: TextStyle(
                    color: primaryColor4,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 60.0,
                alignment: AlignmentDirectional.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(14.0),
                    color: Colors.white,
                    border: Border.all(color: primaryColor4, width: 1)),
                padding: EdgeInsets.only(
                    left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
                child: Theme(
                  data: ThemeData(
                    hintColor: Colors.transparent,
                  ),
                  child: TextFormField(
                    controller: _noLisensiIrkController,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Contoh : 2/xxx/ST/xxxx",
                        hintStyle: TextStyle(
                          fontSize: 12.0,
                          fontFamily: 'Sans',
                          letterSpacing: 0.3,
                          color: Colors.grey,
                        )),
                    validator: (value) {
                      if (value.isEmpty) {
                        setState(() {
                          _isLoading = false;
                        });
                        return "Data Tidak Boleh Kosong!!";
                      }
                      return null;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "No SKA/SKT",
                style: TextStyle(
                    color: primaryColor4,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 60.0,
                alignment: AlignmentDirectional.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(14.0),
                    color: Colors.white,
                    border: Border.all(color: primaryColor4, width: 1)),
                padding: EdgeInsets.only(
                    left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
                child: Theme(
                  data: ThemeData(
                    hintColor: Colors.transparent,
                  ),
                  child: TextFormField(
                    controller: _noSkaController,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Contoh : 2/xxx/ST/xxxx",
                        hintStyle: TextStyle(
                          fontSize: 12.0,
                          fontFamily: 'Sans',
                          letterSpacing: 0.3,
                          color: Colors.grey,
                        )),
                    validator: (value) {
                      if (value.isEmpty) {
                        setState(() {
                          _isLoading = false;
                        });
                        return "Data Tidak Boleh Kosong!!";
                      }
                      return null;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "No Kadastral",
                style: TextStyle(
                    color: primaryColor4,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 60.0,
                alignment: AlignmentDirectional.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(14.0),
                    color: Colors.white,
                    border: Border.all(color: primaryColor4, width: 1)),
                padding: EdgeInsets.only(
                    left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
                child: Theme(
                  data: ThemeData(
                    hintColor: Colors.transparent,
                  ),
                  child: TextFormField(
                    controller: _noKadastral,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Contoh : 2/xxx/ST/xxxx",
                        hintStyle: TextStyle(
                          fontSize: 12.0,
                          fontFamily: 'Sans',
                          letterSpacing: 0.3,
                          color: Colors.grey,
                        )),
                    validator: (value) {
                      if (value.isEmpty) {
                        setState(() {
                          _isLoading = false;
                        });
                        return "Data Tidak Boleh Kosong!!";
                      }
                      return null;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "No Lisensi Kompetensi",
                style: TextStyle(
                    color: primaryColor4,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 60.0,
                alignment: AlignmentDirectional.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(14.0),
                    color: Colors.white,
                    border: Border.all(color: primaryColor4, width: 1)),
                padding: EdgeInsets.only(
                    left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
                child: Theme(
                  data: ThemeData(
                    hintColor: Colors.transparent,
                  ),
                  child: TextFormField(
                    controller: _noLisensiKompentensiController,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Contoh : 2/xxx/ST/xxxx",
                        hintStyle: TextStyle(
                          fontSize: 12.0,
                          fontFamily: 'Sans',
                          letterSpacing: 0.3,
                          color: Colors.grey,
                        )),
                    validator: (value) {
                      if (value.isEmpty) {
                        setState(() {
                          _isLoading = false;
                        });
                        return "Data Tidak Boleh Kosong!!";
                      }
                      return null;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "Unggah KTP",
                style: TextStyle(
                    color: primaryColor4,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: _openFileExplorer,
                      child: Container(
                        padding: const EdgeInsets.all(10.0),
                        child:
                            _fileName == 'Pilih Berkas KTP PDF, JPG/JPEG, PNG'
                                ? Icon(
                                    Icons.add,
                                    color: Colors.grey,
                                    size: 70,
                                  )
                                : Icon(
                                    Icons.picture_as_pdf,
                                    color: Colors.red,
                                    size: 70,
                                  ),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey, width: 1)),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      _fileName,
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                          fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }

  _getListSurveyor() async {
    var idParam = await Session.getValue(Session.USER_ID);
    await API.getListSurveyor(idParam).then((response) async {
      var result = await json.decode(response.body);
      _noKtp.text = result['user_surveyor']['user']['user_profile']['nik'];
      _noAnggotaController.text = result['user_surveyor']['no_anggota'];
      _noSkaController.text = result['user_surveyor']['no_ska_skt'];
      _noLisensiIrkController.text = result['user_surveyor']['no_lisensi_irk'];
      _noKadastral.text = result['user_surveyor']['no_lisensi_kadastral'];
      _noLisensiKompentensiController.text =
          result['user_surveyor']['no_lisensi'];
      setState(() {});
    });
  }

  Future<dynamic> _onSubmit(
      BuildContext context,
      int id,
      String noKtp,
      String noAnggota,
      String noLisensiKompetensi,
      String noSKA,
      String noKadastral,
      String noLisensiIrk) async {
    await RestAPI.updateAgent(
        id: id,
        noAnggota: noAnggota,
        noIRk: noLisensiIrk,
        noKadastral: noKadastral,
        ktp: noKtp,
        isResend: widget.update == 1 ? false : true,
        noLisensi: noLisensiKompetensi,
        noSKASKT: noSKA);

    if (_path != null) {
      if (_path.toLowerCase().contains("pdf") ||
          _path.toLowerCase().contains("png") ||
          _path.toLowerCase().contains("jpg") ||
          _path.toLowerCase().contains("jpeg")) {
        String tipe = "jpg";

        if (_path.toLowerCase().contains("pdf")) {
          tipe = "pdf";
        } else if (_path.toLowerCase().contains("png")) {
          tipe = "png";
        }

        await RestAPI.doUpload(id, _path, tipe);
        Alert(
          context: context,
          image: Image.asset(
            "assets/alert/check.png",
            color: Colors.red,
          ),
          type: AlertType.success,
          title: "Berhasil",
          desc: widget.update == 1
              ? "Data Berhasil Di Edit Silahkan Lakukan Login kembali !! "
              : "Pengajuan Dengan Data Baru Sedang Di Proses Mohon Menunggu !!",
          closeIcon: Container(),
          onWillPopActive: true,
          buttons: [
            DialogButton(
              child: Text(
                "Kembali",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () async {
                await Session.doClear();
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => BtnNavigation(
                              atIndex: 2,
                            )));
              },
              width: 120,
            )
          ],
        ).show();
      }
    } else {
      Alert(
        context: context,
        image: Image.asset(
          "assets/alert/check.png",
          color: Colors.red,
        ),
        type: AlertType.success,
        title: "Berhasil",
        desc: widget.update == 1
            ? "Data Berhasil Di Edit Silahkan Lakukan Login kembali !! "
            : "Pengajuan Dengan Data Baru Sedang Di Proses Mohon Menunggu !!",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Kembali",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () async {
              await Session.doClear();
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BtnNavigation(
                            atIndex: 2,
                          )));
            },
            width: 120,
          )
        ],
      ).show();
    }
  }

  void _openFileExplorer() async {
    _fileName = 'Pilih Berkas Lisensi PDF, JPG/JPEG, PNG';
    failedFile = "";
    try {
      _path = await FilePicker.getFilePath();
      if (_path != null) {
        if (_path.toLowerCase().contains(".pdf") == false &&
            _path.toLowerCase().contains(".png") == false &&
            _path.toLowerCase().contains(".jpg") == false &&
            _path.toLowerCase().contains(".jpeg") == false) {
          failedFile =
              "Gagal!! Pastikan file anda memilkki extension seperti contoh( .pdf, .png, .jpeg, .jpg) dibelakang nama file anda, ubah nama file dan sesuaikan dengan format file anda";
          _path = null;
          _fileName = "Gagal Memproses Berkas!";
        } else {
          _fileName = (_path.toLowerCase().contains("pdf") ||
                  _path.toLowerCase().contains("png") ||
                  _path.toLowerCase().contains("jpg") ||
                  _path.toLowerCase().contains("jpeg")
              ? _path.split('/').last
              : "Pilih Berkas Lisensi PDF, JPG/JPEG, PNG");
        }
      } else {
        _fileName = "Gagal Memproses Berkas!";
        _path = null;
      }
      setState(() {});
    } on PlatformException catch (e) {
      print(e.message);
    }
  }
}
