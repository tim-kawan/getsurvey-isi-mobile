import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/model/ProductService.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class EditUSer extends StatefulWidget {
  @override
  _EditUSerState createState() => _EditUSerState();
}

class _EditUSerState extends State<EditUSer> {
  final DefaultCacheManager _cacheManager = new DefaultCacheManager();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _passwordConfirmController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  File _image;
  bool _obscureText = true;
  bool _obscureTextConfirm = true;
  String name;
  String fotoUpdate =
      'https://storage.googleapis.com/getsurvey-c2c51.appspot.com/banners/banner-no-image.jpg?GoogleAccessId=firebase-adminsdk-r8syc@getsurvey-c2c51.iam.gserviceaccount.com&Expires=1662037539&Signature=TEdrvd93jY4xIzH+JR9W7MVl8stP9S6qWR3GsxCMDshzBzAkydc4PNZrrWQvsw0ifNkX7lsGNdLN6ltb8RHTLXBaqIjxNVDoBMEncUYqVVJ584Jn/DoFCpp3f2oeC4plMW8+sCnZOcB+um9ZVmSxuu008HOOG+drrHeFYDmF/YMuzH5NS7ZtjHR/fPjkgnzyn1CZiDboglgzeSmnoPBOJKHviGJIBjp2MI5WSdovRShDCE+m1QY6ncleyBM178RTNkHIZiNAAVQ6VcSqkNdvsnOG3VrHkhX4dEC8TMnHyg+LDSM2qnRicLXSZtBb9kz4jzY7b1toK6To0kbzYaAvyg==';
  bool _isLoading = false;
  bool isLogout = false;
  var listUser = List<UserProfile>();
  List<UserProfile> images;
  int idUser = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: primaryColor,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              }),
          title: Text(
            "Ubah Profil",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          )),
      body: ModalProgressHUD(inAsyncCall: _isLoading, child: _body(context)),
      bottomNavigationBar: BottomAppBar(
        child: Builder(
          builder: (context) => InkWell(
            onTap: () async {
              bool isAllow = false;

              if (_formKey.currentState.validate()) {
                if (_passwordController.text.isNotEmpty) {
                  if (_passwordController.text ==
                      _passwordConfirmController.text) {
                    isAllow = true;
                    setState(() {
                      isLogout = true;
                    });
                  } else {
                    Component.showAppNotification(context, "Gagal",
                        "Cek Kembali Password yang Anda Masukkan");
                  }
                } else {
                  isAllow = true;
                }
              }

              if (isAllow) {
                setState(() {
                  _isLoading = true;
                });
                await _onSubmit(context, idUser, _nameController.text,
                    _passwordController.text, _image);
                setState(() {
                  _isLoading = false;
                });
              }
            },
            child: Container(
              height: 50.0,
              decoration: BoxDecoration(
                color: primaryColor,
              ),
              child: Center(
                child: Text(
                  "Edit Data",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: "Sans",
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _getListUSer();
  }

  Widget _body(BuildContext context) {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    (_image != null)
                        ? Container(
                            width: 140.0,
                            height: 140.0,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: FileImage(_image),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: BorderRadius.circular(80.0),
                              border: Border.all(
                                color: primaryColor,
                                width: 2.0,
                              ),
                            ),
                          )
                        : Container(
                            width: 140.0,
                            height: 140.0,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(fotoUpdate),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: BorderRadius.circular(80.0),
                            )),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Pilih Photo Profil",
                      style: TextStyle(
                          color: primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: InkWell(
                            onTap: getImageFoto,
                            child: Container(
                              height: 40.0,
                              decoration: BoxDecoration(
                                  border: Border.all(), color: primaryColor),
                              child: Center(
                                child: Text(
                                  "Ambil Dari Kamera",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 10.0),
                        Expanded(
                          child: InkWell(
                            onTap: getImageGaleri,
                            child: Container(
                              height: 40.0,
                              decoration: BoxDecoration(
                                  border: Border.all(), color: primaryColor),
                              child: Center(
                                child: Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Text(
                                    "Ambil Dari Galeri",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: _nameController,
                enabled: false,
                textCapitalization: TextCapitalization.sentences,
                inputFormatters: [
                  LengthLimitingTextInputFormatter(100),
                ],
                decoration: const InputDecoration(
                    hintText: 'Nama Lengkap', labelText: 'Nama Lengkap'),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Isikan Nama Lengkap';
                  }

                  return null;
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: TextFormField(
                controller: _passwordController,
                obscureText: _obscureText,
                decoration: InputDecoration(
                  hintText: 'Kata Sandi',
                  suffixIcon: GestureDetector(
                    onTap: () => setState(() {
                      _obscureText = !_obscureText;
                    }),
                    child: Icon(
                      _obscureText ? Icons.visibility_off : Icons.visibility,
                      color: primaryColor,
                      semanticLabel:
                          _obscureText ? 'Lihat Sandi' : 'Tutup Sandi',
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: TextFormField(
                controller: _passwordConfirmController,
                obscureText: _obscureTextConfirm,
                decoration: InputDecoration(
                  hintText: 'Konfirmasi Kata Sandi',
                  suffixIcon: GestureDetector(
                    onTap: () => setState(() {
                      _obscureTextConfirm = !_obscureTextConfirm;
                    }),
                    child: Icon(
                      _obscureTextConfirm
                          ? Icons.visibility_off
                          : Icons.visibility,
                      color: primaryColor,
                      semanticLabel:
                          _obscureTextConfirm ? 'Lihat Sandi' : 'Tutup Sandi',
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future getImageFoto() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.camera,
      imageQuality: 60,
    );
    setState(() {
      _image = image;
    });
  }

  Future getImageGaleri() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 60,
    );
    setState(() {
      _image = image;
    });
  }

  _getListUSer() async {
    await _cacheManager.emptyCache();
    idUser = await Session.getValue(Session.USER_ID);
    fotoUpdate = await Session.getValue(Session.USER_PHOTO);
    _nameController.text = await Session.getValue(Session.USER_NAME);
    setState(() {});
  }

  Future<dynamic> _onSubmit(
    BuildContext context,
    int id,
    String nama,
    String sandi,
    File foto,
  ) async {
    bool isEdit = await RestAPI.updateBio(
      id,
      nama,
      sandi.isEmpty ? null : sandi,
      (foto != null) ? base64.encode(foto.readAsBytesSync()) : null,
    );
    Session.doSave(Session.USER_NAME, nama);

    Alert(
      context: context,
      image: Image.asset(
        "assets/alert/check.png",
        color: Colors.red,
      ),
      type: AlertType.success,
      title: "Berhasil",
      desc: "Profil Anda berhasil dirubah",
      closeIcon: Container(),
      onWillPopActive: true,
      buttons: [
        DialogButton(
          child: Text(
            "Kembali",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () async {
            if (isLogout) {
              await Session.doClear();
            }

            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => BtnNavigation(
                          atIndex: 2,
                        )));
          },
          width: 120,
        )
      ],
    ).show();
  }
}
