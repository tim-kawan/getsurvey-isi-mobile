class LoginEntity {
  final bool success;
  final String message;

  LoginEntity(this.success, this.message);
}
