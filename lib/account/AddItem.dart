import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_survey_app/account/UpgradeToSurveyors.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class AddItemScreen extends StatefulWidget {
  @override
  _AddItemScreenState createState() => _AddItemScreenState();
}

class _AddItemScreenState extends State<AddItemScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _merekCtrl = TextEditingController();
  TextEditingController _nomerSerictrl = TextEditingController();

  String _selectedProdYear;
  String _selectedTipe;
  String _selectedMerk;
  bool _isLoading = false;

  String _fileName;
  String _path;
  File _image;

  final List<String> _listTipe = [
    "GNSS Geodetic",
    "Total Station",
    "Distometer"
  ];

  final List<String> _listMerk = [
    "Topcon",
    "Nikon",
    "Leica",
    "Geomax",
    "Sokkia",
    "Chcnav",
    "South",
    "Hi-target",
    "Trimble",
    "Produk Lain(isi)"
  ];

  final List<String> _listProdYear = [
    '1990',
    '1991',
    '1992',
    '1993',
    '1994',
    '1995',
    '1996',
    '1997',
    '1998',
    '1999',
    '2000',
    '2001',
    '2002',
    '2003',
    '2004',
    '2005',
    '2006',
    '2007',
    '2008',
    '2009',
    '2010',
    '2011',
    '2012',
    '2013',
    '2014',
    '2015',
    '2016',
    '2017',
    '2018',
    '2019',
    '2020',
    '2021'
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
            color: Colors.white,
          ),
          backgroundColor: primaryColor,
          title: Text(
            "Tambah Alat",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ),
        body: ModalProgressHUD(
          child: _body(),
          inAsyncCall: _isLoading,
        ),
        bottomNavigationBar: BottomAppBar(
          child: Builder(
            builder: (context) => InkWell(
              onTap: () async {
                String merkBrand;
                if (_selectedTipe != null &&
                    _selectedMerk != null &&
                    _selectedProdYear != null) {
                  if (_selectedMerk != "Produk Lain(isi)") {
                    merkBrand = _selectedMerk;
                  } else if (_merekCtrl.text != null &&
                      _selectedMerk == "Produk Lain(isi)") {
                    merkBrand = _merekCtrl.text;
                  }
                  print(merkBrand);
                  setState(() {
                    _isLoading = true;
                  });
                  if (_image.path != null) {
                    if (_image.path.toLowerCase().contains("png") ||
                        _image.path.toLowerCase().contains("jpg") ||
                        _image.path.toLowerCase().contains("jpeg")) {
                      String tipe = "jpg";

                      if (_image.path.toLowerCase().contains("png")) {
                        tipe = "png";
                      }
                      int idUser = await Session.getValue(Session.USER_ID);
                      await _onSubmit(
                          _selectedTipe,
                          merkBrand,
                          _nomerSerictrl.text,
                          _selectedProdYear,
                          idUser,
                          _image.path,
                          tipe,
                          context);
                      setState(() {
                        _isLoading = false;
                      });
                    } else {
                      Component.showAppNotification(
                          context, "Gagal", "Lengkapi Data yang anda Masukkan");
                    }
                  }
                }
              },
              child: Container(
                height: 50.0,
                decoration: BoxDecoration(
                  color: primaryColor,
                ),
                child: Center(
                  child: Text(
                    "Tambah",
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: "Sans",
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future getImageGaleri() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 60,
    );
    setState(() {
      _image = image;
      _fileName = image.path != null
          ? (image.path.toLowerCase().contains("png") ||
                  image.path.toLowerCase().contains("jpg") ||
                  image.path.toLowerCase().contains("jpeg")
              ? image.path.split('/').last
              : "Pilih JPG/JPEG, PNG")
          : "Gagal memproses berkas!";
    });
  }

  Future getImageFoto() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.camera,
      imageQuality: 60,
    );
    setState(() {
      _image = image;
      _fileName = image.path != null
          ? (image.path.toLowerCase().contains("png") ||
                  image.path.toLowerCase().contains("jpg") ||
                  image.path.toLowerCase().contains("jpeg")
              ? _path.split('/').last
              : "Pilih JPG/JPEG, PNG")
          : "Gagal memproses berkas!";
    });
  }

  Widget _body() {
    return Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: 140.0,
                  height: 140.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: _image == null
                          ? AssetImage(
                              "assets/img/camera.png",
                            )
                          : FileImage(_image),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(80.0),
                    border: Border.all(
                      color: primaryColor,
                      width: 2.0,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Pilih Foto Alat",
                  style: TextStyle(
                      color: primaryColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: getImageFoto,
                        child: Container(
                          height: 40.0,
                          decoration: BoxDecoration(
                              border: Border.all(), color: primaryColor),
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Text(
                                "Ambil Dari Kamera",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: getImageGaleri,
                        child: Container(
                          height: 40.0,
                          decoration: BoxDecoration(
                              border: Border.all(), color: primaryColor),
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Text(
                                "Ambil Dari Galeri",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Tipe Produk"),
                      DropdownButton(
                        isExpanded: true,
                        hint: Text('Pilih'),
                        // Not necessary for Option 1
                        value: _selectedTipe,
                        onChanged: (newValue) {
                          setState(() {
                            _selectedTipe = newValue;
                          });
                        },
                        items: _listTipe.map((category) {
                          return DropdownMenuItem(
                            child: new Text(category),
                            value: category,
                          );
                        }).toList(),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Merk/Brand Produk"),
                      DropdownButton(
                        isExpanded: true,
                        hint: Text('Pilih'),
                        // Not necessary for Option 1
                        value: _selectedMerk,
                        onChanged: (newValue) {
                          setState(() {
                            _selectedMerk = newValue;
                          });
                        },
                        items: _listMerk.map((category) {
                          return DropdownMenuItem(
                            child: new Text(category),
                            value: category,
                          );
                        }).toList(),
                      ),
                    ],
                  ),
                ),
                (_selectedMerk == "Produk Lain(isi)")
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          autofocus: true,
                          controller: _merekCtrl,
                          textCapitalization: TextCapitalization.sentences,
                          decoration: const InputDecoration(
                            labelText: 'Merk/Brand',
                          ),
                        ),
                      )
                    : Container(),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: _nomerSerictrl,
                    textCapitalization: TextCapitalization.sentences,
                    decoration: const InputDecoration(
                      labelText: 'Nomer Seri',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Tahun Produksi"),
                      DropdownButton(
                        isExpanded: true,
                        hint: Text('Pilih'),
                        // Not necessary for Option 1
                        value: _selectedProdYear,
                        onChanged: (newValue) {
                          setState(() {
                            _selectedProdYear = newValue;
                          });
                        },
                        items: _listProdYear.map((category) {
                          return DropdownMenuItem(
                            child: new Text(category),
                            value: category,
                          );
                        }).toList(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  Future<dynamic> _onSubmit(
      String tipeAlat,
      String merk,
      String nomerSeri,
      String tahunProd,
      int userSurId,
      String path,
      String tipe,
      BuildContext context) async {
    bool dosSaveAlat = await RestAPI.doUploadAlat(
        tipeAlat, merk, nomerSeri, tahunProd, userSurId, path, tipe);
    if (dosSaveAlat) {
      Alert(
        context: context,
        image: Image.asset(
          "assets/alert/check.png",
          color: Colors.red,
        ),
        type: AlertType.success,
        title: "Berhasil",
        desc: "Data Alat Berhasil di Tambahkan !!",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Kembali",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () => Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => UpgardeToSurveyors()),
                    (Route<dynamic> route) => true),
            width: 120,
          )
        ],
      ).show();

      setState(() {
        _isLoading = false;
      });

    } else {
      Component.showAppNotification(context, "Gagal",
          "Tambah Alat Gagal Cek kembali data yang Anda masukkan");
    }
  }
}
