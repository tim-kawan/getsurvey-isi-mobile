import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_survey_app/component/const.dart';

import 'package:get_survey_app/product/ListProduct.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/ServiceGetData.dart';
import 'package:shimmer/shimmer.dart';

class DetailSurveyor extends StatefulWidget {
  final String name;
  final String email;
  final String noAnggota;
  final String img;
  final String alamat;
  final double lat;
  final double long;
  final int id;
  final String rate;
  final int medal;
  final String dateJoin;

  DetailSurveyor(
      {this.alamat,
      this.email,
      this.lat,
      this.img,
      this.long,
      this.name,
      this.noAnggota,
      this.id,
      this.rate,
      this.medal,
      this.dateJoin,
      Key key})
      : super(key: key);

  @override
  _DetailSurveyorState createState() => _DetailSurveyorState();
}

class _DetailSurveyorState extends State<DetailSurveyor> {
  int idUser;
  String ska = "";
  String kompetensi = "";
  String kadastral = "";
  String irk = "";
  List reviewProduct = [];
  List reviewJasa = [];
  int totalReview = 0;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    _getUserId();
    print(widget.img);
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    var smallestDimension = MediaQuery.of(context).size.shortestSide;
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: primaryColor4.withOpacity(0.8),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
          centerTitle: true,
          title: Text(
            "Info Surveyor",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      _buildProfileImage(),
                      _buildSeparator(screenSize),
                      // _subTitle('Kinerja Get-IRK'),
                      // _buildTrackSurveyor(),
                      // _buildSeparator(screenSize),
                      _subTitle('Jasa dan produk yang ditawarkan'),
                      _surveyorService("assets/icons/get_surveyor.svg", 'Jasa',
                          'Survey dan Pemetaan', 1),
                      _surveyorService("assets/icons/get_alatukur.svg",
                          'Produk', 'Alat Ukur (Jual Beli atau Sewa)', 2),
                      _buildSeparator(screenSize),
                      _subTitle('Ulasan dari Pengguna ($totalReview)'),
                      _buildSurveyorRate(),
                      TabBar(
                        labelColor: primaryBlack,
                        indicatorColor: primaryColor4,
                        tabs: [
                          Tab(
                            text: "Jasa",
                          ),
                          Tab(
                            text: "Produk",
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                height: smallestDimension > 600 ? 600 : 400,
                child: TabBarView(
                  children: [
                    Container(
                      child: (_isLoading)
                          ? Shimmer.fromColors(
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Center(
                                  child: Text(
                                    "Tunggu Sebentar...",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 22.5,
                                    ),
                                  ),
                                ),
                              ),
                              baseColor: Colors.grey[700],
                              highlightColor: Colors.grey[200],
                            )
                          : reviewJasa.isNotEmpty
                              ? ListView.builder(
                                  itemCount: reviewJasa.length,
                                  itemBuilder: (context, index) {
                                    return Padding(
                                      padding: const EdgeInsets.only(
                                          left: 16, right: 16, bottom: 2),
                                      child: Card(
                                        child: Padding(
                                          padding: const EdgeInsets.all(8),
                                          child: Column(
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Row(
                                                        children: [
                                                          CircleAvatar(
                                                              radius:
                                                                  smallestDimension >
                                                                          600
                                                                      ? 30
                                                                      : 15,
                                                              backgroundImage: reviewJasa[index]
                                                                              ["userData"]
                                                                          [
                                                                          "image"] !=
                                                                      null
                                                                  ? NetworkImage(
                                                                      reviewJasa[index]
                                                                              [
                                                                              'userData']
                                                                          [
                                                                          'image'])
                                                                  : AssetImage(
                                                                      "assets/logo.png")),
                                                          SizedBox(
                                                            width: 5,
                                                          ),
                                                          Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceEvenly,
                                                            children: [
                                                              Text(
                                                                reviewJasa[index]
                                                                        [
                                                                        'userData']
                                                                    ['nama'],
                                                                style:
                                                                    TextStyle(
                                                                  fontSize:
                                                                      smallestDimension >
                                                                              600
                                                                          ? 17
                                                                          : 12,
                                                                  color:
                                                                      primaryColor4,
                                                                ),
                                                              ),
                                                              Text(
                                                                reviewJasa[
                                                                        index]
                                                                    ['waktu'],
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        smallestDimension >
                                                                                600
                                                                            ? 16
                                                                            : 10),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                      Text(
                                                          reviewJasa[index]
                                                                      [
                                                                      'jasaData'] ==
                                                                  null
                                                              ? "Informasi Rencana Kota"
                                                              : reviewJasa[index]
                                                                      [
                                                                      'jasaData']
                                                                  ['nama'],
                                                          style: TextStyle(
                                                              fontSize:
                                                                  smallestDimension >
                                                                          600
                                                                      ? 16
                                                                      : 11,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                      Text(
                                                        reviewJasa[index]
                                                            ['message'],
                                                        style: TextStyle(
                                                            fontSize:
                                                                smallestDimension >
                                                                        600
                                                                    ? 16
                                                                    : 11,
                                                            fontStyle: FontStyle
                                                                .italic),
                                                      ),
                                                    ],
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Text(
                                                        reviewJasa[index]
                                                                ['rating']
                                                            .toString(),
                                                        style: TextStyle(
                                                          fontSize:
                                                              smallestDimension >
                                                                      600
                                                                  ? 17
                                                                  : 12,
                                                        ),
                                                      ),
                                                      Icon(
                                                        Icons.star,
                                                        size:
                                                            smallestDimension >
                                                                    600
                                                                ? 24
                                                                : 18,
                                                        color: primaryColor4,
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  })
                              : Center(
                                  child: Text(
                                    "Belum Ada Ulasan Terhadap Surveyor",
                                    style: TextStyle(
                                        color: primaryColor,
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                    ),
                    Container(
                      color: Colors.white,
                      padding: EdgeInsets.all(5),
                      child: reviewProduct.isNotEmpty
                          ? ListView.builder(
                              itemCount: reviewProduct.length,
                              itemBuilder: (context, index) {
                                return Card(
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    CircleAvatar(
                                                      backgroundImage: reviewProduct[
                                                                          index]
                                                                      [
                                                                      "userData"]
                                                                  ["image"] ==
                                                              null
                                                          ? NetworkImage(
                                                              reviewProduct[
                                                                          index]
                                                                      [
                                                                      'userData']
                                                                  ['image'])
                                                          : AssetImage(
                                                              "assets/logo.png"),
                                                      radius:
                                                          smallestDimension >
                                                                  600
                                                              ? 30
                                                              : 15,
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: [
                                                        Text(
                                                          reviewProduct[index]
                                                                  ['userData']
                                                              ['nama'],
                                                          style: TextStyle(
                                                            fontSize:
                                                                smallestDimension >
                                                                        600
                                                                    ? 17
                                                                    : 12,
                                                            color:
                                                                primaryColor4,
                                                          ),
                                                        ),
                                                        Text(
                                                          reviewProduct[index]
                                                              ['waktu'],
                                                          style: TextStyle(
                                                              fontSize:
                                                                  smallestDimension >
                                                                          600
                                                                      ? 16
                                                                      : 10),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                                Text(
                                                  reviewProduct[index]
                                                      ['produkData']['nama'],
                                                  style: TextStyle(
                                                      fontSize:
                                                          smallestDimension >
                                                                  600
                                                              ? 16
                                                              : 11,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Text(
                                                  reviewProduct[index]
                                                      ['message'],
                                                  style: TextStyle(
                                                      fontSize:
                                                          smallestDimension >
                                                                  600
                                                              ? 16
                                                              : 11,
                                                      fontStyle:
                                                          FontStyle.italic),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  reviewProduct[index]['rating']
                                                      .toString(),
                                                  style: TextStyle(
                                                    fontSize:
                                                        smallestDimension > 600
                                                            ? 17
                                                            : 12,
                                                  ),
                                                ),
                                                Icon(
                                                  Icons.star,
                                                  size: smallestDimension > 600
                                                      ? 24
                                                      : 18,
                                                  color: primaryColor4,
                                                ),
                                              ],
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              })
                          : Center(
                              child: Text(
                                "Belum Ada Ulasan Terhadap Surveyor",
                                style: TextStyle(
                                    color: primaryColor,
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget iconsRate(int rateProduk) {
    List<Widget> widget = [];
    for (int i = 1; i <= rateProduk; i++) {
      widget.add(Icon(
        Icons.star,
        color: Colors.green,
      ));
    }
    for (int i = 5 - rateProduk; i > 0; i--) {
      widget.add(Icon(
        Icons.star_border,
        color: Colors.grey[700],
      ));
    }
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: widget,
      ),
    );
  }

  void _getUserId() async {
    await API.getListSurveyor(widget.id).then((response) async {
      var result = await json.decode(response.body);
      kompetensi = result['user_surveyor']["no_lisensi"];
      kadastral = result['user_surveyor']["no_lisensi_kadastral"];
      irk = result['user_surveyor']["no_lisensi_irk"];
      ska = result['user_surveyor']["no_ska_skt"];
    });
    List review = await RestAPI.getReview(widget.id, 'jasa', 10, 0);
    review.forEach((e) {
      reviewJasa.add(e);
    });
    List reviewProduk = await RestAPI.getReview(widget.id, "produk", 10, 0);
    reviewProduk.forEach((e) {
      reviewProduct.add(e);
    });
    totalReview = reviewJasa.length + reviewProduct.length;
    setState(() {});
    setState(() {
      _isLoading = false;
    });
  }

  Widget _btnDetailProduct(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(height: 12.0),
        Row(
          children: <Widget>[
            Expanded(
              child: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ListProductScreen(
                                addItem: 1,
                                id: widget.id,
                              )));
                },
                child: Container(
                  height: 40.0,
                  decoration: BoxDecoration(
                    border: Border.all(),
                  ),
                  child: Center(
                    child: Text(
                      "Daftar Jasa",
                      style: TextStyle(
                        color: primaryColor,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10.0),
            Expanded(
              child: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ListProductScreen(
                                addItem: 2,
                                id: widget.id,
                              )));
                },
                child: Container(
                  height: 40.0,
                  decoration: BoxDecoration(
                    border: Border.all(),
                  ),
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text(
                        "Daftar Produk",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20.0),
      ],
    );
  }

  Widget _buildProfileImage() {
    return Row(
      children: <Widget>[
        Flexible(
          flex: 1,
          child: Container(
            height: 120,
            width: 120,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: widget.img == null || widget.img == ""
                    ? AssetImage('assets/GetSurvey.png')
                    : NetworkImage(widget.img),
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.circular(16.0),
            ),
          ),
        ),
        SizedBox(
          width: 5,
        ),
        Flexible(
          flex: 2,
          fit: FlexFit.tight,
          child: Container(
              height: 120,
              width: 120,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.transparent,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      flex: 5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.name,
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: primaryBlack),
                          ),
                          Row(
                            children: [
                              // Icon(Icons.location_on, size: 20,color: primaryBlack,),
                              Flexible(
                                child: Text(widget.alamat,
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: primaryBlack, fontSize: 12)),
                              )
                            ],
                          ),
                          Text(
                              "Bergabung sejak ${widget.dateJoin.substring(6, 10)}",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style:
                                  TextStyle(color: primaryBlack, fontSize: 12)),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      flex: 2,
                      // fit: FlexFit.tight,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Jumlah',
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                          Text(
                            'Lisensi',
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                          Text(
                            widget.medal.toString(),
                            style: TextStyle(
                                fontSize: 14,
                                color: primaryColor4,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )),
        ),
      ],
    );
  }

  Widget _buildTrackSurveyor() {
    return Row(
      children: <Widget>[
        Flexible(
          flex: 1,
          fit: FlexFit.tight,
          child: _buildPekerjaan(
              'Jumlah', 'assets/work_progress/ic_add.svg', '10'),
        ),
        SizedBox(
          width: 5,
        ),
        Flexible(
          flex: 1,
          fit: FlexFit.tight,
          child: _buildPekerjaan(
              'Proses', 'assets/work_progress/ic_process.svg', '2'),
        ),
        SizedBox(
          width: 5,
        ),
        Flexible(
          flex: 1,
          fit: FlexFit.tight,
          child: _buildPekerjaan(
              'Selesai', 'assets/work_progress/ic_done.svg', '8'),
        ),
      ],
    );
  }

  Widget _buildSurveyorRate() {
    return Row(
      children: <Widget>[
        Flexible(
          flex: 1,
          fit: FlexFit.tight,
          child: Text(
            'Rata-rata',
            style: TextStyle(
              fontSize: 12,
            ),
          ),
        ),
        SizedBox(
          width: 5,
        ),
        Flexible(
          flex: 1,
          fit: FlexFit.tight,
          child: _buildRow(),
        ),
      ],
    );
  }

  Widget _subTitle(String subTitle) {
    return Align(
        alignment: Alignment.centerLeft,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text(
            subTitle,
            style: TextStyle(
              fontSize: 12,
              color: primaryBlack,
              fontWeight: FontWeight.bold,
            ),
          ),
        ));
  }

  Widget _surveyorService(String svgIcon, String serviceTitle,
      String serviceSubTitle, int urlService) {
    return ListTile(
      leading: Container(width: 25, child: SvgPicture.asset(svgIcon)),
      title: Text(serviceTitle),
      trailing: Icon(
        Icons.arrow_forward_ios_rounded,
        color: primaryColor4,
        size: 14,
      ),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ListProductScreen(
                      addItem: urlService,
                      id: widget.id,
                    )));
      },
      dense: true,
      subtitle: Text(serviceSubTitle),
    );
  }

  Widget _buildPekerjaan(String subJudul, String svgWork, String workValue) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.transparent,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(2.0),
            child: SvgPicture.asset(
              svgWork,
              width: 40,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: Text(
                  subJudul,
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(2.0),
                child: Text(
                  workValue,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: primaryColor4,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildRow() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.transparent,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Padding(
            padding: const EdgeInsets.all(2.0),
            child: Text(
              widget.rate,
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(2.0),
            child: Icon(
              Icons.star,
              color: primaryColor4,
              size: 40,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSeparator(Size screenSize) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 16.0,
        bottom: 16.0,
      ),
      child: Container(
        width: screenSize.width,
        height: 2.0,
        color: Colors.grey[200],
        margin: EdgeInsets.only(top: 4.0),
      ),
    );
  }
}
