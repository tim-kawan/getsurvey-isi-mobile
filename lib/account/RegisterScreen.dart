import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _noHpController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _passwordVerController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  String _selectedGender;
  int _kelamin = 1;
  bool _isLoading = false;
  File _image;
  bool _obscureText = true;
  bool _obscureVerText = true;
  int gender;

  int maxLength = 6;

  var _txtCustomHead = TextStyle(
    color: primaryColor,
    fontSize: 20.0,
    fontWeight: FontWeight.w600,
    fontFamily: "Gotik",
  );

  /// Custom Text Description for Dialog after user succes payment
  var _txtCustomSub = TextStyle(
    color: Colors.white,
    fontSize: 15.0,
    fontWeight: FontWeight.w500,
    fontFamily: "Gotik",
  );

  Future<bool> _onWillPop() async {
    return (await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Apakah anda yakin ?'),
        content: new Text(
            'Jika Anda Membatalkan Proses akan Membuat data tidak Sepenuhnya Terkirim !!!'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('Tidak'),
          ),
          new FlatButton(
            onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => BtnNavigation(
                          atIndex: 0,
                        ))),
            child: new Text('Iya'),
          ),
        ],
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              _onWillPop();
            },
            color: Colors.white,
          ),
          backgroundColor: primaryColor,
          title: Text(
            "DAFTAR",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ),
        backgroundColor: Colors.white,
        body: ModalProgressHUD(inAsyncCall: _isLoading, child: _body(context)),
      ),
    );
  }

  Future getImageGaleri() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 60,
    );
    setState(() {
      _image = image;
    });
  }

  Future getImageCamera() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.camera,
      imageQuality: 60,
    );
    setState(() {
      _image = image;
    });
  }

  Widget _body(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: ListView(
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        children: [
          SizedBox(
            height: 20,
          ),
          Center(
            child: GestureDetector(
              onTap: () {
                _showPicker(context);
              },
              child: CircleAvatar(
                radius: 55,
                backgroundColor: primaryColor4,
                child: _image != null
                    ? ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child: Image.file(
                          _image,
                          width: 100,
                          height: 100,
                          fit: BoxFit.cover,
                        ),
                      )
                    : Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.circular(50)),
                        width: 100,
                        height: 100,
                        child: Icon(
                          Icons.camera_alt,
                          color: Colors.grey[800],
                        ),
                      ),
              ),
            ),
          ),
          textField(context, "Nama lengkap sesuai KTP",
              "Nama lengkap sesuai KTP", _nameController),
          textField(context, "Email", "Email", _emailController),
          textFieldNumb(
              context, "Nomor Handphone ", "Nomor Handphone", _noHpController),
          textFieldPass(
              context, "Kata Sandi ", "kata Sandi", _passwordController),
          textFieldPassVer(context, "Ulang Kata Sandi ", "Ulang kata Sandi",
              _passwordVerController),
          SizedBox(
            height: 20,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                isExpanded: true,
                hint: Text("Jenis Kelamin"),
                value: _selectedGender,
                items: <String>[
                  'Laki-Laki',
                  'Perempuan',
                ].map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (value) {
                  _selectedGender = value;
                  if (_selectedGender == "Laki-Laki") {
                    gender = 1;
                  } else {
                    gender = 2;
                  }
                  print(gender);
                  setState(() {});
                },
              ),
            ),
            decoration: BoxDecoration(
                border: Border.all(width: 1, color: Colors.grey),
                borderRadius: BorderRadius.circular(14)),
          ),
          SizedBox(
            height: 20,
          ),
          InkWell(
            onTap: () {
              _onSubmit(context);
            },
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 20),
              width: MediaQuery.of(context).size.width,
              child: Center(
                child: Text(
                  "Daftar",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
              decoration: BoxDecoration(
                color: primaryColor4,
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Anda Punya akun? ",
                style: TextStyle(color: Colors.grey),
              ),
              InkWell(
                child: Text(
                  "Masuk",
                  style: TextStyle(color: primaryColor4),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }

  Widget textField(BuildContext context, String label, String hint,
      TextEditingController controller) {
    return Column(
      children: [
        SizedBox(
          height: 20,
        ),
        new TextFormField(
          controller: controller,
          decoration: new InputDecoration(
            labelText: label,
            hintText: hint,
            labelStyle: TextStyle(color: Colors.grey),
            fillColor: Colors.white,
            border: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(15.0),
              borderSide: new BorderSide(color: primaryColor4, width: 1),
            ),
            focusedBorder: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(15.0),
              borderSide: new BorderSide(color: primaryColor4, width: 1),
            ),
            //fillColor: Colors.green
          ),
          validator: (val) {
            if (val.length == 0) {
              return "Form Tidak boleh kosong";
            } else {
              return null;
            }
          },
          keyboardType: TextInputType.text,
          style: new TextStyle(
            fontFamily: "Poppins",
          ),
        ),
      ],
    );
  }

  Widget textFieldNumb(BuildContext context, String label, String hint,
      TextEditingController controller) {
    return Column(
      children: [
        SizedBox(
          height: 20,
        ),
        new TextFormField(
          controller: controller,
          decoration: new InputDecoration(
            labelText: label,
            hintText: hint,
            labelStyle: TextStyle(color: Colors.grey),
            fillColor: Colors.white,
            border: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(15.0),
              borderSide: new BorderSide(color: primaryColor4, width: 1),
            ),
            focusedBorder: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(15.0),
              borderSide: new BorderSide(color: primaryColor4, width: 1),
            ),
            //fillColor: Colors.green
          ),
          validator: (val) {
            if (val.length == 0) {
              return "Form Tidak boleh kosong";
            } else {
              return null;
            }
          },
          keyboardType: TextInputType.phone,
          style: new TextStyle(
            fontFamily: "Poppins",
          ),
        ),
      ],
    );
  }

  Widget textFieldPassVer(BuildContext context, String label, String hint,
      TextEditingController controller) {
    return Column(
      children: [
        SizedBox(
          height: 20,
        ),
        new TextFormField(
          controller: controller,
          obscureText: _obscureVerText,
          decoration: new InputDecoration(
            labelText: label,
            hintText: hint,
            labelStyle: TextStyle(color: Colors.grey),
            fillColor: Colors.white,
            suffixIcon: GestureDetector(
              onTap: () => setState(() {
                _obscureVerText = !_obscureVerText;
              }),
              child: Icon(
                _obscureVerText ? Icons.visibility_off : Icons.visibility,
                color: primaryColor,
                semanticLabel: _obscureVerText ? 'Lihat Sandi' : 'Tutup Sandi',
              ),
            ),
            border: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(15.0),
              borderSide: new BorderSide(color: primaryColor4, width: 1),
            ),
            focusedBorder: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(15.0),
              borderSide: new BorderSide(color: primaryColor4, width: 1),
            ),
            //fillColor: Colors.green
          ),
          validator: (val) {
            if (val.length == 0) {
              return "Form Tidak boleh kosong";
            } else {
              return null;
            }
          },
          keyboardType: TextInputType.text,
          style: new TextStyle(
            fontFamily: "Poppins",
          ),
        ),
      ],
    );
  }

  Widget textFieldPass(BuildContext context, String label, String hint,
      TextEditingController controller) {
    return Column(
      children: [
        SizedBox(
          height: 20,
        ),
        new TextFormField(
          controller: controller,
          obscureText: _obscureText,
          decoration: new InputDecoration(
            labelText: label,
            hintText: hint,
            labelStyle: TextStyle(color: Colors.grey),
            fillColor: Colors.white,
            suffixIcon: GestureDetector(
              onTap: () => setState(() {
                _obscureText = !_obscureText;
              }),
              child: Icon(
                _obscureText ? Icons.visibility_off : Icons.visibility,
                color: primaryColor,
                semanticLabel: _obscureText ? 'Lihat Sandi' : 'Tutup Sandi',
              ),
            ),
            border: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(15.0),
              borderSide: new BorderSide(color: primaryColor4, width: 1),
            ),
            focusedBorder: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(15.0),
              borderSide: new BorderSide(color: primaryColor4, width: 1),
            ),
            //fillColor: Colors.green
          ),
          validator: (val) {
            if (val.length == 0) {
              return "Form Tidak boleh kosong";
            } else {
              return null;
            }
          },
          keyboardType: TextInputType.text,
          style: new TextStyle(
            fontFamily: "Poppins",
          ),
        ),
      ],
    );
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        getImageGaleri();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      getImageCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future<dynamic> _onSubmit(
    BuildContext context,
  ) async {
    setState(() {
      _isLoading = true;
    });
    String fotoUser =
        (_image != null) ? base64.encode(_image.readAsBytesSync()) : null;

    var isRegistered = await RestAPI.doRegister(
        _nameController.text,
        _emailController.text,
        _passwordController.text,
        _noHpController.text,
        fotoUser,
        gender);
    if (fotoUser != null) {
      if (_nameController.text.isNotEmpty &&
          _emailController.text.isNotEmpty &&
          _passwordController.text.isNotEmpty &&
          _noHpController.text.isNotEmpty &&
          gender != null) {
        if (_passwordController.text == _passwordVerController.text) {
          if (isRegistered.success) {
            Alert(
              context: context,
              image: Image.asset(
                "assets/alert/check.png",
                color: Colors.green,
              ),
              type: AlertType.success,
              title: "Berhasil",
              desc: isRegistered.message,
              closeIcon: Container(),
              onWillPopActive: true,
              buttons: [
                DialogButton(
                  child: Text(
                    "Setuju",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  onPressed: () => Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (context) => BtnNavigation(
                                atIndex: 3,
                              )),
                      (Route<dynamic> route) => false),
                  width: 120,
                )
              ],
            ).show();
          } else {
            Component.showAppNotification(
                context, "Register Gagal!", "Email Sudah Pernah digunakan!!");
          }
        } else {
          Component.showAppNotification(context, "Register Gagal!",
              "Pastikan Verifikasi kata sandi benar!!");
        }
      } else {
        Component.showAppNotification(
            context, "Gagal Daftar", "Form daftar tidak boleh kosong");
      }
    } else {
      Component.showAppNotification(
          context, "Gagal Daftar", "Lengkapi Foto Profile anda");
    }
    setState(() {
      _isLoading = false;
    });
  }
}
