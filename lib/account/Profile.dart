import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:get_survey_app/account/EditSurveyor.dart';
import 'package:get_survey_app/account/EditUser.dart';
import 'package:get_survey_app/account/UpgradeToSurveyors.dart';
import 'package:get_survey_app/component/ImageUtils.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/job/TimeLineIrk.dart';
import 'package:get_survey_app/map/MapsScreen.dart';
import 'package:get_survey_app/model/ProductService.dart';
import 'package:get_survey_app/product/ListProduct.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/ServiceGetData.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:location/location.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class UserProfileScreen extends StatefulWidget {
  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen> {
  int idUser = 0;
  int statusSurveyor = -1;
  String fullName = "...";
  String status = "...";
  String email;
  String noTelp;
  int jk;
  String alamat;
  double lat;
  double lon;
  int cekStatusKeanggotaan = -1;
  String img = "";
  bool _isLoading = false;
  final RefreshController _refreshController = RefreshController();

  List<UserSurveyor> listSurveyor = [];
  final DefaultCacheManager _cacheManager = new DefaultCacheManager();

  dynamic imageItem;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        child: SmartRefresher(
            controller: _refreshController,
            enablePullDown: true,
            header: WaterDropHeader(),
            onRefresh: () {
              setState(() {
                _isLoading = true;
              });
              listSurveyor.clear();
              _getUser();
              setState(() {
                _isLoading = false;
              });
              _refreshController.refreshCompleted();
            },
            child: _body(context)),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    imageItem = AssetImage("assets/logo.png");
    _getUser();
    // _getListSurveyorPerId();
  }

  Widget _body(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: screenSize.height / 20.0,
                ),
                _buildProfileImage(),
                _buildFullName(),
                _buildStatus(context),
                SizedBox(height: screenSize.height / 25.0),
                _buildStatContainer(context),
                SizedBox(height: 8.0),
                _buildButtons(context),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Container _buildButtons(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    Container container = Container();

    if (status == "User") {
      container = Container(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => EditUSer()));
                      },
                      child: Container(
                        height: 40.0,
                        decoration: BoxDecoration(
                          border: Border.all(),
                        ),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Text(
                              "Edit Data Diri",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        Alert(
                          context: context,
                          type: AlertType.warning,
                          image: Image.asset(
                            "assets/alert/warning.png",
                            color: Colors.blueAccent[900],
                          ),
                          title: "Keluar Akun",
                          desc: "Anda yakin ingin keluar dari akun ini?",
                          buttons: [
                            DialogButton(
                              child: Text(
                                "Tidak",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                              ),
                              onPressed: () => Navigator.pop(context),
                              color: Colors.red,
                            ),
                            DialogButton(
                              child: Text(
                                "Iya",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                              ),
                              onPressed: () => _doOut(),
                              color: Colors.blueAccent[900],
                            )
                          ],
                        ).show();
                      },
                      child: Container(
                        height: 40.0,
                        decoration: BoxDecoration(
                            border: Border.all(), color: Colors.red),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Text(
                              "Keluar",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => TabbarScreen()));
                },
                child: Container(
                  height: 40.0,
                  decoration: BoxDecoration(
                    border: Border.all(),
                    color: Color(0xFF404A5C),
                  ),
                  child: Center(
                    child: Text(
                      "Proses Pengerjaan IRK",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10.0),
              _buildSeparator(screenSize),
              _buildPengajuan(
                  context, statusSurveyor == -1 || statusSurveyor == 3),
              SizedBox(height: 10.0),
              statusSurveyor == -1 || statusSurveyor == 3
                  ? InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => (statusSurveyor == 3)
                                    ? EditSurveyorScreen(
                                        update: 2,
                                      )
                                    : UpgardeToSurveyors()));
                      },
                      child: Container(
                        height: 40.0,
                        decoration: BoxDecoration(
                          border: Border.all(),
                          color: Color(0xFF404A5C),
                        ),
                        child: Center(
                          child: Text(
                            "Pengajuan Penyedia Jasa",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      );
    } else if (status == "Surveyor") {
      container = Container(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => EditSurveyorScreen(
                                        update: 1,
                                      )));
                        },
                        child: Container(
                          height: 40.0,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(),
                          ),
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Text(
                                "Edit Data Surveyor",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => EditUSer()));
                        },
                        child: Container(
                          height: 40.0,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(),
                          ),
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Text(
                                "Edit Data Diri",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ListProductScreen(
                                        addItem: 1,
                                        id: idUser,
                                        editItem: 1,
                                      )));
                        },
                        child: Container(
                          height: 40.0,
                          decoration: BoxDecoration(
                              border: Border.all(), color: primaryColor),
                          child: Center(
                            child: Text(
                              "Daftar Jasa",
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 10.0),
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ListProductScreen(
                                        addItem: 2,
                                        id: idUser,
                                        editItem: 1,
                                      )));
                        },
                        child: Container(
                          height: 40.0,
                          decoration: BoxDecoration(
                              border: Border.all(), color: primaryColor),
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Text(
                                "Daftar Produk",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
                _buildSeparator(screenSize),
                SizedBox(
                  height: 20.0,
                ),
                InkWell(
                  onTap: () async {
                    await Alert(
                      context: context,
                      type: AlertType.warning,
                      image: Image.asset(
                        "assets/alert/warning.png",
                        color: Colors.blueAccent[900],
                      ),
                      title: "Keluar Akun",
                      desc: "Anda yakin ingin keluar dari akun ini?",
                      buttons: [
                        DialogButton(
                          child: Text(
                            "Tidak",
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                          onPressed: () => Navigator.pop(context),
                          color: Colors.red,
                        ),
                        DialogButton(
                          child: Text(
                            "Iya",
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                          onPressed: () => _doOut(),
                          color: Colors.blueAccent[900],
                        )
                      ],
                    ).show();
                  },
                  child: Container(
                    height: 40.0,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      border: Border.all(),
                    ),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          "Keluar",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
    return container;
  }

  Widget _buildFullName() {
    TextStyle _nameTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: 28.0,
      fontWeight: FontWeight.w700,
    );

    return Text(
      fullName.toString().toUpperCase(),
      textAlign: TextAlign.center,
      style: _nameTextStyle,
    );
  }

  Widget _buildPengajuan(BuildContext context, bool isProposed) {
    return (isProposed)
        ? Container(
            color: Theme.of(context).scaffoldBackgroundColor,
            padding: EdgeInsets.only(top: 8.0),
            child: Text(
              (statusSurveyor == 3)
                  ? "Mengajukan Kembali Menjadi Surveyor?"
                  : "Apakah Anda ingin Menjadi Surveyor ${fullName.toString().toUpperCase().split(" ")[0]} ?",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 16.0,
              ),
            ),
          )
        : Container(
            color: Theme.of(context).scaffoldBackgroundColor,
            padding: EdgeInsets.only(top: 8.0),
            child: Text(
              "Akun Anda Sedang Dalam Proses Pengajuan, Silahkan Menunggu !!",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 16.0,
              ),
            ),
          );
  }

  Widget _buildProfileImage() {
    return Center(
        child: CircleAvatar(
      backgroundImage: imageItem,
      radius: 70.0,
    ));
  }

  Widget _buildSeparator(Size screenSize) {
    return Container(
      width: screenSize.width / 1.6,
      height: 2.0,
      color: Colors.black54,
      margin: EdgeInsets.only(top: 4.0),
    );
  }

  Container _buildStatContainer(BuildContext context) {
    Container container = Container();

    if (status == "User") {
      container = Container(
        margin: EdgeInsets.only(top: 15.0, left: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _buildStatItem(
                "Email",
                email.toString(),
                Icon(
                  Icons.email,
                  color: primaryColor3,
                  size: 20.0,
                ),
                17.0),
            SizedBox(height: 15.0),
            _buildStatItem(
                "No Telp",
                noTelp.toString(),
                Icon(
                  Icons.phone,
                  color: primaryColor3,
                  size: 20.0,
                ),
                17.0),
            SizedBox(height: 15.0),
          ],
        ),
      );
    } else if (status == "Surveyor") {
      container = Container(
        margin: EdgeInsets.only(top: 15.0, left: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _buildStatItem(
                "Email",
                email.toString(),
                Icon(
                  Icons.email,
                  color: primaryColor3,
                  size: 20.0,
                ),
                17.0),
            SizedBox(height: 15.0),
            _buildStatItem(
                "No Telp",
                noTelp.toString(),
                Icon(
                  Icons.phone,
                  color: primaryColor3,
                  size: 20.0,
                ),
                17.0),
            SizedBox(height: 15.0),
            _buildStatItemAddress(
                "Alamat",
                alamat.toString(),
                Icon(
                  Icons.home,
                  color: primaryColor3,
                  size: 20.0,
                ),
                15.0),
            SizedBox(height: 15.0),
            InkWell(
              onTap: () async {
                Location _location = new Location();
                bool _isOn = await _location.requestService();
                PermissionStatus _isAllow = await _location.requestPermission();

                if (_isOn && _isAllow == PermissionStatus.GRANTED) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MapsScreen(
                        tipe: "1",
                        lat: lat,
                        lon: lon,
                      ),
                    ),
                  );
                }
              },
              child: _buildStatItem(
                  "Tap",
                  "Lokasi Berdasarkan Maps",
                  Icon(
                    Icons.location_on,
                    color: primaryColor3,
                    size: 20.0,
                  ),
                  17.0),
            ),
            SizedBox(height: 15.0),
          ],
        ),
      );
    }
    return container;
  }

  Widget _buildStatItem(String label, String count, Icon icon, double size) {
    TextStyle _statLabelTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: size,
    );
    TextStyle _statCountTextStyle = TextStyle(
      color: Colors.black54,
      fontSize: 16,
    );
    return Padding(
      padding: const EdgeInsets.only(
        right: 20.0,
      ),
      child: Container(
        alignment: AlignmentDirectional.center,
        padding: EdgeInsets.only(left: 20.0, right: 30.0, top: 10.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14.0),
            color: Colors.white,
            boxShadow: [BoxShadow(blurRadius: 10.0, color: Colors.black12)]),
        child: Theme(
          data: ThemeData(hintColor: Colors.transparent),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              icon,
              SizedBox(
                width: 20.0,
              ),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      label,
                      style: _statCountTextStyle,
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: Text(
                        count,
                        style: _statLabelTextStyle,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildStatItemAddress(
      String label, String count, Icon icon, double size) {
    TextStyle _statLabelTextStyle = TextStyle(
      fontFamily: 'Roboto',
      color: Colors.black,
      fontSize: size,
    );
    TextStyle _statCountTextStyle = TextStyle(
      color: Colors.black54,
      fontSize: 16,
    );
    return Padding(
      padding: const EdgeInsets.only(right: 20.0),
      child: Container(
        height: 140.0,
        alignment: AlignmentDirectional.center,
        padding: EdgeInsets.only(
          left: 20.0,
          right: 20.0,
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(14.0),
            color: Colors.white,
            boxShadow: [BoxShadow(blurRadius: 10.0, color: Colors.black12)]),
        child: Theme(
          data: ThemeData(hintColor: Colors.transparent),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              icon,
              SizedBox(
                width: 20.0,
              ),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      label,
                      style: _statCountTextStyle,
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Text(
                      count,
                      style: _statLabelTextStyle,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildStatus(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 6.0),
      decoration: BoxDecoration(
        color: Theme.of(context).scaffoldBackgroundColor,
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Text(
        status.toString(),
        style: TextStyle(
          fontFamily: 'Spectral',
          color: Colors.black,
          fontSize: 20.0,
          fontWeight: FontWeight.w300,
        ),
      ),
    );
  }

  _doOut() async {
    setState(() {
      _isLoading = true;
    });
    await Session.doClear();
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => BtnNavigation()),
        (Route<dynamic> route) => false);
  }

  _getUser() async {
    setState(() {
      _isLoading = true;
    });
    String imgAva = await Session.getValue(Session.USER_PHOTO);

      imageItem = NetworkImage(imgAva);
    statusSurveyor = await Session.getValue(Session.USER_LANJUT);
    int type = await Session.getValue(Session.USER_STATUS);
    idUser = await Session.getValue(Session.USER_ID);
    fullName = await Session.getValue(Session.USER_NAME);
    email = await Session.getValue(Session.USER_EMAIL);
    noTelp = await Session.getValue(Session.USER_NO_TELP);
    alamat = await Session.getValue(Session.ALAMAT);
    jk = await Session.getValue(Session.JENIS_KELAMIN);
    lat = await Session.getValue(Session.LAT);
    lon = await Session.getValue(Session.LON);

    if (type > 0) {
      if (statusSurveyor == 1 || statusSurveyor == 2 || statusSurveyor == 3) {
        status = "Surveyor";
      } else {
        status = "User";
      }
    } else {
      status = "User";
    }

    setState(() {});
    setState(() {
      _isLoading = false;
    });
  }
}
