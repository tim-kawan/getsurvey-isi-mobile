import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get_survey_app/account/AddItem.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/map/PickLocation.dart';
import 'package:get_survey_app/model/Alat.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:url_launcher/url_launcher.dart';

class UpgardeToSurveyors extends StatefulWidget {
  @override
  _UpgardeToSurveyorsState createState() => _UpgardeToSurveyorsState();
}

class _UpgardeToSurveyorsState extends State<UpgardeToSurveyors> {
  TextEditingController _noAnggotaController = TextEditingController();
  TextEditingController _tempatLahirController = TextEditingController();
  TextEditingController _nikController = TextEditingController();
  TextEditingController _jurusanPendidikanController = TextEditingController();
  TextEditingController _perguruanTinggiController = TextEditingController();
  TextEditingController _noLisensiKompetensiController =
      TextEditingController();
  TextEditingController _noLisensiKadastralController = TextEditingController();
  TextEditingController _noSkaController = TextEditingController();
  TextEditingController _noLisensiIrkController = TextEditingController();
  TextEditingController _noSktController = TextEditingController();
  TextEditingController _noImbController = TextEditingController();
  TextEditingController _noAskbController = TextEditingController();
  TextEditingController _kotaTempatTinggalController = TextEditingController();
  TextEditingController _alamatController = TextEditingController();

  List<String> _jenjangPendidikan = [
    'SMP',
    'SMA',
    'D3',
    'S1',
    'S2',
    'S3',
  ];

  List<String> _jenjangSurveyor = [
    'Surveyor',
    'Asisten Surveyor',
    'Perusahaan',
  ];

  String _selectedJenjang;
  String _selectedSurveyor;

  final form = GlobalKey<FormState>();
  List<Alat> listAlat = [];
  int kelamin = 1;
  String _fileName = 'Pilih Berkas Lisensi PDF, JPG/JPEG, PNG';
  String _path;

  bool _isLoading = false;
  bool aktifValue = false;
  String failedFile;

  double long;
  double lat;
  String alamat;

  String nama;
  String email;
  String noTelp;
  int jenisKelamin;
  String tanggalLahir = 'Tanggal Lahir';

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => BtnNavigation(
                    atIndex: 2,
                  )),
          (Route<dynamic> route) => false),
      child: SafeArea(
          child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (context) => BtnNavigation(
                            atIndex: 2,
                          )),
                  (Route<dynamic> route) => false);
            },
            color: Colors.white,
          ),
          backgroundColor: primaryColor,
          title: Text(
            "Daftar Menjadi Surveyor",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ),
        body: ModalProgressHUD(inAsyncCall: _isLoading, child: _body()),
        bottomNavigationBar: BottomAppBar(
          child: Builder(
              builder: (context) => InkWell(
                    onTap: () async {
                      if (form.currentState.validate()) {
                        if (lat != null && long != null && alamat != null) {
                          if (_fileName !=
                              'Pilih Berkas Lisensi PDF, JPG/JPEG, PNG') {
                            setState(() {
                              _isLoading = true;
                            });
                            int idUser =
                                await Session.getValue(Session.USER_ID);
                            await _onSubmit(
                                idUser,
                                int.parse(_nikController.text),
                                _tempatLahirController.text,
                                tanggalLahir,
                                _kotaTempatTinggalController.text,
                                _alamatController.text,
                                lat,
                                long,
                                _noAnggotaController.text,
                                _noSkaController.text,
                                _selectedSurveyor,
                                _noLisensiIrkController.text,
                                _noLisensiKadastralController.text,
                                _selectedJenjang,
                                _jurusanPendidikanController.text,
                                _perguruanTinggiController.text,
                                DateFormat('yyyy-MM-dd').format(DateTime.now()),
                                _noLisensiKompetensiController.text,
                                context);
                            setState(() {
                              _isLoading = false;
                            });
                          } else {
                            Component.showAppNotification(
                                context, "Gagal", "Upload File Sertipikat");
                          }
                        } else {
                          Component.showAppNotification(context, "Gagal",
                              "Lenkapi alamat! pilih melalui map");
                        }
                      }
                    },
                    child: Container(
                      height: 50.0,
                      decoration: BoxDecoration(
                        color: primaryColor,
                      ),
                      child: Center(
                        child: Text(
                          "Daftar",
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: "Sans",
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  )),
        ),
      )),
    );
  }

  Future<bool> getAktifValue() async {
    return await Session.getValue("aktifValue") ?? false;
  }

  Future<String> getAlamat() async {
    return await Session.getValue("alamats") ?? "";
  }

  Future<String> getFileName() async {
    return await Session.getValue("_fileName") ??
        "Pilih Berkas Lisensi PDF, JPG/JPEG, PNG";
  }

  Future<String> getFilePath() async {
    return await Session.getValue("_path") ?? "";
  }

  Future<String> getJurusanPen() async {
    return await Session.getValue("jurusanPen") ?? "";
  }

  Future<String> getKotaTempTing() async {
    return await Session.getValue("kota_tempat_tinggal") ?? "";
  }

  Future<String> getNik() async {
    return await Session.getValue("nik") ?? "";
  }

  Future<String> getNoAnggota() async {
    return await Session.getValue("no_anggota") ?? "";
  }

  Future<String> getNoAskb() async {
    return await Session.getValue("no_askb") ?? "";
  }

  Future<String> getNoLisensiKom() async {
    return await Session.getValue("no_lisensi_kompetensi") ?? "";
  }

  Future<String> getNoKadastral() async {
    return await Session.getValue("no_kadastral") ?? "";
  }

  Future<String> getNoLisensi() async {
    return await Session.getValue("no_lisensi") ?? "";
  }

  Future<String> getNoSka() async {
    return await Session.getValue("no_ska") ?? "";
  }

  Future<String> getNoSkb() async {
    return await Session.getValue("no_skb") ?? "";
  }

  Future<String> getNoSkt() async {
    return await Session.getValue("no_skt") ?? "";
  }

  Future<String> getPendidikanSelect() async {
    return await Session.getValue("belajarMentok") ?? null;
  }

  Future<String> getPerguruanTing() async {
    return await Session.getValue("perguruanTing") ?? "";
  }

  Future<String> getSebagaiSelect() async {
    return await Session.getValue("sebagaiSrv") ?? null;
  }

  Future<String> getTanggalLahir() async {
    return await Session.getValue("date") ?? "";
  }

  Future<String> getTempatLahir() async {
    return await Session.getValue("tempat_lahir") ?? "";
  }

  void initData() async {
    await _getUser();
    await _getListAlat();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    initData();

    getNik().then((value) => _nikController.text = value);
    getNoAnggota().then((value) => _noAnggotaController.text = value);
    getNoAskb().then((value) => _noAskbController.text = value);
    getNoSka().then((value) => _noSkaController.text = value);
    getNoSkb().then((value) => _noLisensiIrkController.text = value);
    getNoLisensiKom()
        .then((value) => _noLisensiKompetensiController.text = value);
    getNoLisensi().then((value) => _noLisensiIrkController.text = value);
    getNoKadastral()
        .then((value) => _noLisensiKadastralController.text = value);
    getNoSkt().then((value) => _noSktController.text = value);
    getTempatLahir().then((value) => _tempatLahirController.text = value);
    getKotaTempTing()
        .then((value) => _kotaTempatTinggalController.text = value);
    getTanggalLahir().then((value) => tanggalLahir = value);
    getJurusanPen().then((value) => _jurusanPendidikanController.text = value);
    getPerguruanTing().then((value) => _perguruanTinggiController.text = value);
    getAlamat().then((value) => _alamatController.text = value);
    getAktifValue().then((value) => aktifValue = value);
    getSebagaiSelect().then((value) => _selectedSurveyor = value);
    getPendidikanSelect().then((value) => _selectedJenjang = value);
    getFilePath().then((value) => _path = value);
    getFileName().then((value) => _fileName = value);
  }

  void saveData() async {
    await Session.doSave("aktifValue", aktifValue);
    await Session.doSave("nik", _nikController.text);
    await Session.doSave("no_anggota", _noAnggotaController.text);
    await Session.doSave("no_lisensi", _noLisensiIrkController.text);
    await Session.doSave("no_kadastral", _noLisensiKadastralController.text);
    await Session.doSave("no_ska", _noSkaController.text);
    await Session.doSave(
        "no_lisensi_kompetensi", _noLisensiKompetensiController.text);
    await Session.doSave("no_imb", _noImbController.text);
    await Session.doSave("no_skt", _noSktController.text);
    await Session.doSave("no_askb", _noAskbController.text);
    await Session.doSave("tempat_lahir", _tempatLahirController.text);
    await Session.doSave(
        "kota_tempat_tinggal", _kotaTempatTinggalController.text);
    await Session.doSave("date", tanggalLahir);
    await Session.doSave("jurusanPen", _jurusanPendidikanController.text);
    await Session.doSave("perguruanTing", _perguruanTinggiController.text);
    await Session.doSave("sebagaiSrv", _selectedSurveyor);
    await Session.doSave("belajarMentok", _selectedJenjang);
    await Session.doSave("_fileName", _fileName);
    await Session.doSave("_path", _path);
  }

  Widget _body() {
    return SingleChildScrollView(
      child: Material(
        elevation: 1,
        clipBehavior: Clip.antiAlias,
        child: Form(
          key: form,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              AppBar(
                automaticallyImplyLeading: false,
                elevation: 0,
                title: Text('Data Diri'),
                backgroundColor: primaryColor,
                centerTitle: true,
              ),
              Padding(
                  padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Container(
                    height: 50.0,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text("Nama",
                            style:
                                TextStyle(fontSize: 14.0, color: Colors.blue)),
                        SizedBox(
                          height: 5.0,
                        ),
                        Flexible(
                          child: Text(
                            nama.toString().toUpperCase(),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 16.0),
                          ),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Container(
                          height: 2.0,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.blue,
                        )
                      ],
                    ),
                  )),
              Padding(
                  padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Container(
                    height: 50.0,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          "Email ",
                          style: TextStyle(fontSize: 14.0, color: Colors.blue),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          email.toString(),
                          style: TextStyle(fontSize: 16.0),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Container(
                          height: 2.0,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.blue,
                        )
                      ],
                    ),
                  )),
              Padding(
                  padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Container(
                    height: 50.0,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          "Jenis Kelamin",
                          style: TextStyle(fontSize: 14.0, color: Colors.blue),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          jenisKelamin == 0 ? "Perempuan" : "Laki-laki",
                          style: TextStyle(fontSize: 16.0),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Container(
                          height: 2.0,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.blue,
                        )
                      ],
                    ),
                  )),
              Padding(
                  padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
                  child: Container(
                    height: 50.0,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          "No Telepon",
                          style: TextStyle(fontSize: 14.0, color: Colors.blue),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          noTelp.toString(),
                          style: TextStyle(fontSize: 16.0),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Container(
                          height: 2.0,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.blue,
                        )
                      ],
                    ),
                  )),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: TextFormField(
                  controller: _nikController,
                  validator: (val) =>
                      val.length > 14 ? null : 'No KTP Tidak valid',
                  decoration: InputDecoration(
                    labelText: 'No Kartu Identitas(NIK)',
                    hintText: 'Contoh : 3456789xxx',
                    isDense: true,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: TextFormField(
                  controller: _tempatLahirController,
                  validator: (val) =>
                      val.length > 0 ? null : 'Tempat Lahir tidak Valid',
                  decoration: InputDecoration(
                    labelText: 'Tempat Lahir',
                    hintText: 'Contoh : Jakarta',
                    isDense: true,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 50.0,
                      width: MediaQuery.of(context).size.width / 2,
                      color: Colors.white,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              tanggalLahir,
                              style: TextStyle(
                                  color: Colors.black, fontSize: 16.0),
                            ),
                            Container(
                              height: 2.0,
                              width: MediaQuery.of(context).size.width,
                              color: Colors.blue,
                            )
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        saveData();
                        DatePicker.showDatePicker(context,
                            theme: DatePickerTheme(
                              containerHeight: 210.0,
                            ),
                            showTitleActions: true,
                            minTime: DateTime(1950, 01, 01),
                            maxTime: DateTime.now(), onConfirm: (date) {
                          String formattedDate =
                              DateFormat('yyyy-MM-dd').format(date);
                          tanggalLahir = formattedDate;
                          setState(() {});
                        },
                            currentTime: DateTime(1988, 05, 21),
                            locale: LocaleType.id);
                      },
                      child: Container(
                        height: 50,
                        color: Colors.blue[900],
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Center(
                            child: Text(
                              "Pilih Tanggal",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
              AppBar(
                automaticallyImplyLeading: false,
                elevation: 0,
                title: Text('Kelengkapan Data Anggota'),
                backgroundColor: primaryColor,
                centerTitle: true,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text('Mengajukan Sebagai',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w700)),
                    ),
                    DropdownButton(
                      isExpanded: true,
                      hint: Text('Kategori GetSurvey (default: Surveyor)'),
                      // Not necessary for Option 1
                      value: _selectedSurveyor,
                      onChanged: (newValue) {
                        setState(() {
                          _selectedSurveyor = newValue;
                        });
                      },
                      items: _jenjangSurveyor.map((category) {
                        return DropdownMenuItem(
                          child: new Text(category),
                          value: category,
                        );
                      }).toList(),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: TextFormField(
                  controller: _noAnggotaController,
                  validator: (val) =>
                      val.length > 0 ? null : 'No Anggota Tidak Valid',
                  decoration: InputDecoration(
                    labelText: 'No Anggota ISI*',
                    hintText: 'Contoh : 765434xxx',
                    isDense: true,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
                child: Text(
                  "Jika Anda Belum Terdaftar Sebagai Anggota ISI Silahkan Daftarkan Diri Anda Untuk Mendapatkan No Anggota Isi ",
                  style: TextStyle(color: Colors.grey[700]),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(left: 8.0, top: 3.0, bottom: 3.0),
                child: InkWell(
                  onTap: () =>
                      launch('http://www.isi.or.id/keanggotaan/daftar/'),
                  child: Container(
                    width: 100.0,
                    height: 50.0,
                    color: primaryColor,
                    child: Center(
                      child: Text(
                        "Daftar",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: Row(
                  children: [
                    Expanded(
                      child: Image.asset(
                        "assets/medali/kompetensi.png",
                        height: 30,
                      ),
                      flex: 1,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      flex: 8,
                      child: TextFormField(
                        controller: _noLisensiKompetensiController,
                        validator: (val) => val.length > 0
                            ? null
                            : 'No Lisensi kompetensi Tidak Valid',
                        decoration: InputDecoration(
                          labelText: 'No Sertipikat kompetensi*',
                          hintText: 'Contoh : 345678xxx',
                          isDense: true,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: Row(
                  children: [
                    Expanded(
                      child: Image.asset(
                        "assets/medali/ska.png",
                        height: 30,
                      ),
                      flex: 1,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      flex: 8,
                      child: TextFormField(
                        controller: _noSkaController,
                        decoration: InputDecoration(
                          labelText: 'No SKA/SKT',
                          hintText: 'Contoh : 56789xxx',
                          isDense: true,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: Row(
                  children: [
                    Expanded(
                      child: Image.asset(
                        "assets/medali/kadastral.png",
                        height: 30,
                      ),
                      flex: 1,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      flex: 8,
                      child: TextFormField(
                        controller: _noLisensiKadastralController,
                        decoration: InputDecoration(
                          labelText: 'No Lisensi Kadastral',
                          hintText: 'Contoh : 345678xxx',
                          isDense: true,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: Row(
                  children: [
                    Expanded(
                      child: Image.asset(
                        "assets/medali/irk.png",
                        height: 30,
                      ),
                      flex: 1,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      flex: 8,
                      child: TextFormField(
                        controller: _noLisensiIrkController,
                        decoration: InputDecoration(
                          labelText: 'No Lisensi IRK',
                          hintText: 'Contoh : 56789xxx',
                          isDense: true,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
              AppBar(
                automaticallyImplyLeading: false,
                elevation: 0,
                title: Text('Kelengkapan Alamat'),
                backgroundColor: primaryColor,
                centerTitle: true,
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: TextFormField(
                  controller: _kotaTempatTinggalController,
                  validator: (val) =>
                      val.length > 0 ? null : 'Kota Tempat Tinggal Tidak Valid',
                  decoration: InputDecoration(
                    labelText: 'Kota Tempat Tinggal',
                    hintText: 'Contoh : Jakarta',
                    isDense: true,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: TextFormField(
                  controller: _alamatController,
                  validator: (val) =>
                      val.length > 0 ? null : 'Alamat Lengkap tidak Valid',
                  maxLines: 3,
                  decoration: InputDecoration(
                    labelText: 'Alamat Lengkap',
                    isDense: true,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Align(
                alignment: Alignment.center,
                child: InkWell(
                  onTap: () {
                    saveData();
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PickLocation(
                                  navigate: 0,
                                )));
                  },
                  child: Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width / 2,
                    color: Colors.blue[900],
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          "Pilih Lokasi Melalui Map",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              AppBar(
                automaticallyImplyLeading: false,
                elevation: 0,
                title: Text('Kelengkapan Data Pendidikan'),
                backgroundColor: primaryColor,
                centerTitle: true,
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: TextFormField(
                  controller: _jurusanPendidikanController,
                  validator: (val) =>
                      val.length > 0 ? null : 'Jurusan Pendidikan Tidak Valid',
                  decoration: InputDecoration(
                    labelText: 'Jurusan Pendidikan',
                    isDense: true,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text('Pendidikan Terakhir',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w700)),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    DropdownButton(
                      isExpanded: true,
                      hint: Text("Pilih Pendidikan Terakhir"),
                      // Not necessary for Option 1
                      value: _selectedJenjang,
                      onChanged: (newValue) {
                        setState(() {
                          _selectedJenjang = newValue;
                        });
                      },
                      items: _jenjangPendidikan.map((jenjang) {
                        return DropdownMenuItem(
                          child: new Text(jenjang),
                          value: jenjang,
                        );
                      }).toList(),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: TextFormField(
                  controller: _perguruanTinggiController,
                  validator: (val) =>
                      val.length > 0 ? null : 'Perguruan Tinggi',
                  decoration: InputDecoration(
                    labelText: 'Nama Perguruan Tinggi',
                    isDense: true,
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              AppBar(
                automaticallyImplyLeading: false,
                elevation: 0,
                title: Text('Kelengkapan Berkas'),
                backgroundColor: primaryColor,
                centerTitle: true,
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 16, right: 16, top: 16, bottom: 40),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Pilih Berkas Lisensi",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16.0,
                              fontWeight: FontWeight.w700),
                        ),
                        Text(
                          _fileName,
                          style: TextStyle(color: Colors.black, fontSize: 16.0),
                        ),
                        Container(
                          height: 2.0,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.blue,
                        ),
                        Text(
                          failedFile == null ? "" : failedFile,
                          style: TextStyle(color: Colors.red, fontSize: 16.0),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(
                    top: 8,
                    bottom: 8,
                  ),
                  child: Center(
                    child: RaisedButton(
                      onPressed: _openFileExplorer,
                      child: Text(
                        "Pilih File Sertipikat",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                            fontWeight: FontWeight.w700),
                      ),
                      color: primaryColor,
                      padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                    ),
                  )),
              Padding(
                padding: (aktifValue == false)
                    ? EdgeInsets.fromLTRB(8, 0, 8, 15)
                    : EdgeInsets.fromLTRB(8, 0, 8, 4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Checkbox(
                      value: aktifValue,
                      onChanged: (bool value) {
                        setState(() {
                          aktifValue = value;
                        });
                      },
                    ),
                    Text("Memiliki Peralatan Survey GPS/ Total Station"),
                  ],
                ),
              ),
              (aktifValue == true)
                  ? Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: InkWell(
                          onTap: () {
                            saveData();
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddItemScreen()));
                          },
                          child: Container(
                            height: 50,
                            width: 100,
                            child: Center(
                              child: Text(
                                "Tambah Alat",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: primaryColor),
                          ),
                        ),
                      ),
                    )
                  : Container(),
              SizedBox(
                height: 15,
              ),
              listAlat.length > 0 && aktifValue == true
                  ? Container(
                      width: MediaQuery.of(context).size.width,
                      height: 350,
                      child: ListView.builder(
                          itemCount: listAlat.length,
                          itemBuilder: (context, index) {
                            return Center(
                              child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Stack(
                                  children: <Widget>[
                                    Container(
                                      height: 150,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(20),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.black,
                                            blurRadius: 5,
                                            offset: Offset(0, 1),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Positioned.fill(
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      5, 15, 5, 15),
                                              child: Container(
                                                child: Image.network(
                                                  listAlat[index].image,
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            flex: 2,
                                          ),
                                          Expanded(
                                            flex: 4,
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  "Tipe  = " +
                                                      listAlat[index].tipe,
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily: 'Avenir',
                                                      fontWeight:
                                                          FontWeight.w700),
                                                ),
                                                Text(
                                                  "Merek = " +
                                                      listAlat[index].merek,
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: 'Avenir',
                                                  ),
                                                ),
                                                Text(
                                                  "Nomer Seri = " +
                                                      listAlat[index].nomerSeri,
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: 'Avenir',
                                                  ),
                                                ),
                                                Text(
                                                  "Tahun Prod = " +
                                                      listAlat[index]
                                                          .tahunProduksi,
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: 'Avenir',
                                                  ),
                                                ),
                                                SizedBox(height: 16),
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            child: Builder(
                                              builder: (context) => IconButton(
                                                  icon: Icon(Icons.delete,
                                                      color: Colors.black),
                                                  onPressed: () {
                                                    saveData();
                                                    showAlertDialog(
                                                        context, index);
                                                  }),
                                            ),
                                            flex: 2,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }),
                    )
                  : Container(),
              SizedBox(
                height: 30.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context, int index) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Iya"),
      onPressed: () async {
        await _onDelete(listAlat[index].id, context);
        listAlat.removeAt(index);
        setState(() {});
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => UpgardeToSurveyors()));
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Tidak"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Hapus Data"),
      content: Text("Anda Yakin ingin menghapus Data Alat ini?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _getListAlat() async {
    int id = await Session.getValue(Session.USER_ID);

    await RestAPI.getAlat(id).then((results) async {
      results.forEach((irk) {
        listAlat.add(Alat.fromJson(irk));
      });
    });
  }

  _getUser() async {
    nama = await Session.getValue(Session.USER_NAME);
    noTelp = await Session.getValue(Session.USER_NO_TELP);
    email = await Session.getValue(Session.USER_EMAIL);
    jenisKelamin = await Session.getValue(Session.JENIS_KELAMIN);
    long = await Session.getValue("longitude");
    lat = await Session.getValue("latitude");
    alamat = await Session.getValue("alamats");
    setState(() {});
  }

  Future<dynamic> _onDelete(int id, BuildContext context) async {
    int isUpgrade = await RestAPI.deleteAlat(id);
    if (isUpgrade == 1) {
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text("Data Berhasil Dihapus")));
    } else {
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text("Coba lagi nanti")));
    }
  }

  Future<dynamic> _onSubmit(
    int id,
    int nik,
    String tempatLahir,
    String tanggalLahir,
    String kotaTempatTinggal,
    String alamatLengkap,
    double lat,
    double long,
    String noAnggota,
    String noSka,
    String sebagai,
    String noLisensIrki,
    String noLisensiKadastral,
    String jenjangPendidikan,
    String jurusanPendidikan,
    String perguruanTinggi,
    String daftar,
    String nolisensiKompetensi,
    BuildContext context,
  ) async {
    if (_selectedSurveyor == null) {
      sebagai = "Surveyor";
    }

    bool isUpgraded = await RestAPI.doUpgrade(
        id: id,
        nik: nik,
        tempatLahir: tempatLahir,
        tanggalLahir: tanggalLahir,
        long: long,
        lat: lat,
        noAnggota: noAnggota,
        noLisensiIrk: noLisensIrki,
        noLisensiKadastral: noLisensiKadastral,
        noLisensiKompetensi: nolisensiKompetensi,
        noSka: noSka,
        alamatLengkap: alamatLengkap,
        daftar: daftar,
        jenjangPendidikan: jenjangPendidikan,
        jurusanPendidikan: jurusanPendidikan,
        kotaTempatTinggal: kotaTempatTinggal,
        perguruanTinggi: perguruanTinggi,
        sebagai: sebagai);

    if (isUpgraded) {
      if (_path != null) {
        if (_path.toLowerCase().contains("pdf") ||
            _path.toLowerCase().contains("png") ||
            _path.toLowerCase().contains("jpg") ||
            _path.toLowerCase().contains("jpeg")) {
          String tipe = "jpg";

          if (_path.toLowerCase().contains("pdf")) {
            tipe = "pdf";
          } else if (_path.toLowerCase().contains("png")) {
            tipe = "png";
          }

          var upload =await RestAPI.doUpload(id, _path, tipe);
          print(upload);
          Alert(
            context: context,
            image: Image.asset(
              "assets/alert/check.png",
              color: Colors.red,
            ),
            type: AlertType.success,
            title: "Berhasil",
            desc:
                "Data Sedang Dalam Proses Pengajuan Silahkan Menunggu Notifikasi Dari email!! ",
            closeIcon: Container(),
            onWillPopActive: true,
            buttons: [
              DialogButton(
                child: Text(
                  "Kembali",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () async {
                  await Session.doClear();
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => BtnNavigation(
                                atIndex: 2,
                              )));
                },
                width: 120,
              )
            ],
          ).show();
        }
      } else {
        Alert(
          context: context,
          image: Image.asset(
            "assets/alert/check.png",
            color: Colors.red,
          ),
          type: AlertType.success,
          title: "Berhasil",
          desc:
              "Data Sedang Dalam Proses Pengajuan Silahkan Menunggu Notifikasi Dari email!! ",
          closeIcon: Container(),
          onWillPopActive: true,
          buttons: [
            DialogButton(
              child: Text(
                "Kembali",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () async {
                await Session.doClear();
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => BtnNavigation(
                              atIndex: 2,
                            )));
              },
              width: 120,
            )
          ],
        ).show();
      }
    } else {
      Component.showAppNotification(
        context,
        "Gagal!",
        "Proses pengajuan menjadi Surveyor gagal, Pastikan semua data terisi dengan benar!",
      );
    }
  }

  void _openFileExplorer() async {
    _fileName = 'Pilih Berkas Lisensi PDF, JPG/JPEG, PNG';
    failedFile = "";
    try {
      _path = await FilePicker.getFilePath();
      if (_path != null) {
        if (_path.toLowerCase().contains(".pdf") == false &&
            _path.toLowerCase().contains(".png") == false &&
            _path.toLowerCase().contains(".jpg") == false &&
            _path.toLowerCase().contains(".jpeg") == false) {
          failedFile =
              "Gagal!! Pastikan file anda memilkki extension seperti contoh( .pdf, .png, .jpeg, .jpg) dibelakang nama file anda, ubah nama file dan sesuaikan dengan format file anda";
          _path = null;
          _fileName = "Gagal Memproses Berkas!";
        } else {
          _fileName = (_path.toLowerCase().contains("pdf") ||
                  _path.toLowerCase().contains("png") ||
                  _path.toLowerCase().contains("jpg") ||
                  _path.toLowerCase().contains("jpeg")
              ? _path.split('/').last
              : "Pilih Berkas Lisensi PDF, JPG/JPEG, PNG");
        }
      } else {
        _fileName = "Gagal Memproses Berkas!";
        _path = null;
      }
      setState(() {});
    } on PlatformException catch (e) {
      print(e.message);
    }
  }
}
