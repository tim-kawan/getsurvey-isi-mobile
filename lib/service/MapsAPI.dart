import 'package:get_survey_app/service/RestAPI.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/distance.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:url_launcher/url_launcher.dart';

class MapsAPI {
  MapsAPI._();

  static Future<List<Element>> distanceMatrix(
      LatLng asal, LatLng tujuan) async {
    final distanceMatrix =
        new GoogleDistanceMatrix(apiKey: RestAPI.GOOGLE_API_KEY);
    List<Element> distanceValues = [];
    DistanceResponse distanceResponse = await distanceMatrix
        .distanceWithLocation([
      Location(asal.latitude, asal.longitude)
    ], [
      Location(tujuan.latitude, tujuan.longitude)
    ], travelMode: TravelMode.driving, languageCode: "id");

    if (distanceResponse.isOkay) {
      distanceResponse.results.forEach((element) {
        element.elements.forEach((inner) {
          distanceValues.add(inner);
        });
      });
    }

    return distanceValues;
  }

  static void openUrlMaps(LatLng origin, LatLng destination) async {
    String parameters = [
      "origin=${origin.latitude},${origin.longitude}",
      "destination=${destination.latitude},${destination.longitude}",
      "travelmode=driving",
      "dir_action=navigate"
    ].join("&");
    final url = "https://www.google.com/maps/dir/?api=1&$parameters";

    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  static Future<List<dynamic>> searchPlaces(String lokasi) async {
    final places = new GoogleMapsPlaces(apiKey: RestAPI.GOOGLE_API_KEY);
    PlacesSearchResponse placesResponse = await places.searchByText(lokasi);
    List results = [];

    if (placesResponse.isOkay) {
      placesResponse.results.forEach((element) {
        results.add({
          "name": element.name,
          "address": element.formattedAddress,
          "lat": element.geometry.location.lat,
          "lng": element.geometry.location.lng,
        });
      });
    }

    return results;
  }
}
