import 'package:get_survey_app/model/ChatModel.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class DBProvider {
  static final String _dbName = 'cfm_system.db';
  static Database _database;

  DBProvider._();

  static Future<bool> chatDelete(int room) async {
    final db = await _getDB();
    int affected = await db.delete(
      "data_chat",
      where: "at = ?",
      whereArgs: [room],
    );

    return (affected > 0);
  }

  static Future<bool> chatInsert(ChatModel chat) async {
    final db = await _getDB();
    int latestID = await db.insert("data_chat", chat.toMap());

    return (latestID > 0);
  }

  static Future<ChatModel> chatRecord(int id) async {
    final db = await _getDB();
    List<Map> queries = await db.query(
      "data_chat",
      where: "id = ?",
      whereArgs: [id],
    );

    return ((queries.isNotEmpty) ? ChatModel.fromMap(queries.first) : null);
  }

  static Future<bool> chatRemove(int id) async {
    final db = await _getDB();
    int affected = await db.delete(
      "data_chat",
      where: "id = ?",
      whereArgs: [id],
    );

    return (affected > 0);
  }

  static Future<List<ChatModel>> chatSee(int room) async {
    final db = await _getDB();
    List chats = <ChatModel>[];
    List<Map> queries = await db.query(
      "data_chat",
      where: "at = ?",
      whereArgs: [room],
    );

    queries.forEach((query) {
      chats.add(ChatModel.fromMap(query));
    });

    return chats;
  }

  static Future<void> closing() async {
    final db = await _getDB();
    db.close().then((_) => _database = null);
  }

  static Future<Database> _getDB() async {
    if (_database != null) {
      return _database;
    }

    _database = await _initDatabase();

    return _database;
  }

  static Future<Database> _initDatabase() async {
    var appDocDir = await getApplicationDocumentsDirectory();
    String path = join(appDocDir.path, _dbName);

    return await openDatabase(path, version: 1,
        onCreate: (database, version) async {
      await database.transaction((transaction) async {
        await transaction.execute('''
          CREATE TABLE data_chat(
            id INT PRIMARY KEY,
            at INT,
            sender INT,
            chat TEXT,
            date TEXT,
            room TEXT,
            isMe INT
          )''');
      });
    });
  }
}
