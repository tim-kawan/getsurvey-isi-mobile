import 'dart:convert';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get_survey_app/chat/Chat.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';

class Helper {
  static BuildContext _context;
  static GlobalKey<NavigatorState> navigatorKey;

  Helper._();

  static Future<bool> fcmOut() async {
    return await FirebaseMessaging().deleteInstanceID();
  }

  static Future<String> fcmToken(BuildContext context) async {
    final FirebaseMessaging firebase = FirebaseMessaging();
    _context = context;

    if (Platform.isIOS) {
      firebase.requestNotificationPermissions(IosNotificationSettings(
        sound: true,
        badge: true,
        alert: true,
      ));
      // firebase.onIosSettingsRegistered
      //     .listen((IosNotificationSettings settings) async {
      return await firebase.getToken();
      // });
    } else if (Platform.isAndroid) {
      return await firebase.getToken();
    }
  }

  static initFirebase(BuildContext context, GlobalKey<NavigatorState> navKey) {
    final FirebaseMessaging firebase = FirebaseMessaging();
    navigatorKey = navKey;
    _context = context;

    try {
      firebase.configure(
        onMessage: (Map<String, dynamic> message) async {
          _showNotification(context, message);
        },
        onResume: (Map<String, dynamic> message) async {
          _showNotification(context, message);
        },
        onLaunch: (Map<String, dynamic> message) async {
          _showNotification(context, message);
        },
        onBackgroundMessage: (Map<String, dynamic> message) async {
          _showNotification(context, message);
        },
      );
      firebase.onTokenRefresh.listen((token) async {
        bool isSignedIn = await Session.isExist(Session.USER_ID);

        if (isSignedIn) {
          int idUser = await Session.getValue(Session.USER_ID);
          await RestAPI.saveToken(idUser, token);
        }
      });
    } catch (e) {}
  }

  static Future _iOSLocalNotif(
    int id,
    String title,
    String body,
    String payload,
  ) {
    return Future<void>.value();
  }

  static Future _selectAction(String payload) async {
    Map object = json.decode(payload);

    if (object.containsKey("chat")) {
      Navigator.push(
          _context,
          MaterialPageRoute(
              builder: (_) => ChatScreen(
                    room: object['room'],
                  )));
    }

    return Future<void>.value();
  }

  static void _showNotification(
    BuildContext context,
    Map<String, dynamic> message,
  ) async {
    _context = context;
    FlutterLocalNotificationsPlugin notificationsPlugin =
        FlutterLocalNotificationsPlugin();
    await notificationsPlugin.show(
      21,
      message['aps']['alert']["title"], //ios
      message['aps']['alert']["body"], //ios
      // message["notification"]["title"],
      // message["notification"]["body"],
      NotificationDetails(
        AndroidNotificationDetails(
            "getsurvey-c2c51", "GetSurvey", "GetSurvey Android Application",
            importance: Importance.Max,
            priority: Priority.High,
            playSound: true,
            sound: RawResourceAndroidNotificationSound("dungdung"),
            icon: 'ic_launcher'),
        IOSNotificationDetails(),
      ),
      payload: message["extra"], //ios
      // payload: message["data"]["extra"],
    );
    await notificationsPlugin.initialize(
        InitializationSettings(
          AndroidInitializationSettings("ic_launcher"),
          IOSInitializationSettings(
              onDidReceiveLocalNotification: _iOSLocalNotif,
              requestBadgePermission: false,
              requestAlertPermission: false,
              requestSoundPermission: false),
        ),
        onSelectNotification: _selectAction);
  }
}
