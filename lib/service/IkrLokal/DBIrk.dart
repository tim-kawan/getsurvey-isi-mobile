import 'package:get_survey_app/model/irk.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class DBProviderIRK {
  static final String _dbName = 'irk.db';
  static Database _database;

  DBProviderIRK._();

  static Future<bool> irkInsert(IrkModel irk) async {
    final db = await _getDB();
    int latestID = await db.insert("dataIrk", irk.toMap());

    return (latestID > 0);
  }

  static Future<IrkModel> irkRecord(int id) async {
    final db = await _getDB();
    List<Map> queries = await db.query(
      "dataIrk",
      where: "id = ?",
      whereArgs: [id],
    );

    return ((queries.isNotEmpty) ? IrkModel.fromMap(queries.first) : null);
  }

  static Future<void> closing() async {
    final db = await _getDB();
    db.close().then((_) => _database = null);
  }

  static Future<Database> _getDB() async {
    if (_database != null) {
      return _database;
    }

    _database = await _initDatabase();

    return _database;
  }

  static Future<Database> _initDatabase() async {
    var appDocDir = await getApplicationDocumentsDirectory();
    String path = join(appDocDir.path, _dbName);

    return await openDatabase(path, version: 1,
        onCreate: (database, version) async {
      await database.transaction((transaction) async {
        await transaction.execute('''
            CREATE TABLE dataIrk(
                id INT PRIMARY KEY,
                id_irk INT
          )''');
      });
    });
  }
}

// class DBProviderIRK {
//   static DBProviderIRK _dbHelper;
//   static Database _database;

//   DBProviderIRK._createObject();

//   factory DBProviderIRK() {
//     if (_dbHelper == null) {
//       _dbHelper = DBProviderIRK._createObject();
//     }
//     return _dbHelper;
//   }

//   Future<Database> initDb() async {
//     //untuk menentukan nama database dan lokasi yg dibuat
//     Directory directory = await getApplicationDocumentsDirectory();
//     String path = directory.path + 'contact.db';

//     //create, read databases
//     var todoDatabase = openDatabase(path, version: 1, onCreate: _createDb);

//     //mengembalikan nilai object sebagai hasil dari fungsinya
//     return todoDatabase;
//   }

//   //buat tabel baru dengan nama contact
//   void _createDb(Database db, int version) async {
//     await db.execute('''
//         CREATE TABLE dataIrk(
//             id INT PRIMARY KEY,
//             id_irk INT,
//             nama_pem TEXT,
//             luas TEXT
//       )
//     ''');
//   }

//   Future<Database> get database async {
//     if (_database == null) {
//       _database = await initDb();
//     }
//     return _database;
//   }

//   Future<List<Map<String, dynamic>>> select() async {
//     Database db = await this.database;
//     var mapList = await db.query('dataIrk', orderBy: 'name');
//     return mapList;
//   }

// //create databases
//   Future<int> insert(IrkModel object) async {
//     Database db = await this.database;
//     int count = await db.insert('dataIrk', object.toMap());
//     return count;
//   }

// //update databases
//   Future<int> update(IrkModel object) async {
//     Database db = await this.database;
//     int count = await db.update('dataIrk', object.toMap(),
//         where: 'id=?', whereArgs: [object.id]);
//     return count;
//   }

// //delete databases
//   Future<int> delete(int id) async {
//     Database db = await this.database;
//     int count = await db.delete('dataIrk', where: 'id=?', whereArgs: [id]);
//     return count;
//   }

//   Future<List<IrkModel>> getContactList() async {
//     var contactMapList = await select();
//     int count = contactMapList.length;
//     List<IrkModel> contactList = List<IrkModel>();
//     for (int i = 0; i < count; i++) {
//       contactList.add(IrkModel.fromMap(contactMapList[i]));
//     }
//     return contactList;
//   }
// }
