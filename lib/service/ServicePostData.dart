import 'dart:convert';

import 'package:get_survey_app/service/RestAPI.dart';
import 'package:http/http.dart' as http;

Future<http.Response> addGaleries(foto, produk, jasa, ke) async {
  var url = RestAPI.API_URL + '/ps-galleries';

  Map data = {"foto": foto, "produk": produk, "jasa": jasa, "ke": ke};
  var body = json.encode(data);

  var response = await http.post(url,
      headers: {
        "Content-Type": "application/json",
      },
      body: body);
  return response;
}

Future<http.Response> addJasa(nama, deskripsi, userId) async {
  var url = RestAPI.API_URL + '/services';

  Map data = {"nama": nama, "deskripsi": deskripsi, "user": userId};
  var body = json.encode(data);

  var response = await http.post(url,
      headers: {
        "Content-Type": "application/json",
      },
      body: body);
  return response;
}

Future<http.Response> addProduct(nama, harga, deskripsi, userId) async {
  var url = RestAPI.API_URL + '/products';

  Map data = {
    "nama": nama,
    "harga": harga,
    "deskripsi": deskripsi,
    "user": userId,
  };
  var body = json.encode(data);

  var response = await http.post(url,
      headers: {
        "Content-Type": "application/json",
      },
      body: body);
  return response;
}

Future<http.Response> editGaleries(foto, produk, jasa, id, ke) async {
  var url = RestAPI.API_URL + '/ps-galleries/$id';

  Map data = {"foto": foto, "produk": produk, "jasa": jasa, "ke": ke};
  var body = json.encode(data);

  var response = await http.post(url,
      headers: {
        "Content-Type": "application/json",
      },
      body: body);
  return response;
}

Future<http.Response> editJasa(nama, deskripsi, userId, id) async {
  var url = RestAPI.API_URL + '/services/$id';

  Map data = {
    "nama": nama,
    "deskripsi": deskripsi,
    "user": userId,
    'aksi': 'ubah'
  };
  var body = json.encode(data);

  var response = await http.post(url,
      headers: {
        "Content-Type": "application/json",
      },
      body: body);
  return response;
}

Future<http.Response> editProduct(nama, harga, deskripsi, userId, id) async {
  var url = RestAPI.API_URL + '/products/$id';

  Map data = {
    "nama": nama,
    "harga": harga,
    "deskripsi": deskripsi,
    "user": userId,
    'aksi': 'ubah'
  };
  var body = json.encode(data);

  var response = await http.post(url,
      headers: {
        "Content-Type": "application/json",
      },
      body: body);
  return response;
}
