import 'dart:convert';

import 'package:get_survey_app/core/network/env.dart';
import 'package:get_survey_app/account/entity/LoginEntity.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';

class RestAPI {
  static const String GOOGLE_API_KEY =
      "AIzaSyC2ENUbN_33aXBr-PBmxN9Rd2VDntBnkQY";
  static String BASE_URL = Constants.SERVER;
  static String API_URL = "${Constants.SERVER}api";
  static String FOTO_URL = "${Constants.SERVER}/files";
  static final Map<String, String> headers = {
    "Content-Type": "application/json;charset=UTF-8",
    "Accept": "application/json",
  };

  RestAPI._();

  static Future<bool> addIzinKerja(
    String kode,
    String namaPemilik,
    double lat,
    double long,
    String alamattanah,
    String nik,
    String npwp,
    String haktanah,
    int luasTanah,
    int userId,
  ) async {
    var isDone;

    try {
      var record = await _doRequest("irk/save", {
        "kode": kode,
        "nama_pemilik": namaPemilik,
        "long": long,
        "lat": lat,
        'alamat_tanah': alamattanah,
        "nik": nik,
        "npwp": npwp,
        "hak_tanah": haktanah,
        "luas_tanah": luasTanah,
        "user_id": userId,
      });

      isDone = record["status"];
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<Map<String, dynamic>> bookRoom(
    int user, {
    int produk: 0,
    int jasa: 0,
    int irk: 0,
  }) async {
    var record;

    try {
      var request = await _doRequest("chat/initRoom", {
        "user": user,
        "produk": produk,
        "jasa": jasa,
        "irk": irk,
      });
      record = request["result"];
    } catch (exp) {
      record = null;
    }

    return record;
  }

  static Future<bool> endingTracking(int idIrk) async {
    var record;

    try {
      var request = await _doRequest("irk/mapping/end/$idIrk", {});
      record = request["result"] != null;
    } catch (exp) {
      record = null;
    }

    return record;
  }

  static Future<int> deleteAlat(int id) async {
    var isDone = 1;

    try {
      var record = await _doDelete(
        "survey-tool/$id",
      );
      isDone = record["status"];
    } catch (exp) {
      isDone = 0;
    }

    return isDone;
  }

  static Future<bool> doAlatSurvey(
    String tipe,
    String merek,
    String nomerSeri,
    int tahunProduksi,
    int idUser,
  ) async {
    var isDone;

    try {
      var record = await _doRequest("survey-tool/", {
        "tipe": tipe,
        "merek": merek,
        "nomer_seri": nomerSeri,
        "tahun_produksi": tahunProduksi,
        "user_surveyor_id": idUser
      });
      isDone = record["status"];
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future doChat(String room, int user, String text, {int forMe}) async {
    var chat;

    try {
      var record = await _doRequest(
        "chat/sendChat",
        {"pengirim": user, "room": room, "text": text, "forme": forMe},
      );
      chat = record["result"];
    } catch (exp) {
      chat = null;
    }

    return chat;
  }

  static Future<bool> doDoingIrk(String kode, String tanggalMulai) async {
    var isDone;

    try {
      var record = await _doRequest("irk/start-work", {
        "kode": kode,
        "work_date": tanggalMulai,
      });
      isDone = record["status"];
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<bool> doDone(String kode) async {
    var isDone;

    try {
      var record = await _doDone(
        "irk/finish/$kode",
      );
      isDone = record["status"];
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<String> convertImageBase64(String img) async {
    var isDone;

    try {
      var record = await _doRequest("imageToBase64", {"url": img});
      isDone = record['imageString'];
    } catch (exp) {
      isDone = "";
    }

    return isDone;
  }

  static Future<bool> doLogHit(
    String userId,
    String long,
    String lat,
    String waktu,
    String aktifitas,
  ) async {
    var isDone;

    try {
      var record = await _doRequest("log/user", {
        "user_id": userId,
        "long": long,
        "lat": lat,
        "waktu": waktu,
        "aktifitas": aktifitas,
      });
      isDone = record["status"];
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<Map<String, dynamic>> doLogin(
    String email,
    String password,
  ) async {
    var record;

    try {
      record = await _doRequest("auth/login-user", {
        "email": email,
        "password": password,
      });
    } catch (exp) {
      record = null;
    }

    return record;
  }

  static Future<bool> doLupaSandi(String email) async {
    var isDone;

    try {
      var record = await _doRequest(
        "auth/forgot-password-request",
        {"email": email},
      );
      isDone = record["status"];
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<LoginEntity> doRegister(
    String nama,
    String email,
    String password,
    String telp,
    String foto,
    int kelamin,
  ) async {
    var isDone;

    try {
      var record = await _doRequest("auth/register", {
        "nama": nama,
        "email": email,
        "password": password,
        "jenis_kelamin": kelamin,
        "no_hp": telp,
        "foto_user": foto,
      });
      isDone = LoginEntity(record["status"], record["message"]);
    } catch (exp) {
      LoginEntity(false, "Terjadi kesalahan di Server");
    }

    return isDone;
  }

  static Future<bool> doCekNotif(
    int id,
  ) async {
    var isDone;

    try {
      var record = await _doRequest("fcm/doTrial", {
        "user": id,
        "title": "Halo Pengguna Get-Survey...",
        "body": "Cek notification Anda berhasil"
      });
      if (record['error'] == null) {
        isDone = true;
      }
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<bool> doReport(String kode, String laporan) async {
    var isDone;

    try {
      var record = await _doRequest(
        "irk/report/save",
        {"kode": kode, 'laporan': laporan},
      );
      isDone = record["status"];
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<void> doDeal(int kode, String room) async {
    try {
      await _doRequest(
        "chat/dealViaChat",
        {"dealer": kode, 'room': room},
      );
    } catch (exp) {}
  }

  static Future<bool> doTakeIrk(String kode, int idUserSur) async {
    var isDone;

    try {
      var record = await _doRequest("irk/apply", {
        "kode": kode,
        "user_surveyor_id": idUserSur,
      });
      isDone = record["status"];
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<bool> doUpgrade(
      {int id,
      int nik,
      String tempatLahir,
      String tanggalLahir,
      String kotaTempatTinggal,
      String alamatLengkap,
      double lat,
      double long,
      String noAnggota,
      String noSka,
      String sebagai,
      String noLisensiKompetensi,
      String noLisensiKadastral,
      String jenjangPendidikan,
      String jurusanPendidikan,
      String perguruanTinggi,
      String daftar,
      String noLisensiIrk}) async {
    var isDone;

    try {
      var record = await _doRequest("user-public/upgrade-surveyor", {
        "id": id,
        "nik": nik,
        "tempat_lahir": tempatLahir,
        "tanggal_lahir": tanggalLahir,
        "kota_tempat_tinggal": kotaTempatTinggal,
        "alamat_tempat_tinggal": alamatLengkap,
        "lat": lat,
        "long": long,
        "no_anggota": noAnggota,
        "jenjang_pendidikan": jenjangPendidikan,
        "jurusan_pendidikan": jurusanPendidikan,
        "perguruan_tinggi": perguruanTinggi,
        "no_ska_skt": noSka,
        "sebagai": sebagai,
        "no_lisensi": noLisensiKompetensi,
        "no_lisensi_kadastral": noLisensiKadastral,
        "tanggal_awal_berlaku": daftar,
        "no_lisensi_irk": noLisensiIrk
      });
      isDone = record["status"] == 1;
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<bool> doUpload(int user, String path, String tipe) async {
    var isDone;

    try {
      var request = MultipartRequest(
          "POST", Uri.parse(API_URL + "/user-public/upgrade-surveyor/upload"))
        ..fields['id'] = user.toString()
        ..files.add(await MultipartFile.fromPath(
          "upload_file",
          path,
          contentType: (tipe == "pdf")
              ? MediaType("application", "pdf")
              : MediaType("image", tipe),
        ));
      var response = await request.send();
      isDone = response.statusCode == 200;
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<bool> doUploadAlat(
    String tipeAlat,
    String merk,
    String nomerSeri,
    String tahunProd,
    int userSurId,
    String path,
    String tipe,
  ) async {
    var isDone;

    try {
      var request =
          MultipartRequest("POST", Uri.parse(RestAPI.API_URL + "/survey-tool/"))
            ..fields['tipe'] = tipeAlat
            ..fields['merek'] = merk
            ..fields['nomer_seri'] = nomerSeri
            ..fields['tahun_produksi'] = tahunProd
            ..fields['user_surveyor_id'] = userSurId.toString()
            ..files.add(await MultipartFile.fromPath("upload_file", path,
                contentType: MediaType("image", tipe)));
      var response = await request.send();
      isDone = response.statusCode == 200;
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<bool> doUploadFile(
    int user,
    String kode,
    String path,
    String tipe,
  ) async {
    var isDone;

    try {
      var request = MultipartRequest(
          "POST", Uri.parse(RestAPI.API_URL + "/irk/save/sertifikat"))
        ..fields['kode'] = kode
        ..files.add(await MultipartFile.fromPath(
          "upload_file",
          path,
          contentType: (tipe == "pdf")
              ? MediaType("application", "pdf")
              : MediaType("image", tipe),
        ));
      var response = await request.send();
      isDone = response.statusCode == 200;
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<bool> doUploadHasilUkur(
    String kode,
    String path,
    String tipe,
  ) async {
    var isDone;

    try {
      var request = MultipartRequest(
          "POST", Uri.parse(RestAPI.API_URL + "/irk/report/save/hasil-ukur"))
        ..fields['kode'] = kode
        ..files.add(await MultipartFile.fromPath(
          "upload_file",
          path,
          contentType: (tipe == "pdf")
              ? MediaType("application", "pdf")
              : MediaType("image", tipe),
        ));

      var response = await request.send();
      isDone = response.statusCode == 200;
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<bool> doUploadKtp(
    int user,
    String kode,
    String path,
    String tipe,
  ) async {
    var isDone;

    try {
      var request =
          MultipartRequest("POST", Uri.parse(RestAPI.API_URL + "/irk/save/ktp"))
            ..fields['kode'] = kode
            ..files.add(await MultipartFile.fromPath(
              "upload_file",
              path,
              contentType: (tipe == "pdf")
                  ? MediaType("application", "pdf")
                  : MediaType("image", tipe),
            ));

      var response = await request.send();
      isDone = response.statusCode == 200;
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<bool> doUploadLampiran(
    String kode,
    String path,
    String tipe,
  ) async {
    var isDone;

    try {
      var request = MultipartRequest(
          "POST", Uri.parse(RestAPI.API_URL + "/irk/report/save/lampiran"))
        ..fields['kode'] = kode
        ..files.add(await MultipartFile.fromPath(
          "upload_file",
          path,
          contentType: (tipe == "zip")
              ? MediaType("application", "zip")
              : MediaType("image", tipe),
        ));

      var response = await request.send();
      isDone = response.statusCode == 200;
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<bool> doUploadPayment(
    String namaPengirim,
    String bankAsal,
    String bankTujuan,
    String path,
    String tipe,
    String kode,
  ) async {
    var isDone;

    try {
      var request = MultipartRequest(
          "POST", Uri.parse(RestAPI.API_URL + "/irk/pay/$kode"))
        ..fields['nama_pengirim'] = namaPengirim
        ..fields['bank_asal'] = bankAsal
        ..fields['bank_tujuan'] = "BNI Cab. Melawai"
        ..files.add(await MultipartFile.fromPath("upload_file", path,
            contentType: MediaType("image", tipe)));
      var response = await request.send();
      isDone = response.statusCode == 200;
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<bool> editIzinKerja(String kode, int luasTanah) async {
    var isDone;

    try {
      var record =
          await _doRequest("irk/save", {"luas_tanah": luasTanah, 'kode': kode});

      isDone = record["status"];
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<List> fetchTopSurveyors({int limit}) async {
    var list = [];

    try {
      var request = await _doParams("ratesum/surveyor/$limit");
      list = request["result"];
    } catch (_) {}

    return list;
  }

  static Future<List<dynamic>> getAlat(int id) async {
    var list = [];

    try {
      var request = await _doParams("survey-tool/?user_surveyor_id=$id");

      if (request["status"]) {
        list = request["data"];
      }
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future<List<dynamic>> getSurveyorbyID(int id) async {
    var list = [];

    try {
      var request = await _doParams("user-surveyor/$id");

      if (request["status"] == 1) {
        list = request["user_surveyor"];
      }
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future<List<dynamic>> getAllIrk() async {
    var list = [];

    try {
      var request = await _doParams("irk/");

      if (request["status"]) {
        list = request["data"];
      }
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future<List<dynamic>> getAllIrkStatus0() async {
    var list = [];

    try {
      var request = await _doParams("irk/?status=0");

      if (request["status"]) {
        list = request["data"];
      }
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future<List<dynamic>> getBanner() async {
    var list = [];

    try {
      var request = await _doParams("banner");

      if (request != []) {
        list = request;
      }
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future getBannerPopUp() async {
    var list;

    try {
      list = await _doParams("banner/popup/app");
    } catch (exp) {}

    return list;
  }

  static Future<List<dynamic>> getIrkDone({int status}) async {
    var list = [];

    try {
      var request = await _doParams("irk/?status=$status");

      if (request["status"]) {
        list = request["data"];
      }
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future<List<dynamic>> getIrkWithIdUser({int id, int status}) async {
    var list = [];

    try {
      var request = await _doParams("irk/?user_id=$id");

      if (request["status"]) {
        list = request["data"];
      }
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future<List<dynamic>> getIrkWithIdSurveyor({
    int id,
  }) async {
    var list = [];

    try {
      var request = await _doParams("irk/?user_surveyor_id=$id");

      if (request["status"]) {
        list = request["data"];
      }
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future<List<dynamic>> getIrkWithIdUserAndStatus({
    int id,
    int status,
  }) async {
    var list = [];

    try {
      var request = await _doParams("irk/?user_id=$id&status=$status");

      if (request["status"]) {
        list = request["data"];
      }
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future<List<dynamic>> getReport(int id) async {
    var list = [];

    try {
      var request = await _doParams("irk/report/$id");

      if (request["status"]) {
        list = request["data"];
      }
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future<List<dynamic>> getReview(
    int idSurveyor,
    String section,
    int limit,
    int offset,
  ) async {
    var list = [];

    try {
      var request =
          await _doParams("ratesum/review/$idSurveyor/$section/$limit/$offset");

      if (request["result"] != []) {
        list = request['result'];
      }
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future joinRoom(int user, String room) async {
    var record;

    try {
      var request = await _doRequest(
        "chat/joinRoom",
        {"room": room, "member": user},
      );
      record = request["result"];
    } catch (exp) {
      record = null;
    }

    return record;
  }

  static Future<bool> rateProdServIRK(
    int user,
    int rate, {
    int product: 0,
    int service: 0,
    int irk: 0,
    String ulasan,
  }) async {
    bool saved;

    try {
      var request = await _doRequest(
        "chat/rateViaChat",
        {
          "user": user,
          "nilai": rate,
          "produk": product,
          "jasa": service,
          "irk": irk,
          "ulasan": ulasan
        },
      );
      saved = request["result"]["status"];
    } catch (exp) {
      saved = false;
    }

    return saved;
  }

  static Future<bool> saveToken(int user, String token) async {
    bool isDone;

    try {
      var record = await _doRequest("fcm/saveToken", {
        "user": user,
        "token": token,
      });
      isDone = record["result"]["status"];
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<List<dynamic>> seeChatRoom(int idUser) async {
    var list = [];

    try {
      var request = await _doParams("chat/listRoom/user/$idUser");
      list = request["result"];
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future<List<dynamic>> seeChats(
    String room,
    int idUser, {
    int last: 0,
  }) async {
    var list = [];

    try {
      var request = await _doParams("chat/listChat/$room/$idUser/$last");
      list = request["result"];
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future<int> seeMyRate(
    String part,
    int me,
    int id,
  ) async {
    int rate;

    try {
      var request = await _doParams("chat/rateFromChat/$part/$id/$me");
      rate = request["result"];
    } catch (exp) {
      rate = 0;
    }

    return rate;
  }

  static Future<List<dynamic>> seeProdServRate({
    String tipe,
    int limit: 8,
  }) async {
    var list = [];

    try {
      var request = await _doParams("ratesum/best/$tipe/$limit");
      list = request["result"];
    } catch (_) {}

    return list;
  }

  static Future<List<dynamic>> seeProducts() async {
    var list = [];

    try {
      var request = await _doParams("products");

      if (request["status"]) {
        list = request["list"];
      }
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future<List<dynamic>> seeServices() async {
    var list = [];

    try {
      var request = await _doParams("services");

      if (request["status"]) {
        list = request["list"];
      }
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future<List<dynamic>> seeSurveyors() async {
    var list = [];

    try {
      var request = await _doParams("user-surveyor");

      if (request["status"] == 1) {
        list = request["list"];
      }
    } catch (exp) {
      list = [];
    }

    return list;
  }

  static Future<dynamic> trackGet(int irk) async {
    var result;

    try {
      var request = await _doParams("irk/mapping/track/$irk");
      result = request["result"];
    } catch (_) {
      result = null;
    }

    return result;
  }

  static Future<bool> trackGoing(int irk, LatLng aktual, bool isDone) async {
    bool isSaved;

    try {
      var request = await _doRequest("irk/mapping/going", {
        "irk": irk,
        "done": isDone,
        "aktualLat": aktual.latitude,
        "aktualLng": aktual.longitude
      });

      if (request["result"].length > 0) {
        isSaved = request["result"][0] > 0;
      }
    } catch (_) {
      isSaved = false;
    }

    return isSaved;
  }

  static Future<bool> trackStart(
    int irk,
    LatLng asal,
    LatLng tujuan,
    LatLng aktual,
  ) async {
    bool isSaved;

    try {
      var request = await _doRequest("irk/mapping/start", {
        "irk": irk,
        "asalLat": asal.latitude,
        "asalLng": asal.longitude,
        "tujuanLat": tujuan.latitude,
        "tujuanLng": tujuan.longitude,
        "aktualLat": aktual.latitude,
        "aktualLng": aktual.longitude
      });

      isSaved = request["result"] != null;
    } catch (_) {
      isSaved = false;
    }

    return isSaved;
  }

  static Future<bool> updateAgent({
    int id,
    String ktp,
    String noAnggota,
    String noLisensi,
    String noSKASKT,
    String noIRk,
    String noKadastral,
    bool isResend,
  }) async {
    var isDone;
    int status = 0;

    if (isResend != null) {
      status = (isResend) ? 1 : 0;
    }

    try {
      var record = await _doRequest("user-surveyor/itsme", {
        "id": id,
        "nik": ktp,
        "anggota": noAnggota,
        "lisensi": noLisensi,
        "kadastral": noKadastral,
        "irk": noIRk,
        "skaskt": noSKASKT,
        "ulang": status,
      });
      isDone = record["status"];
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<bool> updateBio(
    int id,
    String nama,
    String sandi,
    String foto,
  ) async {
    var isDone;

    try {
      var record = await _doRequest("user-public/itsme", {
        "id": id,
        "nama": nama,
        "sandi": sandi,
        "foto": foto,
      });
      isDone = record["status"];
    } catch (exp) {
      isDone = false;
    }

    return isDone;
  }

  static Future<dynamic> _doDelete(String endpoint) async {
    Response response;

    try {
      response = await delete(
        "$API_URL/$endpoint",
        headers: headers,
      ).timeout(
        Duration(minutes: 2),
        onTimeout: () {
          throw Exception(
              "API process trouble, Please check your internet connection!");
        },
      );
    } catch (exp) {
      throw Exception("No Internet, Please check your internet connection!");
    }

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception("API request failed!");
    }
  }

  static Future<dynamic> _doDone(String endpoint) async {
    Response response;

    try {
      response = await post(
        "$API_URL/$endpoint",
        headers: headers,
      ).timeout(
        Duration(minutes: 2),
        onTimeout: () {
          throw Exception(
              "API process trouble, Please check your internet connection!");
        },
      );
    } catch (exp) {
      throw Exception("No Internet, Please check your internet connection!");
    }

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception("API request failed!");
    }
  }

  static Future<dynamic> _doParams(String endpoint) async {
    Response response;

    try {
      response = await get("$API_URL/$endpoint").timeout(
        Duration(minutes: 1),
        onTimeout: () {
          throw Exception(
              "API process trouble, Please check your internet connection!");
        },
      );
    } catch (exp) {
      throw Exception("No Internet, Please check your internet connection!");
    }

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception("API request failed!");
    }
  }

  static Future<dynamic> _doRequest(
    String endpoint,
    Map<String, dynamic> params,
  ) async {
    Response response;

    try {
      response = await post(
        "$API_URL/$endpoint",
        headers: headers,
        body: json.encode(params),
      ).timeout(
        Duration(minutes: 2),
        onTimeout: () {
          throw Exception(
              "API process trouble, Please check your internet connection!");
        },
      );
    } catch (exp) {
      throw Exception("No Internet, Please check your internet connection!");
    }

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception("API request failed!");
    }
  }
}
