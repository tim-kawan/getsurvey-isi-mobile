import 'package:get_survey_app/service/Helper.dart';
import 'package:shared_preferences/shared_preferences.dart'
    show SharedPreferences;

class Session {
  static const String USER_ID = "id";
  static const String USER_NAME = "nama";
  static const String USER_EMAIL = "email";
  static const String USER_STATUS = "type";
  static const String USER_LANJUT = "iniSurveyor";
  static const String USER_NO_TELP = "noTelp";
  static const String USER_PHOTO = "userPhoto";
  static const String LAT = "lat";
  static const String LON = "lon";
  static const String ALAMAT = "alamat";
  static const String JENIS_KELAMIN = "jk";
  static const String AUTH_TOKEN = "auth_token";

  Session._();

  static Future<bool> doClear() async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    bool out = await preferences.clear();
    await Helper.fcmOut();
    Session.doSave("opened", true);
    Session.doSave('locationPermission', true);
    return out;
  }

  static Future<bool> doRemove(String key) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();

    return await preferences.remove(key);
  }

  static Future<void> doSave(String key, dynamic value) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();

    switch (value.runtimeType) {
      case int:
        preferences.setInt(key, value);

        break;
      case double:
        preferences.setDouble(key, value);

        break;
      case bool:
        preferences.setBool(key, value);

        break;
      case String:
        preferences.setString(key, value);

        break;
      default:
        preferences.setStringList(key, value);
    }
  }

  static Future<void> doSetActiveUser(var object) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    String pp = object['user']["user_profile"]['foto_user'];

    if (pp != null) {
      if (pp.contains("data:image/")) {
        var pps = pp.split(",");
        pp = pps[1];
      }
    }

    preferences.setInt(USER_ID, object['user']["id"]);
    preferences.setString(USER_NAME, object['user']["nama"]);
    preferences.setString(USER_EMAIL, object['user']["email"]);
    preferences.setString(USER_PHOTO, pp);
    preferences.setInt(
        USER_LANJUT,
        (object['user']["user_surveyor_id"] != null)
            ? object['user']["user_surveyor"]["status_keanggotaan"]
            : -1);
    preferences.setInt(
        USER_STATUS,
        (object['user']["user_surveyor_id"] == null
            ? 0
            : object['user']["user_surveyor_id"]));
    preferences.setString(
        USER_NO_TELP, object['user']["user_profile"]['no_hp']);
    preferences.setDouble(LAT, object['user']["user_profile"]['lat']);
    preferences.setDouble(LON, object['user']["user_profile"]['long']);
    preferences.setString(
        ALAMAT, object['user']["user_profile"]['alamat_tempat_tinggal']);
    preferences.setInt(
        JENIS_KELAMIN, object['user']["user_profile"]['jenis_kelamin']);
    preferences.setString(AUTH_TOKEN, "Bearer ${object['access_token']}");
  }

  static Future<dynamic> getValue(String key) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();

    return preferences.get(key);
  }

  static Future<bool> isExist(String key) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();

    return preferences.containsKey(key);
  }
}
