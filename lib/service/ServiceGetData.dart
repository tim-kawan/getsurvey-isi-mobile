import 'package:get_survey_app/service/RestAPI.dart';
import 'package:http/http.dart' as http;

final String getIrdId = RestAPI.API_URL + "/irk/id";
final String getListIRK = RestAPI.API_URL + "/irk";
final String getNewCodeIRK = RestAPI.API_URL + "/irk/code-new";
final String getReport = RestAPI.API_URL + "/irk/report";
final String listProductsByUser = RestAPI.API_URL + "/products/user";
final String listServicesByUser = RestAPI.API_URL + "/services/user";
final String listSurveyor = RestAPI.API_URL + "/user-surveyor";
final String listUser = RestAPI.API_URL + "/user-public";

class API {
  static Future getListIdIrk(id) {
    var url = "$getIrdId/$id";
    return http.get(
      url,
    );
  }

  static Future getListIrk() {
    var url = "$getListIRK";
    return http.get(
      url,
    );
  }

  static Future getListProductByUser(id) {
    var url = "$listProductsByUser/$id";
    return http.get(
      url,
    );
  }

  static Future getListReport(id) {
    var url = "$getReport/$id";
    return http.get(
      url,
    );
  }

  static Future getListServicesByUser(id) {
    var url = "$listServicesByUser/$id";
    return http.get(
      url,
    );
  }

  static Future getListSurveyor(id) {
    var url = "$listSurveyor/$id";
    return http.get(
      url,
    );
  }

  static Future getListUser(id) {
    var url = "$listUser/$id";
    return http.get(
      url,
    );
  }

  static Future getNewCode() {
    var url = "$getNewCodeIRK";
    return http.get(
      url,
    );
  }
}
