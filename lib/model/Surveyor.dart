class Surveyor {
  int id;
  int idUser;
  String name;
  String email;
  String phoneNum;
  String img;
  String alamat;
  String noAnggota;
  String kotaAsal;
  double lat;
  double long;
  bool isVisible;
  String joinDate;
  String rate;
  int medali;

  Surveyor(
      this.name,
      this.phoneNum,
      this.alamat,
      this.img,
      this.lat,
      this.long,
      this.email,
      this.id,
      this.noAnggota,
      this.kotaAsal,
      this.idUser,
      this.isVisible,
      this.joinDate,
      this.rate,
      this.medali);

  Surveyor.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    noAnggota = json["no_anggota"];
    idUser = json['user']['id'];
    name = json['user']['nama'];
    email = json['user']['email'];
    alamat = json['user']['user_profile']['alamat_tempat_tinggal'];
    kotaAsal = json['user']['user_profile']['kota_tempat_tinggal'];
    phoneNum = json['user']['user_profile']['no_hp'];
    lat = json['user']['user_profile']['lat'];
    long = json['user']['user_profile']['long'];
    img = json['user']['user_profile']['foto_user'];
    isVisible = json["status_keanggotaan"] == 1;
    joinDate = json['tanggal_awal_berlaku'];
    rate = json['rate'];
    medali = json['medali'];
  }
}
