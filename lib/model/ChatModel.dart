class ChatModel {
  int id;
  int at;
  int sender;
  String chat;
  String date;
  String room;
  bool isMe;

  ChatModel({
    this.isMe,
    this.at,
    this.id,
    this.chat,
    this.sender,
    this.room,
    this.date,
  });

  factory ChatModel.fromMap(Map<String, dynamic> map) => new ChatModel(
        id: map["id"],
        at: map["at"],
        sender: map["sender"],
        chat: map["chat"],
        date: map["date"],
        room: map["room"],
        isMe: (map["isMe"] == 1),
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "at": at,
        "sender": sender,
        "chat": chat,
        "date": date,
        "room": room,
        "isMe": isMe ? 1 : 0,
      };

  static ChatModel fromJson(
    Map<String, dynamic> json, {
    bool isMe,
    String chatRoom,
  }) {
    return new ChatModel(
      chat: json["chat_text"],
      id: json["id"],
      at: json["chat_room_id"],
      sender: json["user_id"],
      date: json["send_time"],
      isMe: isMe,
      room: chatRoom,
    );
  }
}
