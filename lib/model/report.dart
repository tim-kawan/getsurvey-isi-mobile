class Report {
  int id;
  String laporan;
  String fileHasilUkur;
  String fileLampiran;

  Report({this.id, this.fileHasilUkur, this.fileLampiran, this.laporan});
  Report.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    laporan = json['laporan'];
    fileHasilUkur = json['file_hasil_ukur'];
    fileLampiran = json['file_lampiran'];
  }
}
