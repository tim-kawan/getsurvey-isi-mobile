class Irk {
  int id;
  int userID;
  String namaPemilik;
  double long;
  double lat;
  int luasTanah;
  String alamatTanah;
  String hakTanah;
  int status;
  String npwp;
  String nik;
  String kode;
  String fileSer;
  int idSurveyor;


  Irk(
      {this.id,
      this.userID,
      this.namaPemilik,
      this.long,
      this.lat,
      this.luasTanah,
      this.alamatTanah,
      this.hakTanah,
      this.status,
      this.npwp,
      this.kode,
      this.fileSer,
      this.nik,
      this.idSurveyor});
  Irk.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userID = json['user_id'];
    namaPemilik = json['nama_pemilik'];
    long = json['long'];
    lat = json['lat'];
    luasTanah = json['luas_tanah'];
    alamatTanah = json['alamat_tanah'];
    hakTanah = json['hak_tanah'];
    npwp = json['npwp'];
    status = json['status'];
    nik = json['nik'];
    kode = json['kode'];
    fileSer = json['file_sertifikat'];
    idSurveyor = json['irk_user_surveyor'] == null
        ? null
        : json['irk_user_surveyor']['id'];
  }
}
