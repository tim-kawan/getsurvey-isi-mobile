class IrkModel {
  int id;
  int idIrk;

  IrkModel({this.id, this.idIrk});

  factory IrkModel.fromMap(Map<String, dynamic> map) => new IrkModel(
        id: map["id"],
        idIrk: map['id_irk'],
      );

  Map<String, dynamic> toMap() => {"id": id, 'id_irk': idIrk};

  static IrkModel fromJson(
    Map<String, dynamic> json,
  ) {
    return new IrkModel(
      id: json["id"],
      idIrk: json['id_irk'],
    );
  }
}

// class IrkModel {
//   int _id;
//   int _idIrk;
//   String _name;
//   String _luas;
//   IrkModel(this._name, this._luas, this._idIrk);
//   IrkModel.fromMap(Map<String, dynamic> map) {
//     this._id = map['id'];
//     this._id = map['id_irk'];
//     this._name = map['name'];
//     this._luas = map['luas'];
//   }

//   int get id => _id;
//   int get idIrk => _idIrk;
//   String get name => _name;
//   String get luas => _luas;

//   set idIrks(int value) {
//     _idIrk = value;
//   }

//   set names(String value) {
//     _name = value;
//   }

//   set luass(String value) {
//     _luas = value;
//   }

//   Map<String, dynamic> toMap() {
//     Map<String, dynamic> map = Map<String, dynamic>();
//     map['id'] = this._id;
//     map['id_irk'] = idIrk;
//     map['name'] = name;
//     map['luas'] = luas;
//     return map;
//   }
// }
