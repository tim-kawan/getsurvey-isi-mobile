class Alat {
  int id;
  String tipe;
  String merek;
  String nomerSeri;
  String tahunProduksi;
  String akurasi;
  int jumlah;
  String statusMilik;
  int idUSer;
  String image;

  Alat(
      {this.tipe,
      this.merek,
      this.akurasi,
      this.idUSer,
      this.jumlah,
      this.nomerSeri,
      this.statusMilik,
      this.tahunProduksi,
      this.image,
      this.id});

  Alat.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    tipe = json['tipe'];
    merek = json['merek'];
    nomerSeri = json['nomer_seri'];
    tahunProduksi = json['tahun_produksi'];
    akurasi = json['akurasi'];
    jumlah = json['jumlah'];
    statusMilik = json['status_milik'];
    idUSer = json['user_surveyor_id'];
    image = json['survey_tool_image']['file_gambar'];
  }
}
