import 'package:get_survey_app/model/ProductService.dart';

import 'AddIrk.dart';

class RoomChat {
  int id;
  String date;
  String title;
  int status;
  String image;
  bool userDeal;
  bool ownerDeal;
  int idProd;
  String prodPriv;
  int unread;
  int ownerID;

  RoomChat(
    this.date,
    this.id,
    this.title,
    this.status,
    this.image,
    this.ownerDeal,
    this.userDeal,
    this.idProd,
    this.prodPriv,
    this.unread,
    this.ownerID,
  );

  RoomChat.fromJson(Map<String, dynamic> json) {
    unread = 0;
    ownerID = json['user_receiver_id'];
    date = json['created'];
    id = json["id"];
    status = json["room_status"];
    ownerDeal = json["surveyor_deal"];
    userDeal = json["user_deal"];
    title = json['product_goods_id'] == null
        ? json['product_service']["nama"]
        : json['product_good']["nama"];
    ProductService productService;
    Irk irk;

    if (json["product_goods_id"] != null) {
      productService = ProductService.fromJson(json["product_good"]);
      prodPriv = "produk";
    } else if (json["product_services_id"] != null) {
      productService = ProductService.fromJson(json["product_service"]);
      prodPriv = "jasa";
    } else if (json["product_services_id"] == null &&
        json["product_goods_id"] == null) {
      irk = Irk.fromJson(json["product_service"]);
      prodPriv = 'jasa';
    }

    if (productService != null) {
      if (productService.productPhotos.isNotEmpty) {
        image = productService.productPhotos[0].foto;
        idProd = productService.id;
      }
    } else if (irk != null) {
      image = 'assets/logo.png';
      idProd = irk.id;
    }
  }
}
