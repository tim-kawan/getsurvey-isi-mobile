class ProductPhotos {
  int id;
  String foto;
  int productGoodsId;
  int productServicesId;
  String createdAt;
  String updatedAt;

  ProductPhotos({
    this.id,
    this.foto,
    this.productGoodsId,
    this.productServicesId,
    this.createdAt,
    this.updatedAt,
  });

  ProductPhotos.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productGoodsId = json['product_goods_id'];
    productServicesId = json['product_services_id'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    foto = json['foto'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['foto'] = this.foto;
    data['product_goods_id'] = this.productGoodsId;
    data['product_services_id'] = this.productServicesId;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}

class ProductService {
  int id;
  String nama;
  int harga;
  String deskripsi;
  bool terpakai;
  int userSurveyorId;
  String createdAt;
  String updatedAt;
  List<ProductPhotos> productPhotos;
  UserSurveyor userSurveyor;

  ProductService({
    this.id,
    this.nama,
    this.harga,
    this.deskripsi,
    this.terpakai,
    this.userSurveyorId,
    this.createdAt,
    this.updatedAt,
    this.productPhotos,
    this.userSurveyor,
  });

  ProductService.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nama = json['nama'];
    harga = json['harga'];
    deskripsi = json['deskripsi'];
    terpakai = json['terpakai'];
    userSurveyorId = json['user_surveyor_id'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    productPhotos = new List<ProductPhotos>();

    if (json['product_photos'].isNotEmpty) {
      json['product_photos'].forEach((v) {
        productPhotos.add(new ProductPhotos.fromJson(v));
      });
    } else {
      productPhotos.add(new ProductPhotos(foto: null));
    }

    userSurveyor = json['user_surveyor'] != null
        ? new UserSurveyor.fromJson(json['user_surveyor'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nama'] = this.nama;
    data['harga'] = this.harga;
    data['deskripsi'] = this.deskripsi;
    data['terpakai'] = this.terpakai;
    data['user_surveyor_id'] = this.userSurveyorId;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;

    if (this.productPhotos.isNotEmpty) {
      data['product_photos'] =
          this.productPhotos.map((v) => v.toJson()).toList();
    }

    if (this.userSurveyor != null) {
      data['user_surveyor'] = this.userSurveyor.toJson();
    }

    return data;
  }
}

class User {
  int id;
  String nama;
  String email;
  int emailConfirm;
  String emailConfirmCode;
  String password;
  String passwordForgotCode;
  int statusUser;
  int userTokenId;
  int userProfileId;
  int userSurveyorId;
  String created;
  UserProfile userProfile;

  User(
      {this.id,
      this.nama,
      this.email,
      this.emailConfirm,
      this.emailConfirmCode,
      this.password,
      this.passwordForgotCode,
      this.statusUser,
      this.userTokenId,
      this.userProfileId,
      this.userSurveyorId,
      this.created,
      this.userProfile});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nama = json['nama'];
    email = json['email'];
    emailConfirm = json['email_confirm'];
    emailConfirmCode = json['email_confirm_code'];
    password = json['password'];
    passwordForgotCode = json['password_forgot_code'];
    statusUser = json['status_user'];
    userTokenId = json['user_token_id'];
    userProfileId = json['user_profile_id'];
    userSurveyorId = json['user_surveyor_id'];
    created = json['created'];
    userProfile = json['user_profile'] != null
        ? new UserProfile.fromJson(json['user_profile'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nama'] = this.nama;
    data['email'] = this.email;
    data['email_confirm'] = this.emailConfirm;
    data['email_confirm_code'] = this.emailConfirmCode;
    data['password'] = this.password;
    data['password_forgot_code'] = this.passwordForgotCode;
    data['status_user'] = this.statusUser;
    data['user_token_id'] = this.userTokenId;
    data['user_profile_id'] = this.userProfileId;
    data['user_surveyor_id'] = this.userSurveyorId;
    data['created'] = this.created;
    if (this.userProfile != null) {
      data['user_profile'] = this.userProfile.toJson();
    }
    return data;
  }
}

class UserProfile {
  int id;
  String nik;
  int jenisKelamin;
  String noHp;
  String tempatLahir;
  String tanggalLahir;
  String kotaTempatTinggal;
  String alamatTempatTinggal;
  double lat;
  double long;
  String fotoUser;

  UserProfile(
      {this.id,
      this.nik,
      this.jenisKelamin,
      this.noHp,
      this.tempatLahir,
      this.tanggalLahir,
      this.kotaTempatTinggal,
      this.alamatTempatTinggal,
      this.lat,
      this.long,
      this.fotoUser});

  UserProfile.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nik = json['nik'];
    jenisKelamin = json['jenis_kelamin'];
    noHp = json['no_hp'];
    tempatLahir = json['tempat_lahir'];
    tanggalLahir = json['tanggal_lahir'];
    kotaTempatTinggal = json['kota_tempat_tinggal'];
    alamatTempatTinggal = json['alamat_tempat_tinggal'];
    lat = json['lat'];
    long = json['long'];
    fotoUser = json['foto_user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nik'] = this.nik;
    data['jenis_kelamin'] = this.jenisKelamin;
    data['no_hp'] = this.noHp;
    data['tempat_lahir'] = this.tempatLahir;
    data['tanggal_lahir'] = this.tanggalLahir;
    data['kota_tempat_tinggal'] = this.kotaTempatTinggal;
    data['alamat_tempat_tinggal'] = this.alamatTempatTinggal;
    data['lat'] = this.lat;
    data['long'] = this.long;
    data['foto_user'] = this.fotoUser;
    return data;
  }
}

class UserSurveyor {
  int id;
  String noAnggota;
  int statusKeanggotaan;
  String jenjangPendidikan;
  String jurusanPendidikan;
  String perguruanTinggi;
  String tanggalAwalBerlaku;
  int masaBerlaku;
  String tanggalAkhirBerlaku;
  String noLisensi;
  String fileLisensi;
  String noSka;
  User user;

  UserSurveyor(
      {this.id,
      this.noAnggota,
      this.statusKeanggotaan,
      this.jenjangPendidikan,
      this.jurusanPendidikan,
      this.perguruanTinggi,
      this.tanggalAwalBerlaku,
      this.masaBerlaku,
      this.tanggalAkhirBerlaku,
      this.noLisensi,
      this.fileLisensi,
      this.noSka,
      this.user});

  UserSurveyor.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    noAnggota = json['no_anggota'];
    statusKeanggotaan = json['status_keanggotaan'];
    jenjangPendidikan = json['jenjang_pendidikan'];
    jurusanPendidikan = json['jurusan_pendidikan'];
    perguruanTinggi = json['perguruan_tinggi'];
    tanggalAwalBerlaku = json['tanggal_awal_berlaku'];
    masaBerlaku = json['masa_berlaku'];
    tanggalAkhirBerlaku = json['tanggal_akhir_berlaku'];
    noLisensi = json['no_lisensi'];
    fileLisensi = json['file_lisensi'];
    noSka = json['no_ska'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['no_anggota'] = this.noAnggota;
    data['status_keanggotaan'] = this.statusKeanggotaan;
    data['jenjang_pendidikan'] = this.jenjangPendidikan;
    data['jurusan_pendidikan'] = this.jurusanPendidikan;
    data['perguruan_tinggi'] = this.perguruanTinggi;
    data['tanggal_awal_berlaku'] = this.tanggalAwalBerlaku;
    data['masa_berlaku'] = this.masaBerlaku;
    data['tanggal_akhir_berlaku'] = this.tanggalAkhirBerlaku;
    data['no_lisensi'] = this.noLisensi;
    data['file_lisensi'] = this.fileLisensi;
    data['no_ska'] = this.noSka;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}
