
import 'core/network/env.dart';
import 'main.dart';

void main() {
  Constants.setEnvironment(Environment.DEV);
  mainDelegate();
}
