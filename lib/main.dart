import 'package:flutter/material.dart';
import 'package:get_survey_app/service/Helper.dart';
import 'package:no_context_navigation/no_context_navigation.dart';

import 'core/di/injection_container.dart' as di;
import 'core/router/router.dart';
import 'core/router/routes_path.dart';

void mainDelegate() async {
  await di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Helper.initFirebase(context, NavigationService.navigationKey);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'GetSurvey',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        navigatorKey: NavigationService.navigationKey,
        initialRoute: RoutesPath.SplashRoute,
        onGenerateRoute: generateRoute);
  }
}
