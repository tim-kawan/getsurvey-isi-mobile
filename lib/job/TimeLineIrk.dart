import 'package:flutter/material.dart';
import 'package:get_survey_app/model/AddIrk.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';

class TabbarScreen extends StatefulWidget {
  @override
  _TabbarScreenState createState() => _TabbarScreenState();
}

class _TabbarScreenState extends State<TabbarScreen>
    with SingleTickerProviderStateMixin {
  List<Irk> listBelumDiProses = [];
  List<Irk> listTelahDiProses = [];
  List<Irk> listTelahDiBayar = [];
  List<Irk> listProsesPengerjaan = [];
  List<Irk> listSelesaiPengerjaan = [];

  @override
  void initState() {
    super.initState();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text(
              'Proses Irk Anda',
              style: TextStyle(fontSize: 16.0),
            ),
            bottom: PreferredSize(
                child: TabBar(
                    isScrollable: true,
                    unselectedLabelColor: Colors.white.withOpacity(0.3),
                    indicatorColor: Colors.white,
                    tabs: [
                      Tab(
                        child: Text('Belum diproses'),
                      ),
                      Tab(
                        child: Text('Telah diproses'),
                      ),
                      Tab(
                        child: Text('Telah dibayar'),
                      ),
                      Tab(
                        child: Text('Proses Pengerjaan'),
                      ),
                      Tab(
                        child: Text('Selesai Pengerjaan'),
                      ),
                    ]),
                preferredSize: Size.fromHeight(30.0)),
          ),
          body: TabBarView(
            children: <Widget>[
              Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: ListView.builder(
                      itemCount: listBelumDiProses.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          title: Text(listBelumDiProses[index].namaPemilik),
                          subtitle: Text(listBelumDiProses[index].alamatTanah),
                        );
                      })),
              Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: ListView.builder(
                      itemCount: listTelahDiProses.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          title: Text(listTelahDiProses[index].namaPemilik),
                          subtitle: Text(listTelahDiProses[index].alamatTanah),
                        );
                      })),
              Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: ListView.builder(
                      itemCount: listTelahDiBayar.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          title: Text(listTelahDiBayar[index].namaPemilik),
                          subtitle: Text(listTelahDiBayar[index].alamatTanah),
                        );
                      })),
              Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: ListView.builder(
                      itemCount: listProsesPengerjaan.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          title: Text(listProsesPengerjaan[index].namaPemilik),
                          subtitle:
                              Text(listProsesPengerjaan[index].alamatTanah),
                        );
                      })),
              Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: ListView.builder(
                      itemCount: listSelesaiPengerjaan.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          title: Text(listSelesaiPengerjaan[index].namaPemilik),
                          subtitle:
                              Text(listSelesaiPengerjaan[index].alamatTanah),
                        );
                      })),
            ],
          )),
    );
  }

  void initData() async {
    await _getListIrk();
  }

  _getListIrk() async {
    int idUser = await Session.getValue(Session.USER_ID);
    await RestAPI.getIrkWithIdUser(id: idUser).then((results) async {
      results.forEach((irk) {
        Irk items = Irk.fromJson(irk);
        if (items.status == 0) {
          setState(() {
            listBelumDiProses.add(items);
          });
        } else if (items.status == 1) {
          setState(() {
            listTelahDiProses.add(items);
          });
        } else if (items.status == 2) {
          setState(() {
            listTelahDiBayar.add(items);
          });
        } else if (items.status == 3 || items.status == 4) {
          setState(() {
            listProsesPengerjaan.add(items);
          });
        } else if (items.status == 5 || items.status == 6) {
          setState(() {
            listSelesaiPengerjaan.add(items);
          });
        }
      });
      setState(() {});
    });
  }
}
