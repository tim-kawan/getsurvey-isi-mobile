// import 'dart:io';

// import 'package:flutter/material.dart';
// import 'package:get_survey_app/component/const.dart';
// import 'package:get_survey_app/model/Photo.dart';
// import 'package:get_survey_app/service/DBHelperPhoto.dart';
// import 'package:get_survey_app/service/Utility.dart';
// import 'package:image_picker/image_picker.dart';

// class JobUpdateScreen extends StatefulWidget {
//   @override
//   _JobUpdateScreenState createState() => _JobUpdateScreenState();
// }

// class _JobUpdateScreenState extends State<JobUpdateScreen> {
//   final _formKey = GlobalKey<FormState>();
//   TextEditingController _laporanCtrl = TextEditingController();

//   Future<File> imageFile;
//   Image image;
//   DBHelper dbHelper;
//   List<Photo> images;

//   @override
//   void initState() {
//     super.initState();
//     images = [];
//     dbHelper = DBHelper();
//     refreshImages();
//   }

//   refreshImages() {
//     setState(() {
//       dbHelper.getPhotos();
//     });
//   }

//   pickImageFromGallery() {
//     ImagePicker.pickImage(source: ImageSource.gallery).then((imgFile) {
//       String imgString = Utility.base64String(imgFile.readAsBytesSync());
//       Photo photo = Photo(0, imgString);
//       dbHelper.save(photo);
//       refreshImages();
//     });
//   }

//   gridView() {
//     return Padding(
//       padding: EdgeInsets.all(5.0),
//       child: GridView.count(
//         crossAxisCount: 2,
//         childAspectRatio: 0.7,
//         mainAxisSpacing: 4.0,
//         crossAxisSpacing: 4.0,
//         children: images.map((photo) {
//           return Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: Container(
//                 height: 150,
//                 width: 80,
//                 child: Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     Utility.imageFromBase64String(photo.photoName),
//                     RaisedButton(
//                       onPressed: () {
//                         dbHelper.delete(photo.id);
//                         refreshImages();
//                       },
//                       child: Padding(
//                         padding: EdgeInsets.all(5),
//                         child: Text("Hapus"),
//                       ),
//                       color: Colors.blue[900],
//                     )
//                   ],
//                 )),
//           );
//         }).toList(),
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       child: Scaffold(
//         backgroundColor: Colors.grey[200],
//         appBar: AppBar(
//           centerTitle: true,
//           leading: IconButton(
//             icon: Icon(Icons.arrow_back),
//             onPressed: () {
//               Navigator.pop(context);
//             },
//             color: Colors.white,
//           ),
//           backgroundColor: primaryColor,
//           title: Text(
//             "Laporan Tugas Pekerjaan",
//             style: TextStyle(
//                 color: Colors.white,
//                 fontSize: 16.0,
//                 fontWeight: FontWeight.bold),
//           ),
//         ),
//         body: _body(),
//       ),
//     );
//   }

//   Widget _body() {
//     return SingleChildScrollView(
//       child: Form(
//           key: _formKey,
//           child: Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Text(
//                   'Laporan',
//                   style: TextStyle(
//                       color: Colors.black, fontWeight: FontWeight.bold),
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 Container(
//                   height: 100.0,
//                   width: MediaQuery.of(context).size.width,
//                   decoration: BoxDecoration(
//                       color: Colors.white,
//                       borderRadius: BorderRadius.circular(10.0)),
//                   child: Padding(
//                     padding: const EdgeInsets.only(
//                         top: 8.0, bottom: 8.0, left: 13.0, right: 13.0),
//                     child: TextFormField(
//                       maxLines: 4,
//                       controller: _laporanCtrl,
//                       decoration: InputDecoration.collapsed(
//                           hintText: "Type a detail here.."),
//                     ),
//                   ),
//                 ),
//                 SizedBox(
//                   height: 20,
//                 ),
//                 Container(
//                   height: 400,
//                   width: MediaQuery.of(context).size.width,
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     children: [
//                       Flexible(
//                         child: gridView(),
//                       ),
//                     ],
//                   ),
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 Align(
//                   alignment: Alignment.bottomRight,
//                   child: InkWell(
//                     onTap: () {
//                       pickImageFromGallery();
//                     },
//                     child: Container(
//                       height: 40,
//                       width: 100,
//                       child: Center(
//                           child: Text(
//                         "Tambah foto",
//                         style: TextStyle(
//                           color: Colors.white,
//                         ),
//                       )),
//                       decoration: BoxDecoration(
//                         color: Colors.blue[900],
//                         borderRadius: BorderRadius.circular(10),
//                       ),
//                     ),
//                   ),
//                 ),
//                 InkWell(
//                   onTap: () {},
//                   child: Container(
//                     height: 40,
//                     width: 100,
//                     child: Center(
//                         child: Text(
//                       "hapus foto",
//                       style: TextStyle(
//                         color: Colors.white,
//                       ),
//                     )),
//                     decoration: BoxDecoration(
//                       color: Colors.blue[900],
//                       borderRadius: BorderRadius.circular(10),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           )),
//     );
//   }
// }
