import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/job/ListIrkUser/ListInformasiRencanaKota.dart';
import 'package:get_survey_app/job/ListIrkUser/TabbarListIrkUser.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class FormPembayaran extends StatefulWidget {
  final String kode;
  FormPembayaran({this.kode, Key key}) : super(key: key);
  @override
  _FormPembayaranState createState() => _FormPembayaranState();
}

class _FormPembayaranState extends State<FormPembayaran> {
  final form = GlobalKey<FormState>();
  TextEditingController _namaPengirimCtrl = TextEditingController();
  TextEditingController _bankAsalCtrl = TextEditingController();

  File _image;
  String _fileName;
  bool _isLoad = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
          color: Colors.white,
        ),
        backgroundColor: primaryColor,
        title: Text(
          "Form Pembayaran",
          style: TextStyle(
              color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),
        ),
      ),
      body: ModalProgressHUD(inAsyncCall: _isLoad, child: _body()),
      bottomNavigationBar: BottomAppBar(
        child: Builder(
          builder: (context) => InkWell(
            onTap: () async {
              if (form.currentState.validate()) {
                setState(() {
                  _isLoad = true;
                });
                if (_image.path != null) {
                  if (_image.path.toLowerCase().contains("png") ||
                      _image.path.toLowerCase().contains("jpg") ||
                      _image.path.toLowerCase().contains("jpeg")) {
                    String tipe = "jpg";

                    if (_image.path.toLowerCase().contains("png")) {
                      tipe = "png";
                    }

                    await _onSubmit(
                        _namaPengirimCtrl.text,
                        _bankAsalCtrl.text,
                        "BNI Cab. Melawai",
                        widget.kode,
                        _image.path,
                        tipe,
                        context);

                    setState(() {
                      _isLoad = false;
                    });
                  }
                }
              } else {
                Component.showAppNotification(
                    context, "Gagal", "Lengkapi Data yang anda Masukkan");
              }
            },
            child: Container(
              height: 50.0,
              decoration: BoxDecoration(
                color: primaryColor,
              ),
              child: Center(
                child: Text(
                  "Tambah",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: "Sans",
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _body() {
    return SingleChildScrollView(
      child: Form(
          key: form,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: TextFormField(
                  controller: _namaPengirimCtrl,
                  validator: (val) =>
                      val.length > 1 ? null : 'Nama Tidak Valid',
                  decoration: InputDecoration(
                    labelText: 'Nama Pengirim',
                    hintText: 'Sesuai Bukti Transfer ',
                    isDense: true,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: TextFormField(
                  controller: _bankAsalCtrl,
                  validator: (val) =>
                      val.length > 1 ? null : ' Nama Bank Asal Tidak Valid',
                  decoration: InputDecoration(
                    labelText: 'Nama Bank Pengirim',
                    hintText: 'Sesuai Bukti Transfer ',
                    isDense: true,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                    child: Text(
                      "Sesuaikan pembayaran dengan nominal harga yang di tentukan",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w700,
                          fontSize: 17),
                    )),
              ),
              SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                child: Center(
                  child: Container(
                    height: 200,
                    width: 140,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: _image == null
                            ? AssetImage(
                                "assets/img/camera.png",
                              )
                            : FileImage(_image),
                        fit: _image == null ? BoxFit.fill : BoxFit.cover,
                      ),
                      border: Border.all(
                        color: primaryColor,
                        width: 2.0,
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: Text(
                  "Upload Bukti Transfer",
                  style: TextStyle(
                      fontWeight: FontWeight.w600, color: Colors.black),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(
                  8,
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: getImageFoto,
                        child: Container(
                          height: 40.0,
                          decoration: BoxDecoration(
                              border: Border.all(), color: primaryColor),
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Text(
                                "Ambil Dari Kamera",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: getImageGaleri,
                        child: Container(
                          height: 40.0,
                          decoration: BoxDecoration(
                              border: Border.all(), color: primaryColor),
                          child: Center(
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Text(
                                "Ambil Dari Galeri",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20.0,
              ),

            ],
          )),
    );
  }

  Future getImageFoto() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.camera,
      imageQuality: 60,
    );
    setState(() {
      _image = image;
      _fileName = image.path != null
          ? (image.path.toLowerCase().contains("png") ||
                  image.path.toLowerCase().contains("jpg") ||
                  image.path.toLowerCase().contains("jpeg")
              ? image.path.split('/').last
              : "Pilih JPG/JPEG, PNG")
          : "Gagal memproses berkas!";
    });
  }

  Future getImageGaleri() async {
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 60,
    );
    setState(() {
      _image = image;
      _fileName = image.path != null
          ? (image.path.toLowerCase().contains("png") ||
                  image.path.toLowerCase().contains("jpg") ||
                  image.path.toLowerCase().contains("jpeg")
              ? image.path.split('/').last
              : "Pilih JPG/JPEG, PNG")
          : "Gagal memproses berkas!";
    });
  }

  Future<dynamic> _onSubmit(
      String namaPengirim,
      String bankAsal,
      String bankTujuan,
      String kode,
      String path,
      String tipe,
      BuildContext context) async {
    bool dosSaveAlat = await RestAPI.doUploadPayment(
        namaPengirim, bankAsal, bankTujuan, path, tipe, kode);
    if (dosSaveAlat) {
      Alert(
        context: context,
        image: Image.asset(
          "assets/alert/check.png",
          color: Colors.red,
        ),
        type: AlertType.success,
        title: "Berhasil",
        desc:"Pembayaran Anda berhasil dan akan diproses!! ",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Kembali",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed:() async {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => TabbarIRKUserScreen()));
            },
            width: 120,
          )
        ],
      ).show();
    } else {
      Component.showAppNotification(context, "Gagal",
          "Pembayaran gagal Cek kembali data yang Anda masukkan");
    }
  }
}
