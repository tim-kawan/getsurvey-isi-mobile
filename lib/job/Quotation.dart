import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:url_launcher/url_launcher.dart';

class FileSertifikat extends StatefulWidget {
  final String kode;
  final String file;

  FileSertifikat({this.kode, this.file, Key key}) : super(key: key);
  @override
  _FileSertifikatState createState() => _FileSertifikatState();
}

class _FileSertifikatState extends State<FileSertifikat> {
  _launchURL(String kode, String file) async {
    var urlInvoice = "${RestAPI.FOTO_URL}/$kode/$file";

    if (await canLaunch(urlInvoice)) {
      await launch(urlInvoice);
    } else {
      throw 'Could not launch $urlInvoice';
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
          color: Colors.white,
        ),
        backgroundColor: primaryColor,
        title: Text(
          "File Sertifikat",
          style: TextStyle(
              color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),
        ),
      ),
      body: Container(
        child: _launchURL(widget.kode, widget.file),
      ),
    );
  }
}
