import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/model/report.dart';
import 'package:get_survey_app/service/RestAPI.dart';

import 'package:get_survey_app/service/ServiceGetData.dart';
import 'package:url_launcher/url_launcher.dart';

class ReportScreen extends StatefulWidget {
  final String kode;
  final int id;
  ReportScreen({this.kode, this.id, Key key}) : super(key: key);
  @override
  _ReportScreenState createState() => _ReportScreenState();
}

class _ReportScreenState extends State<ReportScreen> {
  @override
  void initState() {
    super.initState();
    initData();
  }

  void initData() async {
    await _getReport();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
          color: Colors.white,
        ),
        backgroundColor: primaryColor,
        title: Text(
          "Download File Informasi Rencana Kota",
          style: TextStyle(
              color: Colors.white, fontSize: 15.0, fontWeight: FontWeight.bold),
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: InkWell(
                  onTap: () => launch(
                      '${RestAPI.FOTO_URL}/${widget.kode}/${fileHasilUkur.toString()}'),
                  child: Container(
                    height: 40.0,
                    decoration:
                        BoxDecoration(border: Border.all(), color: Colors.blue),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          "File Hasil Ukur",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 10.0,
              ),
              Expanded(
                child: InkWell(
                  onTap: () => launch(
                      '${RestAPI.FOTO_URL}/${widget.kode}/${fileLampiran.toString()}'),
                  child: Container(
                    height: 40.0,
                    decoration:
                        BoxDecoration(border: Border.all(), color: Colors.blue),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          "File Lampiran",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  var listReport = List<Report>();
  String fileHasilUkur;
  String fileLampiran;

  _getReport() async {
    await API.getListReport(widget.id).then((response) async {
      var result = await json.decode(response.body);
      fileHasilUkur = result['data']['file_hasil_ukur'];
      fileLampiran = result['data']['file_lampiran'];
      print(fileHasilUkur);
      print(fileLampiran);
    });
  }
}
