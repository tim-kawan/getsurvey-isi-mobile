import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_survey_app/account/DetailSurveyor.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/SearchScreen.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:shimmer/shimmer.dart';

class ListSurveyorAll extends StatefulWidget {
  @override
  _ListSurveyorAllState createState() => _ListSurveyorAllState();
}

class _ListSurveyorAllState extends State<ListSurveyorAll> {
  List surveyors = [];
  bool _isLoading = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SearchScreen()));
              })
        ],
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
        centerTitle: true,
        backgroundColor: primaryColor,
        title: Text(
          "List Surveyor",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
          child: (_isLoading)
              ? Center(
                  child: Shimmer.fromColors(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        "Tunggu Sebentar...",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 22.5,
                        ),
                      ),
                    ),
                    baseColor: Colors.grey[700],
                    highlightColor: Colors.grey[200],
                  ),
                )
              : surveyors.isNotEmpty
                  ? GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 40,
                          mainAxisSpacing: 15),
                      itemCount: surveyors.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.of(context)
                                .push(PageRouteBuilder(
                                    opaque: false,
                                    pageBuilder: (BuildContext context, _,
                                            __) =>
                                        DetailSurveyor(
                                          id: surveyors[index]["record"]
                                              ["idSurveyor"],
                                          name: surveyors[index]["record"]
                                              ["nama"],
                                          img: surveyors[index]["record"]
                                              ["fotoUser"],
                                          long: surveyors[index]["record"]
                                              ["long"],
                                          lat: surveyors[index]["record"]
                                              ["lat"],
                                          alamat: surveyors[index]["record"]
                                              ["alamatTempatTinggal"],
                                          email: surveyors[index]["record"]
                                              ["email"],
                                          noAnggota: surveyors[index]["record"]
                                              ["noAnggota"],
                                          rate: surveyors[index]["rate"],
                                          medal: surveyors[index]['medal'],
                                          dateJoin: surveyors[index]["record"]
                                              ["tanggalAwalBerlaku"],
                                        )))
                                .then((_) {
                              _fillListSurveyor();
                            });
                          },
                          child: Container(
                            child: Stack(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 50.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      color: backGround.withOpacity(0.1),
                                    ),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 8.0),
                                          child: Text(
                                            surveyors[index]["record"]["nama"],
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color: primaryBlack,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 8.0, right: 8, bottom: 8),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                width: MediaQuery.of(context).size.width/5,
                                                child: Row(
                                                  children: [
                                                    Icon(
                                                      Icons.location_on,
                                                      color: primaryColor4,
                                                      size: 12,
                                                    ),
                                                    Flexible(
                                                      child: Text(
                                                        surveyors[index]["record"]
                                                            ["kotaTempatTinggal"],
                                                        style: TextStyle(
                                                          color: primaryBlack,
                                                          fontSize: 12,
                                                        ),
                                                        maxLines: 1,
                                                        overflow: TextOverflow.ellipsis,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Row(
                                                children: [
                                                  Text(
                                                    surveyors[index]["rate"],
                                                    style: TextStyle(
                                                      color: primaryBlack,
                                                      fontSize: 14,
                                                    ),
                                                  ),
                                                  Icon(
                                                    Icons.star,
                                                    color: primaryColor4,
                                                    size: 20,
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    bottom: 55.0,
                                  ),
                                  child: Center(
                                    child: Container(
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                image: surveyors[index]
                                                                ["record"]
                                                            ["fotoUser"] ==
                                                        ""
                                                    ? AssetImage(
                                                        'assets/GetSurvey.png')
                                                    : NetworkImage(
                                                        surveyors[index]
                                                                ["record"]
                                                            ["fotoUser"]),
                                                fit: BoxFit.cover,
                                              ),
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(16),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.black54,
                                                  // blurRadius: 1.0,
                                                  // spreadRadius: 1.0,
                                                ),
                                              ],
                                            ),
                                            alignment: Alignment.bottomCenter,
                                            width: 120,
                                            height: 120,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: primaryColor4
                                                      .withOpacity(0.6),
                                                  borderRadius:
                                                      BorderRadius.only(
                                                    bottomRight:
                                                        Radius.circular(16),
                                                    bottomLeft:
                                                        Radius.circular(16),
                                                  )),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  surveyors[index]["record"]
                                                              ["noLisensi"] ==
                                                          ""
                                                      ? SizedBox()
                                                      : Image.asset(
                                                          "assets/medali/kompetensi.png",
                                                          height: 20,
                                                        ),
                                                  SizedBox(
                                                    width: 4.0,
                                                  ),
                                                  surveyors[index]["record"][
                                                              "noLisensiKadastral"] ==
                                                          ""
                                                      ? SizedBox()
                                                      : Image.asset(
                                                          "assets/medali/kadastral.png",
                                                          height: 20,
                                                        ),
                                                  SizedBox(
                                                    width: 4.0,
                                                  ),
                                                  surveyors[index]["record"]
                                                              ["noSkaSkt"] ==
                                                          ""
                                                      ? SizedBox()
                                                      : Image.asset(
                                                          "assets/medali/ska.png",
                                                          height: 20,
                                                        ),
                                                  SizedBox(
                                                    width: 4.0,
                                                  ),
                                                  surveyors[index]["record"][
                                                              "noLisensiIrk"] ==
                                                          ""
                                                      ? SizedBox()
                                                      : Image.asset(
                                                          "assets/medali/irk.png",
                                                          height: 20,
                                                        ),
                                                ],
                                              ),
                                            ),
                                          ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    )
                  : Container(
                      child: Center(
                        child: Text(
                          "Tidak Ada Data",
                          style: TextStyle(color: Colors.black, fontSize: 19),
                        ),
                      ),
                    ),
        ),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fillListSurveyor();
  }

  void _fillListSurveyor() async {
    surveyors.clear();
    List temp = await RestAPI.fetchTopSurveyors(limit: 100);
    surveyors.addAll(temp);
    setState(() {});
    setState(() => _isLoading = false);
  }
}
