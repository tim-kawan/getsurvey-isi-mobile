import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/job/FormPayment.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:url_launcher/url_launcher.dart';

class Payment extends StatefulWidget {
  final String kode;
  Payment({this.kode, Key key}) : super(key: key);
  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
          color: Colors.white,
        ),
        backgroundColor: primaryColor,
        title: Text(
          "Instruksi Pembayaran",
          style: TextStyle(
              color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FormPembayaran(
                                  kode: widget.kode,
                                )));
                  },
                  child: Container(
                    height: 40.0,
                    decoration:
                        BoxDecoration(border: Border.all(), color: Colors.blue),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          "Form Pembayaran",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 10.0,
              ),
              Expanded(
                child: InkWell(
                  onTap: () => launch(
                      "${RestAPI.BASE_URL}/view/irk/invoice/${widget.kode}"),
                  child: Container(
                    height: 40.0,
                    decoration:
                        BoxDecoration(border: Border.all(), color: Colors.blue),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          "Cara pembayaran",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
