import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/job/ListIrkUser/ListInformasiRencanaKota.dart';
import 'package:get_survey_app/job/ListIrkUser/ListInformasiRencanaKotaDone.dart';
import 'package:get_survey_app/job/ListIrkUser/ListInformasiRencanaKotaNew.dart';
import 'package:get_survey_app/job/ListIrkUser/SearchIrkUser.dart';

class TabbarIRKUserScreen extends StatefulWidget {
  @override
  _TabbarIRKUserScreenState createState() => _TabbarIRKUserScreenState();
}

class _TabbarIRKUserScreenState extends State<TabbarIRKUserScreen>
    with SingleTickerProviderStateMixin {
  TabController _controller;

  List<Widget> list = [
    Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Text(
        "Baru",
        style: TextStyle(fontSize: 16),
        textAlign: TextAlign.center,
      ),
    ),
    Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Text(
        "Proses",
        style: TextStyle(fontSize: 16),
        textAlign: TextAlign.center,
      ),
    ),
    Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Text(
        "Selesai",
        style: TextStyle(fontSize: 16),
        textAlign: TextAlign.center,
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: WillPopScope(
        onWillPop: () => Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (context) => BtnNavigation(
                  atIndex: 0,
                )),
                (Route<dynamic> route) => false),
        child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (context) => BtnNavigation(
                        atIndex: 0,
                      )),
                      (Route<dynamic> route) => false),
              color: Colors.white,
            ),
            backgroundColor: primaryColor,
            title: Text(
              "List Informasi Rencana kota",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            ),
            bottom: TabBar(
              controller: _controller,
              tabs: list,
            ),
            actions: [
              IconButton(
                  icon: Icon(
                    Icons.search,
                    color: Colors.white,
                    size: 20,
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SearchIrkUser()));
                  })
            ],
          ),
          body: TabBarView(
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: ListIzinRencanaKotaNew(),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: ListIzinRencanaKOta(),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: ListIzinRencanaKotaDone(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}