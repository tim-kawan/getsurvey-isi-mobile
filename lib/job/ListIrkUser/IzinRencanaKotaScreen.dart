import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/job/ListIrkUser/ListInformasiRencanaKota.dart';
import 'package:get_survey_app/job/ListIrkUser/TabbarListIrkUser.dart';
import 'package:get_survey_app/map/PickLocation.dart';
import 'package:get_survey_app/model/irk.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class IzinRencanaKotaScreen extends StatefulWidget {
  final double lat;
  final double long;
  final String fullname;
  final String lokasiTanah;
  IzinRencanaKotaScreen(
      {this.lat, this.long, this.fullname, this.lokasiTanah, Key key})
      : super(key: key);
  @override
  _IzinRencanaKotaScreenState createState() => _IzinRencanaKotaScreenState();
}

class _IzinRencanaKotaScreenState extends State<IzinRencanaKotaScreen> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController _namaPemilikTanahCtrl = TextEditingController();
  TextEditingController _npwpCtrl = TextEditingController();
  TextEditingController _lokasiTanahCtrl = TextEditingController();
  TextEditingController _nikCtrl = TextEditingController();
  TextEditingController _luasTanahCtrl = TextEditingController();
  TextEditingController _hakTanahCtrl = TextEditingController();

  String _selectedHak;
  bool _isLoading = false;
  String _fileNameKtp = 'Pilih Berkas KTP PDF, JPG/JPEG, PNG';
  String _fileNameSer = 'Pilih Berkas Lisensi PDF, JPG/JPEG, PNG';
  String _pathKtp;
  String _pathSer;
  String failedFileKtp;
  String failedFileSer;

  String alamat;
  double lat;
  double lon;

  String erorNama = "";

  int hakId = 1;

  final List<String> _listHak = [
    "Hak Milik",
    "Hak Guna Bangunan",
    "Hak Guna Usaha",
    "Hak Pakai",
    'Hak Lainnya(isi manual)'
  ];

  void saveData() async {
    await Session.doSave("namaPem", _namaPemilikTanahCtrl.text);
  }

  Future<String> getNama() async {
    return await Session.getValue("namaPem") ?? "";
  }

  _getData() async {
    lon = await Session.getValue("longitude");
    lat = await Session.getValue("latitude");
    setState(() {});
  }

  void initData() async {
    await _getData();
  }

  @override
  void initState() {
    super.initState();
    initData();
    getNama().then((value) => _namaPemilikTanahCtrl.text = value);
    _lokasiTanahCtrl.text =
        widget.lokasiTanah == null ? "" : widget.lokasiTanah;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => TabbarIRKUserScreen()),
          (Route<dynamic> route) => false),
      child: SafeArea(
          child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (context) => TabbarIRKUserScreen()),
                  (Route<dynamic> route) => false);
            },
            color: Colors.white,
          ),
          backgroundColor: primaryColor,
          title: Text(
            "Form Informasi Rencana Kota",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ),
        body: ModalProgressHUD(
          child: _body(),
          inAsyncCall: _isLoading,
        ),
        bottomNavigationBar: BottomAppBar(
          child: Builder(
              builder: (context) => InkWell(
                    onTap: () async {
                      setState(() {
                        _isLoading = true;
                      });
                      String hak;
                      if (_formKey.currentState.validate() &&
                          _selectedHak != null) {
                        if (_selectedHak != 'Hak Lainnya(isi manual)') {
                          hak = _selectedHak;
                        } else if (_selectedHak == 'Hak Lainnya(isi manual)') {
                          hak = _hakTanahCtrl.text;
                        }

                        int idUser = await Session.getValue(Session.USER_ID);
                        String kode = await Session.getValue("kodeIrk");

                        await _onSubmit(
                            kode,
                            _namaPemilikTanahCtrl.text,
                            lat,
                            lon,
                            _lokasiTanahCtrl.text,
                            _nikCtrl.text,
                            _npwpCtrl.text,
                            hak,
                            int.parse(_luasTanahCtrl.text),
                            idUser,
                            context);
                      }
                    },
                    child: Container(
                      height: 50.0,
                      decoration: BoxDecoration(
                        color: primaryColor,
                      ),
                      child: Center(
                        child: Text(
                          "Kirim",
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: "Sans",
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  )),
        ),
      )),
    );
  }

  Widget _body() {
    return Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: ListView(children: [
            SizedBox(
              height: 20,
            ),
            Text(
              "Alamat Tanah",
              style: TextStyle(
                  color: primaryColor4,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 60.0,
              alignment: AlignmentDirectional.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(14.0),
                  color: Colors.white,
                  border: Border.all(color: primaryColor4, width: 1)),
              padding: EdgeInsets.only(
                  left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
              child: Theme(
                data: ThemeData(
                  hintColor: Colors.transparent,
                ),
                child: TextFormField(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PickLocation(
                                  navigate: 1,
                                )));
                  },
                  controller: _lokasiTanahCtrl,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Alamat Sertipikat",
                      suffixIcon: Icon(
                        Icons.location_on,
                        color: Colors.grey,
                      ),
                      hintStyle: TextStyle(
                        fontSize: 12.0,
                        fontFamily: 'Sans',
                        letterSpacing: 0.3,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        _isLoading = false;
                      });
                      return "Data Tidak Boleh Kosong!!";
                    }
                    return null;
                  },
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "Nama Pemilik",
              style: TextStyle(
                  color: primaryColor4,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 60.0,
              alignment: AlignmentDirectional.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(14.0),
                  color: Colors.white,
                  border: Border.all(color: primaryColor4, width: 1)),
              padding: EdgeInsets.only(
                  left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
              child: Theme(
                data: ThemeData(
                  hintColor: Colors.transparent,
                ),
                child: TextFormField(
                  controller: _namaPemilikTanahCtrl,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Nama Pemilik Sertipikat",
                      suffixIcon: Icon(
                        Icons.person,
                        color: Colors.grey,
                      ),
                      hintStyle: TextStyle(
                        fontSize: 12.0,
                        fontFamily: 'Sans',
                        letterSpacing: 0.3,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        _isLoading = false;
                      });
                      return "Data Tidak Boleh Kosong!!";
                    }
                    return null;
                  },
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "NIK",
              style: TextStyle(
                  color: primaryColor4,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 60.0,
              alignment: AlignmentDirectional.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(14.0),
                  color: Colors.white,
                  border: Border.all(color: primaryColor4, width: 1)),
              padding: EdgeInsets.only(
                  left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
              child: Theme(
                data: ThemeData(
                  hintColor: Colors.transparent,
                ),
                child: TextFormField(
                  controller: _nikCtrl,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Nomor Identitas Kependudukan Anda",
                      suffixIcon: Icon(
                        Icons.credit_card,
                        color: Colors.grey,
                      ),
                      hintStyle: TextStyle(
                        fontSize: 12.0,
                        fontFamily: 'Sans',
                        letterSpacing: 0.3,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        _isLoading = false;
                      });
                      return "Data Tidak Boleh Kosong!!";
                    }
                    return null;
                  },
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "NPWP",
              style: TextStyle(
                  color: primaryColor4,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 60.0,
              alignment: AlignmentDirectional.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(14.0),
                  color: Colors.white,
                  border: Border.all(color: primaryColor4, width: 1)),
              padding: EdgeInsets.only(
                  left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
              child: Theme(
                data: ThemeData(
                  hintColor: Colors.transparent,
                ),
                child: TextFormField(
                  controller: _npwpCtrl,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Nomor NPWP Anda",
                      suffixIcon: Icon(
                        Icons.credit_card,
                        color: Colors.grey,
                      ),
                      hintStyle: TextStyle(
                        fontSize: 12.0,
                        fontFamily: 'Sans',
                        letterSpacing: 0.3,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        _isLoading = false;
                      });
                      return "Data Tidak Boleh Kosong!!";
                    }
                    return null;
                  },
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "Luas Tanah",
              style: TextStyle(
                  color: primaryColor4,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 60.0,
              alignment: AlignmentDirectional.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(14.0),
                  color: Colors.white,
                  border: Border.all(color: primaryColor4, width: 1)),
              padding: EdgeInsets.only(
                  left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
              child: Theme(
                data: ThemeData(
                  hintColor: Colors.transparent,
                ),
                child: TextFormField(
                  controller: _luasTanahCtrl,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Masukkan Luas Tanah Sesuai Sertipikat",
                      suffixIcon: Icon(
                        Icons.crop_landscape,
                        color: Colors.grey,
                      ),
                      hintStyle: TextStyle(
                        fontSize: 12.0,
                        fontFamily: 'Sans',
                        letterSpacing: 0.3,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      setState(() {
                        _isLoading = false;
                      });
                      return "Data Tidak Boleh Kosong!!";
                    }
                    return null;
                  },
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Jenis Hak Tanah",
                  style: TextStyle(
                      color: primaryColor4,
                      fontSize: 16,
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 60.0,
                  alignment: AlignmentDirectional.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(14.0),
                      color: Colors.white,
                      border: Border.all(color: primaryColor4, width: 1)),
                  padding: EdgeInsets.only(
                      left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
                  child: Theme(
                    data: ThemeData(
                      hintColor: Colors.transparent,
                    ),
                    child: DropdownButton(
                      isExpanded: true,
                      icon: Icon(Icons.keyboard_arrow_down),
                      underline: Container(),
                      hint: Text(
                        'Pilih',
                        style: TextStyle(
                          fontSize: 12.0,
                          fontFamily: 'Sans',
                          letterSpacing: 0.3,
                          color: Colors.grey,
                        ),
                      ),
                      // Not necessary for Option 1
                      value: _selectedHak,
                      onChanged: (newValue) {
                        setState(() {
                          _selectedHak = newValue;
                        });
                      },
                      items: _listHak.map((category) {
                        return DropdownMenuItem(
                          child: new Text(category),
                          value: category,
                        );
                      }).toList(),
                    ),
                  ),
                )
              ],
            ),
            (_selectedHak == "Hak Lainnya(isi manual)")
                ? Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Tuliskan Jenis Alas Hak yang Anda Miliki",
                          style: TextStyle(
                              color: primaryColor4,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 60.0,
                          alignment: AlignmentDirectional.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(14.0),
                              color: Colors.white,
                              border:
                                  Border.all(color: primaryColor4, width: 1)),
                          padding: EdgeInsets.only(
                              left: 20.0, right: 30.0, top: 0.0, bottom: 0.0),
                          child: Theme(
                            data: ThemeData(
                              hintColor: Colors.transparent,
                            ),
                            child: TextFormField(
                              controller: _hakTanahCtrl,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Alas Hak",
                                  suffixIcon: Icon(
                                    Icons.category,
                                    color: Colors.grey,
                                  ),
                                  hintStyle: TextStyle(
                                    fontSize: 12.0,
                                    fontFamily: 'Sans',
                                    letterSpacing: 0.3,
                                    color: Colors.grey,
                                  )),
                              validator: (value) {
                                if (value.isEmpty) {
                                  setState(() {
                                    _isLoading = false;
                                  });
                                  return "Data Tidak Boleh Kosong!!";
                                }
                                return null;
                              },
                            ),
                          ),
                        ),
                      ],
                    ))
                : Container(),
            SizedBox(
              height: 20,
            ),
            Text(
              "Unggah Sertipikat",
              style: TextStyle(
                  color: primaryColor4,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 10,
            ),
            SizedBox(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: _openFileSertifikat,
                    child: Container(
                      padding: const EdgeInsets.all(10.0),
                      child: _fileNameSer ==
                              'Pilih Berkas Lisensi PDF, JPG/JPEG, PNG'
                          ? Icon(
                              Icons.add,
                              color: Colors.grey,
                              size: 70,
                            )
                          : Icon(
                              Icons.file_present,
                              color: Colors.red,
                              size: 70,
                            ),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey, width: 1)),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    _fileNameSer,
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "Unggah KTP",
              style: TextStyle(
                  color: primaryColor4,
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 10,
            ),
            SizedBox(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: _openFileKtp,
                    child: Container(
                      padding: const EdgeInsets.all(10.0),
                      child:
                          _fileNameKtp == 'Pilih Berkas KTP PDF, JPG/JPEG, PNG'
                              ? Icon(
                                  Icons.add,
                                  color: Colors.grey,
                                  size: 70,
                                )
                              : Icon(
                                  Icons.file_present,
                                  color: Colors.red,
                                  size: 70,
                                ),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey, width: 1)),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    _fileNameKtp,
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ]),
        ));
  }

  void _openFileKtp() async {
    _fileNameKtp = 'Pilih Berkas Lisensi PDF, JPG/JPEG, PNG';
    failedFileKtp = "";
    try {
      _pathKtp = await FilePicker.getFilePath();
      if (_pathKtp != null) {
        if (_pathKtp.toLowerCase().contains(".pdf") == false &&
            _pathKtp.toLowerCase().contains(".png") == false &&
            _pathKtp.toLowerCase().contains(".jpg") == false &&
            _pathKtp.toLowerCase().contains(".jpeg") == false) {
          failedFileKtp =
              "Gagal!! Pastikan file anda memilkki extension seperti contoh( .pdf, .png, .jpeg, .jpg) dibelakang nama file anda, ubah nama file dan sesuaikan dengan format file anda";
          _pathKtp = null;
          _fileNameKtp = "Gagal Memproses Berkas!";
        } else {
          _fileNameKtp = (_pathKtp.toLowerCase().contains("pdf") ||
                  _pathKtp.toLowerCase().contains("png") ||
                  _pathKtp.toLowerCase().contains("jpg") ||
                  _pathKtp.toLowerCase().contains("jpeg")
              ? _pathKtp.split('/').last
              : "Pilih Berkas Lisensi PDF, JPG/JPEG, PNG");
        }
      } else {
        _fileNameKtp = "Gagal Memproses Berkas!";
        _pathKtp = null;
      }
      setState(() {});
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  void _openFileSertifikat() async {
    _fileNameSer = 'Pilih Berkas Lisensi PDF, JPG/JPEG, PNG';
    failedFileSer = "";
    try {
      _pathSer = await FilePicker.getFilePath();
      if (_fileNameSer == "Pilih Berkas Lisensi PDF, JPG/JPEG, PNG" &&
          _fileNameSer == "") {
        _pathSer = null;
      }
      if (_pathSer != null) {
        if (_pathSer.toLowerCase().contains(".pdf") == false &&
            _pathSer.toLowerCase().contains(".png") == false &&
            _pathSer.toLowerCase().contains(".jpg") == false &&
            _pathSer.toLowerCase().contains(".jpeg") == false) {
          failedFileSer =
              "Gagal!! Pastikan file anda memilkki extension seperti contoh( .pdf, .png, .jpeg, .jpg) dibelakang nama file anda, ubah nama file dan sesuaikan dengan format file anda";
          _pathSer = null;
          _fileNameSer = "Gagal Memproses Berkas!";
        } else {
          _fileNameSer = (_pathSer.toLowerCase().contains("pdf") ||
                  _pathSer.toLowerCase().contains("png") ||
                  _pathSer.toLowerCase().contains("jpg") ||
                  _pathSer.toLowerCase().contains("jpeg")
              ? _pathSer.split('/').last
              : "Pilih Berkas Lisensi PDF, JPG/JPEG, PNG");
        }
      } else {
        _fileNameSer = "Gagal Memproses Berkas!";
        _pathSer = null;
      }
      setState(() {});
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  Future<dynamic> _onSubmit(
    String kode,
    String namaPemilik,
    double lat,
    double long,
    String alamatTanah,
    String nik,
    String npwp,
    String haktanah,
    int luasTanah,
    int userId,
    BuildContext context,
  ) async {
    if (_pathKtp != null && _pathSer != null) {
      await RestAPI.addIzinKerja(
        kode,
        namaPemilik,
        lat,
        long,
        alamatTanah,
        nik,
        npwp,
        haktanah,
        luasTanah,
        userId,
      );

      if (_pathKtp.toLowerCase().contains("pdf") ||
          _pathKtp.toLowerCase().contains("png") ||
          _pathKtp.toLowerCase().contains("jpg") ||
          _pathKtp.toLowerCase().contains("jpeg")) {
        String tipe = "jpg";

        if (_pathKtp.toLowerCase().contains("pdf")) {
          tipe = "pdf";
        } else if (_pathKtp.toLowerCase().contains("png")) {
          tipe = "png";
        }
        await RestAPI.doUploadKtp(userId, kode, _pathKtp, tipe);

        if (_pathSer.toLowerCase().contains("pdf") ||
            _pathSer.toLowerCase().contains("png") ||
            _pathSer.toLowerCase().contains("jpg") ||
            _pathSer.toLowerCase().contains("jpeg")) {
          String tipe = "jpg";

          if (_pathSer.toLowerCase().contains("pdf")) {
            tipe = "pdf";
          } else if (_pathSer.toLowerCase().contains("png")) {
            tipe = "png";
          }
          await RestAPI.doUploadFile(userId, kode, _pathSer, tipe);
        }
      }

      Alert(
        context: context,
        image: Image.asset(
          "assets/alert/check.png",
          color: Colors.green,
        ),
        type: AlertType.success,
        title: "Berhasil",
        desc: "Data Informasi Rencana Kota Berhasil di Tambahkan!!",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Kembali",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () => _sucsessSubmit(),
            width: 120,
          )
        ],
      ).show();
    } else {
      setState(() {
        _isLoading = false;
      });
      Component.showAppNotification(
          context, "Gagal!", "Cek Kembali data yang Anda masukkan");
    }
  }

  _sucsessSubmit() async {
    await Session.doRemove("longitude");
    await Session.doRemove("latitude");
    await Session.doRemove("namaPem");
    await Session.doRemove("kodeIrk");
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => TabbarIRKUserScreen()),
        (Route<dynamic> route) => false);
  }
}
