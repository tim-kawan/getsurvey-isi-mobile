import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/job/DetailIrk.dart';
import 'package:get_survey_app/job/ListIrkUser/IzinRencanaKotaScreen.dart';
import 'package:get_survey_app/model/AddIrk.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/ServiceGetData.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class ListIzinRencanaKOta extends StatefulWidget {
  final String status;
  ListIzinRencanaKOta({this.status, Key key}) : super(key: key);
  @override
  _ListIzinRencanaKOtaState createState() => _ListIzinRencanaKOtaState();
}

class _ListIzinRencanaKOtaState extends State<ListIzinRencanaKOta> {
  String kode;
  String status;
  int statusSurveyor = -1;
  bool _isLoading = true;
  List<Irk> listIRK = [];
  int cekStatusKeanggotaan = -1;
  int idUser;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => BtnNavigation()),
          (Route<dynamic> route) => false),
      child: SafeArea(
        child: Scaffold(
            backgroundColor: Colors.white,
            body: ModalProgressHUD(inAsyncCall: _isLoading, child: _body()),
            floatingActionButton: FloatingActionButton(
              onPressed: () async {
                await _getCodeIrk();
                await Session.doSave("kodeIrk", kode);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => IzinRencanaKotaScreen()));
              },
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
            )),
      ),
    );
  }

  void initData() async {
    await _getListIrk();
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    initData();
    super.initState();
  }

  Widget _body() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: listIRK.isNotEmpty
          ? ListView.builder(
              itemCount: listIRK.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 2,
                              blurRadius: 4,
                              offset:
                                  Offset(0, 3), // changes position of shadow
                            ),
                          ]),
                      child: ListTile(
                        title: Row(
                          children: [
                            Container(
                                width: 200,
                                child: Text("Nama Pemilik : " +
                                    listIRK[index].namaPemilik)),
                            Text(
                              "#" + listIRK[index].id.toString(),
                              style: TextStyle(color: Colors.blueAccent),
                            ),
                          ],
                        ),
                        subtitle: Text("Luas Tanah : " +
                            listIRK[index].luasTanah.toString() +
                            "/m2"),
                        trailing: Icon(
                          Icons.arrow_forward_ios,
                        ),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DetailIrk(
                                        status: "User",
                                        statusIrk: listIRK[index].status,
                                        id: listIRK[index].id,
                                        owner: listIRK[index].userID,
                                        namaPemilik: listIRK[index].namaPemilik,
                                        alamatTanah: listIRK[index].alamatTanah,
                                        tipe: "jasa",
                                        kode: listIRK[index].kode,
                                        file: listIRK[index].fileSer,
                                        lat: listIRK[index].lat,
                                        long: listIRK[index].long,
                                      )));
                        },
                      )),
                );
              })
          : Center(
              child: Text(
                "Tidak Ada Data Irk",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w800),
              ),
            ),
    );
  }

  _getCodeIrk() async {
    await API.getNewCode().then((response) async {
      var result = await json.decode(response.body);
      kode = result['data']['kode'];
    });
  }

  _getListIrk() async {
    int idUser = await Session.getValue(Session.USER_ID);
    await RestAPI.getIrkWithIdUser(id: idUser).then((results) async {
      results.forEach((irk) {
        Irk items = Irk.fromJson(irk);
        if (items.userID == idUser && items.status < 5) {
          listIRK.add(items);
        }
      });
      setState(() {});
    });
  }
}
