import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/job/DetailIrk.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class SearchIrkUser extends StatefulWidget {
  @override
  _SearchIrkUserState createState() => _SearchIrkUserState();
}

class _SearchIrkUserState extends State<SearchIrkUser> {
  final TextEditingController _filter = new TextEditingController();
  final dio = new Dio();
  String _searchText = "";
  List names = new List();
  List filteredNames = new List();
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = new Text(
    'Search Informasi Rencana Kota',
    style: TextStyle(fontSize: 15),
  );
  bool _isLoading = true;

  _SearchIrkUserState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredNames = names;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  @override
  void initState() {
    this._getNames();
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: Container(
        child: ModalProgressHUD(inAsyncCall: _isLoading, child: _buildList()),
      ),
      // resizeToAvoidBottomPadding: false,
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      backgroundColor: primaryColor,
      centerTitle: true,
      title: _appBarTitle,
      actions: [
        new IconButton(
          icon: _searchIcon,
          onPressed: _searchPressed,
        ),
      ],
    );
  }

  Widget _buildList() {
    if (_searchText.isNotEmpty) {
      List tempList = new List();
      for (int i = 0; i < filteredNames.length; i++) {
        if (filteredNames[i]['nama_pemilik']
            .toLowerCase()
            .contains(_searchText.toLowerCase()) ||
            filteredNames[i]['id']
                .toString()
                .toLowerCase()
                .contains(_searchText.toLowerCase())) {
          tempList.add(filteredNames[i]);
        }
      }
      filteredNames = tempList;
    }
    return filteredNames.isNotEmpty
        ? ListView.builder(
      itemCount: names == null ? 0 : filteredNames.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 4,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ]),
            child: ListTile(
              title: Row(
                children: [
                  Container(
                      width: 200,
                      child: Text("Nama Pemilik : " +
                          filteredNames[index]['nama_pemilik'])),
                  Text(
                    "#" + filteredNames[index]['id'].toString(),
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                ],
              ),
              subtitle: Text("Luas Tanah : " +
                  filteredNames[index]['luas_tanah'].toString() +
                  "/m2"),
              trailing: Icon(
                Icons.arrow_forward_ios,
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DetailIrk(
                          status: "Surveyor",
                          statusIrk: filteredNames[index]['status'],
                          id: filteredNames[index]['id'],
                          owner: filteredNames[index]['user_id'],
                          namaPemilik: filteredNames[index]
                          ['nama_pemilik'],
                          alamatTanah: filteredNames[index]
                          ['alamat_tanah'],
                          tipe: "jasa",
                          kode: filteredNames[index]['kode'],
                          file: filteredNames[index]
                          ['file_sertifikat'],
                          lat: filteredNames[index]['lat'],
                          long: filteredNames[index]['long'],
                        )));
              },
            ),
          ),
        );
      },
    )
        : Center(
      child: Text(
        "Tidak Ada Data Irk",
        style: TextStyle(
            color: Colors.black,
            fontSize: 18.0,
            fontWeight: FontWeight.w800),
      ),
    );
  }

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          style: TextStyle(color: Colors.white),
          decoration: new InputDecoration(
              hintText: 'Search...', hintStyle: TextStyle(color: Colors.white)),
        );
      } else {
        this._searchIcon = new Icon(
          Icons.search,
          color: Colors.white,
        );
        this._appBarTitle = new Text(
          'Search Informasi Rencana Kota',
          style: TextStyle(fontSize: 15),
        );
        filteredNames = names;
        _filter.clear();
      }
    });
  }

  void _getNames() async {
    int id = await Session.getValue(Session.USER_ID);
    final response = await dio
        .get('${RestAPI.BASE_URL}/api/irk/?user_id=$id');
    List tempList = new List();
    for (int i = 0; i < response.data['data'].length; i++) {
      tempList.add(response.data['data'][i]);
    }
    setState(() {
      names = tempList;
      names.shuffle();
      filteredNames = names;
      _isLoading = false;
    });
  }
}
