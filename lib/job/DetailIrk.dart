import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_survey_app/chat/Chat.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BannerPopUp.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/job/EditIRk.dart';
import 'package:get_survey_app/job/FormPayment.dart';
import 'package:get_survey_app/job/ReportIRk.dart';
import 'package:get_survey_app/map/TrackingLocation.dart';
import 'package:get_survey_app/map/TrackingUser.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/ServiceGetData.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:permission_handler/permission_handler.dart' as handler;

class DetailIrk extends StatefulWidget {
  final String namaPemilik;
  final String alamatTanah;
  final String luasTanah;
  final String nik;
  final String npwp;
  final String hakTanah;
  final double long;
  final double lat;
  final String status;
  final int id;
  final int owner;
  final String tipe;
  final String kode;
  final int statusIrk;
  final String file;
  DetailIrk(
      {this.namaPemilik,
      this.alamatTanah,
      this.hakTanah,
      this.luasTanah,
      this.lat,
      this.long,
      this.nik,
      this.npwp,
      this.status,
      this.id,
      this.owner,
      this.kode,
      this.tipe,
      this.statusIrk,
      this.file,
      Key key})
      : super(key: key);
  @override
  _DetailIrkState createState() => _DetailIrkState();
}

class _DetailIrkState extends State<DetailIrk> {
  var dataRoom;
  bool _isLoad = true;
  String fileHasilUkur;
  String fileLampiran;
  String namaPemilik;
  String nik;
  String luasTanah;
  String alamatTanah;
  int status;
  int diTeruskanPengawas;
  String npwp;
  String hakTanah;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.grey[300],
          appBar: AppBar(
            centerTitle: true,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context),
              color: Colors.white,
            ),
            backgroundColor: primaryColor,
            title: Text(
              "Detail Informasi Rencana Kota",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            ),
          ),
          body: ModalProgressHUD(inAsyncCall: _isLoad, child: _body())),
    );
  }

  @override
  void initState() {
    super.initState();
    print(widget.file);
    _initData();
  }

  Widget _body() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Nama Pemilik",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(namaPemilik == null ? "" : namaPemilik.toString(),
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w900)),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10), color: Colors.white),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Nik",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(nik == null ? "" : nik.toString()),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10), color: Colors.white),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Luas tanah",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                    luasTanah == null ? "" : luasTanah.toString() + ' /m2'),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10), color: Colors.white),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Alamat Tanah",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(alamatTanah == null ? "" : alamatTanah.toString()),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10), color: Colors.white),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Npwp",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(npwp == null ? "" : npwp.toString()),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10), color: Colors.white),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Hak Tanah",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(hakTanah == null ? "" : hakTanah.toString()),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10), color: Colors.white),
            ),
            SizedBox(
              height: 5,
            ),
            Center(
              child: RaisedButton(
                onPressed: () {
                  if (widget.file.toLowerCase().contains("png") ||
                      widget.file.toLowerCase().contains("jpg") ||
                      widget.file.toLowerCase().contains("jpeg")) {
                    print("foto");
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => WebViewPopUp(
                                  url: widget.file,
                                  title: 2,
                                )));
                  } else {
                    launch("${widget.file}");
                  }
                },
                child: Text(
                  "Lihat Sertipikat",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            widget.status == "Surveyor"
                ? _detailIrkSurveyor(context)
                : _detailIrkUser(context)
          ],
        ),
      ),
    );
  }

  Container _detailIrkSurveyor(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    Container container = Container(
      child: Center(
        child: Text("-"),
      ),
    );
    if (status == 0) {
      container = Container(
          width: size.width,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Container(
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                onPressed: () async {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => EditIrk(
                                luasTanah: widget.luasTanah,
                                kode: widget.kode,
                                id: widget.id,
                              )));
                },
                child: Text(
                  "Edit Data",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Builder(
                builder: (context) => RaisedButton(
                  onPressed: () => _onSubmitApply(context),
                  child: Text(
                    "Ambil",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w700),
                  ),
                  color: primaryColor,
                  padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                ),
              ),
            )
          ]));
    } else if (status == 1) {
      container = Container(
        width: size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                onPressed: () => launch(
                    '${RestAPI.BASE_URL}/view/irk/quotation/${widget.kode}'),
                child: Text(
                  "Quotation",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                onPressed: () => _doChat(existRoom: dataRoom),
                child: Text(
                  "Chat",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
          ],
        ),
      );
    } else if (status == 2) {
      container = Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
              child: Center(
                  child: Text(
                "Menunggu admin memproses pembayaran",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w700,
                    fontSize: 17),
              )),
            ),
            Container(
              width: size.width,
              child: RaisedButton(
                onPressed: () => _doChat(existRoom: dataRoom),
                child: Text(
                  "Chat",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
          ],
        ),
      );
    } else if (status == 3) {
      container = Container(
        width: size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
                child: RichText(
              text: TextSpan(
                children: const <TextSpan>[
                  TextSpan(
                    text: "Note : ",
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                  TextSpan(
                      text: 'Batas maksimal Mulai pengukuran adalah',
                      style: TextStyle(color: Colors.black)),
                  TextSpan(
                      text:
                          ' 7 hari setelah pembayaran di setujui oleh admin! ',
                      style: TextStyle(color: Colors.red)),
                  TextSpan(
                      text:
                          ' jika selama 7 hari tersebut anda tidak melakukan pengukuran maka status surveyor anda akan menjadi ',
                      style: TextStyle(color: Colors.black)),
                  TextSpan(
                      text: ' Evaluasi ', style: TextStyle(color: Colors.red)),
                ],
              ),
            )),
            SizedBox(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Builder(
                builder: (context) => RaisedButton(
                  onPressed: () async {
                    _onSubmitDoing(context);
                  },
                  child: Text(
                    "Mulai Pengukuran hari ini",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w700),
                  ),
                  color: primaryColor,
                  padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                onPressed: () => _doChat(existRoom: dataRoom),
                child: Text(
                  "Chat",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
          ],
        ),
      );
    } else if (status == 4) {
      container = Container(
        width: size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                onPressed: () => _doChat(existRoom: dataRoom),
                child: Text(
                  "Chat",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                onPressed: () async {
                  var tracked = await RestAPI.trackGet(widget.id);
                  var isShow = true;

                  if (tracked != null) {
                    if (tracked["status"] == 0) {
                      isShow = false;
                    }
                  }
                  handler.ServiceStatus serviceStatus =
                      await handler.PermissionHandler()
                          .checkServiceStatus(handler.PermissionGroup.location);
                  print("ngecek");
                  print(serviceStatus);
                  bool cekStatusPermission =
                      await Session.getValue("locationPermission");
                  if (cekStatusPermission == true) {
                    if (isShow) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TrackingLocation(
                                    lat: widget.lat,
                                    lng: widget.long,
                                    id: widget.id,
                                    namaPemilik: widget.namaPemilik,
                                    alamatTanah: widget.alamatTanah,
                                  )));
                    } else {
                      Component.showAppNotification(
                          context, "Info", "Tracking sudah Selesai!");
                    }
                  } else {
                    await Future.delayed(Duration.zero, () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) =>
                              CupertinoAlertDialog(
                                title: new Text(
                                    "Kami akan melakukan tracking lokasi Anda, agar pemohon dapat mengetahui lokasi Anda selama menuju lokasi survey. Lokasi Anda akan kami simpan di getsurvey.id"),
                                content: new Text(
                                    " Jika Anda setuju maka Anda dapat memilih tombol (Setuju) agar dapat melakukan proses berikutnya, dengan memberi hak akses lokasi. Jika Anda (Tidak setuju), maka proses selanjutnya tidak dapat dilanjutkan. Terima Kasih."),
                                actions: <Widget>[
                                  CupertinoDialogAction(
                                    isDefaultAction: true,
                                    child: Text("Setuju"),
                                    onPressed: () async {
                                      serviceStatus =
                                          await handler.PermissionHandler()
                                              .checkServiceStatus(handler
                                                  .PermissionGroup.location);
                                      print("aktivasi");
                                      print(serviceStatus.runtimeType);
                                      await Session.doSave(
                                          'locationPermission', true);
                                      Navigator.pop(context);
                                      if (serviceStatus ==
                                          handler.ServiceStatus.enabled) {
                                        if (isShow) {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      TrackingLocation(
                                                        lat: widget.lat,
                                                        lng: widget.long,
                                                        id: widget.id,
                                                        namaPemilik:
                                                            widget.namaPemilik,
                                                        alamatTanah:
                                                            widget.alamatTanah,
                                                      )));
                                        } else {
                                          Component.showAppNotification(
                                              context,
                                              "Info",
                                              "Tracking sudah Selesai!");
                                        }
                                      } else {
                                        Navigator.pop(context);
                                      }

                                      setState(() {});
                                    },
                                  ),
                                  CupertinoDialogAction(
                                    child: Text("Tidak Setuju"),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                  )
                                ],
                              ));
                    });
                  }
                },
                child: Text(
                  "live location Anda",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                onPressed: () async {
                  var tracked = await RestAPI.trackGet(widget.id);
                  if (tracked['isEnding']) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ReportIrk(
                                  kode: widget.kode,
                                )));
                  } else {
                    Component.showAppNotification(context, "Info",
                        "Tracking belum diselesaikan/disetujui Pemohon!");
                  }
                },
                child: Text(
                  "Laporkan Pekerjaan Selesai",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
          ],
        ),
      );
    } else if (status == 5 && diTeruskanPengawas == 0) {
      container = Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
                child: Text(
              "Data sedang diproses oleh admin",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 17),
            )),
            RaisedButton(
              onPressed: () => _doChat(existRoom: dataRoom),
              child: Text(
                "Chat",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontWeight: FontWeight.w700),
              ),
              color: primaryColor,
              padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
            ),
          ],
        ),
      );
    } else if (status == 5 && diTeruskanPengawas == 1) {
      container = Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
                child: Text(
              "Data Berhasil di Setujui",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 17),
            )),
            RaisedButton(
              onPressed: () => _doChat(existRoom: dataRoom),
              child: Text(
                "Chat",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontWeight: FontWeight.w700),
              ),
              color: primaryColor,
              padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
            ),
          ],
        ),
      );
    } else if (status == 6) {
      container = Container(
        width: MediaQuery.of(context).size.width,
        child: Center(
            child: Text(
          "Pembayaran sudah bisa dicairkan silahkan menunggu pemberitahuan atau menghubungi admin",
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.w700, fontSize: 17),
        )),
      );
    } else {
      container = Container();
    }
    return container;
  }

  Container _detailIrkUser(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    Container container = Container(
      child: Center(
        child: Text("-"),
      ),
    );
    if (status == 0) {
      //Belum diProses
      container = Container(
        width: size.width,
        child: RaisedButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => EditIrk(
                          luasTanah: luasTanah.toString(),
                          kode: widget.kode,
                          id: widget.id,
                        )));
          },
          child: Text(
            "Edit Data",
            style: TextStyle(
                color: Colors.white, fontSize: 17, fontWeight: FontWeight.w700),
          ),
          color: primaryColor,
          padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
        ),
      );
    } else if (status == 1) {
      container = Container(
        width: size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Segera Lunasi pembayaran Anda paling lambat 3*24 jam, jika tidak data Anda akan berstatus kadaluarsa/expired atau tidak dapat diproses lagi.",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 17),
            ),
            Container(
              width: size.width,
              child: RaisedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FormPembayaran(
                                kode: widget.kode,
                              )));
                },
                child: Text(
                  "Form Pembayaran",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
            Container(
              width: size.width,
              child: RaisedButton(
                onPressed: () => launch(
                    '${RestAPI.BASE_URL}/view/irk/quotation/${widget.kode}'),
                child: Text(
                  "Quotation",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
            Container(
              width: size.width,
              child: RaisedButton(
                onPressed: () => _doChat(existRoom: dataRoom),
                child: Text(
                  "Chat",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
          ],
        ),
      );
    } else if (status == 2) {
      //Proses Pengajuan Pembayaran
      container = Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
                child: Text(
              "Mohon menunggu, Pembayaran Anda sedang ditinjau admin, tidak lebih dari 1x24 jam pada hari kerja (Senin-Jumat)",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 17),
            )),
            Container(
              width: size.width,
              child: RaisedButton(
                onPressed: () => _doChat(existRoom: dataRoom),
                child: Text(
                  "Chat",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
          ],
        ),
      );
    } else if (status == 3) {
      container = Container(
        width: size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
                child: Text(
              "Pembayaran sudah di terima silahkan menghubungi Surveyor Anda untuk menyepakati jadwal pengukuran",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 17),
            )),
            Container(
              width: size.width,
              child: RaisedButton(
                onPressed: () => _doChat(existRoom: dataRoom),
                child: Text(
                  "Chat",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
          ],
        ),
      );
    } else if (status == 4) {
      container = Container(
        width: size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Surveyor Anda akan melakukan pengukuran hari ini, silahkan chat Surveyor terkait",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 17),
            ),
            Container(
              width: size.width,
              child: RaisedButton(
                onPressed: () async {
                  var tracked = await RestAPI.trackGet(widget.id);

                  if (tracked != null) {
                    if (tracked["status"] == 1) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TrackingLocationUser(
                                    lat: widget.lat,
                                    lng: widget.long,
                                    id: widget.id,
                                    namaPemilik: widget.namaPemilik,
                                    alamatTanah: widget.alamatTanah,
                                  )));
                    } else {
                      if (tracked['isEnding']) {
                        Component.showAppNotification(
                            context, "Info", "Tracking sudah Selesai!");
                      } else {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TrackingLocationUser(
                                      lat: widget.lat,
                                      lng: widget.long,
                                      id: widget.id,
                                      namaPemilik: widget.namaPemilik,
                                      alamatTanah: widget.alamatTanah,
                                    )));
                      }
                    }
                  } else {
                    Component.showAppNotification(context, "Info",
                        "Surveyor belum memberikan live location pada Anda");
                  }
                },
                child: Text(
                  "Tracking Surveyor",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
              ),
            ),
            Container(
              width: size.width,
              child: RaisedButton(
                onPressed: () => _doChat(existRoom: dataRoom),
                child: Text(
                  "Chat",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
          ],
        ),
      );
    } else if (status == 5 && diTeruskanPengawas == 0) {
      container = Container(
        width: size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
                child: Text(
              "Surveyor Anda telah melaporkan hasil pekerjaan dan saat ini sedang di review oleh Admin. Hasil pekerjaan akan dikirim ke Anda setelah di setujui oleh Admin",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 16),
            )),
            Container(
              width: size.width,
              child: RaisedButton(
                onPressed: () => _doChat(existRoom: dataRoom),
                child: Text(
                  "Chat",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
          ],
        ),
      );
    } else if (status == 5 && diTeruskanPengawas == 1) {
      container = Container(
        width: size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
                child: Text(
              "File hasil pekerjaan Surveyor Anda sudah disetujui oleh Admin. Silahkan cek kembali file yang Anda terima",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 16),
            )),
            Container(
              width: size.width,
              child: RaisedButton(
                onPressed: () => launch(fileHasilUkur),
                child: Text(
                  "Download File Hasil Pengukuran",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
              ),
            ),
            Container(
              width: size.width,
              child: RaisedButton(
                onPressed: () => launch(fileLampiran),
                child: Text(
                  "Download File Lampiran",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
              ),
            ),
            Container(
              width: size.width,
              child: Builder(
                builder: (context) => RaisedButton(
                  onPressed: () {
                    _onSubmitDone(context);
                  },
                  child: Text(
                    "Pekerjaan Selesai",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.w700),
                  ),
                  color: primaryColor,
                ),
              ),
            ),
            Container(
              width: size.width,
              child: RaisedButton(
                onPressed: () => _doChat(existRoom: dataRoom),
                child: Text(
                  "Chat",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.w700),
                ),
                color: primaryColor,
                padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
              ),
            ),
          ],
        ),
      );
    } else if (status == 6) {
      Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
              child: Text(
            "Kerjaan ini telah di selesaikan",
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w700, fontSize: 17),
          )),
          Container(
            width: size.width,
            child: RaisedButton(
              onPressed: () => launch(fileHasilUkur),
              child: Text(
                "Download File Hasil Pengukuran",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontWeight: FontWeight.w700),
              ),
              color: primaryColor,
            ),
          ),
          Container(
            width: size.width,
            child: RaisedButton(
              onPressed: () => launch(fileLampiran),
              child: Text(
                "Download File Lampiran",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontWeight: FontWeight.w700),
              ),
              color: primaryColor,
            ),
          ),
        ],
      );
    } else {
      container = Container();
    }
    return container;
  }

  _doChat({
    String isState,
    var existRoom,
  }) async {
    int idUser = await Session.getValue(Session.USER_ID);
    String nama = await Session.getValue(Session.USER_NAME);

    if (idUser == null) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => BtnNavigation(
            atIndex: 2,
          ),
        ),
      );
    } else {
      if (existRoom == null) {
        existRoom = await RestAPI.bookRoom(
          idUser,
          irk: widget.id,
        );
        setState(() {
          dataRoom = existRoom;
        });
      }

      if (isState == "apply") {
        await RestAPI.joinRoom(widget.owner, existRoom["kode"]);
        await RestAPI.doChat(existRoom["kode"], idUser,
            "$nama menerima permohan Get-Informasi Rencana Kota Anda. Silahkan periksa Quotation/Penawaran yang tertera dan pembayaran maksimal 3x24 jam atau permohonan Anda ditutup dan Anda dapat melakukan permohonan ulang. \nTerima Kasih",
            forMe: widget.owner);
      } else if (isState == "ukur") {
        await RestAPI.doChat(existRoom["kode"], idUser,
            "$nama akan melakukan pengukuran dilokasi sekarang, menuju lokasi bidang tanah yang Anda mohon. \nTerima Kasih");
      } else if (isState == "done") {
        await RestAPI.doChat(existRoom["kode"], idUser,
            "$nama telah menyatakan selesai dalam melakukan proses Get-Informasi Rencana Kota. \nTerima Kasih");
      }

      Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(
          builder: (context) => ChatScreen(
                room: existRoom,
                chatSistem: true,
                status: widget.status,
              )));
    }
  }

  _getDetailIrk() async {
    await API.getListIdIrk(widget.id).then((response) async {
      var result = await json.decode(response.body);
      namaPemilik = result['data']['nama_pemilik'] == null
          ? ""
          : result['data']['nama_pemilik'];
      nik = result['data']['nik'] == null ? "" : result['data']['nik'];
      luasTanah = result['data']['luas_tanah'].toString() == null
          ? ""
          : result['data']['luas_tanah'].toString();
      alamatTanah = result['data']['alamat_tanah'] == null
          ? ""
          : result['data']['alamat_tanah'];
      npwp = result['data']['npwp'] == null ? "" : result['data']['npwp'];
      hakTanah = result['data']['hak_tanah'] == null
          ? ""
          : result['data']['hak_tanah'];
      status = result['data']['status'] == null ? "" : result['data']['status'];
      diTeruskanPengawas = result['data']['teruskan_pengawas'] == null
          ? ""
          : result['data']['teruskan_pengawas'];
    });
  }

  _getReport() async {
    await API.getListReport(widget.id).then((response) async {
      var result = await json.decode(response.body);
      if (result['status'] == false) {
        fileHasilUkur = "";
        fileLampiran = "";
      } else if (result['status'] == true) {
        fileHasilUkur = result['data']['file_hasil_ukur'] == null
            ? ""
            : result['data']['file_hasil_ukur'];
        fileLampiran = result['data']['file_lampiran'] == null
            ? ""
            : result['data']['file_lampiran'];
      }
    });
  }

  _getRoomChat() async {
    var idUser = await Session.getValue(Session.USER_ID);
    List temp = await RestAPI.seeChatRoom(idUser);

    temp.forEach((element) {
      if (element["irk"] == widget.id) {
        dataRoom = element;
      }
    });
  }

  void _initData() async {
    await _getReport();
    await _getDetailIrk();
    await _getRoomChat();
    _isLoad = false;
    setState(() {});
  }

  Future<dynamic> _onSubmitApply(BuildContext context) async {
    setState(() => _isLoad = true);
    int idSurveyor = await Session.getValue(Session.USER_ID);
    bool isRegistred = await RestAPI.doTakeIrk(widget.kode, idSurveyor);
    if (isRegistred) {
      await Alert(
        context: context,
        image: Image.asset(
          "assets/alert/check.png",
          color: Colors.green,
        ),
        type: AlertType.success,
        title: "Berhasil",
        desc: "Data telah di ambil silahkan chat dengan user terkait",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Chat",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () async {
              await _doChat(isState: "apply", existRoom: dataRoom);
            },
            width: 120,
          )
        ],
      ).show();
    } else {
      setState(() => _isLoad = false);
      Component.showAppNotification(
          context, "Gagal", "Cobalah Beberapa Saat lagi !");
    }
  }

  Future<dynamic> _onSubmitDoing(BuildContext context) async {
    setState(() => _isLoad = true);
    bool isRegistred = await RestAPI.doDoingIrk(
      widget.kode,
      DateFormat('yyyy-MM-dd').format(DateTime.now()),
    );
    if (isRegistred) {
      await Alert(
        context: context,
        image: Image.asset(
          "assets/alert/check.png",
          color: Colors.green,
        ),
        type: AlertType.info,
        title: "Berhasil",
        desc: "Silahkan melakukan Pengukuran, infokan juga kepada user terkait",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Chat",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () async {
              await _doChat(isState: "ukur", existRoom: dataRoom);
            },
            width: 120,
          )
        ],
      ).show();
    } else {
      setState(() => _isLoad = false);
      Component.showAppNotification(
          context, "Gagal", "Cobalah Beberapa Saat lagi !");
    }
  }

  Future<dynamic> _onSubmitDone(BuildContext context) async {
    setState(() => _isLoad = true);
    bool isRegistred = await RestAPI.doDone(
      widget.kode,
    );
    if (isRegistred) {
      await Alert(
        context: context,
        image: Image.asset(
          "assets/alert/check.png",
          color: Colors.green,
        ),
        type: AlertType.success,
        title: "Berhasil",
        desc:
            "Pekerjaan Surveyor Anda sudah selesai, Silakan berikan Ulasan Anda",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Chat",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () async {
              await _doChat(isState: "done", existRoom: dataRoom);
            },
            width: 120,
          )
        ],
      ).show();
    } else {
      setState(() => _isLoad = false);
      Component.showAppNotification(
          context, "Gagal", "Cobalah Beberapa Saat lagi !");
    }
  }
}
