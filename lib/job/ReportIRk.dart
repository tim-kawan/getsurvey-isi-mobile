import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/job/ListIrkSurveyor/TabbarlistIrk.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class ReportIrk extends StatefulWidget {
  final String kode;
  ReportIrk({this.kode, Key key}) : super(key: key);
  @override
  _ReportIrkState createState() => _ReportIrkState();
}

class _ReportIrkState extends State<ReportIrk> {
  TextEditingController _reportCtrl = TextEditingController();
  final _form = GlobalKey<FormState>();
  String _fileNameHasilUkur = 'Pilih Berkas Hasil Ukur PDF';
  String _fileNameLampiran = 'Pilih Berkas Lampiran ZIP';
  bool _isLoading = false;
  String failedFileHasilUKur;
  String failedFileLampiran;
  bool irkDone = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
          color: Colors.white,
        ),
        backgroundColor: primaryColor,
        title: Text(
          "Form Report Informasi Rencana Kota",
          style: TextStyle(
              color: Colors.white, fontSize: 15.0, fontWeight: FontWeight.bold),
        ),
      ),
      body: ModalProgressHUD(
        child: _body(),
        inAsyncCall: _isLoading,
      ),
      bottomNavigationBar: BottomAppBar(
        child: Builder(
            builder: (context) => InkWell(
                  onTap: () async {
                    if (_form.currentState.validate()) {
                      setState(() {
                        _isLoading = true;
                      });
                      if (_fileNameHasilUkur != 'Pilih Berkas Lisensi PDF' &&
                          _fileNameLampiran != 'Pilih Berkas Lisensi ZIP') {
                        await _onSubmit(widget.kode, _reportCtrl.text);
                        setState(() {
                          _isLoading = false;
                        });
                      }
                    } else {
                      Component.showAppNotification(context, 'Gagal',
                          'Lengkapi Semua Data Di bawah ini ');
                    }
                  },
                  child: Container(
                    height: 50.0,
                    decoration: BoxDecoration(
                      color: primaryColor,
                    ),
                    child: Center(
                      child: Text(
                        "Kirim",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: "Sans",
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                )),
      ),
    );
  }

  Widget _body() {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Form(
          key: _form,
          child: Column(
            children: [
              TextFormField(
                controller: _reportCtrl,
                maxLines: 4,
                validator: (val) =>
                    val.length > 10 ? null : 'Laporan Minimal 10 karakter',
                decoration: InputDecoration(
                  labelText: 'Laporan',
                  isDense: true,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: 50.0,
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Unggah File Hasil Ukur",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w700),
                      ),
                      Text(
                        _fileNameHasilUkur,
                        style: TextStyle(color: Colors.black, fontSize: 16.0),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Container(
                        height: 2.0,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.blue,
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                  padding: EdgeInsets.only(
                    top: 8,
                    bottom: 8,
                  ),
                  child: Center(
                    child: RaisedButton(
                      onPressed: _openFileAlatUkur,
                      child: Text(
                        "Unggah File Hasil Ukur",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                            fontWeight: FontWeight.w700),
                      ),
                      color: primaryColor,
                      padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                    ),
                  )),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                color: Colors.white,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Unggah File Lampiran",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w700),
                      ),
                      Text(
                        _fileNameLampiran,
                        style: TextStyle(color: Colors.black, fontSize: 16.0),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Container(
                        height: 2.0,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.blue,
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                  padding: EdgeInsets.only(
                    top: 8,
                    bottom: 8,
                  ),
                  child: Center(
                    child: RaisedButton(
                      onPressed: _openFileLampiran,
                      child: Text(
                        "Unggah File Lampiran",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                            fontWeight: FontWeight.w700),
                      ),
                      color: primaryColor,
                      padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }

  String _pathHasilUkur;
  String _pathLampiran;

  void _openFileAlatUkur() async {
    _fileNameHasilUkur = 'Pilih Berkas Lisensi PDF, JPG/JPEG, PNG';
    failedFileHasilUKur = "";
    try {
      _pathHasilUkur = await FilePicker.getFilePath();
      if (_pathHasilUkur != null) {
        if (_pathHasilUkur.toLowerCase().contains(".pdf") == false &&
            _pathHasilUkur.toLowerCase().contains(".png") == false &&
            _pathHasilUkur.toLowerCase().contains(".jpg") == false &&
            _pathHasilUkur.toLowerCase().contains(".jpeg") == false) {
          failedFileHasilUKur =
              "Gagal!! Pastikan file anda memilkki extension seperti contoh( .pdf, .png, .jpeg, .jpg) dibelakang nama file anda, ubah nama file dan sesuaikan dengan format file anda";
          _pathHasilUkur = null;
          _fileNameHasilUkur = "Gagal Memproses Berkas!";
        } else {
          _fileNameHasilUkur = (_pathHasilUkur.toLowerCase().contains("pdf") ||
                  _pathHasilUkur.toLowerCase().contains("png") ||
                  _pathHasilUkur.toLowerCase().contains("jpg") ||
                  _pathHasilUkur.toLowerCase().contains("jpeg")
              ? _pathHasilUkur.split('/').last
              : "Pilih Berkas Lisensi PDF, JPG/JPEG, PNG");
        }
      } else {
        _fileNameHasilUkur = "Gagal Memproses Berkas!";
        _pathHasilUkur = null;
      }
      setState(() {});
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  void _openFileLampiran() async {
    _fileNameLampiran = 'Pilih Berkas Lisensi PDF, JPG/JPEG, PNG';
    failedFileLampiran = "";
    try {
      _pathLampiran = await FilePicker.getFilePath();
      if (_pathLampiran != null) {
        if (_pathLampiran.toLowerCase().contains(".pdf") == false &&
            _pathLampiran.toLowerCase().contains(".png") == false &&
            _pathLampiran.toLowerCase().contains(".jpg") == false &&
            _pathLampiran.toLowerCase().contains(".jpeg") == false &&
            _pathLampiran.toLowerCase().contains(".zip") == false) {
          failedFileLampiran =
              "Gagal!! Pastikan file anda memilkki extension seperti contoh( .pdf, .png, .jpeg, .jpg) dibelakang nama file anda, ubah nama file dan sesuaikan dengan format file anda";
          _pathLampiran = null;
          _fileNameLampiran = "Gagal Memproses Berkas!";
        } else {
          _fileNameLampiran = (_pathLampiran.toLowerCase().contains("pdf") ||
                  _pathLampiran.toLowerCase().contains("png") ||
                  _pathLampiran.toLowerCase().contains("jpg") ||
                  _pathLampiran.toLowerCase().contains("jpeg") ||
                  _pathLampiran.toLowerCase().contains(".zip")
              ? _pathLampiran.split('/').last
              : "Pilih Berkas Lisensi PDF, JPG/JPEG, PNG");
        }
      } else {
        _fileNameLampiran = "Gagal Memproses Berkas!";
        _pathHasilUkur = null;
      }
      setState(() {});
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  Future<dynamic> _onSubmit(String kode, String laporan) async {
    bool isSubmit = false;
    if (!irkDone) {
      irkDone = await RestAPI.doReport(kode, laporan);
    }

    if (irkDone && _pathHasilUkur != null && _pathLampiran != null) {
      bool hasiUkur = false;
      bool lampiran = false;
      setState(() {});
      if (_pathHasilUkur.toLowerCase().contains("pdf") ||
          _pathHasilUkur.toLowerCase().contains("png") ||
          _pathHasilUkur.toLowerCase().contains("jpg") ||
          _pathHasilUkur.toLowerCase().contains("jpeg")) {
        String tipe = "jpg";

        if (_pathHasilUkur.toLowerCase().contains("pdf")) {
          tipe = "pdf";
        } else if (_pathHasilUkur.toLowerCase().contains("png")) {
          tipe = "png";
        }
        hasiUkur = await RestAPI.doUploadHasilUkur(kode, _pathHasilUkur, tipe);
        if (_pathLampiran.toLowerCase().contains("zip") ||
            _pathLampiran.toLowerCase().contains("pdf") ||
            _pathLampiran.toLowerCase().contains("png") ||
            _pathLampiran.toLowerCase().contains("jpg") ||
            _pathLampiran.toLowerCase().contains("jpeg") ||
            _pathLampiran.toLowerCase().contains("zip")) {
          String tipe = "zip";

          if (_pathLampiran.toLowerCase().contains("pdf")) {
            tipe = "pdf";
          } else if (_pathLampiran.toLowerCase().contains("png")) {
            tipe = "png";
          }
          lampiran = await RestAPI.doUploadLampiran(kode, _pathLampiran, tipe);
        }
        isSubmit = hasiUkur && lampiran;
      }
    }
    if (isSubmit) {
      Alert(
        context: context,
        image: Image.asset(
          "assets/alert/check.png",
          color: Colors.red,
        ),
        type: AlertType.success,
        title: "Berhasil",
        desc:
            "Data Report Informasi Rencana Kota berhasil di kirim, silahkan menunggu informasi selanjutnya",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Kembali",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () => Navigator.of(context, rootNavigator: true).push(
                MaterialPageRoute(builder: (context) => TabbarIRKScreen())),
            width: 120,
          )
        ],
      ).show();
    } else {
      Alert(
        context: context,
        image: Image.asset(
          "assets/alert/error.png",
          color: Colors.red,
        ),
        type: AlertType.error,
        title: "Gagal",
        desc:
            "Data Report Informasi Rencana Kota gagal dikirim, cobalah beberapa saat lagi",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Kembali",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () => Navigator.of(context, rootNavigator: true).push(
                MaterialPageRoute(builder: (context) => TabbarIRKScreen())),
            width: 120,
          )
        ],
      ).show();
    }
  }
}
