import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/job/ListIrkSurveyor/ListInformasiRencanaKotaSur.dart';
import 'package:get_survey_app/job/ListIrkSurveyor/ListInformasiRencanaKotaSurDone.dart';
import 'package:get_survey_app/job/ListIrkSurveyor/SearchIrkSurveyor.dart';
import 'ListInformasiRencanaKotaFree.dart';

class TabbarIRKScreen extends StatefulWidget {
  @override
  _TabbarIRKScreenState createState() => _TabbarIRKScreenState();
}

class _TabbarIRKScreenState extends State<TabbarIRKScreen>
    with SingleTickerProviderStateMixin {
  TabController _controller;

  List<Widget> list = [
    Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Text(
        "Baru",
        style: TextStyle(fontSize: 16),
        textAlign: TextAlign.center,
      ),
    ),
    Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Text(
        "Proses",
        style: TextStyle(fontSize: 16),
        textAlign: TextAlign.center,
      ),
    ),
    Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Text(
        "Selesai",
        style: TextStyle(fontSize: 16),
        textAlign: TextAlign.center,
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => BtnNavigation(
                    atIndex: 0,
                  )),
          (Route<dynamic> route) => false),
      child: MaterialApp(
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            centerTitle: true,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                      builder: (context) => BtnNavigation(
                            atIndex: 0,
                          )),
                  (Route<dynamic> route) => false),
              color: Colors.white,
            ),
            backgroundColor: primaryColor,
            title: Text(
              "Rencana kota",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            ),
            actions: [
              IconButton(
                  icon: Icon(
                    Icons.search,
                    color: Colors.white,
                    size: 20,
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SearchIrkSurveyor()));
                  })
            ],
          ),
          body: DefaultTabController(
            length: 3,
            child: Column(
              children: <Widget>[
                Container(
                  constraints: BoxConstraints(maxHeight: 150.0),
                  decoration: BoxDecoration(
                      border: Border(
                    bottom: BorderSide(width: 2.0, color: Colors.grey[100]),
                  )),
                  child: Material(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: TabBar(
                        tabs: list,
                        indicatorColor: Color(0xff2EAF51),
                        unselectedLabelColor: Colors.grey,
                        labelColor: Color(0xff2EAF51),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: TabBarView(
                    children: [
                      ListIzinRencanaKOtaFree(),
                      ListIzinRencanaKotaSurveyor(),
                      ListIzinRencanaKotaSurveyorDone(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
