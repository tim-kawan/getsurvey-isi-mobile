import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get_survey_app/home/BottomNavigationBar.dart';
import 'package:get_survey_app/job/DetailIrk.dart';
import 'package:get_survey_app/job/ListIrkUser/IzinRencanaKotaScreen.dart';
import 'package:get_survey_app/model/AddIrk.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:get_survey_app/service/ServiceGetData.dart';
import 'package:get_survey_app/service/Session.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class ListIzinRencanaKotaSurveyor extends StatefulWidget {
  @override
  _ListIzinRencanaKotaSurveyorState createState() =>
      _ListIzinRencanaKotaSurveyorState();
}

class _ListIzinRencanaKotaSurveyorState
    extends State<ListIzinRencanaKotaSurveyor> {
  String kode;
  String status;
  int statusSurveyor = -1;
  bool _isLoading = true;
  List<Irk> listIRK = [];
  int cekStatusKeanggotaan = -1;
  int idUser;

  _goBack() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => BtnNavigation(atIndex: 0),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _goBack(),
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          body: ModalProgressHUD(inAsyncCall: _isLoading, child: _body()),
          floatingActionButton: status == "User"
              ? FloatingActionButton(
                  onPressed: () async {
                    await _getCodeIrk();
                    await Session.doSave("kodeIrk", kode);
                    Navigator.of(context)
                        .push(PageRouteBuilder(
                            opaque: false,
                            pageBuilder: (BuildContext context, _, __) =>
                                IzinRencanaKotaScreen()))
                        .then((_) {
                      _getListIrk();
                    });
                  },
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                )
              : Container(),
        ),
      ),
    );
  }

  getStatusUser() async {
    // await _cacheManager.emptyCache();

    statusSurveyor = await Session.getValue(Session.USER_LANJUT);

    if (cekStatusKeanggotaan > 0) {
      if (cekStatusKeanggotaan != statusSurveyor) {
        await Session.doSave(Session.USER_LANJUT, cekStatusKeanggotaan);
        statusSurveyor = cekStatusKeanggotaan;
      }
    }

    int type = await Session.getValue(Session.USER_STATUS);

    if (type > 0) {
      if (statusSurveyor == 1 || statusSurveyor == 2) {
        status = "Surveyor";
      } else {
        status = "User";
      }
    } else {
      status = "User";
    }

    setState(() {});
  }

  void initData() async {
    await getStatusUser();
    await _getListIrk();
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    initData();
    super.initState();
  }

  Widget _body() {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: listIRK.isNotEmpty
          ? ListView.builder(
              itemCount: listIRK.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ]),
                    child: ListTile(
                      title: Row(
                        children: [
                          Container(
                              width: 200,
                              child: Text("Nama Pemilik : " +
                                  listIRK[index].namaPemilik)),
                          Text(
                            "#" + listIRK[index].id.toString(),
                            style: TextStyle(color: Colors.blueAccent),
                          ),
                        ],
                      ),
                      subtitle: Text("Luas Tanah : " +
                          listIRK[index].luasTanah.toString() +
                          "/m2"),
                      trailing: Icon(
                        Icons.arrow_forward_ios,
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DetailIrk(
                                      status: "Surveyor",
                                      statusIrk: listIRK[index].status,
                                      id: listIRK[index].id,
                                      owner: listIRK[index].userID,
                                      namaPemilik: listIRK[index].namaPemilik,
                                      alamatTanah: listIRK[index].alamatTanah,
                                      tipe: "jasa",
                                      kode: listIRK[index].kode,
                                      file: listIRK[index].fileSer,
                                      lat: listIRK[index].lat,
                                      long: listIRK[index].long,
                                    )));
                      },
                    ),
                  ),
                );
              })
          : Center(
              child: Text(
                "Tidak Ada Data Irk",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w800),
              ),
            ),
    );
  }

  _getCodeIrk() async {
    await API.getNewCode().then((response) async {
      var result = await json.decode(response.body);
      kode = result['data']['kode'];
    });
  }

  _getListIrk() async {
    listIRK.clear();
    int idUser = await Session.getValue(Session.USER_ID);
    await RestAPI.getAllIrk().then((results) async {
      results.forEach((irk) {
        Irk items = Irk.fromJson(irk);
        if (items.idSurveyor == idUser && items.status < 5) {
          listIRK.add(items);
        }
      });
      setState(() {});
    });
  }
}
