import 'package:flutter/material.dart';
import 'package:get_survey_app/component/const.dart';
import 'package:get_survey_app/job/DetailIrk.dart';
import 'package:get_survey_app/job/ListIrkUser/ListInformasiRencanaKota.dart';
import 'package:get_survey_app/service/RestAPI.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class EditIrk extends StatefulWidget {
  final String luasTanah;
  final String kode;
  final int id;
  EditIrk({this.luasTanah, this.kode, this.id, Key key}) : super(key: key);
  @override
  _EditIrkState createState() => _EditIrkState();
}

class _EditIrkState extends State<EditIrk> {
  TextEditingController _luasTanahCtrl = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _luasTanahCtrl.text = _luasTanahCtrl.text == null ? "" : widget.luasTanah;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
            color: Colors.white,
          ),
          backgroundColor: primaryColor,
          title: Text(
            "Detail Informasi Rencana Kota",
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ),
        body: ModalProgressHUD(
          child: _body(),
          inAsyncCall: _isLoading,
        ),
      ),
    );
  }

  Widget _body() {
    return Column(
      children: [
        Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: _luasTanahCtrl,
              textCapitalization: TextCapitalization.sentences,
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                  labelText: 'Luas Tanah/m2', suffixText: "/m2"),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Isi Form Terkait';
                }
                return null;
              },
            ),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          child: Builder(
            builder: (context) => RaisedButton(
              onPressed: () async {
                setState(() {
                  _isLoading = true;
                });
                await _onSubmit(
                    int.parse(_luasTanahCtrl.text), widget.kode, context);
                setState(() {
                  _isLoading = false;
                });
              },
              child: Text(
                "Edit Data",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontWeight: FontWeight.w700),
              ),
              color: primaryColor,
            ),
          ),
        )
      ],
    );
  }

  Future<dynamic> _onSubmit(
      int luasTanah, String kode, BuildContext context) async {
    bool edit = await RestAPI.editIzinKerja(kode, luasTanah);
    if (edit) {
      Alert(
        context: context,
        image: Image.asset(
          "assets/alert/check.png",
          color: Colors.red,
        ),
        type: AlertType.success,
        title: "Berhasil",
        desc: "Edit Data Berhasil!",
        closeIcon: Container(),
        onWillPopActive: true,
        buttons: [
          DialogButton(
            child: Text(
              "Kembali",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DetailIrk(
                            id: widget.id,
                          )));
            },
            width: 120,
          )
        ],
      ).show();
    } else {
      Component.showAppNotification(
          context, "Eror", 'Cek Kembali Data yang Anda Masukkan');
    }
  }
}
