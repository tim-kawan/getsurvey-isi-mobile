// To parse this JSON data, do
//
//     final jasa = jasaFromMap(jsonString);

import 'dart:convert';


class Jasa {
  Jasa({
    this.record,
    this.rate,
  });


  Record record;
  dynamic rate;

  factory Jasa.fromJson(String str) => Jasa.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());


  factory Jasa.fromMap(Map<String, dynamic> json) => Jasa(
    record: json["record"] == null ? null : Record.fromMap(json["record"]),
    rate: json["rate"] == null ? null : json["rate"],
  );

  Map<String, dynamic> toMap() => {
    "record": record == null ? null : record.toMap(),
    "rate": rate == null ? null : rate,
  };
}

class Record {
  Record({
    this.id,
    this.nama,
    this.deskripsi,
    this.terpakai,
    this.userSurveyorId,
    this.createdAt,
    this.updatedAt,
    this.productPhotos,
    this.userSurveyor,
  });

  int id;
  String nama;
  String deskripsi;
  bool terpakai;
  int userSurveyorId;
  DateTime createdAt;
  DateTime updatedAt;
  List<ProductPhoto> productPhotos;
  UserSurveyor userSurveyor;

  factory Record.fromJson(String str) => Record.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Record.fromMap(Map<String, dynamic> json) => Record(
    id: json["id"] == null ? null : json["id"],
    nama: json["nama"] == null ? null : json["nama"],
    deskripsi: json["deskripsi"] == null ? null : json["deskripsi"],
    terpakai: json["terpakai"] == null ? null : json["terpakai"],
    userSurveyorId: json["user_surveyor_id"] == null ? null : json["user_surveyor_id"],
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
    updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    productPhotos: json["product_photos"] == null ? null : List<ProductPhoto>.from(json["product_photos"].map((x) => ProductPhoto.fromMap(x))),
    userSurveyor: json["user_surveyor"] == null ? null : UserSurveyor.fromMap(json["user_surveyor"]),
  );

  Map<String, dynamic> toMap() => {
    "id": id == null ? null : id,
    "nama": nama == null ? null : nama,
    "deskripsi": deskripsi == null ? null : deskripsi,
    "terpakai": terpakai == null ? null : terpakai,
    "user_surveyor_id": userSurveyorId == null ? null : userSurveyorId,
    "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
    "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
    "product_photos": productPhotos == null ? null : List<dynamic>.from(productPhotos.map((x) => x.toMap())),
    "user_surveyor": userSurveyor == null ? null : userSurveyor.toMap(),
  };
}

class ProductPhoto {
  ProductPhoto({
    this.id,
    this.foto,
    this.productGoodsId,
    this.productServicesId,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String foto;
  dynamic productGoodsId;
  int productServicesId;
  DateTime createdAt;
  DateTime updatedAt;

  factory ProductPhoto.fromJson(String str) => ProductPhoto.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ProductPhoto.fromMap(Map<String, dynamic> json) => ProductPhoto(
    id: json["id"] == null ? null : json["id"],
    foto: json["foto"] == null ? null : json["foto"],
    productGoodsId: json["product_goods_id"],
    productServicesId: json["product_services_id"] == null ? null : json["product_services_id"],
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
    updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
  );

  Map<String, dynamic> toMap() => {
    "id": id == null ? null : id,
    "foto": foto == null ? null : foto,
    "product_goods_id": productGoodsId,
    "product_services_id": productServicesId == null ? null : productServicesId,
    "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
    "updatedAt": updatedAt == null ? null : updatedAt.toIso8601String(),
  };
}

class UserSurveyor {
  UserSurveyor({
    this.id,
    this.jenjangPendidikan,
    this.jurusanPendidikan,
    this.perguruanTinggi,
    this.tanggalAwalBerlaku,
    this.masaBerlaku,
    this.tanggalAkhirBerlaku,
    this.subBidangSurvey,
    this.sebagai,
    this.bonusNilai,
    this.statusKeanggotaan,
    this.noAnggota,
    this.noLisensi,
    this.fileLisensi,
    this.noSertifikat,
    this.noSkaSkt,
    this.noLisensiKadastral,
    this.noLisensiIrk,
    this.noSka,
    this.noSkb,
    this.noImb,
    this.noSkt,
    this.noAskb,
    this.user,
  });

  int id;
  String jenjangPendidikan;
  String jurusanPendidikan;
  String perguruanTinggi;
  DateTime tanggalAwalBerlaku;
  int masaBerlaku;
  DateTime tanggalAkhirBerlaku;
  dynamic subBidangSurvey;
  String sebagai;
  int bonusNilai;
  int statusKeanggotaan;
  String noAnggota;
  String noLisensi;
  String fileLisensi;
  dynamic noSertifikat;
  String noSkaSkt;
  String noLisensiKadastral;
  dynamic noLisensiIrk;
  dynamic noSka;
  dynamic noSkb;
  dynamic noImb;
  dynamic noSkt;
  dynamic noAskb;
  User user;

  factory UserSurveyor.fromJson(String str) => UserSurveyor.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory UserSurveyor.fromMap(Map<String, dynamic> json) => UserSurveyor(
    id: json["id"] == null ? null : json["id"],
    jenjangPendidikan: json["jenjang_pendidikan"] == null ? null : json["jenjang_pendidikan"],
    jurusanPendidikan: json["jurusan_pendidikan"] == null ? null : json["jurusan_pendidikan"],
    perguruanTinggi: json["perguruan_tinggi"] == null ? null : json["perguruan_tinggi"],
    tanggalAwalBerlaku: json["tanggal_awal_berlaku"] == null ? null : DateTime.parse(json["tanggal_awal_berlaku"]),
    masaBerlaku: json["masa_berlaku"] == null ? null : json["masa_berlaku"],
    tanggalAkhirBerlaku: json["tanggal_akhir_berlaku"] == null ? null : DateTime.parse(json["tanggal_akhir_berlaku"]),
    subBidangSurvey: json["sub_bidang_survey"],
    sebagai: json["sebagai"] == null ? null : json["sebagai"],
    bonusNilai: json["bonus_nilai"] == null ? null : json["bonus_nilai"],
    statusKeanggotaan: json["status_keanggotaan"] == null ? null : json["status_keanggotaan"],
    noAnggota: json["no_anggota"] == null ? null : json["no_anggota"],
    noLisensi: json["no_lisensi"] == null ? null : json["no_lisensi"],
    fileLisensi: json["file_lisensi"] == null ? null : json["file_lisensi"],
    noSertifikat: json["no_sertifikat"],
    noSkaSkt: json["no_ska_skt"] == null ? null : json["no_ska_skt"],
    noLisensiKadastral: json["no_lisensi_kadastral"] == null ? null : json["no_lisensi_kadastral"],
    noLisensiIrk: json["no_lisensi_irk"],
    noSka: json["no_ska"],
    noSkb: json["no_skb"],
    noImb: json["no_imb"],
    noSkt: json["no_skt"],
    noAskb: json["no_askb"],
    user: json["user"] == null ? null : User.fromMap(json["user"]),
  );

  Map<String, dynamic> toMap() => {
    "id": id == null ? null : id,
    "jenjang_pendidikan": jenjangPendidikan == null ? null : jenjangPendidikan,
    "jurusan_pendidikan": jurusanPendidikan == null ? null : jurusanPendidikan,
    "perguruan_tinggi": perguruanTinggi == null ? null : perguruanTinggi,
    "tanggal_awal_berlaku": tanggalAwalBerlaku == null ? null : tanggalAwalBerlaku.toIso8601String(),
    "masa_berlaku": masaBerlaku == null ? null : masaBerlaku,
    "tanggal_akhir_berlaku": tanggalAkhirBerlaku == null ? null : tanggalAkhirBerlaku.toIso8601String(),
    "sub_bidang_survey": subBidangSurvey,
    "sebagai": sebagai == null ? null : sebagai,
    "bonus_nilai": bonusNilai == null ? null : bonusNilai,
    "status_keanggotaan": statusKeanggotaan == null ? null : statusKeanggotaan,
    "no_anggota": noAnggota == null ? null : noAnggota,
    "no_lisensi": noLisensi == null ? null : noLisensi,
    "file_lisensi": fileLisensi == null ? null : fileLisensi,
    "no_sertifikat": noSertifikat,
    "no_ska_skt": noSkaSkt == null ? null : noSkaSkt,
    "no_lisensi_kadastral": noLisensiKadastral == null ? null : noLisensiKadastral,
    "no_lisensi_irk": noLisensiIrk,
    "no_ska": noSka,
    "no_skb": noSkb,
    "no_imb": noImb,
    "no_skt": noSkt,
    "no_askb": noAskb,
    "user": user == null ? null : user.toMap(),
  };
}

class User {
  User({
    this.id,
    this.nama,
    this.email,
    this.emailConfirm,
    this.emailConfirmCode,
    this.password,
    this.passwordForgotCode,
    this.statusUser,
    this.userTokenId,
    this.userProfileId,
    this.userSurveyorId,
    this.created,
    this.userProfile,
  });

  int id;
  String nama;
  String email;
  int emailConfirm;
  dynamic emailConfirmCode;
  String password;
  dynamic passwordForgotCode;
  int statusUser;
  int userTokenId;
  int userProfileId;
  int userSurveyorId;
  DateTime created;
  UserProfile userProfile;

  factory User.fromJson(String str) => User.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory User.fromMap(Map<String, dynamic> json) => User(
    id: json["id"] == null ? null : json["id"],
    nama: json["nama"] == null ? null : json["nama"],
    email: json["email"] == null ? null : json["email"],
    emailConfirm: json["email_confirm"] == null ? null : json["email_confirm"],
    emailConfirmCode: json["email_confirm_code"],
    password: json["password"] == null ? null : json["password"],
    passwordForgotCode: json["password_forgot_code"],
    statusUser: json["status_user"] == null ? null : json["status_user"],
    userTokenId: json["user_token_id"] == null ? null : json["user_token_id"],
    userProfileId: json["user_profile_id"] == null ? null : json["user_profile_id"],
    userSurveyorId: json["user_surveyor_id"] == null ? null : json["user_surveyor_id"],
    created: json["created"] == null ? null : DateTime.parse(json["created"]),
    userProfile: json["user_profile"] == null ? null : UserProfile.fromMap(json["user_profile"]),
  );

  Map<String, dynamic> toMap() => {
    "id": id == null ? null : id,
    "nama": nama == null ? null : nama,
    "email": email == null ? null : email,
    "email_confirm": emailConfirm == null ? null : emailConfirm,
    "email_confirm_code": emailConfirmCode,
    "password": password == null ? null : password,
    "password_forgot_code": passwordForgotCode,
    "status_user": statusUser == null ? null : statusUser,
    "user_token_id": userTokenId == null ? null : userTokenId,
    "user_profile_id": userProfileId == null ? null : userProfileId,
    "user_surveyor_id": userSurveyorId == null ? null : userSurveyorId,
    "created": created == null ? null : created.toIso8601String(),
    "user_profile": userProfile == null ? null : userProfile.toMap(),
  };
}

class UserProfile {
  UserProfile({
    this.id,
    this.nik,
    this.jenisKelamin,
    this.noHp,
    this.tempatLahir,
    this.tanggalLahir,
    this.kotaTempatTinggal,
    this.alamatTempatTinggal,
    this.lat,
    this.long,
    this.fotoUser,
  });

  int id;
  dynamic nik;
  int jenisKelamin;
  String noHp;
  String tempatLahir;
  DateTime tanggalLahir;
  String kotaTempatTinggal;
  String alamatTempatTinggal;
  double lat;
  double long;
  String fotoUser;

  factory UserProfile.fromJson(String str) => UserProfile.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory UserProfile.fromMap(Map<String, dynamic> json) => UserProfile(
    id: json["id"] == null ? null : json["id"],
    nik: json["nik"],
    jenisKelamin: json["jenis_kelamin"] == null ? null : json["jenis_kelamin"],
    noHp: json["no_hp"] == null ? null : json["no_hp"],
    tempatLahir: json["tempat_lahir"] == null ? null : json["tempat_lahir"],
    tanggalLahir: json["tanggal_lahir"] == null ? null : DateTime.parse(json["tanggal_lahir"]),
    kotaTempatTinggal: json["kota_tempat_tinggal"] == null ? null : json["kota_tempat_tinggal"],
    alamatTempatTinggal: json["alamat_tempat_tinggal"] == null ? null : json["alamat_tempat_tinggal"],
    lat: json["lat"] == null ? null : json["lat"].toDouble(),
    long: json["long"] == null ? null : json["long"].toDouble(),
    fotoUser: json["foto_user"] == null ? null : json["foto_user"],
  );

  Map<String, dynamic> toMap() => {
    "id": id == null ? null : id,
    "nik": nik,
    "jenis_kelamin": jenisKelamin == null ? null : jenisKelamin,
    "no_hp": noHp == null ? null : noHp,
    "tempat_lahir": tempatLahir == null ? null : tempatLahir,
    "tanggal_lahir": tanggalLahir == null ? null : tanggalLahir.toIso8601String(),
    "kota_tempat_tinggal": kotaTempatTinggal == null ? null : kotaTempatTinggal,
    "alamat_tempat_tinggal": alamatTempatTinggal == null ? null : alamatTempatTinggal,
    "lat": lat == null ? null : lat,
    "long": long == null ? null : long,
    "foto_user": fotoUser == null ? null : fotoUser,
  };
}
