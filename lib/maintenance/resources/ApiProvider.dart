import 'dart:io';

import 'package:dio/dio.dart';
import 'package:get_survey_app/service/RestAPI.dart';

class RestAPII {
  static String HOST = RestAPI.API_URL;
  static Response _response;
  static Dio _dio = new Dio(new BaseOptions(
    headers: {
      "Content-Type": "application/json;charset=UTF-8",
      "Accept": "application/json"
    },
    baseUrl: HOST,
    connectTimeout: 30000,
    receiveTimeout: 30000,
  ));

  RestAPII._();

  static Future<dynamic> getProdukWithId({int id}) async {
    var userInfo;

    try {
      _response = await _dio.get("/products/$id");

      if (_response.statusCode == HttpStatus.ok) {
        userInfo = _response.data['product'];
      }
    } on DioError catch (e) {
      print("Response ${e.message}");
    }

    return userInfo;
  }

  static Future<dynamic> getServiceWithId({int id}) async {
    var userInfo;

    try {
      _response = await _dio.get("/services/$id");

      if (_response.statusCode == HttpStatus.ok) {
        userInfo = _response.data['service'];
      }
    } on DioError catch (e) {
      print("Response ${e.message}");
    }

    return userInfo;
  }
}
